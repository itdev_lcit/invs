
$(document).ready(function(){

    $('#normal-draft').DataTable({
            //responsive: true
    });



$("table").off("click", ".change-dr-normal");
$("table").on("click", ".change-dr-normal", function(e) {
            e.preventDefault();
            
            $('#remarkdr').modal('show');

            var $row = $(this).parents('tr.r-normal');
            var id = $row.data('id')

            $('#id_dr').val(id);
            
});


$('.save-remark').click(function(){
            var remark_inv = $('#comment').val();
            var id_dr = $('#id_dr').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel Draft*');
                console.log('null');
            } else {
                console.log('insert remark');

                $.ajax({
                    url:'<?php echo site_url(); ?>Createdraft/CancelDrNormal',
                        method:'POST',
                        data:{ id_dr:id_dr, remark_inv:remark_inv }
                }).done(function(data){
                   var o = JSON.parse(data);
                   alert(o.msg);
                   location.reload();
                })
            }
          
});

    });
