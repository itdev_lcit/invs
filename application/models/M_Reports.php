<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Reports extends CI_Model {
	function __construct() {
		parent::__construct();
	}

public function CountPrintDR($search){

			
			$this->db->group_by('print_no, username');
			$this->db->order_by("print_no", "desc");

			if(!empty($search)){
				$this->db->where('print_no',$search);
			}

			$this->db->select('username , print_no, updated, created , count(print_no) as num_print');
			$inv = $this->db->get('log_print_dr');

			return $inv->num_rows();	

}

 public function fetchPrintDR($limit, $start , $search , $sort , $typeD) {

 			$this->db->limit($limit, $start);
			$this->db->order_by("print_no", "desc");
			$this->db->group_by('print_no, username');
			
			$this->db->select('username , print_no, updated, created , count(print_no) as num_print');

				if(!empty($search)){
					$this->db->where('print_no',$search);
				}

			$query_normal = $this->db->get('log_print_dr');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


public function CountPrintINV($search){

			
			$this->db->order_by("invoice_no", "desc");

			if(!empty($search)){
				$this->db->where('invoice_no',$search);
			}

			$inv = $this->db->get('log_print_invoice');

			return $inv->num_rows();	

}

public function fetchPrintINV($limit, $start , $search , $sort , $typeD) {

 			$this->db->limit($limit, $start);

 			$this->db->order_by("invoice_no", "desc");

				if(!empty($search)){
					$this->db->where('invoice_no',$search);
				}

			$query_normal = $this->db->get('log_print_invoice');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

public function CountPrintReINV($search){

			
			$this->db->order_by("invoice_no", "desc");
			$this->db->group_by('invoice_no, username');

			if(!empty($search)){
				$this->db->where('invoice_no',$search);
			}

			$this->db->select('username , updated , invoice_no , count(invoice_no) as num_print');
			$inv = $this->db->get('log_re_print_invoice');

			return $inv->num_rows();	

}

public function fetchPrintReINV($limit, $start , $search , $sort , $typeD) {

 			$this->db->limit($limit, $start);
 			$this->db->group_by('invoice_no, username');
 			$this->db->order_by("invoice_no", "desc");

				if(!empty($search)){
					$this->db->where('invoice_no',$search);
				}

			$this->db->select('username , updated , invoice_no , count(invoice_no) as num_print');
			$query_normal = $this->db->get('log_re_print_invoice');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

  public function CountPrintCanINV($search){

			
			$this->db->order_by("invoice_no", "desc");

			if(!empty($search)){
				$this->db->where('invoice_no',$search);
			}

			$inv = $this->db->get('log_cancel_invoice');

			return $inv->num_rows();	

}

public function fetchPrintCanINV($limit, $start , $search , $sort , $typeD) {

 			$this->db->limit($limit, $start);
 			$this->db->order_by("invoice_no", "desc");

				if(!empty($search)){
					$this->db->where('invoice_no',$search);
				}

			$query_normal = $this->db->get('log_cancel_invoice');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

}
?>

