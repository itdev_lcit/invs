<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Inv extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountDraft($search){

			$this->db->join('customer','customer.custom_id = billing_order.id_comp');
			$this->db->where('billing_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->where('billing_order.dr_no',$search);
			$this->db->group_by('billing_order.dr_no');

			$inv = $this->db->get('billing_order');


			return $inv->num_rows();

	}

	public function CountInv($limit, $start , $search , $sort){

			$this->db->limit($limit, $start);
			$this->db->join('customer','customer.custom_id = billing_order.id_comp');
			$this->db->where('billing_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('billing_order.dr_no');
			$this->db->select('billing_order.dr_no, billing_order.payment  , billing_order.prefix_invoice , billing_order.invoice_no , billing_order.type , billing_order.ref_id , billing_order.is_vat   , billing_order.is_use , billing_order.remark_cancel , billing_order.created , billing_order.updated , customer.customer_code');

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->order_by('billing_order.dr_no', "desc");
				} else if($sort == 'code_comp'){
					$this->db->order_by('customer.customer_code', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('billing_order.created', "desc");
				}

			} else {
				$this->db->order_by('billing_order.dr_no', "desc");
			}


				if(!empty($search)){
					$this->db->where('billing_order.dr_no',$search);
				}

			$query_normal = $this->db->get('billing_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

	public function CountNoted($search){

			$this->db->join('customer','customer.custom_id = billing_noted.id_comp');
			$this->db->where('billing_noted.noted_no is NOT NULL', NULL, FALSE);
			$this->db->where('billing_noted.noted_no',$search);
			$this->db->group_by('billing_noted.noted_no');

			$inv = $this->db->get('billing_noted');


			return $inv->num_rows();

	}

	public function FetchNoted($limit, $start , $search , $sort){

			$this->db->limit($limit, $start);
			$this->db->join('customer','customer.custom_id = billing_noted.id_comp');
			$this->db->where('billing_noted.noted_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('billing_noted.noted_no');
			$this->db->select('billing_noted.noted_no, billing_noted.type_noted,billing_noted.payment  , billing_noted.prefix_invoice , billing_noted.invoice_no , billing_noted.type , billing_noted.ref_id , billing_noted.is_vat   , billing_noted.is_use , billing_noted.remark_cancel , billing_noted.created , billing_noted.updated , customer.customer_code');

			if(!empty($sort)){
				
				if($sort == 'noted_no'){
					$this->db->order_by('billing_noted.noted_no', "desc");
				} else if($sort == 'code_comp'){
					$this->db->order_by('customer.customer_code', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('billing_noted.created', "desc");
				}

			} else {
				$this->db->order_by('billing_noted.noted_no', "desc");
			}


				if(!empty($search)){
					$this->db->where('billing_noted.noted_no',$search);
				}

			$query_normal = $this->db->get('billing_noted');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

}
?>

