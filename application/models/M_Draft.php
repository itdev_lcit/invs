<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Draft extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountDraft($search){

			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);

			if(!empty($search)){
				$this->db->where('normal_order.dr_no',$search);
				$this->db->or_where('book_order.book_an',$search);
			}
			
			$this->db->group_by('book_order.id');
			$this->db->order_by("normal_order.dr_no", "desc");

			$this->db->select('normal_order.dr_no,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$inv = $this->db->get('status_dr');

			return $inv->num_rows();
			

	}

	 public function fetch_draft($limit, $start , $search , $sort , $typeD) {

			$this->db->limit($limit, $start);
			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('normal_order.dr_no');

			if(!empty($typeD)){

				if($typeD == 'dr'){
					$this->db->where('normal_order.is_use', '0');
				} else if($typeD == 'inv'){
					$this->db->where('normal_order.is_use', '2');
				} 

			} 

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->order_by('normal_order.dr_no', "desc");
				} else if($sort == 'code_comp'){
					$this->db->order_by('normal_order.code_comp', "desc");
				} else if($sort == 'book_an'){
					$this->db->order_by('book_order.book_an', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('book_order.created', "desc");
				}

			} else {
				$this->db->order_by("normal_order.dr_no", "desc");
			}

			
			$this->db->select('normal_order.dr_no,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');

				if(!empty($search)){
					$this->db->where('normal_order.dr_no',$search);
					$this->db->or_where('book_order.book_an',$search);
				}

			$query_normal = $this->db->get('status_dr');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
?>

