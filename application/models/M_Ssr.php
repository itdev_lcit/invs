<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Ssr extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountDraft($search , $sort){

			$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
			$this->db->join('tariff_type','tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.dr_no is NOT NULL', NULL, FALSE);

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->like('ssr_order.dr_no', $search);
				} else {
					$this->db->like('customer.customer_code', $search);
				}

			}

			//$this->db->where('ssr_order.type !=','CANCEL');
			$this->db->group_by('ssr_order.dr_no');

			$inv = $this->db->get('ssr_order');


			return $inv->num_rows();

	}

	public function CountSsr($limit, $start , $search , $sort){

			$pieces = explode(", ", $search);
			$test = preg_replace('/\s+/', '', $pieces);

			$this->db->limit($limit, $start);
			$this->db->order_by("ssr_order.dr_no", "desc");
			$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
			$this->db->join('tariff_type','tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('ssr_order.dr_no');
			$this->db->select('ssr_order.dr_no , ssr_order.prefix_invoice, ssr_order.payment , ssr_order.invoice_no , ssr_order.type , ssr_order.ref_id , ssr_order.vessel , ssr_order.vsl_visit , ssr_order.is_use , ssr_order.remark_cancel, ssr_order.receipt_type , ssr_order.created , ssr_order.updated , customer.customer_code, tariff_type.tariff_code, ssr_order.remark_doc, ssr_order.id_comp');
			//$this->db->where('ssr_order.type !=','CANCEL');

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->where_in('ssr_order.dr_no', $test);
				} else if($sort == 'code_comp'){
					$this->db->where('customer.customer_code', $search);
				}

			}


			$query_normal = $this->db->get('ssr_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}


	public function CountNoted($search , $sort){

			$this->db->join('customer','customer.custom_id = ssr_noted.id_comp');
			$this->db->join('tariff_type','tariff_type.tariff_id = ssr_noted.tariff_id');
			$this->db->where('ssr_noted.dr_no is NOT NULL', NULL, FALSE);

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->like('ssr_noted.dr_no', $search);
				} else {
					$this->db->like('customer.customer_code', $search);
				}

			}

			$this->db->where('ssr_noted.type !=','CANCEL');
			$this->db->group_by('ssr_noted.dr_no');

			$inv = $this->db->get('ssr_noted');


			return $inv->num_rows();

	}

	public function FetchNoted($limit, $start , $search , $sort){

			$this->db->limit($limit, $start);
			$this->db->order_by("ssr_noted.created", "desc");
			$this->db->join('customer','customer.custom_id = ssr_noted.id_comp');
			$this->db->join('tariff_type','tariff_type.tariff_id = ssr_noted.tariff_id');
			$this->db->where('ssr_noted.noted_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('ssr_noted.noted_no');
			$this->db->select('ssr_noted.noted_no , ssr_noted.type_noted, ssr_noted.status_noted , ssr_noted.prefix_invoice, ssr_noted.payment , ssr_noted.invoice_no , ssr_noted.type , ssr_noted.ref_id , ssr_noted.vessel , ssr_noted.vsl_visit , ssr_noted.is_use , ssr_noted.remark_cancel , ssr_noted.created , ssr_noted.updated , customer.customer_code, tariff_type.tariff_code, ssr_noted.remark_doc');
			$this->db->where('ssr_noted.type !=','CANCEL');

			if(!empty($sort)){
				
				if($sort == 'noted_no'){
					$this->db->where('ssr_noted.noted_no', $search);
				} else if($sort == 'code_comp'){
					$this->db->where('customer.customer_code', $search);
				}

			}


			$query_normal = $this->db->get('ssr_noted');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

}
?>

