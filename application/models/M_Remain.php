<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Remain extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountRemain($search){

			$this->db->where('is_sub', 2);
			$this->db->where('book_con !=', 0);

			if(!empty($search)){
				$this->db->where('book_an',$search);
			}
			
			$inv = $this->db->get('book_order');

			return $inv->num_rows();
			

	}

	 public function fetch_remain($limit, $start , $search  , $sort) {

			$this->db->where('is_sub', 2);
			$this->db->where('book_con !=', 0);


			if(!empty($search)){
				$this->db->where('book_an',$search);
			}

			if(!empty($sort)){
				
				if($sort == 'book_an'){
					$this->db->order_by('book_an', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('created', "desc");
				}

			} else {
				$this->db->order_by("created", "desc");
			}

			$query_normal = $this->db->get('book_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
?>

