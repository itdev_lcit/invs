<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Billing extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountStatusInv($search){

			$prefix = $this->db->get_where('prefix_invoice', array('status' => 0 , 'is_delete' => 0))->row();

			$voice = str_replace($prefix->prefix,"",$search);


			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.invoice_no is NOT NULL', NULL, FALSE);
			
			if(!empty($search)){
				$this->db->where('normal_order.dr_no',$voice);
				$this->db->or_where('status_invoice.invoice_no',$voice);
			}

			$this->db->group_by('book_order.id');
			$this->db->order_by("normal_order.dr_no", "desc");

			$this->db->select('normal_order.dr_no,normal_order.is_reprint ,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$inv = $this->db->get('status_dr');
			
			return $inv->num_rows();
			

	}

	public function CountDraft($search){

			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			
			if(!empty($search)){
				$this->db->where('normal_order.dr_no',$search);
			}

			$this->db->group_by('book_order.id');
			$this->db->order_by("normal_order.dr_no", "desc");

			$this->db->select('normal_order.dr_no,normal_order.is_reprint ,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$inv = $this->db->get('status_dr');
			
			return $inv->num_rows();

			

	}

	public function CountSSR($search, $type){


			$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
			$this->db->group_by('ssr_order.dr_no');
				if(!empty($search)){
					$date = date_create($search);
					$search_d = date_format($date,"Y-m-d");
					$this->db->like('ssr_order.created',$search_d);
				} else {
					$search_d = date('Y-m-d');
					$this->db->like('ssr_order.created',$search_d);
				}

				if($type != 'ALL'){
					$this->db->like('ssr_order.payment',$type);
				} 

			$this->db->select('customer.customer_code, customer.customer_name, ssr_order.dr_no, ssr_order.vessel, ssr_order.vsl_visit, ssr_order.payment, ssr_order.created');
			$inv = $this->db->get('ssr_order');


			return $inv->num_rows();

			

	}

	public function CountInv($search){

			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->join('status_invoice','status_invoice.dr_no = normal_order.dr_no');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.prefix_invoice is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.invoice_no is NOT NULL', NULL, FALSE);
			if(!empty($search)){
					$date = date_create($search);
					$search_d = date_format($date,"Y-m-d");
					$this->db->like('status_invoice.created',$search_d);
				} else {
					$search_d = date('Y-m-d');
					$this->db->like('status_invoice.created',$search_d);
				}
			$this->db->group_by('normal_order.dr_no');
			$this->db->select('normal_order.dr_no,normal_order.is_reprint ,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$inv = $this->db->get('status_dr');


			return $inv->num_rows();

			

	}

	 public function fetch_draft($limit, $start , $search , $sort) {

			$this->db->limit($limit, $start);
			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('normal_order.dr_no');

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->order_by('normal_order.dr_no', "desc");
				} else if($sort == 'code_comp'){
					$this->db->order_by('normal_order.code_comp', "desc");
				} else if($sort == 'book_an'){
					$this->db->order_by('book_order.book_an', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('book_order.created', "desc");
				}

			} else {
				$this->db->order_by("normal_order.dr_no", "desc");
			}

			
			$this->db->select('normal_order.dr_no,normal_order.code_comp, normal_order.is_reprint, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');

				if(!empty($search)){
					$this->db->where('normal_order.dr_no',$search);
				}

			$query_normal = $this->db->get('status_dr');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_status_inv($limit, $start , $search) {

   			$prefix = $this->db->get_where('prefix_invoice', array('status' => 0 , 'is_delete' => 0))->row();

			$voice = str_replace($prefix->prefix,"",$search);

			$this->db->limit($limit, $start);
			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.invoice_no is NOT NULL', NULL, FALSE);
			
			if(!empty($search)){
				$this->db->where('normal_order.dr_no',$voice);
				$this->db->or_where('status_invoice.invoice_no',$voice);
			}

			$this->db->group_by('book_order.id');
			$this->db->order_by("normal_order.dr_no", "desc");

			$this->db->select('normal_order.dr_no,normal_order.is_reprint ,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$query_normal = $this->db->get('status_dr');


        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_inv($limit, $start , $search , $sort) {

			$this->db->limit($limit, $start);
			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->join('status_invoice','status_invoice.dr_no = normal_order.dr_no');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.prefix_invoice is NOT NULL', NULL, FALSE);
			$this->db->where('normal_order.invoice_no is NOT NULL', NULL, FALSE);
			$this->db->group_by('normal_order.dr_no');

			if(!empty($sort)){
				
				if($sort == 'dr_no'){
					$this->db->order_by('normal_order.dr_no', "desc");
				} else if($sort == 'code_comp'){
					$this->db->order_by('normal_order.code_comp', "desc");
				} else if($sort == 'book_an'){
					$this->db->order_by('book_order.book_an', "desc");
				} else if($sort == 'created'){
					$this->db->order_by('book_order.created', "desc");
				}

			} else {
				$this->db->order_by("normal_order.dr_no", "desc");
			}

			
			$this->db->select('normal_order.dr_no,normal_order.code_comp, normal_order.is_reprint, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , status_invoice.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');

				if(!empty($search)){
					$date = date_create($search);
					$search_d = date_format($date,"Y-m-d");
					$this->db->like('status_invoice.created',$search_d);
				} else {
					$search_d = date('Y-m-d');
					$this->db->like('status_invoice.created',$search_d);
				}

			$query_normal = $this->db->get('status_dr');


			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

      public function fetch_SSR($limit, $start , $search , $sort, $type) {

			$this->db->limit($limit, $start);
			$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
			$this->db->group_by('ssr_order.dr_no');
				if(!empty($search)){
					$date = date_create($search);
					$search_d = date_format($date,"Y-m-d");
					$this->db->like('ssr_order.created',$search_d);
				} else {
					$search_d = date('Y-m-d');
					$this->db->like('ssr_order.created',$search_d);
				}

				if($type != 'ALL'){
					$this->db->like('ssr_order.payment',$type);
				} 

			$this->db->select('customer.customer_code, customer.customer_name, ssr_order.dr_no, ssr_order.vessel, ssr_order.vsl_visit, ssr_order.payment, ssr_order.created');
			$query_normal = $this->db->get('ssr_order');


			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

}
?>

