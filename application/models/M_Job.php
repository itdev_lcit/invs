<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Job extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountDraft($search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by("job_order.order_id", "desc");
			$this->db->group_by('job_order.job_no');

			
			if(!empty($sort) AND $sort <> 'ALL'){
					$this->db->where('job_order.job_cat',$sort);
				}

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			$inv = $this->db->get('job_order');


			return $inv->num_rows();

	}

	public function FetchJob($limit, $start , $search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$pieces = explode(", ", $search);
			$test = preg_replace('/\s+/', '', $pieces);

			$this->db->limit($limit, $start);
			$this->db->order_by("job_order.order_id", "desc");

			if($username->role == 'MNGOSL'){
				$this->db->order_by("job_order.mng_status", "asc");
			} if($username->role == 'SUPOSL'){
				$this->db->order_by("job_order.sup_status", "asc");
			} else {
				$this->db->order_by("job_order.job_no", "desc");
			}
			$this->db->group_by('job_order.job_no');

			if(!empty($sort) AND $sort <> 'ALL'){
					$this->db->where('job_order.job_cat',$sort);
				}

			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$query_normal = $this->db->get('job_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

	public function CountJobWait($search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by("job_order.order_id", "desc");
			$this->db->group_by('job_order.job_no');

			
			if(!empty($sort) AND $sort <> 'ALL'){
					$this->db->where('job_order.job_cat',$sort);
				}

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}

			$this->db->where("type !=","CANCEL");

			if($username->rold = 'SUPOSL'){
				$this->db->where("sup_status","WAIT");
			} else if ($username->rold = 'MNGOSL'){
				$this->db->where("mng_status","WAIT");
			} else {
				$this->db->where("(sup_status='WAIT' OR mng_status='WAIT')", NULL, FALSE);
			}
			
			$this->db->where('job_order.is_del','N');
			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			$inv = $this->db->get('job_order');


			return $inv->num_rows();

	}

	public function FetchJobWait($limit, $start , $search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$pieces = explode(", ", $search);
			$test = preg_replace('/\s+/', '', $pieces);

			$this->db->limit($limit, $start);
			$this->db->order_by("job_order.order_id", "desc");

			if($username->role == 'MNGOSL'){
				$this->db->order_by("job_order.mng_status", "asc");
			} if($username->role == 'SUPOSL'){
				$this->db->order_by("job_order.sup_status", "asc");
			} else {
				$this->db->order_by("job_order.job_no", "desc");
			}
			$this->db->group_by('job_order.job_no');

			if(!empty($sort) AND $sort <> 'ALL'){
				$this->db->where('job_order.job_cat',$sort);
			}

			$this->db->where("type !=","CANCEL");

			if($username->rold = 'SUPOSL'){
				$this->db->where("sup_status","WAIT");
			} else if ($username->rold = 'MNGOSL'){
				$this->db->where("mng_status","WAIT");
			} else {
				$this->db->where("(sup_status='WAIT' OR mng_status='WAIT')", NULL, FALSE);
			}

			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$query_normal = $this->db->get('job_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

	public function CountJobApprove($search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by("job_order.order_id", "desc");
			$this->db->group_by('job_order.job_no');

			
			if(!empty($sort) AND $sort <> 'ALL'){
					$this->db->where('job_order.job_cat',$sort);
				}

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}

			$this->db->where("type !=","CANCEL");
			$this->db->where("sup_status","APPROVE");
			$this->db->where("mng_status","APPROVE");

			$this->db->where('job_order.is_del','N');
			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			$inv = $this->db->get('job_order');


			return $inv->num_rows();

	}

	public function FetchJobApprove($limit, $start , $search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$pieces = explode(", ", $search);
			$test = preg_replace('/\s+/', '', $pieces);

			$this->db->limit($limit, $start);
			$this->db->order_by("job_order.order_id", "desc");

			if($username->role == 'MNGOSL'){
				$this->db->order_by("job_order.mng_status", "asc");
			} if($username->role == 'SUPOSL'){
				$this->db->order_by("job_order.sup_status", "asc");
			} else {
				$this->db->order_by("job_order.job_no", "desc");
			}
			$this->db->group_by('job_order.job_no');

			if(!empty($sort) AND $sort <> 'ALL'){
				$this->db->where('job_order.job_cat',$sort);
			}

			$this->db->where("type !=","CANCEL");
			$this->db->where("sup_status","APPROVE");
			$this->db->where("mng_status","APPROVE");

			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$query_normal = $this->db->get('job_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}

	public function CountFin($search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by("job_order.job_no", "desc");
			$this->db->group_by('job_order.job_no');

			
			if(!empty($sort) AND $sort <> 'ALL'){
					if($sort == 'Y'){
						$this->db->where('fin_status','INVOICE');
					} else {
						$this->db->where('fin_status','NONE');
					}
					
			}

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			$inv = $this->db->get('job_order');


			return $inv->num_rows();

	}

	public function FetchFin($limit, $start , $search , $sort){

			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$pieces = explode(", ", $search);
			$test = preg_replace('/\s+/', '', $pieces);

			$this->db->limit($limit, $start);
			if($username->role == 'MNGOSL'){
				$this->db->order_by("job_order.mng_status", "asc");
			} if($username->role == 'SUPOSL'){
				$this->db->order_by("job_order.sup_status", "asc");
			} else {
				$this->db->order_by("job_order.job_no", "desc");
			}
			$this->db->group_by('job_order.job_no');

			if(!empty($sort) AND $sort <> 'ALL'){
					if($sort == 'Y'){
						$this->db->where('fin_status','INVOICE');
					} else {
						$this->db->where('fin_status','NONE');
					}
					
			}

			$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.sup_tm, job_order.mng_tm, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');

			if(!empty($search)){
				
				$this->db->like('job_order.job_no', $search);

			}
			$this->db->where('job_order.is_del','N');
			$query_normal = $this->db->get('job_order');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

			

	}



}
?>

