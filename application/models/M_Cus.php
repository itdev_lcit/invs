<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Cus extends CI_Model {
	function __construct() {
		parent::__construct();
	}


	public function CountCus($search){


			$this->db->order_by("ar_company", "DESC");
			$this->db->where("is_del", "NO");
			if(!empty($search)){

					$this->db->like('customer_name',$search);
					$this->db->or_like('customer_code',$search);
					
				}
			$this->db->get('customer');
			
			return $this->db->count_all('customer');
			

	}

	 public function fetch_Cus($limit, $start , $search , $sort) {

			$this->db->limit($limit, $start);
			$this->db->order_by("created", "DESC");
			$this->db->where("is_del", "NO");

				if(!empty($search)){

					$this->db->like('customer_name',$search);
					$this->db->or_like('customer_code',$search);
					
				}
			$this->db->select('customer.*, SUBSTRING(custom_id,1,3) as TypeCus');
			$query_normal = $this->db->get('customer');

			//$query= $query_normal->result_array();

        if ($query_normal->num_rows() > 0) {
            foreach ($query_normal->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
?>

