<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Export Job Orders</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <form action="<?php echo site_url(); ?>Job/Export" target="_blank" method="POST">
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="row">

                                        <div class="col-lg-12"> 
                                            <div class="col-md-2">
                                                <label>Customer / Project </label>
                                                <input type="text" id="customers_proj" name="customers_proj" class="form-control"> 
                                            </div>
                                            <div class="col-md-2">
                                                <label>From</label>
                                                <input type="text" id="dlv_from" name="dlv_from" class="form-control"> 
                                            </div>
                                            <div class="col-md-2">
                                                <label>To</label>
                                                <input type="text" id="dlv_to" name="dlv_to" class="form-control"> 
                                            </div>
                                            <div class="col-md-2">
                                                <label for="type">Status Job</label>
                                                  <select class="form-control" id="type" name="type">
                                                        <option value="ALL">ALL</option>
                                                        <option value="COMPLETE">COMPLETE</option>
                                                        <option value="COMPLETE">PROCESS</option>
                                                  </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="type">Type Job</label>
                                                  <select class="form-control" id="job_cat" name="job_cat">
                                                        <option value="ALL">ALL</option>
                                                    <?php foreach($job_type as $rs) { ?>
                                                        <option value="<?php echo $rs['job_cat']; ?>"><?php echo $rs['job_cat']; ?></option>
                                                    <?php } ?>
                                                  </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label>From Date</label>
                                                <input class="form-control" type="date" name="first_date" value="<?php echo $first_date; ?>">
                                            </div>
                                            <div class="col-md-2">
                                                <label>To Date</label>
                                                <input class="form-control" type="date" name="second_date" value="<?php echo $second_date; ?>">
                                            </div>
                                            <div class="col-md-12">
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="col-md-12" align="right">
                                                <button class="btn btn-info"><i class="fa fa-download"></i>Export</button>
                                            </div>
                                        </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->


                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </form>
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>