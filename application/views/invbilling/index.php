
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Create Invoice</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">


               <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Type Invoice 
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div align="center">
                                        <form class="form-inline" action="<?php echo site_url() . 'Inv/Create'; ?>" method="POST">
                                            <label for="sel1">Type</label>
                                              <select class="form-control" name="sort" style="width: 20%">
                                                <option value="vat">Vat</option>   
                                                <option value="none">None Vat </option>                  
                                              </select>
                                            <button class="btn btn-info">Go</button>
                                            &nbsp;&nbsp;
                                        </form>
                                    </div> 
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
