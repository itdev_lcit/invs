<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Invoice Manual Noted
        </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
               
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Inv/NotedList'; ?>" method="POST">
                                <label for="sel1">Sort By</label>
                                  <select class="form-control" name="sort">
                                    <option value="dr_no">Draft No.</option>
                                    <option value="code_comp">Customers</option>
                                    <option value="created">Created</option>                        
                                  </select>
                                <input class="form-control" type="text" name="search" value="" placeholder="Inv Manual No...">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>     
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Noted No.</th>
                                            <th>Ref No.</th>
                                            <th style="text-align: center;">Customers</th>
                                            <th style="text-align: center;">Noted</th>
                                            <th style="text-align: center;">Status</th>
                                            <th>Created</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {?>
                                            <tr class="r-invoice"   data-noted_no="<?php echo $rs_normal->noted_no; ?>" <?php if($rs_normal->is_use == 1){ echo "style='color: red;'"; } ?> >
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td>
                                                     <?php echo $rs_normal->noted_no; ?>
                                                </td>
                                                <td>
                                                     <?php echo $rs_normal->prefix_invoice.$rs_normal->invoice_no; ?>
                                                </td>
                                                <td style="text-align: center;"><?php echo $rs_normal->customer_code; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->type_noted; ?></td>
                                                <td style="text-align: center;">
                                                    <?php 
                                                         if($rs_normal->is_use == '0'){
                                                            echo "<p style='color:green;'><b>USE</b></p>";
                                                         }else if ($rs_normal->is_use == '1'){
                                                            echo "<p style='color:red;'><b>CANCEL</b></p>";
                                                         }
                                                    ?>
                                                    
                                                </td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->created)); ?></td>
                                                <td align="center">
                                                    <?php if($rs_normal->is_use != '1'){ ?>
                                                     <a href="<?php echo site_url(); ?>Inv/PrintNoted/<?php echo $rs_normal->noted_no; ?>" title="Generate Invoice" target="_blank"><button class="btn btn-success  btn-xs re-dr-normal"><span class="glyphicon glyphicon-print"></span>&nbsp;Print</button></a>
                                                    <?php } else {
                                                        echo "Remark : ".$rs_normal->remark_cancel;
                                                    } ?>

                                                    <?php if($special == 1   OR $role == 'SUPBILLING' AND $rs_normal->is_use != '1') {?>
                                                       <button class="btn btn-danger btn-xs cancel-invoice" title="Cancel Invoice"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Cancel </button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php $i++; } 
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="6" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="remarkdr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Remark for Cancel Draft*</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="noted_no" id="noted_no" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>





<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){

    $('#normal-draft').DataTable({
            //responsive: true
    });



$("table").off("click", ".cancel-invoice");
$("table").on("click", ".cancel-invoice", function(e) {
            e.preventDefault();
            
            $('#remarkdr').modal('show');

            var $row = $(this).parents('tr.r-invoice');
            var noted_no = $row.data('noted_no');
            $('#noted_no').val(noted_no);
            
});



$('.save-remark').click(function(){

            var remark_inv = $('#comment').val();
            var noted_no = $('#noted_no').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel*');
                console.log('null');
            } else {

                console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>Inv/CancelNoted',
                            method:'POST',
                            data:{ noted_no:noted_no, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                })
            }
          
});


    });

</script>