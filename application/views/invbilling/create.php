
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Create Invoice</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">


               <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Customer 
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-3" align="right">
                                        <input type="text" id="payer_nm" name="payer_nm" class="form-control" placeholder="Search Payer name"> 
                                        <br>
                                    </div>

                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">Code</th>
                                            <th  width="40%">Name</th>
                                            <th  width="40%">Address</th>
                                            <th >A/C No.</th>
                                            <th >TAX</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="payer">
                                        <tr>
                                            <td colspan="5" style="text-align: center;">Please Search Payer name</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Option
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Customer</th>
                                                <th width="50%">Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td> <div id="customer_name"></td>
                                                        <td> <div id="customer_address"></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>

                               

                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: center;">#</th>
                                                <th style="text-align: left; width: 20%;">Type </th>
                                                <th style="text-align: right; width: 20%;">QTY</th>
                                                <th style="text-align: right; width: 20%">Rate (THB)</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <input type="hidden" name="ref" id="ref" value="">
                                            <input type="hidden" name="comp_his" id="comp_his" value="<?php echo $comp_his; ?>">
                                            <input type="hidden" name="id_comp" id="id_comp" value="">
                                            <input type="hidden" id="numOrder" value="<?php echo $numOrder; ?>">
                                            <?php for ($i=1; $i <= $numOrder; $i++) {  ?>
                                                    <tr class="r-order" data-rang="<?php echo $i; ?>">
                                                        <td style="text-align: center;"><?php echo $i; ?></td>
                                                        <td>
                                                            <select class="form-control type" name="type" id="type-<?php echo $i; ?>">
                                                                    <option value="0">-----</option> 
                                                                <?php foreach ($setting_billing as $rs) { ?>
                                                                    <option value="<?php echo $rs['id']; ?>"><?php echo $rs['type']; ?> | <?php echo $rs['description']; ?></option>      
                                                                <?php } ?>                 
                                                            </select>  
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <input type="number" name="qty" class="form-control value-split" id="qty-<?php echo $i; ?>" value="0">  
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="rate_u-<?php echo $i; ?>">0.00</p>
                                                            <input type="number" id="rate-real-<?php echo $i; ?>" class="form-control r_rate" style="display: none;">
                                                            <input type="hidden" id="rate-pro-<?php echo $i; ?>">

                                                        </td>
                                                        <td style="text-align: right;"><p id="total-<?php echo $i; ?>">0.00</p></td>
                                                        <td  style="text-align: center;"> 
                                                            <?php if($i > 1) { ?>
                                                                <form class="form-inline" action="<?php echo site_url() . 'Inv/Create'; ?>" method="POST">
                                                                    <input type="hidden" name="sort" value="<?php echo $type; ?>">
                                                                    <input type="hidden" name="comp_his" class="comp_his"  value="<?php echo $comp_his; ?>">
                                                                    <input type="hidden" name="numOrder" value="<?php echo $numOrder-1; ?>">
                                                                    <button class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                                                    &nbsp;&nbsp;
                                                                </form>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>  
                                            <?php } ?>
                                                    <tr>
                                                        <td colspan="5">
                                                            <form class="form-inline" action="<?php echo site_url() . 'Inv/Create'; ?>" method="POST">
                                                                <input type="hidden" name="sort" value="<?php echo $type; ?>">
                                                                <input type="hidden" name="numOrder" value="<?php echo $numOrder+1; ?>">
                                                                <input type="hidden" name="comp_his" class="comp_his" value="<?php echo $comp_his; ?>">
                                                                <button class="btn btn-success"><i class="fa fa-plus-circle"></i></button>
                                                                &nbsp;&nbsp;
                                                            </form>
                                                            </td>
                                                        </tr>  
                                        </tbody>
                                    </table>   

                                     <div class="col-lg-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Remark 
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-1"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-2"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-3"></textarea>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->

                                    <div class="col-lg-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Option 
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12"> 
                                                        <label>Inv Date</label>
                                                            <input class="form-control" type="date" name="inv_date" id="inv_date" value="" style="width: 200px;">
                                                        <label>Payment</label>
                                                        <select class="form-control type" name="payment" id="payment" style="width: 200px;">
                                                           <option value="0">-----</option> 
                                                           <option value="CASH">CASH</option>     
                                                           <option value="CREDIT">CREDIT</option>                 
                                                        </select>  
                                                        <br>
                                                        <button class="btn btn-success printDr"><span class="glyphicon glyphicon-print"></span>Generate</button>
                                                        <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger can-dr"><span class="glyphicon glyphicon-remove"></span>cancel</button></a>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="insert_tax" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Input Tax ID, IF Customer live in Thailand input tax id 13 number , Else Input 0*</p>
          </div>

                <div class="form-group" align="center">
                    <label>TAX ID</label>

                    <input type="text" name="in_tax" id="in_tax" value="" class="form-control" style="width: 550px;" >
                    <font color="red"><p id="msg-error-tax"></p></font>
                    <input type="hidden" name="in_customer_name" id="in_customer_name" value="">
                    <input type="hidden" name="in_customer_address" id="in_customer_address" value="">
                    <input type="hidden" name="in_id_comp" id="in_id_comp" value="">
                    <input type="hidden" name="in_payer_code" id="in_payer_code" value="">
                    <input type="hidden" name="in_payer_tel" id="in_payer_tel" value="">
                </div>  
                <div class="form-group" align="center">
                    <label>Country</label>
                    <input type="text" name="in_country" id="in_country" value="Thailand" class="form-control" style="width: 550px;" placeholder="Auto Complete Country">
                    <font color="red"><p id="msg-error-country"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-tax">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/lib/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

var comp_his = '<?php echo $comp_his; ?>'


if(comp_his != 0){
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
                method : 'POST',
                data : {payer:comp_his}
            }).done(function(data){
                var o = JSON.parse(data);
                var i = 0;

                for(i=0; i < o.length; i++){

                    $('#customer_name').html(o[i]['COMPANY_NM']);
                    $('#customer_address').html(o[i]['STREET_ADDRESS1_DS']+o[i]['STREET_ADDRESS2_DS']+o[i]['STREET_ADDRESS3_DS']);  
                    $('#id_comp').val(o[i]['COMPANY_ID']);   

                }

            })

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
}


var states = [
    'Aruba','Antigua and Barbuda','United Arab Emirates','Afghanistan','Algeria','Azerbaijan','Albania','Armenia','Andorra','Angola','American Samoa','Argentina','Australia','Ashmore and Cartier Islands','Austria','Anguilla','Åland Islands'
    ,'Antarctica','Bahrain','Barbados','Botswana','Bermuda','Belgium','Bahamas, The','Bangladesh','Belize','Bosnia and Herzegovina','Bolivia','Myanmar','Benin','Belarus','Solomon Islands','Navassa Island','Brazil','Bassas da India','Bhutan'
    ,'Bulgaria','Bouvet Island','Brunei','Burundi','Canada','Cambodia','Chad','Sri Lanka','Congo, Republic of the','Congo, Democratic Republic of the','China','Chile','Cayman Islands','Cocos (Keeling) Islands','Cameroon','Comoros','Colombia'
    ,'Northern Mariana Islands','Coral Sea Islands','Costa Rica','Central African Republic','Cuba','Cape Verde','Cook Islands','Cyprus','Denmark','Djibouti','Dominica','Jarvis Island','Dominican Republic','Dhekelia Sovereign Base Area'
    ,'Ecuador','Egypt','Ireland','Equatorial Guinea','Estonia','Eritrea','El Salvador','Ethiopia','Europa Island','Czech Republic','French Guiana','Finland','Fiji','Falkland Islands (Islas Malvinas)','Micronesia, Federated States of'
    ,'Faroe Islands','French Polynesia','Baker Island','France','French Southern and Antarctic Lands','Gambia, The','Gabon','Georgia','Ghana','Gibraltar','Grenada','Guernsey','Greenland','Germany','Glorioso Islands','Guadeloupe','Guam'
    ,'Greece','Guatemala','Guinea','Guyana','Gaza Strip','Haiti','Hong Kong','Heard Island and McDonald Islands','Honduras','Howland Island','Croatia','Hungary','Iceland','Indonesia','Isle of Man','India','British Indian Ocean Territory'
    ,'Clipperton Island','Iran','Israel','Italy','Cote d Ivoire','Iraq','Japan','Jersey','Jamaica','Jan Mayen','Jordan','Johnston Atoll','Juan de Nova Island','Kenya','Kyrgyzstan','Korea, North','Kingman Reef','Kiribati','KoreaSouth'
    ,'Christmas Island','Kuwait','Kosovo','Kazakhstan','Laos','Lebanon','Latvia','Lithuania','Liberia','Slovakia','Palmyra Atoll','Liechtenstein','Lesotho','Luxembourg','Libyan Arab','Madagascar','Martinique','Macau','Moldova, Republic of'
    ,'Mayotte','Mongolia','Montserrat','Malawi','Montenegro','The Former Yugoslav Republic of Macedonia','Mali','Monaco','Morocco','Mauritius','Midway Islands','Mauritania','Malta','Oman','Maldives','Mexico','Malaysia','Mozambique'
    ,'New Caledonia','Niue','Norfolk Island','Niger','Vanuatu','Nigeria','Netherlands','No Mans Land','Norway','Nepal','Nauru','Suriname','Netherlands Antilles','Nicaragua','New Zealand','Paraguay','Pitcairn Islands','Peru','Paracel Islands'
    ,'Spratly Islands','Pakistan','Poland','Panama','Portugal','Papua New Guinea','Palau','Guinea-Bissau','Qatar','Reunion','Serbia','Marshall Islands','Saint Martin','Romania','Philippines','Puerto Rico','Russia','Rwanda','Saudi Arabia'
    ,'Saint Pierre and Miquelon','Saint Kitts and Nevis','Seychelles','South Africa','Senegal','Saint Helena','Slovenia','Sierra Leone','San Marino','Singapore','Somalia','Spain','Saint Lucia','Sudan','Svalbard','Sweden'
    ,'South Georgia and the Islands','Syrian Arab Republic','Switzerland','Trinidad and Tobago','Tromelin Island','Thailand','Tajikistan','Turks and Caicos Islands','Tokelau','Tonga','Togo','Sao Tome and Principe','Tunisia'
    ,'East Timor','Turkey','Tuvalu','Taiwan','Turkmenistan','Tanzania, United Republic of','Uganda','United Kingdom','Ukraine','United States','Burkina Faso','Uruguay','Uzbekistan','Saint Vincent and the Grenadines','Venezuela'
    ,'British Virgin Islands','Vietnam','Virgin Islands (US)','Holy See (Vatican City)','Namibia','West Bank','Wallis and Futuna','Western Sahara','Wake Island','Samoa','Swaziland','Serbia and Montenegro','Yemen','Zambia','Zimbabwe'
];

$('#in_country').autocomplete({
    source:[states]
});


$('#in_country').on('input', function(e){
    var in_country = $(this).val();
    if(in_country != null){
        $('#msg-error-country').html('');
        $.ajax({
            url:'<?php echo site_url(); ?>Setting/CheckCountry',
            method:'POST',
            data:{ in_country:in_country }
        }).done(function(data){
              
        })
   
    } else {
         $('#msg-error-country').html('');
    }
});


$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();

            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id  = $row.data('ar_company_id');


             $.ajax({
                url:'<?php echo site_url(); ?>Setting/SavePayer',
                method:'POST',
                data:{ 
                    id_customer:id_customer ,
                    customer_address:customer_address,
                    customer_name:customer_name,
                    customer_code:customer_code,
                    telephone_an:telephone_an,
                    tax_reg_no:tax_reg_no,
                    ar_company_id:ar_company_id
                }
            }).done(function(data){

            })

            $('#customer_name').html(customer_name);
            $('#customer_address').html(customer_address);  
            $('#id_comp').val(id_customer);  
            $('#comp_his').val(customer_name);



        });



$("div").off("click", ".save-tax");
        $("div").on("click", ".save-tax", function(e) {
            e.preventDefault();
            
            var in_tax = $('#in_tax').val();
            var in_country = $('#in_country').val();
            if(in_tax == ''){
                $('#msg-error-tax').html('Please Input Tax ID.');
                console.log('null');
            } else {
                console.log('insert tax');
                $('#insert_tax').modal('hide');
                    var id_customer = $('#in_id_comp').val();
                    var customer_address = $('#in_customer_address').val();
                    var customer_name = $('#in_customer_name').val();
                    var customer_code = $('#in_payer_code').val();
                    var telephone_an  = $('#in_payer_tel').val();
                    var tax_reg_no  = in_tax;
                    var country = in_country;

                    $.ajax({
                            url:'<?php echo site_url(); ?>Setting/SavePayerTax',
                            method:'POST',
                            data:{ 
                                id_customer:id_customer ,
                                customer_address:customer_address,
                                customer_name:customer_name,
                                customer_code:customer_code,
                                telephone_an:telephone_an,
                                tax_reg_no:tax_reg_no,
                                country:country
                            }
                    }).done(function(data){

                    })

                        $('#customer_name').html(customer_name);
                        $('#customer_address').html(customer_address);  
                        $('#id_comp').val(id_customer); 
                        $('#payer_address').val(customer_address);
                        $('#payer_name').val(customer_name); 
                        $('#payer_code').val(customer_code);    
                        $('#payer_tel').val(telephone_an);  
                        $('#payer_tax').val(tax_reg_no);   
            }

          
});


$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_address2 = $row.data('customer_address2');
            var customer_address3 = $row.data('customer_address3');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id = $row.data('ar_company_id');

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            if(tax_reg_no != null){
                 $.ajax({
                    url:'<?php echo site_url(); ?>Setting/SavePayer',
                    method:'POST',
                    data:{ 
                        id_customer:id_customer ,
                        customer_address:customer_address,
                        customer_address2:customer_address2,
                        customer_address3:customer_address3,
                        customer_name:customer_name,
                        customer_code:customer_code,
                        telephone_an:telephone_an,
                        tax_reg_no:tax_reg_no,
                        ar_company_id:ar_company_id
                    }
                }).done(function(data){

                })


                $('.comp_his').val(customer_code);
                $('#customer_name').html(customer_name);
                $('#customer_address').html(customer_address+customer_address2+customer_address3);  
                $('#id_comp').val(id_customer); 
                $('#payer_address').val(customer_address);
                $('#payer_name').val(customer_name); 
                $('#payer_code').val(customer_code);    
                $('#payer_tel').val(telephone_an);  
                $('#payer_tax').val(tax_reg_no);   
            } else {

                $('.comp_his').val(customer_code);
                $('#insert_tax').modal('show');
                $('#in_customer_name').val(customer_name); 
                $('#in_customer_address').val(customer_address+customer_address2+customer_address3);
                $('#in_id_comp').val(id_customer);   
                $('#in_payer_code').val(customer_code);  
                $('#in_payer_tel').val(telephone_an); 
            }

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
             
});

$('#payer_nm').on('input', function(e){

    $('#payer').html('');
    $('.printDr').prop('disabled',true);
    $('.can-dr').prop('disabled',true);
    
    console.log('sync Zodiac');
    var payer = $('#payer_nm').val();
    var loading = '<tr align="center"><td colspan="6"><img src="<?php echo base_url(); ?>public/img/loading.gif" width="10%"></td></tr>';

    $('#payer').append(loading);

    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
        method : 'POST',
        data : {payer:payer}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;


        if(o.length == '0'){
             console.log('Not Found Record');
            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var COMPANY_ID = o[i]['COMPANY_ID'];
            var OPS_COMPANY_ID = o[i]['OPS_COMPANY_ID'];
            var ADDRESS = o[i]['STREET_ADDRESS1_DS'];
            var ADDRESS2 = o[i]['STREET_ADDRESS2_DS'];
            var ADDRESS3 = o[i]['STREET_ADDRESS3_DS'];
            var COMPANY_NM = o[i]['COMPANY_NM'];
            var TAX_REG_NO = o[i]['TAX_REG_NO'];
            var TELEPHONE_AN = o[i]['TELEPHONE_AN'];
            var AR_COMPANY_ID = o[i]['AR_COMPANY_ID'];

            payer += '<tr class="r-payer" data-TELEPHONE_AN="'+TELEPHONE_AN+'" data-TAX_REG_NO="'+TAX_REG_NO+'" data-id_customer="'+COMPANY_ID+'"  data-customer_address3="'+ADDRESS3+'" data-customer_address2="'+ADDRESS2+'" data-customer_address="'+ADDRESS+'" data-customer_name="'+COMPANY_NM+'" data-customer_code="'+OPS_COMPANY_ID+'" data-ar_company_id="'+AR_COMPANY_ID+'">';

            payer += '<td style="text-align:center; font-size:12px;">';
            payer += OPS_COMPANY_ID;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += COMPANY_NM;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += ADDRESS + ADDRESS2 + ADDRESS3;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += AR_COMPANY_ID;
            payer += '</td>';


            payer += '<td style="font-size:12px;">';
            payer += TAX_REG_NO;
            payer += '</td>';

            payer += '<td>';
            payer += '<button class="btn btn-info btn-xs cus-data">Select</button>'
            payer += '</td>';

            payer += '</tr>';

        }

        }

        
        $('#payer').html('');
        $('#payer').append(payer);

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })
        $('.printDr').prop('disabled',false);
        $('.can-dr').prop('disabled',false);
    })
});

$('.type').change(function(){


    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang');
    var id_type = $(this).val();

    $('#qty-' + rang).val('0');
    $('#rate-pro-' + rang).val('');
    $('#rate_u-' + rang).html('0.00');
    $('#total-' + rang).html('0.00');  

            $.ajax({
                url:'<?php echo site_url(); ?>Inv/getRate',
                method:'POST',
                data:{ 
                    id_type:id_type
                }
            }).done(function(data){
                var o = JSON.parse(data);
                var cur_rate = o.rate;
                var n_rate = parseInt(cur_rate);
                
                if(cur_rate > 0){

                    $('#rate_u-' + rang).show();
                    $('#rate-real-' + rang).hide();  

                    $('#rate-pro-' + rang).val(n_rate);
                    $('#rate_u-' + rang).html(n_rate.toLocaleString('en'));
                    //$('#total-' + rang).html(n_rate.toLocaleString('en'));
                } else {
                    /*$('#rate_u-' + rang).html('0.00');
                    $('#total-' + rang).html('0.00');*/

                    $('#rate_u-' + rang).hide();
                    $('#rate-real-' + rang).show();  
                }
                

            })
});


$('.printDr').click(function(){

    var numOrder = $('#numOrder').val();
    var id_comp = $('#id_comp').val();
    var remark1 = $('#remark-1').val();
    var remark2 = $('#remark-2').val();
    var remark3 = $('#remark-3').val();
    var payment = $('#payment').val();
    var inv_date = $('#inv_date').val();

    var qty = [];
    var type = [];
    var rate_real = [];

 
    var r = confirm("Confirm to generate ?");
    if (r == true) {  
        
        if(id_comp != 0){

             if(numOrder > 1){

                for (var i = 1; i <= numOrder; i++) {

                    qty.push($('#qty-'+i).val());

                    type.push($('#type-'+i).val());

                    rate_real.push($('#rate-real-'+i).val());

                }

                    const needle = '0';
                    const isInArray = type.includes(needle);
                    console.log(isInArray); // true

                    const needleq = '0';
                    const qInArray = qty.includes(needleq);
                    console.log(qInArray); // true

                    if(isInArray == true){

                        alert('Please Select Type.');

                    } else if(qInArray == true){

                        alert('Please Input Qty.');

                    } else if(payment == '' || payment == null){

                        alert('Please Select Type Payment.');

                    } else if(inv_date == '' || inv_date == null){

                        alert('Please Input Invoice Date.');

                    }  else {

                       $.ajax({
                            url:'<?php echo site_url(); ?>Inv/SaveMultiOrder',
                            method:'POST',
                            data:{ 
                                type:type , qty:qty ,id_comp:id_comp, remark1:remark1, remark2:remark2, remark3:remark3, rate_real:rate_real, payment:payment, inv_date:inv_date
                            }
                        }).done(function(data){

                            var o = JSON.parse(data);
                            var refId = o.refId;

                            window.location = '<?php echo site_url(); ?>Inv/Generate/'+refId;
                        });

                    }
                   

            } else {
                    var type = $('#type-1').val();
                    var qty = $('#qty-1').val();
                    var rate_real = $('#rate-real-1').val();

                    if(payment == '' || payment == null){

                        alert('Please Select Type Payment.');

                    } else if(inv_date == '' || inv_date == null){

                        alert('Please Input Invoice Date.');

                    } else {
                        if(type != 0){

                            $.ajax({
                                url:'<?php echo site_url(); ?>Inv/SaveOnceOrder',
                                method:'POST',
                                data:{ 
                                    type:type, qty:qty , id_comp:id_comp, remark1:remark1, remark2:remark2, remark3:remark3, rate_real:rate_real, payment:payment, inv_date:inv_date
                                }
                            }).done(function(data){

                                var o = JSON.parse(data);
                                var refId = o.refId;
                                window.location = '<?php echo site_url(); ?>Inv/Generate/'+refId;
                            });

                        } else {

                            alert('Please Select Type.');

                        }

                    }

                                        
            }

        } else {
            alert('Please Select Customer.');
        }
           
    } 

    
});

$('.value-split').on('input', function(e){
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseInt($('#rate-pro-' + rang).val());
    var qty = parseInt($('#qty-' + rang).val());
    var rate_real = parseInt($('#rate-real-' + rang).val());

    var type = $('#type-' + rang).val();

    var t_order = rate * qty;

    if(type == null || type == 0){
        alert('Please select type.');
        $('#qty-' + rang).val('0');
    } else {
        if(qty < 0){
            alert('Wrong Value, Please Try Again.');
            $('#qty-' + rang).val('');
        } else {

            if(rate == 0){
                if(qty != 0 || qty == null || rate == null){
                    $('#total-' + rang).html(rate_real.toLocaleString('en'));
                } else {
                    $('#total-' + rang).html('0.00');
                    $('#qty-' + rang).val('');
                }
            } else {
                if(qty != 0 || qty == null || rate == null){
                    $('#total-' + rang).html(t_order.toLocaleString('en'));
                } else {
                    $('#total-' + rang).html('0.00');
                    $('#qty-' + rang).val('');
                }
            }
            
        }
    }


});


$('.r_rate').on('input', function(e){
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseInt($('#rate-pro-' + rang).val());
    var qty = parseInt($('#qty-' + rang).val());
    var rate_real = parseInt($('#rate-real-' + rang).val());

    var type = $('#type-' + rang).val();

    var t_order = rate * qty;

    if(type == null || type == 0){
        alert('Please select type.');
        $('#qty-' + rang).val('0');
    } else {
        if(qty < 0){
            alert('Wrong Value, Please Try Again.');
            $('#qty-' + rang).val('');
        } else {

            if(rate == 0){
                if(qty != 0 || qty == null || rate == null){
                    $('#total-' + rang).html(rate_real.toLocaleString('en'));
                } else {
                    $('#total-' + rang).html('0.00');
                    $('#qty-' + rang).val('');
                }
            } else {
                if(qty != 0 || qty == null || rate == null){
                    $('#total-' + rang).html(rate_real.toLocaleString('en'));
                } else {
                    $('#total-' + rang).html('0.00');
                    $('#qty-' + rang).val('');
                }
            }
            
        }
    }


});

});
</script>