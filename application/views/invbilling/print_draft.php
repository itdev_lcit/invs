	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>

<?php $p_vat = $vat->vat; ?>
	<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>

						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b>Original</b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 11px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Invoice No</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
								<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($datePrint)); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> &nbsp;&nbsp; <?php echo $cus->customer_post; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b>&nbsp;<?php echo $cus->customer_branch;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Payment Terms</b></p></td>
								<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $payment; ?> <?php if($payment == 'CREDIT'){ echo $cus->credit_term.' DAYS';} ?></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 11px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b></b></p></td>
								<td width="25%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
							</tr>

						</table>
						<br><br><br>
						<table class="table">
						    <thead>
						        <tr>
						            <th style="font-size: 11px; text-align: left;"  width="20%" colspan="2">Description</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">QTY</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">Rate (THB)</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">Line Amount (THB)</th>
						        </tr>
						    </thead>
						    <tbody id="book" >
						    	<?php 
						    	$all_amount = 0;
						    	foreach ($order as $rs_order) {  
						    		$is_vat =  $rs_order['is_vat'];
						    		$remark =  $rs_order['remark'];
						    	 ?>
						          <tr>
						          	<td style="font-size: 11px;" align="left" colspan="2"><?php echo $rs_order['description']."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['qty'])."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
						          	<td style="font-size: 11px;" align="right">
						          		<?php

						          			if($rs_order['rate'] == 0){
						          				echo number_format($rs_order['cur_rate'] , 2);
						          				$all_amount += $rs_order['cur_rate'];
						          			} else {
						          				$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
							          			$all_amount += $line_amount;
							          			echo number_format($line_amount, 2);
						          			}
						          			
						          		?>
						          	</td>
						          </tr>   
						        <?php }  ?>

						        <?php if($is_vat == 'y'){ ?>
						         <tr>
						          	<td colspan="3" rowspan="5"><p style="font-size: 11px;"><?php echo $remark; ?></p></td>
						          	<td style="font-size: 11px;" align="right"><b>Sub Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 11px;" align="right"><b>VAT (<?php echo $vat->vat; ?>%)</b></td>
						          	<td style="font-size: 11px;"  align="right">
						          	<?php  
						          	
						          			$vat = $all_amount*$vat->vat/100; 
						          		   	echo number_format($vat, 2); 

						          	?></td>
						          </tr> 
						          <?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 11px;" align="right"><b>Total (THB)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php  
						          			if($is_vat == 'y'){	
						          				$g_total = $all_amount + $vat;

						          				$letet_total = $g_total;

						          				echo number_format($g_total, 2); 
						          			} else {
						          				$letet_total = $all_amount;
						          				echo number_format($all_amount, 2); 
						          			}
						          	  ?></td>
						          </tr>
						           	<?php } else { ?>
								        </tr> 

						           		<tr>
								          	<td colspan="3" rowspan="5"></td>
								          	<td style="font-size: 11px;" align="right"><b>Withholding Tax (<?php echo $with_hold; ?>%)</b></td>
								          	<td style="font-size: 11px;"  align="right"><?php 
						          			$vat_hold = $all_amount*$with_hold/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
								        </tr> 
								        <tr>
								          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
								          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount - $vat_hold, 2); ?></td>
								        </tr> 

						           	<?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 11px;" align="right"><b>Withholding Tax (<?php echo $with_hold; ?>%)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php 
						          			$vat_hold = $all_amount*$with_hold/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 11px;" align="right"><b>Net Pay (THB)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php 

						          			$net_total = $g_total - $vat_hold;
						          			echo number_format($net_total, 2); 

						          	?></td>

						           <?php }  ?>
						          </tr>  
						    </tbody>
						</table>

						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 12px;">
						            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

						            	$num = $letet_total;

						            	$rest = substr(number_format(($letet_total), 2), -2);

						            	$num = str_replace(array(',', ' '), '' , trim($num));
									    if(! $num) {
									        return false;
									    }
									    $num = (int) $num;
									    $words = array();
									    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
									        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
									    );
									    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
									    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
									        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
									        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
									    );
									    $num_length = strlen($num);
									    $levels = (int) (($num_length + 2) / 3);
									    $max_length = $levels * 3;
									    $num = substr('00' . $num, -$max_length);
									    $num_levels = str_split($num, 3);
									    for ($i = 0; $i < count($num_levels); $i++) {
									        $levels--;
									        $hundreds = (int) ($num_levels[$i] / 100);
									        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
									        $tens = (int) ($num_levels[$i] % 100);
									        $singles = '';
									        if ( $tens < 20 ) {
									            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
									        } else {
									            $tens = (int)($tens / 10);
									            $tens = ' ' . $list2[$tens] . ' ';
									            $singles = (int) ($num_levels[$i] % 10);
									            $singles = ' ' . $list1[$singles] . ' ';
									        }
									        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
									    } //end for loop
									    $commas = count($words);
									    if ($commas > 1) {
									        $commas = $commas - 1;
									    }

									    if($rest > 0){
									    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
									    } else {
									    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
									    }
									    

						            	 ?></p>
						            	
						            </td>
						        </tr>
						</table>


						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 11px;" width="50%">
						            	REMARK : THE SERVICES QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THAILAND , USER HAS AGREED AND ACCEPTED.
						            </td>
						            <td style="font-size: 11px;" width="50%">
						            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
						            </td>
						        </tr>
						</table>
						<br><br><br><br><br>
						<P style="color:gray; font-size: 11px;">Printed by <?php echo $user_print->name; ?> on <?php echo date("j-F-Y H:i", strtotime($datePrint)); ?></P>
						<br>
	</div>



		<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>

						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b>Copy</b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 11px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Invoice No</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
								<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($datePrint)); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> &nbsp;&nbsp; <?php echo $cus->customer_post; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
								<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b>&nbsp;<?php echo $cus->customer_branch;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b>Payment Terms</b></p></td>
								<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $payment; ?> <?php if($payment == 'CREDIT'){ echo $cus->credit_term.' DAYS';} ?></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 11px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 11px;"><b></b></p></td>
								<td width="25%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
							</tr>

						</table>
						<br><br><br>
						<table class="table">
						    <thead>
						        <tr>
						            <th style="font-size: 11px; text-align: left;"  width="20%" colspan="2">Description</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">QTY</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">Rate (THB)</th>
						            <th style="font-size: 11px; text-align: right;"  width="10%">Line Amount (THB)</th>
						        </tr>
						    </thead>
						    <tbody id="book" >
						    	<?php 
						    	$all_amount = 0;
						    	foreach ($order as $rs_order) {  
						    		$is_vat =  $rs_order['is_vat'];
						    		$remark = $rs_order['remark'];
						    	 ?>
						          <tr>
						          	<td style="font-size: 11px;" align="left" colspan="2"><?php echo $rs_order['description']."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['qty'])."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
						          	<td style="font-size: 11px;" align="right">
						          		<?php

						          			if($rs_order['rate'] == 0){
						          				echo number_format($rs_order['cur_rate'] , 2);
						          				$all_amount += $rs_order['cur_rate'];
						          			} else {
						          				$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
							          			$all_amount += $line_amount;
							          			echo number_format($line_amount, 2);
						          			}
						          			
						          		?>
						          	</td>
						          </tr>   
						        <?php }  ?>

						        <?php if($is_vat == 'y'){ ?>
						         <tr>
						          	<td colspan="3" rowspan="5"><p style="font-size: 11px;"><?php echo $remark; ?></p></td>
						          	<td style="font-size: 11px;" align="right"><b>Sub Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 11px;" align="right"><b>VAT (<?php echo $p_vat; ?>%)</b></td>
						          	<td style="font-size: 11px;"  align="right">
						          	<?php  
						          	
						          			$vat = $all_amount*$p_vat/100; 
						          		   	echo number_format($vat, 2); 

						          	?></td>
						          </tr> 
						          <?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 11px;" align="right"><b>Total (THB)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php  
						          			if($is_vat == 'y'){	
						          				$g_total = $all_amount + $vat;

						          				$letet_total = $g_total;

						          				echo number_format($g_total, 2); 
						          			} else {
						          				$letet_total = $all_amount;
						          				echo number_format($all_amount, 2); 
						          			}
						          	  ?></td>
						          </tr>
						           	<?php } else { ?>
								        </tr> 

						           		<tr>
								          	<td colspan="3" rowspan="5"></td>
								          	<td style="font-size: 11px;" align="right"><b>Withholding Tax (<?php echo $with_hold; ?>%)</b></td>
								          	<td style="font-size: 11px;"  align="right"><?php 
						          			$vat_hold = $all_amount*$with_hold/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
								        </tr> 
								        <tr>
								          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
								          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount - $vat_hold, 2); ?></td>
								        </tr> 

						           	<?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 11px;" align="right"><b>Withholding Tax (<?php echo $with_hold; ?>%)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php 
						          			$vat_hold = $all_amount*$with_hold/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 11px;" align="right"><b>Net Pay (THB)</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php 

						          			$net_total = $g_total - $vat_hold;
						          			echo number_format($net_total, 2); 

						          	?></td>

						           <?php }  ?>
						          </tr>  
						    </tbody>
						</table>


						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 12px;">
						            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

						            	$num = $letet_total;

						            	$rest = substr(number_format(($letet_total), 2), -2);

						            	$num = str_replace(array(',', ' '), '' , trim($num));
									    if(! $num) {
									        return false;
									    }
									    $num = (int) $num;
									    $words = array();
									    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
									        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
									    );
									    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
									    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
									        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
									        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
									    );
									    $num_length = strlen($num);
									    $levels = (int) (($num_length + 2) / 3);
									    $max_length = $levels * 3;
									    $num = substr('00' . $num, -$max_length);
									    $num_levels = str_split($num, 3);
									    for ($i = 0; $i < count($num_levels); $i++) {
									        $levels--;
									        $hundreds = (int) ($num_levels[$i] / 100);
									        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
									        $tens = (int) ($num_levels[$i] % 100);
									        $singles = '';
									        if ( $tens < 20 ) {
									            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
									        } else {
									            $tens = (int)($tens / 10);
									            $tens = ' ' . $list2[$tens] . ' ';
									            $singles = (int) ($num_levels[$i] % 10);
									            $singles = ' ' . $list1[$singles] . ' ';
									        }
									        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
									    } //end for loop
									    $commas = count($words);
									    if ($commas > 1) {
									        $commas = $commas - 1;
									    }

									    if($rest > 0){
									    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
									    } else {
									    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
									    }
									    

						            	 ?></p>
						            	
						            </td>
						        </tr>
						</table>

						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 11px;" width="50%">
						            	REMARK : THE SERVICES QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THAILAND , USER HAS AGREED AND ACCEPTED.
						            </td>
						            <td style="font-size: 11px;" width="50%">
						            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
						            </td>
						        </tr>
						</table>
						<br><br><br><br><br>
						<P style="color:gray; font-size: 11px;">Printed by <?php echo $user_print->name; ?> on <?php echo date("j-F-Y H:i", strtotime($datePrint)); ?></P>
						<br>
	</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Draft/AllDraft/";
	});
});
</script>