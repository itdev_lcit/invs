<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>

<?php $cal_vat =  $vat->vat; ?>

<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>
<div class="page">
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h4 align="center"><b>RECEIPT / TAX INVOICE</b></h4>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 15px;"><b>Original</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $inv_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($datePrint)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $cus->customer_branch;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Ref. No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></p></td>
					</tr>



				</table>
				<br>
				<table class="table">
				    <thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;"  colspan="2">Description</th>
				            <th style="font-size: 11px; text-align: right;"  width="20%"> QTY</th>
				            <th style="font-size: 11px; text-align: right;"  width="20%"> Rate (THB)</th>
				            <th style="font-size: 11px; text-align: right;"  width="25%">Line Amount (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    	$all_amount = 0;
				    	foreach ($order as $rs_order) {  
				    		$is_vat =  $rs_order['is_vat'];
				    		$remark = $rs_order['remark'];
				    		$payment = $rs_order['payment'];
				    		?>
				          <tr>
				          	<td style="font-size: 11px;" align="left" colspan="2"><?php echo $rs_order['description']; ?></td>
				          	<td style="font-size: 11px;" align="right" ><?php echo $rs_order['qty']; ?></td>
				          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
				          	<td style="font-size: 11px;" align="right">
				          		<?php

				          			$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
				          			$all_amount += $line_amount;
				          			echo number_format($line_amount, 2);
				          		?>
				          	</td>
				          </tr>   
				        <?php }  ?>

				        <?php if($is_vat == 'y'){ ?>
				         	<tr>
					          	<td colspan="3" rowspan="5" style="font-size: 11px;"><?php echo $remark; ?></td>
					          	<td style="font-size: 11px;" align="right"><b>Sub Total</b></td>
					          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
				          	</tr> 
				          	<tr>      	
					          	<td style="font-size: 11px;" align="right"><b>VAT (<?php echo $vat->vat; ?>%)</b></td>
					          	<td style="font-size: 11px;"  align="right">
					          	<?php  
					          			$vat = $all_amount*$vat->vat/100; 
					          		   	echo number_format($vat, 2); 

					          	?></td>
				          	</tr> 
				         	<tr>  	
					          	<td style="font-size: 11px;" align="right"><b>Total (THB)</b></td>
					          	<td style="font-size: 11px;"  align="right">
					          	<?php  

					          			$net_total = $all_amount + $vat;
					          			$amount = $all_amount + $vat;
					          			echo number_format($net_total, 2); 

					          	?></td>
				          	</tr> 
				          	<?php if($hold_status == '1'){ ?>

				          	<?php } ?>
				          <?php } else {  
				          		if($hold_status == '1'){
				          			$hold_tax = $all_amount*3/100; 
				          			$net_total = $all_amount - $hold_tax;
				          		} else {
				          			$net_total = $all_amount;
				          		}

				          		$amount = $all_amount;
				          	?>
				          	<?php if($hold_status == '1'){ ?>


					          	<tr>
						          	<td colspan="3" rowspan="5"></td>
						          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
					          	</tr> 

				          	<?php } else { ?>

					          	<tr>
						          	<td colspan="3" rowspan="5"></td>
						          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
					          	</tr> 

				          	<?php } ?>
					          	
				          <?php } ?>
				    </tbody>
				</table>


				<table class="table">
					<thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
				        </tr>
				        <tr>
				            <th style="font-size: 11px; text-align: left;">Date</th>
				            <th style="font-size: 11px; text-align: center;">Payment Method</th>
				            <th style="font-size: 11px; text-align: left;"></th>
				            <th style="font-size: 11px; text-align: right;">Payment Amount</th>
				            <th style="font-size: 11px; text-align: right;">Amount (THB)</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php if($is_vat != 'y'){ ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;"><?php echo date("j-F-Y", strtotime($datePrint)); ?></td>
				    		<td style="font-size: 11px; text-align: center;">CASH</td>
				    		<td style="font-size: 11px; text-align: center;"></td>
				    		<td style="font-size: 11px; text-align: right;">THB&nbsp;<?php echo number_format($all_amount, 2); ?></td>
				    		<td style="font-size: 11px; text-align: right;"><?php echo number_format($all_amount, 2); ?></td>
				    	</tr>

				    	<?php if($all_amount >= 1000) { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1"></td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (1%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php $hold_tax = $all_amount*1/100; 
				          				echo number_format($hold_tax, 2); ?></td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($all_amount - $hold_tax, 2); ?></td>
				    	</tr>
				        <?php } else { ?>
				        <tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" >0.00</td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($all_amount, 2); ?></td>
				    	</tr>
				        <?php } ?>	

				    	<?php } else { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;"><?php echo date("j-F-Y", strtotime($datePrint)); ?></td>
				    		<td style="font-size: 11px; text-align: center;"><?php echo $payment; ?></td>
				    		<td style="font-size: 11px; text-align: center;"></td>
				    		<td style="font-size: 11px; text-align: right;">THB&nbsp;<?php echo number_format($amount, 2); ?></td>
				    		<td style="font-size: 11px; text-align: right;"><?php echo number_format($amount, 2); ?></td>
				    	</tr>
				    	<?php if($hold_status == '1'){ ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php $hold_tax = $all_amount*$with_hold/100; 
				          				echo number_format($hold_tax, 2); ?></td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($net_total-$hold_tax, 2); ?></td>
				    	</tr>
				    	<?php } else { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" >0.00</td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($amount, 2); ?></td>
				    	<?php } ?>
				    	<?php } ?>
				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 11px;" width="50%">
				            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
				            	
				            </td>
				            <td style="font-size: 11px;" width="50%">
				            	<p>This is a computer generated document, no signature required.</p>
				            	<!--<p>Contact detail:</p>
				            	<p>Email :</p>-->
				            	<p>Tel :  (66 38 ) 40 8200</p>
				            	<p>Fax : (66 38) 40 1021 - 2</p>
				            </td>
				        </tr>
				</table>
</div>


<div class="page">
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h4 align="center"><b>RECEIPT / TAX INVOICE</b></h4>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 15px;"><b>Copy</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $inv_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($datePrint)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $cus->customer_branch;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Ref. No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></p></td>
					</tr>



				</table>
				<br>
				<table class="table">
				    <thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;"  colspan="2">Description</th>
				            <th style="font-size: 11px; text-align: right;"  width="20%"> QTY</th>
				            <th style="font-size: 11px; text-align: right;"  width="20%"> Rate (THB)</th>
				            <th style="font-size: 11px; text-align: right;"  width="25%">Line Amount (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    	$all_amount = 0;
				    	foreach ($order as $rs_order) {  
				    		$is_vat =  $rs_order['is_vat'];
				    		$remark = $rs_order['remark'];
				    		$payment = $rs_order['payment'];
				    		?>
				          <tr>
				          	<td style="font-size: 11px;" align="left" colspan="2"><?php echo $rs_order['description']; ?></td>
				          	<td style="font-size: 11px;" align="right" ><?php echo $rs_order['qty']; ?></td>
				          	<td style="font-size: 11px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
				          	<td style="font-size: 11px;" align="right">
				          		<?php

				          			$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
				          			$all_amount += $line_amount;
				          			echo number_format($line_amount, 2);
				          		?>
				          	</td>
				          </tr>   
				        <?php }  ?>

				        <?php if($is_vat == 'y'){ ?>
				         	<tr>
					          	<td colspan="3" rowspan="5" style="font-size: 11px;"><?php echo $remark; ?></td>
					          	<td style="font-size: 11px;" align="right"><b>Sub Total</b></td>
					          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
				          	</tr> 
				          	<tr>      	
					          	<td style="font-size: 11px;" align="right"><b>VAT (<?php echo $cal_vat; ?>%)</b></td>
					          	<td style="font-size: 11px;"  align="right">
					          	<?php  
					          			$vat = $all_amount*$cal_vat/100; 
					          		   	echo number_format($vat, 2); 

					          	?></td>
				          	</tr> 
				         	<tr>  	
					          	<td style="font-size: 11px;" align="right"><b>Total (THB)</b></td>
					          	<td style="font-size: 11px;"  align="right">
					          	<?php  

					          			$net_total = $all_amount + $vat;
					          			$amount = $all_amount + $vat;
					          			echo number_format($net_total, 2); 

					          	?></td>
				          	</tr> 
				          	<?php if($hold_status == '1'){ ?>

				          	<?php } ?>
				          <?php } else {  
				          		if($hold_status == '1'){
				          			$hold_tax = $all_amount*3/100; 
				          			$net_total = $all_amount - $hold_tax;
				          		} else {
				          			$net_total = $all_amount;
				          		}

				          		$amount = $all_amount;
				          	?>
				          	<?php if($hold_status == '1'){ ?>


					          	<tr>
						          	<td colspan="3" rowspan="5"></td>
						          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
					          	</tr> 

				          	<?php } else { ?>

					          	<tr>
						          	<td colspan="3" rowspan="5"></td>
						          	<td style="font-size: 11px;" align="right"><b>Total</b></td>
						          	<td style="font-size: 11px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
					          	</tr> 

				          	<?php } ?>
					          	
				          <?php } ?>
				    </tbody>
				</table>

				

				<table class="table">
					<thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
				        </tr>
				        <tr>
				            <th style="font-size: 11px; text-align: left;">Date</th>
				            <th style="font-size: 11px; text-align: center;">Payment Method</th>
				            <th style="font-size: 11px; text-align: left;"></th>
				            <th style="font-size: 11px; text-align: right;">Payment Amount</th>
				            <th style="font-size: 11px; text-align: right;">Amount (THB)</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php if($is_vat != 'y'){ ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;"><?php echo date("j-F-Y", strtotime($datePrint)); ?></td>
				    		<td style="font-size: 11px; text-align: center;">CASH</td>
				    		<td style="font-size: 11px; text-align: center;"></td>
				    		<td style="font-size: 11px; text-align: right;">THB&nbsp;<?php echo number_format($all_amount, 2); ?></td>
				    		<td style="font-size: 11px; text-align: right;"><?php echo number_format($all_amount, 2); ?></td>
				    	</tr>

				    	<?php if($all_amount >= 1000) { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php $hold_tax = $all_amount*$with_hold/100; 
				          				echo number_format($hold_tax, 2); ?></td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($all_amount - $hold_tax, 2); ?></td>
				    	</tr>
				        <?php } else { ?>
				        <tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" >0.00</td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($all_amount, 2); ?></td>
				    	</tr>
				        <?php } ?>	

				    	<?php } else { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;"><?php echo date("j-F-Y", strtotime($datePrint)); ?></td>
				    		<td style="font-size: 11px; text-align: center;"><?php echo $payment; ?></td>
				    		<td style="font-size: 11px; text-align: center;"></td>
				    		<td style="font-size: 11px; text-align: right;">THB&nbsp;<?php echo number_format($amount, 2); ?></td>
				    		<td style="font-size: 11px; text-align: right;"><?php echo number_format($amount, 2); ?></td>
				    	</tr>
				    	<?php if($hold_status == '1'){ ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php $hold_tax = $all_amount*$with_hold/100; 
				          				echo number_format($hold_tax, 2); ?></td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($net_total-$hold_tax, 2); ?></td>
				    	</tr>
				    	<?php } else { ?>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">
				    			<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

								            	$num = $all_amount+($all_amount*(7/100));

								            	$rest = substr(number_format(($num), 2), -2);

								            	$num = str_replace(array(',', ' '), '' , trim($num));
											    if(! $num) {
											        return false;
											    }
											    $num = (int) $num;
											    $words = array();
											    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
											        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
											    );
											    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
											    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
											        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
											        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
											    );
											    $num_length = strlen($num);
											    $levels = (int) (($num_length + 2) / 3);
											    $max_length = $levels * 3;
											    $num = substr('00' . $num, -$max_length);
											    $num_levels = str_split($num, 3);
											    for ($i = 0; $i < count($num_levels); $i++) {
											        $levels--;
											        $hundreds = (int) ($num_levels[$i] / 100);
											        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
											        $tens = (int) ($num_levels[$i] % 100);
											        $singles = '';
											        if ( $tens < 20 ) {
											            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
											        } else {
											            $tens = (int)($tens / 10);
											            $tens = ' ' . $list2[$tens] . ' ';
											            $singles = (int) ($num_levels[$i] % 10);
											            $singles = ' ' . $list1[$singles] . ' ';
											        }
											        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
											    } //end for loop
											    $commas = count($words);
											    if ($commas > 1) {
											        $commas = $commas - 1;
											    }

											    if($rest > 0){
											    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
											    } else {
											    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
											    }
											    

								            	 ?></p>
				    		</td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Withholding Tax (<?php echo $with_hold; ?>%) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" >0.00</td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="1">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 11px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 11px; text-align: right;" ><b>Amount Received (THB) :</b></td>
				    		<td style="font-size: 11px; text-align: right;" ><?php echo number_format($amount, 2); ?></td>
				    	<?php } ?>
				    	<?php } ?>
				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 11px;" width="50%">
				            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
				            	
				            </td>
				            <td style="font-size: 11px;" width="50%">
				            	<p>This is a computer generated document, no signature required.</p>
				            	<!--<p>Contact detail:</p>
				            	<p>Email :</p>-->
				            	<p>Tel :  (66 38 ) 40 8200</p>
				            	<p>Fax : (66 38) 40 1021 - 2</p>
				            </td>
				        </tr>
				</table>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  	//window.print();
    //document.location.href = "<?php echo site_url(); ?>Billing/AllBill";
    $(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Billing/AllBill";
	});
});
</script>