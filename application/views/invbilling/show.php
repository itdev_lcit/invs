<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Invoice Manual 
            <a href="<?php echo site_url(''); ?>Inv"><button class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Create</button> </a>
            <a href="<?php echo site_url(''); ?>Inv/NotedList"><button class="btn btn" style="background-color:#7400ff;color: white;"><i class="fa fa-paperclip"></i>&nbsp;Noted List</button> </a>
        </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
               
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Inv/All'; ?>" method="POST">
                                <label for="sel1">Sort By</label>
                                  <select class="form-control" name="sort">
                                    <option value="dr_no">Draft No.</option>
                                    <option value="code_comp">Customers</option>
                                    <option value="created">Created</option>                        
                                  </select>
                                <input class="form-control" type="text" name="search" value="" placeholder="Inv Manual No...">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>     
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Inv Manual</th>
                                            <th style="text-align: center;">Customers</th>
                                            <th style="text-align: center;">Payment</th>
                                            <th style="text-align: center;">Vat</th>
                                            <th style="text-align: center;">Status</th>
                                            <th>Created</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {?>
                                            <tr class="r-invoice"   data-dr_no="<?php echo $rs_normal->dr_no; ?>" <?php if($rs_normal->is_use == 1){ echo "style='color: red;'"; } ?> >
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td>
                                                     <?php echo $rs_normal->dr_no; ?>
                                                </td>
                                                <td style="text-align: center;"><?php echo $rs_normal->customer_code; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->payment; ?></td>
                                                <td style="text-align: center;">
                                                    <?php 
                                                         if($rs_normal->is_vat == 'n'){
                                                            echo "<p style='color:red;'><b>NO</b></p>";
                                                         } else if ($rs_normal->is_vat == 'y'){
                                                            echo "<p style='color:green;'><b>YES</b></p>";
                                                         } 
                                                    ?>
                                                    
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php 
                                                         if($rs_normal->is_use == '0'){
                                                            echo "<p style='color:green;'><b>Draft</b></p>";
                                                         } else if ($rs_normal->is_use == '2'){
                                                            echo "<p style='color:green;'><b>Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                         } else if ($rs_normal->is_use == '1'){
                                                            if($rs_normal->invoice_no){
                                                                echo "<p style='color:red;'><b>Cancel-Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                            } else {
                                                                echo "<p style='color:red;'><b>Cancel-Draft#".$rs_normal->dr_no;
                                                            }
                                                         }
                                                    ?>
                                                    
                                                </td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->created)); ?></td>
                                                <td align="center">
                                                    <?php if($rs_normal->is_use != '1'){ ?>
                                                     <a href="<?php echo site_url(); ?>Inv/PrintDraft/<?php echo $rs_normal->ref_id; ?>" title="Generate Invoice"><button class="btn btn-success  btn-xs re-dr-normal"><span class="glyphicon glyphicon-print"></span>&nbsp;Print</button></a>
                                                    <?php } else {
                                                        echo "Remark : ".$rs_normal->remark_cancel;
                                                    } ?>
                                                    <?php if($rs_normal->is_use == '0'){ ?>
                                                        <a href="<?php echo site_url(); ?>Inv/Preview/<?php echo $rs_normal->dr_no; ?>" title="Generate Invoice"><button class="btn btn-info  btn-xs re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Pre-Receipt</button></a>
                                                    <?php } ?>
                                                    <?php if($rs_normal->is_use == '2'){ ?>
                                                        <?php if($special == 1   OR $role == 'SUPBILLING') {?>
                                                            <a href="<?php echo site_url(); ?>Inv/RePreview/<?php echo $rs_normal->dr_no; ?>" title="Preview Invoice"><button class="btn btn-warning btn-xs re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Re - Receipt</button></a>
                                                        <?php } ?>
                                                    <?php } ?>

                                                    <a href="<?php echo site_url(); ?>Inv/CreateNote/<?php echo $rs_normal->dr_no; ?>" title="Create Noted"><button class="btn btn btn-xs " style="background-color:#7400ff;color: white;"><span class="glyphicon glyphicon-paperclip"></span>&nbsp;Noted</button></a>

                                                    <?php if($special == 1   OR $role == 'SUPBILLING' AND $rs_normal->is_use != '1') {?>
                                                       <button class="btn btn-danger btn-xs cancel-invoice" title="Cancel Invoice"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Cancel </button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php $i++; } 
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="6" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="remarkdr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Remark for Cancel Draft*</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="dr_no" id="dr_no" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>





<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){

    $('#normal-draft').DataTable({
            //responsive: true
    });



$("table").off("click", ".cancel-invoice");
$("table").on("click", ".cancel-invoice", function(e) {
            e.preventDefault();
            
            $('#remarkdr').modal('show');

            var $row = $(this).parents('tr.r-invoice');
            var dr_no = $row.data('dr_no');
            $('#dr_no').val(dr_no);
            
});



$('.save-remark').click(function(){

            var remark_inv = $('#comment').val();
            var dr_no = $('#dr_no').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel*');
                console.log('null');
            } else {

                console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>Inv/Cancel',
                            method:'POST',
                            data:{ dr_no:dr_no, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                })
            }
          
});


    });

</script>