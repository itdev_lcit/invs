<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div style="float: left;">
                            <h6>&nbsp;&nbsp;&nbsp;<b>DRAFT NO : </b><?php echo $dr_no; ?></h6>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Code</th>
                                                <th >Customer</th>
                                                <th >Branch</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody  style="font-size: 13px;">
                                           
                                                    <tr>
                                                        <td><?php echo  $cus->customer_code; ?></td>
                                                        <td><?php echo  $cus->customer_name; ?></td>
                                                        <td><?php echo  $cus->customer_branch; ?></td>
                                                        <td><?php echo  $cus->customer_address; ?><?php echo  $cus->customer_address2; ?><?php echo  $cus->customer_address3; ?> <?php echo  $cus->customer_post; ?></td>
                                                        <td><?php echo  $cus->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>

                                <div class="col-lg-2">
                                </div>
  
                                <div class="col-lg-8">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: right;">Rate (THB)</th>
                                                <th style="text-align: right;">QTY</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr"  style="font-size: 13px;">
                                            <?php 
                                                $all_amount= 0;
                                                foreach ($order as $rs_order) { 
                                                $is_vat =  $rs_order['is_vat'];
                                                $line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
                                                $all_amount += $line_amount;
                                            ?>
                                                <tr>
                                                    <td style="text-align: left;"><?php echo $rs_order['description']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['rate']); ?></td>
                                                    <td style="text-align: right;"><?php echo $rs_order['qty']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['rate'] * $rs_order['qty']); ?></td>
                                                <tr>
                                            <?php } ?>  
                                        </tbody>
                                    </table>   
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <?php if($is_vat == 'y'){ ?>
                                        <div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1">VAT(<?php echo $vat->vat; ?>%) (THB)</label>
                                              <p><?php echo number_format($all_amount* $vat->vat / 100); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1">Total (THB)</label>
                                              <p><?php echo number_format( $all_amount + ($all_amount* $vat->vat / 100)); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="holdtax"><input type="checkbox" id="holdtax" value="1" checked>&nbsp; &nbsp; Withholding Tax (<?php echo $with_hold; ?>%) (THB)</label>
                                                  <p id="hold_v"><?php echo number_format($all_amount*$with_hold / 100); ?></p>
                                                  <input type="hidden" name="hold_total" id="hold_total" value="<?php echo $all_amount*$with_hold / 100; ?>">
                                                  <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo $all_amount + ($all_amount * $vat->vat / 100) - ($all_amount*$with_hold / 100); ?>">
                                                  <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  $all_amount + ($all_amount * $vat->vat / 100); ?>">
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount + ($all_amount * $vat->vat / 100) - ($all_amount*$with_hold / 100)); ?></p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount + ($all_amount * $vat->vat / 100)); ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    <?php } else { ?>

                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($is_vat != 'n' AND $all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="holdtax"><input type="checkbox" id="holdtax" value="1" checked>&nbsp; &nbsp; Withholding Tax (<?php echo $with_hold; ?>%) (THB)</label>
                                                  <p id="hold_v"><?php echo number_format($all_amount*$with_hold / 100); ?></p>
                                                  <input type="hidden" name="hold_total" id="hold_total" value="<?php echo $all_amount*$with_hold / 100; ?>">
                                                  <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo $all_amount - ($all_amount* 3 / 100); ?>">
                                                  <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  $all_amount; ?>">
                                                </div>
                                            </div>
                                        <?php } else if ( $all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="holdtax"><input type="checkbox" id="holdtax" value="1" checked>&nbsp; &nbsp; Withholding Tax (1%) (THB)</label>
                                                  <p id="hold_v"><?php echo number_format($all_amount* 1 / 100); ?></p>
                                                  <input type="hidden" name="hold_total" id="hold_total" value="<?php echo $all_amount* 1 / 100; ?>">
                                                  <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo $all_amount - ($all_amount* 1 / 100); ?>">
                                                  <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  $all_amount; ?>">
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount  - ($all_amount* 3 / 100)); ?></p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount); ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    
                                    <?php } ?>
                                    <div align="right">
                                        <label>Receipt Date</label>
                                            <input class="form-control" type="date" name="re_date" id="re_date" value="" style="width: 200px;">
                                        <br>
                                        <input type="hidden" id="is_vat" value="<?php echo $is_vat; ?>">
                                        <input type="hidden" id="none_hold" value="<?php if($all_amount >= 1000){ echo 1;} else { echo 0;}?>">
                                        <input type="hidden" id="dr_no" value="<?php echo $dr_no; ?>">
                                        <button class="btn btn-info btn-xs gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Genarate Receipt</button>
                                        <a href="<?php echo site_url(); ?>Inv/All"><button class="btn btn-danger btn-xs ">Cancel</button></a>
                                    </div>
                                </div>

                                 <div class="col-lg-2">
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    var is_vat = $('#is_vat').val();



$("#holdtax").bind("change",function(){
   var value = $(this).is(":checked");
   if(value){
        var hold_total = parseInt($('#hold_total').val());
        var net_total_h = parseInt($('#net_total_h').val());
        $('#hold_v').html(hold_total.toLocaleString('en'));
        $('#net_p').html(net_total_h.toLocaleString('en'));
        $('#none_hold').val('1');
   } else {

        var net_total_n = parseInt($('#net_total_n').val());
        $('#hold_v').html('0');
        $('#net_p').html(net_total_n.toLocaleString('en'));
         $('#none_hold').val('0');
   }
});

 $('.gen-invoice').click(function(){

        var dr_no = $('#dr_no').val();
        var none_hold = $('#none_hold').val();
        var re_date = $('#re_date').val();

                var r = confirm("Confirm to generate Receipt Invoice ?");
                if (r == true) {  
                    
                    if(re_date == '' || re_date == null){
                       alert('Please Input Receipt Date.');
                    } else {
                        window.location = '<?php echo site_url(); ?>Inv/SetReceipt/'+dr_no+'/'+none_hold+'/'+re_date; 
                    }
                    
                       
                } 
           
        });



});
</script>