
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Create Noted
        <a href="<?php echo site_url(''); ?>Inv/CreateNote/<?php echo $dr_no; ?>/<?php echo $RefNoted; ?>">&nbsp;&nbsp;<button class="btn btn-warning"><i class="fa fa-refresh"></i>&nbsp;Refresh</button></a>
       </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">

                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Option
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Invoice No.</th>
                                                <th width="50%">Receipt No.</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td>
                                                            <p style="color: green;"><b><?php echo $dr_no;?></b></p>
                                                        </td>
                                                        <td> 
                                                            <p style="color: green;"><b><?php echo $inv_no->prefix_invoice.$inv_no->invoice_no;?></b></p>
                                                        </td>   
                                                        
                                                    </tr> 
                                         
                                        </tbody>
                                    </table> 

                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Customer</th>
                                                <th width="50%">Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td> 
                                                            (<?php echo $cus->customer_code;?>)&nbsp;&nbsp;
                                                            <?php echo $cus->customer_name;?>
                                                        </td>
                                                        <td> 
                                                            <?php echo $cus->customer_address; ?>&nbsp;&nbsp;
                                                            <?php echo $cus->customer_address2; ?>&nbsp;&nbsp;
                                                            <?php echo $cus->customer_address3; ?>&nbsp;&nbsp;
                                                            <?php echo $cus->customer_post; ?>&nbsp;&nbsp;
                                                        </td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>

                               

                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: center;">#</th>
                                                <th style="text-align: left; width: 20%;">Type </th>
                                                <th style="text-align: right; width: 20%;">QTY</th>
                                                <th style="text-align: right; width: 20%">Rate (THB)</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <input type="hidden" id="numOrder" value="<?php echo count($order); ?>">
                                            <input type="hidden" id="RefNoted" name="RefNoted" value="<?php echo $RefNoted; ?>">
                                            <?php 

                                                $numOrder = count($order);
                                                $i = 1;
                                                $all_amount = 0;

                                                foreach ($order as $rs_order) {   ?>

                                                    <input type="hidden" id="billing_id-<?php echo $i; ?>" value="<?php echo $rs_order['id']; ?>">
                                                    <tr class="r-order" data-rang="<?php echo $i; ?>" data-idbilling="<?php echo $rs_order['id']; ?>">
                                                        <td style="text-align: center;"><?php echo $i; ?></td>
                                                        <td>
                                                            <?php echo $rs_order['type']." | ".$rs_order['description']; ?>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <input style="text-align: right;" type="number" name="qty-<?php echo $i; ?>" class="form-control value-split" id="qty-<?php echo $i; ?>" value="<?php echo $rs_order['qty']; ?>">  
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <input style="text-align: right;" type="number" name="rate-pro-<?php echo $i; ?>" class="form-control unit_rate" id="rate-pro-<?php echo $i; ?>" value="<?php echo $rs_order['cur_rate']; ?>">

                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="total-<?php echo $i; ?>">
                                                            <?php

                                                                $line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
                                                                $all_amount += $line_amount;
                                                                echo number_format($line_amount, 2);
                                                            ?>
                                                            </p>
                                                            <input type="hidden" id="cal_total-<?php echo $i; ?>" value="<?php echo $all_amount; ?>">
                                                        </td>
                                                        <td  style="text-align: center;"> 
                                                            <button class="btn btn-danger remove-items"><i class="fa fa-remove"></i></button>
                                                        </td>
                                                    </tr>  
                                            <?php $i++;} ?>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right;"><b style="font-size: 17px;">Sum Total<b></td>
                                                        <td style="text-align: right;"><p id="sum_total"><?php echo number_format($all_amount, 2);  ?></p></td>
                                                    </tr>
                                        </tbody>
                                    </table>   

                                    <div class="col-lg-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Remark 
                                            </div>
                                            <br>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-1"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-2"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="7" id="remark-3"></textarea>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                       
                                    </div>

                                     <div class="col-lg-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Noted Type 
                                            </div>
                                            <div class="panel-body">
                                                    <div class="col-lg-12"> 
                                                        <label>Noted Date</label>
                                                            <input class="form-control" type="date" name="noted_date" id="noted_date" value="" style="width: 200px;">
                                                        <label>Noted Type</label>
                                                        <select class="form-control type" name="payment" id="payment" style="width: 200px;">
                                                           <option value="DEBIT">DEBIT</option>     
                                                           <option value="CREDIT">CREDIT</option>                 
                                                        </select>  
                                                        <br>
                                                        <button class="btn btn-success printDr"><span class="glyphicon glyphicon-print"></span>Generate</button>
                                                        <a href="<?php echo site_url(); ?>Inv/All"><button class="btn btn-danger can-dr"><span class="glyphicon glyphicon-remove"></span>cancel</button></a>
                                                    </div>
                                            </div>
                                            
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

      $("table").off("click", ".remove-items");
        $("table").on("click", ".remove-items", function(e) {

        var $row = $(this).parents('tr.r-order');
        var idbilling = $row.data('idbilling');

        var numOrder = $('#numOrder').val();
        
        if(numOrder > 1) {

            var r = confirm("Do you want to remove this item ?");

            if (r == true) {  
                $.ajax({
                    url:'<?php echo site_url(); ?>Inv/DelItemNoted',
                    method:'POST',
                     data:{ idbilling:idbilling }
                }).done(function(data){

                    window.location = '<?php echo site_url(); ?>Inv/CreateNote/'+'<?php echo $dr_no; ?>/<?php echo $RefNoted; ?>'

                });

            } 
        } else {
                alert('Warning ! Cannot remove item when it is last item.');
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    });

    $('.value-split').on('input', function(e){
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseInt($('#rate-pro-' + rang).val());
    var qty = parseInt($('#qty-' + rang).val());
    var numOrder = $('#numOrder').val();

    var t_order = rate * qty;

    console.log(rate);

        if(qty == 0 || qty < 0){
            alert('Wrong Value, Please Try Again.');
            $('#qty-' + rang).val('');
             $('#total-' + rang).html('0.00');


             $('#cal_total-'+rang).val(0);
        } else {
            if(qty != 0 || qty == null || rate == null){

                if(isNaN(t_order)){
                        var check_t_order = 0;
                    } else {
                        var check_t_order = t_order;
                    }

                $('#total-' + rang).html(check_t_order.toLocaleString('en'));


                $('#cal_total-'+rang).val(check_t_order);

                var sum_total = 0;

                for(i=1; i <= numOrder; i++){

                    var caltotal = parseInt($('#cal_total-'+i).val());

                    if(isNaN(caltotal)){
                        var check_total = 0;
                    } else {
                        var check_total = caltotal;
                    }

                    console.log(check_total);

                    console.log(caltotal);

                    sum_total += check_total;
                }

                    
                    console.log(sum_total);

                    $('#sum_total').html(sum_total.toLocaleString('en'))


            } else {
                $('#total-' + rang).html('0.00');
                $('#qty-' + rang).val('');
            }
        }



});

$('.unit_rate').on('input', function(e){
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang');  
    var rate = parseInt($('#rate-pro-' + rang).val());
    var qty = parseInt($('#qty-' + rang).val());
    var numOrder = $('#numOrder').val();


    var t_order = rate * qty;

        if(rate == 0 || rate < 0){
            alert('Wrong Value, Please Try Again.');
            $('#rate-pro-' + ssr_id).val('');
             $('#total-' + rang).html('0.00');


             $('#cal_total-'+rang).val(0);
        } else {
            if(qty != 0 || qty == null || rate == null){

                if(isNaN(t_order)){
                        var check_t_order = 0;
                    } else {
                        var check_t_order = t_order;
                    }

                $('#total-' + rang).html(check_t_order.toLocaleString('en'));


                $('#cal_total-'+rang).val(check_t_order);

                var sum_total = 0;

                for(i=1; i <= numOrder; i++){

                    var caltotal = parseInt($('#cal_total-'+i).val());

                    if(isNaN(caltotal)){
                        var check_total = 0;
                    } else {
                        var check_total = caltotal;
                    }

                    console.log(check_total);

                    console.log(caltotal);

                    sum_total += check_total;
                }

                    
                    console.log(sum_total);

                    $('#sum_total').html(sum_total.toLocaleString('en'))


            } else {
                $('#total-' + ssr_id).html('0.00');
                $('#qty-' + ssr_id).val('');
            }
        }



});


$('.printDr').click(function(){

    var noted_date = $('#noted_date').val();
    var RefNoted = $('#RefNoted').val();
    var numOrder = $('#numOrder').val();
    var remark1 = $('#remark-1').val();
    var remark2 = $('#remark-2').val();
    var remark3 = $('#remark-3').val();
    var payment = $('#payment').val();

    var qty = [];
    var rate_pro = [];
    var billing_id = [];


    if(noted_date != null || noted_date != ''){

        var r = confirm("Confirm to generate ?");
        if (r == true) {  
            
           if(numOrder > 1){

                for (var i = 1; i <= numOrder; i++) {

                    qty.push($('#qty-'+i).val());

                    rate_pro.push($('#rate-pro-'+i).val());

                    billing_id.push($('#billing_id-'+i).val());
                }

                const needle = '0';
                const isInArray = rate_pro.includes(needle);
                console.log(isInArray); // true

                const needleq = '0';
                const qInArray = qty.includes(needleq);
                console.log(qInArray); // true

                    if(qInArray == true){

                        alert('Please Input Qty.');

                    } else if(isInArray == true){

                        alert('Please Input Rate.');

                    } else {

                        $.ajax({
                            url:'<?php echo site_url(); ?>Inv/UpdatedNoted',
                            method:'POST',
                                data:{ 
                                    billing_id:billing_id,
                                    noted_date:noted_date,
                                    RefNoted:RefNoted,
                                    qty:qty,
                                    rate_pro:rate_pro,
                                    remark1:remark1,
                                    remark2:remark2, 
                                    remark3:remark3,
                                    payment:payment
                                }
                        }).done(function(data){
                            var o = JSON.parse(data);
                            var msg = o.msg;
                            window.location = '<?php echo site_url(); ?>Inv/PrintNoted/'+msg;
                        });

                     }
                               

            } else {

                var qty = $('#qty-1').val();
                var rate_pro = $('#rate-pro-1').val();
                var billing_id = $('#billing_id-1').val();

                $.ajax({
                    url:'<?php echo site_url(); ?>Inv/UpdatedSingleNoted',
                    method:'POST',
                        data:{ 
                            billing_id:billing_id,
                            noted_date:noted_date,
                            RefNoted:RefNoted,
                            qty:qty,
                            rate_pro:rate_pro,
                            remark1:remark1,
                            remark2:remark2, 
                            remark3:remark3,
                            payment:payment
                        }
                }).done(function(data){

                    var o = JSON.parse(data);
                    var msg = o.msg;
                    window.location = '<?php echo site_url(); ?>Inv/PrintNoted/'+msg;
                });

            }
               
        } 

    } else{

       alert('Please Input Noted Date.');

    }
});


});
</script>