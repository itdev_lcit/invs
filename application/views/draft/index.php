<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">DRAFT</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
               
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Draft/AllDraft'; ?>" method="POST">
                                <label for="sel1">Show</label>
                                    <select class="form-control" name="typeD">
                                        <option value="all">All</option>
                                        <option value="dr">Draft</option>
                                        <option value="inv">Invoice</option>                     
                                    </select>

                                <label for="sel1">Sort By</label>
                                  <select class="form-control" name="sort">
                                    <option value="">-</option>
                                    <option value="dr_no">Draft No.</option>
                                    <option value="code_comp">Customers</option>
                                    <option value="book_an">Booking</option>
                                    <option value="created">Created</option>                        
                                  </select>
                                <input class="form-control" type="text" name="search" value="" placeholder="Search Draft No & Booking ">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>     
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Draft</th>
                                            <th>Booking</th>
                                            <th style="text-align: center;">Customers</th>
                                            <!--<th style="text-align: center;">Size</th>
                                            <th style="text-align: center;">Type</th>
                                            <th style="text-align: center;">QTY</th>-->
                                            <th style="text-align: center;">Status</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th>Remark</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {?>
                                            <tr class="r-normal"  data-is_multi="<?php echo $rs_normal->is_multi; ?>" data-dr_no="<?php echo $rs_normal->dr_no; ?>" data-id="<?php echo $rs_normal->id; ?>" data-trm=<?php echo $rs_normal->terminal_doc_ref_id; ?> <?php if($rs_normal->is_use == 1){ echo "style='color: red;'"; } ?> >
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td>
                                                    <?php if($rs_normal->is_use != 1) { ?>
                                                    <a href="<?php echo site_url(); ?>Draft/Preview/<?php echo $rs_normal->dr_no; ?>/<?php echo $rs_normal->is_multi; ?>" target="_blank" title="Preview Invoice"><?php echo $rs_normal->dr_no; ?></a>
                                                     <?php } else {?>
                                                     <?php echo $rs_normal->dr_no; ?>
                                                     <?php } ?>
                                                </td>
                                                <td><?php echo $rs_normal->book_an; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->code_comp; ?></td>
                                                <td style="text-align: center;">
                                                    <?php 
                                                         if($rs_normal->is_use == '0'){
                                                            echo "<p style='color:green;'><b>Draft</b></p>";
                                                         } else if ($rs_normal->is_use == '2'){
                                                            echo "<p  style='color:green;'><b>Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                         } else {
                                                            echo "<p style='color:red;'><b>Cancel#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                         }
                                                    ?>
                                                    
                                                </td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->Create_Dr)); ?></td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->Updated_Dr)); ?></td>
                                                <td align="left">
                                                        <?php if ($rs_normal->is_use == 1) {?>
                                                            <?php echo $rs_normal->remark_cancel; ?>
                                                        <?php } else if ($rs_normal->remark_cancel != null) { ?>
                                                            <p style='color:green;'><?php echo $rs_normal->remark_cancel; ?></p>
                                                        <?php } else { ?>
                                                            -
                                                        <?php } ?>
                                                </td>
                                                <td align="center">
                                                     <?php if ($rs_normal->is_use != 1) {?>
                                                    <button class="btn btn-info btn-xs comment-dr"><span class="glyphicon glyphicon-comment"></span> Remark</button>
                                                    <?php } ?>
                                                    <?php if($rs_normal->is_use == 0){ ?>
                                                        <?php if($rs_normal->is_multi == 0) { ?>
                                                            <a href="<?php echo site_url(); ?>Createdraft/PrintDR/<?php echo $rs_normal->id_order ?>/<?php echo $rs_normal->id_comp; ?>"><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span> Preview</button></a>
                                                             <button class="btn btn-danger btn-xs change-dr-normal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</button>
                                                        <?php } else {?>
                                                            <a href="<?php echo site_url(); ?>MultiDraft/PrintDR/<?php echo $rs_normal->dr_no; ?>" ><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span> Preview</button></a>
                                                             <button class="btn btn-danger btn-xs change-dr-normal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</button>
                                                        <?php } ?>
                                                    <?php } else if ($rs_normal->is_use == 2) {?>
                                                        <?php if($rs_normal->is_multi == 0){ ?>
                                                            <a href="<?php echo site_url(); ?>Createdraft/PrintDR/<?php echo $rs_normal->id_order; ?>/<?php echo $rs_normal->id_comp; ?>" ><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span> Preview</button></a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo site_url(); ?>MultiDraft/PrintDR/<?php echo $rs_normal->dr_no; ?>" ><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span> Preview</button></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php $i++; } 
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="8" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="remarkdr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Remark for Cancel Draft*</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="id_dr" id="id_dr" value="">
                    <input type="hidden" name="is_multi" id="is_multi" value="">
                    <input type="hidden" name="dr_no" id="dr_no" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>


<!-- Modal -->
<div id="re-dr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Remark Draft*</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="remark-dr"></textarea>
                    <input type="hidden" name="id_dr_re" id="id_dr_re" value="">
                    <input type="hidden" name="is_multi_re" id="is_multi_re" value="">
                    <input type="hidden" name="dr_no_re" id="dr_no_re" value="">
                    <font color="red"><p id="msg-error-remark_re"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark-re">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>




<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){

    $('#normal-draft').DataTable({
            //responsive: true
    });

$("table").off("click", ".comment-dr");
$("table").on("click", ".comment-dr", function(e) {
            e.preventDefault();
            
            $('#re-dr').modal('show');

            var $row = $(this).parents('tr.r-normal');
            var id = $row.data('id')
            var dr_no = $row.data('dr_no');
            var is_multi = $row.data('is_multi');

            $('#id_dr_re').val(id);
            $('#dr_no_re').val(dr_no);
            $('#is_multi_re').val(is_multi);
            
});



$("table").off("click", ".change-dr-normal");
$("table").on("click", ".change-dr-normal", function(e) {
            e.preventDefault();
            
            $('#remarkdr').modal('show');

            var $row = $(this).parents('tr.r-normal');
            var id = $row.data('id')
            var dr_no = $row.data('dr_no');
            var is_multi = $row.data('is_multi');

            $('#id_dr').val(id);
            $('#dr_no').val(dr_no);
            $('#is_multi').val(is_multi);
            
});



$('.save-remark').click(function(){
            var remark_inv = $('#comment').val();
            var id_dr = $('#id_dr').val();
            var dr_no = $('#dr_no').val();
            var is_multi = $('#is_multi').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel Draft*');
                console.log('null');
            } else {

                if(is_multi == '0'){
                    console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>Createdraft/CancelDrNormal',
                            method:'POST',
                            data:{ id_dr:id_dr, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                    })
                } else {
                    console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>MultiDraft/CancelDrNormal',
                            method:'POST',
                            data:{ id_dr:id_dr, dr_no:dr_no, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                    })

                }
            }
          
});


$('.save-remark-re').click(function(){
            var remark_inv = $('#remark-dr').val();
            var id_dr = $('#id_dr_re').val();
            var dr_no = $('#dr_no_re').val();
            var is_multi = $('#is_multi_re').val();

            if(remark_inv == ''){
                $('#msg-error-remark_re').html('*Please Remark Draft*');
                console.log('null');
            } else {

                $.ajax({
                        url:'<?php echo site_url(); ?>Draft/Remark',
                            method:'POST',
                            data:{ id_dr:id_dr, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                    })
            }
          
});


    });

</script>