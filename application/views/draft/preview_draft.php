
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Preview Invoice No: <?php echo $dr_no; ?></h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                         
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                           <tr> 
                                                <th>Code</th>
                                                <th>TAX</th>
                                                <th>Customer</th>
                                                <th>Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td><?php echo $payer_order->customer_code; ?></td>
                                                        <td><?php echo $payer_order->tax_reg_no; ?></td>
                                                        <td><?php echo $payer_order->customer_name; ?></td>
                                                        <td>
                                                            <?php echo $payer_order->customer_address; ?>
                                                            <?php echo $payer_order->customer_address2; ?>
                                                            <?php echo $payer_order->customer_address3; ?>
                                                        </td>                                                    
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: left;" >QTY</th>
                                                <th style="text-align: right;" >Unit Rate (THB)</th>
                                                <th style="text-align: right;" >Line Amount (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                      
                                            <?php 
                                                $all_amount = 0;
                                                foreach ($order as $rs) { 
                                                $line_amount = $rs['book_con'] * $rs['cur_rate'];
                                                $all_amount += $line_amount;

                                                if($is_multi == 0){
                                                    $id_comp = $rs['id_comp'];
                                                    $id_order = $rs['id'];
                                                }
                                                ?>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <?php echo $dr_t->description."&nbsp;&nbsp;".$rs['size_con']."'"."&nbsp;&nbsp;".$rs['type_con']; ?>                                          
                                                    </td>
                                                    <td style="text-align: left;"><?php echo $rs['book_con']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs['cur_rate'], 2); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($line_amount, 2); ?></td>
                                                </tr>
                                            <?php } ?>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td style="text-align: right;"><b>Sub Total<b></td>
                                                    <td style="text-align: right;"><?php echo number_format($all_amount, 2); ?></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: right;"><b>VAT (<?php echo $vat->vat; ?>%)<b></td>
                                                    <td style="text-align: right;">
                                                        <?php 
                                                            $vat = $all_amount*$vat->vat/100; 
                                                            echo number_format($vat, 2); 
                                                        ?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td style="text-align: right;"><b>Total (THB)<b></td>
                                                    <td style="text-align: right;">
                                                        <?php 
                                                            $g_total = $all_amount + $vat;
                                                            echo number_format($g_total, 2); 
                                                        ?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td style="text-align: right;"><b>Withholding Tax (3%)<b></td>
                                                    <td style="text-align: right;">
                                                        <?php 
                                                            $vat_hold = $all_amount*3/100; 
                                                            echo number_format($vat_hold, 2); 
                                                        ?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td style="text-align: right;"><b>Net Pay (THB)<b></td>
                                                    <td style="text-align: right;">
                                                        <?php 
                                                            $net_total = $g_total - $vat_hold;
                                                            echo number_format($net_total, 2); 
                                                        ?> 
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>   
                                     <div style="float: right;">
                                        <?php if($is_multi == 0) { ?>
                                             <a href="<?php echo site_url(); ?>Createdraft/PrintDR/<?php echo $id_order; ?>/<?php echo $id_comp; ?>" ><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span>Print</button></a>
                                        <?php } else { ?>
                                            <a href="<?php echo site_url(); ?>MultiDraft/PrintDR/<?php echo $dr_no; ?>" ><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span>Print</button></a>
                                        <?php } ?>
                                        <a href="<?php echo site_url(); ?>Draft/AllDraft/"><button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-arrow-left"></span>Back</button></a>
                                    </div>   
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

              

            </div>
            <!-- /.row -->

</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
