<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting TARIFF (SSR)</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config TARIFF (SSR)
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>TARIFF CODE</label>
                                                <input class="form-control" id="tariff_id" name="tariff_id" required="true" value="" type="hidden">
                                                <input class="form-control" id="tariff_code" name="tariff_code" required="true" value="">
                                                <p id="validate-type" style="color: red;"></p>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>TARIFF RATE</label>                                          
                                                <input class="form-control" type="number" id="tariff_rate" name="tariff_rate" required="true" value="">
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>TYPE RATE</label>                                          
                                                <input class="form-control" type="text" id="rate_type" name="rate_type" required="true" value="THB">
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>TYPE UNIT</label>                                          
                                                <input class="form-control" type="text" id="unit_type" name="unit_type" required="true" value="Unit">
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="is_delete" name="is_delete">
                                                    <option value="N">ACTIVE</option>
                                                    <option value="Y">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Vat</label>
                                                <select class="form-control" id="vat" name="vat">
                                                        <option value="N">No</option>
                                                        <?php foreach ($setting_vat as $rs) { ?>
                                                            <option value="<?php echo $rs['code_vat']; ?>"><?php echo $rs['code_vat']." | ".$rs['vat']."%"; ?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Withholding Tax</label>
                                                <select class="form-control" id="holdtax" name="holdtax">
                                                    <option value="N">No</option>
                                                        <?php foreach ($setting_holdtax as $rs) { ?>
                                                            <option value="<?php echo $rs['code_wh']; ?>"><?php echo $rs['code_wh']." | ".$rs['holdtax']."%"; ?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                         <div class="col-lg-12">    
                                            <div class="form-group">
                                              <label for="description">TARIFF DESC</label>
                                              <p id="validate-description" style="color: red;"></p>
                                              <textarea class="form-control" rows="5" id="tariff_des"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" align="right">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveType" id="saveSize">Save</button>
                                                <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-12">
                                    
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Description</th>
                                            <th>Type</th>
                                            <th>Rate</th>
                                            <th>WH</th>
                                            <th>Vat</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($tariff_type){
                                            $i = 1;
                                            foreach ($tariff_type as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type" data-tariff_id="<?php echo $rs['tariff_id']; ?>" data-tariff_code="<?php echo $rs['tariff_code']; ?>" data-tariff_des="<?php echo $rs['tariff_des']; ?>" data-tariff_rate="<?php echo $rs['tariff_rate']; ?>" data-is_delete="<?php echo $rs['is_delete']; ?>" data-code_wh="<?php echo $rs['code_wh'] ?>" data-code_vat="<?php echo $rs['code_vat']; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $rs['tariff_code']; ?></td>
                                                    <td><?php echo $rs['tariff_des']; ?></td>
                                                    <td style="text-align: center"><?php echo $rs['unit_type']; ?></td>
                                                    <td style="text-align: right"><?php echo $rs['tariff_rate']." ".$rs['rate_type']; ?></td>
                                                    <td style="text-align: right"><?php echo $rs['holdtax']; ?>%</td>
                                                    <td style="text-align: right"><?php echo $rs['vat']; ?>%</td>
                                                    <td><?php 
                                                        if($rs['is_delete'] == 'N'){
                                                            echo "<p style='color:green;'><b>ACTIVE</b></p>";
                                                        } else {
                                                            echo "<p style='color:red;'><b>INACTIVE</b></p>";
                                                        }
                                                     ?></td>
                                                     <td><?php echo date("j-F-Y H:i", strtotime($rs['created'])); ?></td>
                                                    <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="7">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeleteTypeTariff" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.EditType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var tariff_id = $row.data('tariff_id');
            var tariff_code = $row.data('tariff_code');
            var tariff_des = $row.data('tariff_des');
            var tariff_rate = $row.data('tariff_rate');
            var is_delete = $row.data('is_delete');
            var code_vat = $row.data('code_vat');
            var code_wh = $row.data('code_wh');


            $('#tariff_id').val('');
            $('#tariff_code').val('');
            $('#tariff_des').val('');
            $('#tariff_rate').val('');
            $('#is_delete').val('');

            $('#tariff_id').val(tariff_id);
            $('#tariff_code').val(tariff_code);
            $('#tariff_des').val(tariff_des);
            $('#tariff_rate').val(tariff_rate);
            $('#is_delete').val(is_delete);
            $('#holdtax').val(code_wh);
            $('#vat').val(code_vat);
        });

         $('.RemoveType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var id = $row.data('tariff_id');
      
            $('#remove_id').val('');
            $('#remove_id').val(id);
            $('#myModal').modal('show');
        });

        $('.saveType').click(function(){
            
            var tariff_id = $('#tariff_id').val();
            var tariff_code = $('#tariff_code').val();
            var tariff_des = $('#tariff_des').val();
            var tariff_rate = $('#tariff_rate').val();
            var is_delete = $('#is_delete').val();
            var code_wh = $('#holdtax').val();
            var code_vat = $('#vat').val();

            $.ajax({
                url:'<?php echo base_url(); ?>Setting/SaveTypeTariff',
                method:'POST',
                data:{ tariff_id:tariff_id, tariff_code:tariff_code, tariff_des:tariff_des, tariff_rate:tariff_rate, is_delete:is_delete, code_vat:code_vat, code_wh:code_wh }
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-type').html('');
                    $('#validate-type').append('Please Enter TARIFF CODE.');
                } 

                if(o.msg == 200){
                    $('#validate-type').html('');
                    $('#validate-type').append("TARIFF CODE'S Already.");
                } 

                if(o.msg == 300){
                    $('#validate-description').html('');
                    $('#validate-description').append('Please Enter TARIFF DESC.');
                } 


                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        }); 
        

        $('.clearType').click(function(){
            
            $('#tariff_id').val('');
            $('#tariff_code').val('');
            $('#tariff_des').val('');
            $('#tariff_rate').val('');
            $('#is_delete').val('');


        }); 

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>