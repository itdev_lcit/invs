<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Type : <?php echo $type->type; ?></h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Size&Rate 
                            <div style="float: right;">
                                         <a href="<?php echo site_url(); ?>Setting/TypeDoc"><button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-arrow-left"></span>Back</button></a>
                                    </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Size</label>
                                                 <input class="form-control" id="id" name="id" required="true" value="" type="hidden">
                                                <input class="form-control" id="size" name="size" required="true" value="">

                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Rate</label>
                                                <input class="form-control" id="rate" name="rate" required="true" value="">
                                                <p id="validate-rate" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveSize">Save</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                    
                                </div>
                                <div class="col-lg-8">
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Size</th>
                                            <th>Rate (THB)</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($setting_size_rate){
                                            foreach ($setting_size_rate as $rs) { 
                                                
                                            ?>

                                                <tr class="r-size" data-id="<?php echo $rs['id']; ?>" data-size="<?php echo $rs['size']; ?>" data-rate="<?php echo $rs['rate']; ?>">

                                                    <td><?php echo $rs['size'].'"'; ?></td>
                                                    <td><?php 

                                                        if(!empty($rs['rate'])) {
                                                            echo number_format($rs['rate'],2); 
                                                        } else {
                                                            echo "-";
                                                        }

                                                    ?></td>
                                                    <td><?php echo $rs['updated']; ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditSize"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                    </td>
                                                </tr>

                                        <?php

                                            } 
                                        }else {
                                            echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#size').prop('disabled', true);
        $('#rate').prop('disabled', true);
        $('#saveSize').prop('disabled', true);

        $('.EditSize').click(function(){

            $('#rate').prop('disabled', false);
            $('#saveSize').prop('disabled', false);
            
            var $row = $(this).parents('tr.r-size');
            var id = $row.data('id');
            var size = $row.data('size');
            var rate = $row.data('rate');

            $('#id').val('');
            $('#size').val('');
            $('#rate').val('');

            $('#id').val(id);
            $('#size').val(size);
            $('#rate').val(rate);
        });

        $('.saveSize').click(function(){
            
            var id = $('#id').val();
            var size = $('#size').val();
            var rate = $('#rate').val();


            $.ajax({
                url:'<?php echo base_url(); ?>Setting/SaveSizeRate',
                method:'POST',
                data:{ id:id, size:size, rate:rate}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 600){
                    $('#validate-rate').html('');
                    $('#validate-rate').append('Only Numeric.');
                } 

                if (o.msg == 500){
                    $('#validate-rate').html('');
                    $('#validate-rate').append('Please Enter Rate.');
                }

                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        });

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>