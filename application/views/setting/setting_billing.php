<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Type Billing
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Type</label>
                                                <input class="form-control" id="id" name="id" required="true" value="" type="hidden">                                           
                                                <input class="form-control" id="type" name="type" required="true" value="">
                                                <p id="validate-type" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Rate</label>                                          
                                                <input class="form-control" type="number" id="rate" name="rate" required="true" value="">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Vat</label>
                                                <select class="form-control" id="vat" name="vat">
                                                    <option value="y">YES</option>
                                                    <option value="n">NO</option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="status" name="status">
                                                    <option value="0">ACTIVE</option>
                                                    <option value="1">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>

                                         <div class="col-lg-12">    
                                            <div class="form-group">
                                              <label for="description">Description</label>
                                              <p id="validate-description" style="color: red;"></p>
                                              <textarea class="form-control" rows="5" id="description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveType" id="saveSize">Save</button>
                                                <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-8">
                                    <div align="right">
                                        <form class="form-inline" action="<?php echo site_url() . 'Setting/TypeBill'; ?>" method="POST">
                                            <label for="sel1"></label>
                                              <select class="form-control" name="sort">
                                                <option value="all" <?php if($type_vat == 'all' or empty($type_vat)){ echo "selected"; } ?> >All</option>
                                                <option value="none" <?php if($type_vat == 'none'){ echo "selected"; } ?>>None</option>
                                                <option value="vat"  <?php if($type_vat == 'vat'){ echo "selected"; } ?>>Vat</option>                     
                                              </select>
                                            <button class="btn btn-info">Go</button>
                                            &nbsp;&nbsp;
                                        </form>
                                    </div> 
                                    <br>
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Vat</th>
                                            <th>Rate (THB)</th>
                                            <th>Status</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($setting_billing){
                                            $i = 1;
                                            foreach ($setting_billing as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type"  data-rate="<?php echo $rs['rate']; ?>"   data-vat="<?php echo $rs['is_vat']; ?>"  data-id="<?php echo $rs['id']; ?>" data-des="<?php echo $rs['description']; ?>" data-type="<?php echo $rs['type']; ?>" data-status="<?php echo $rs['status']; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $rs['type']; ?></td>
                                                     <td><?php 
                                                        if($rs['is_vat'] == 'n'){
                                                            echo "<p style='color:red;'><b>NO</b></p>";
                                                        } else {
                                                            echo "<p style='color:green;'><b>YES</b></p>";
                                                        }
                                                     ?></td>
                                                    <td><?php echo $rs['rate']; ?></td>
                                                    <td><?php 
                                                        if($rs['status'] == '0'){
                                                            echo "<p style='color:green;'><b>ACTIVE</b></p>";
                                                        } else {
                                                            echo "<p style='color:red;'><b>INACTIVE</b></p>";
                                                        }
                                                     ?></td>
                                                    <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="6">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeleteTypeBill" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.EditType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var id = $row.data('id');
            var type = $row.data('type');
            var status = $row.data('status');
            var des = $row.data('des');
            var vat = $row.data('vat');
            var rate = $row.data('rate');

            $('#id').val('');
            $('#status').val('');
            $('#type').val('');
            $('#description').val('');
            $('#vat').val('');
            $('#rate').val('');

            $('#id').val(id);
            $('#status').val(status);
            $('#type').val(type);
            $('#description').val(des);
            $('#vat').val(vat);
            $('#rate').val(rate);
        });

         $('.RemoveType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var id = $row.data('id');
      
            $('#remove_id').val('');
            $('#remove_id').val(id);
            $('#myModal').modal('show');
        });

        $('.saveType').click(function(){
            
            var id = $('#id').val();
            var type = $('#type').val();
            var status = $('#status').val();
            var description = $('#description').val();
            var vat = $('#vat').val();
            var rate = $('#rate').val();

            $.ajax({
                url:'<?php echo base_url(); ?>Setting/SaveTypeBill',
                method:'POST',
                data:{ id:id, type:type, status:status , rate:rate , vat:vat , description:description}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-type').html('');
                    $('#validate-type').append('Please Enter Type Billing.');
                } 

                if(o.msg == 200){
                    $('#validate-type').html('');
                    $('#validate-type').append('Type Billing Already.');
                } 

                if(o.msg == 300){
                    $('#validate-description').html('');
                    $('#validate-description').append('Please Enter description Billing.');
                } 


                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        }); 
        

        $('.clearType').click(function(){
            
           $('#id').val('');

            $('#type').val('');
            $('#description').val('');


        }); 

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>