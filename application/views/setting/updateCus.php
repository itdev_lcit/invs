<style type="text/css">
    .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo base_url(); ?>public/img/loading.gif') 50% 50% no-repeat rgb(249,249,249);
}
</style> 
<div id="loader"></div>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Customer</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">


               <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button class="btn btn-success c-cus"><i class="fa fa-plus-circle"></i>&nbsp;Create (1-STOP)</button>
                            <button class="btn btn-warning s-cus"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Sync LCIT</button>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <!--<div class="col-md-2" align="left">
                                        <label for="sel1">Sort</label>
                                          <select class="form-control" name="sort" id="sort">
                                            <option value="GROUP" <?php if($search == 'GROUP') echo 'selected'; ?>>All</option>
                                            <option value="OSL" <?php if($search == 'OSL') echo 'selected'; ?>>1-STOP</option>                    
                                            <option value="LCIT" <?php if($search == 'LCIT') echo 'selected'; ?>>LCIT</option>
                                          </select>
                                        <br>
                                    </div>-->
                                     <br>
                                    <div align="right">
                                        <form id="f_cus" class="form-inline" action="<?php echo site_url() . 'Setting/UpdateCustomer'; ?>" method="POST">
                                            <input class="form-control" type="text" name="search" id="search" value="<?php if($search != 'GROUP' and $search != 'OSL' and $search != 'LCIT'){ echo $search; }  ?>" placeholder="Search Customer">
                                            <button class="btn btn-info">Search</button>
                                            &nbsp;&nbsp;
                                        </form>
                                        
                                    </div>  
                                    <br> 
                                    <table class="table table-striped table-bordered table-hover" style="font-size: 10px;">
                                    <thead>
                                        <tr>
                                            <th >Group</th>
                                            <th >A/C No.</th>
                                            <th style="text-align: center;" width="5%">Code</th>
                                            <th >Branch</th>
                                            <th >Name</th>
                                            <th >Address</th>
                                            <th ></th>
                                        </tr>
                                    </thead>
                                    <tbody id="payer">
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {?>

                                            <tr class="r-cus" data-CusId="<?php echo $rs_normal->id;?>" data-Cus="<?php echo $rs_normal->custom_id;?>" data-Code="<?php echo $rs_normal->customer_code;?>" data-ar_company="<?php echo $rs_normal->ar_company;?>" data-customer_name="<?php echo $rs_normal->customer_name;?>" data-customer_address="<?php echo $rs_normal->customer_address;?>" data-customer_address2="<?php echo $rs_normal->customer_address2;?>" data-customer_address3="<?php echo $rs_normal->customer_address3;?>" data-customer_post="<?php echo $rs_normal->customer_post;?>" data-credit_term="<?php echo $rs_normal->credit_term;?>"data-customer_branch="<?php echo $rs_normal->customer_branch;?>"data-tax_reg_no="<?php echo $rs_normal->tax_reg_no;?>"data-country="<?php echo $rs_normal->country;?>">
                                                <td><?php 
                                                    if($rs_normal->TypeCus == 'OSL'){
                                                        echo 'OSL';
                                                    } else {
                                                        echo 'LCIT';
                                                    }
                                                ?></td>
                                                <td><?php echo $rs_normal->ar_company; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->customer_code; ?></td>
                                                <td><?php echo $rs_normal->customer_branch; ?></td>
                                                <td><?php echo $rs_normal->customer_name; ?></td>
                                                <td>
                                                    <?php echo $rs_normal->customer_address; ?>
                                                    <?php echo $rs_normal->customer_address2; ?>
                                                    <?php echo $rs_normal->customer_address3; ?>
                                                    <?php echo $rs_normal->customer_post; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($rs_normal->TypeCus == 'OSL'){
                                                            echo '<button type="button" class="btn btn-warning re-cus" title="Revised Data"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
                                                        } 
                                                    ?>

                                                    <?php if($rs_normal->TypeCus != 'OSL'){ ?>
                                                        <button type="button" class="btn btn-info up-cus" title="Sync LCIT to Updated Data"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                                    <?php } ?>
                                                    
                                                        <button type="button" class="btn btn-danger del-cus" title="Removed Data"><i class="fa fa-trash" aria-hidden="true"></i></button>

                                                </td>
                                            </tr>

                                            <?php $i++; } 
                                            } else {
                                            ?>
                                                <tr>
                                                    <td colspan="8" style="text-align: center;">-No data available-</td>
                                                </tr>
                                            <?php } ?>
                                    </tbody>
                                </table>
                                 <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

</div>

<!-- Modal -->
<div id="insert_tax" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Input Tax ID, IF Customer live in Thailand input tax id 13 number , Else Input 0*</p>
          </div>

                <div class="form-group" align="center">
                    <label>TAX ID</label>

                    <input type="text" name="in_tax" id="in_tax" value="" class="form-control" style="width: 550px;" >
                    <font color="red"><p id="msg-error-tax"></p></font>
                    <input type="hidden" name="in_customer_name" id="in_customer_name" value="">
                    <input type="hidden" name="in_customer_address" id="in_customer_address" value="">
                    <input type="hidden" name="in_id_comp" id="in_id_comp" value="">
                    <input type="hidden" name="in_payer_code" id="in_payer_code" value="">
                    <input type="hidden" name="in_payer_tel" id="in_payer_tel" value="">
                </div>  
                <div class="form-group" align="center">
                    <label>Country</label>
                    <input type="text" name="in_country" id="in_country" value="Thailand" class="form-control" style="width: 550px;" placeholder="Auto Complete Country">
                    <font color="red"><p id="msg-error-country"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-tax">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

        </div>

  </div>
</div>


<!-- Modal -->
<div id="insert_cus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div align="center">
                <h3>Customer Profiles (1-STOP)</h3>
            </div>
            <div class="form-group">
                <label>A/C No.</label>
                <input type="hidden" name="customers_id" id="customers_id" value="" class="form-control" style="width: 550px;" >
                <input type="text" name="ar_company" id="ar_company" value="" class="form-control" style="width: 550px;" >
                <font color="red"><p id="msg-error-ar_company"></p></font>
            </div>
            <div class="form-group">
                <label>Credit Term</label>
                <input type="text" name="credit_term" id="credit_term" value="" class="form-control" style="width: 550px;" >
            </div> 
            <div class="form-group">
                <label>Code</label>
                <input type="text" name="customer_code" id="customer_code" value="" class="form-control" style="width: 550px;" >
                <font color="red"><p id="msg-error-customer_code"></p></font>
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="customer_name" id="customer_name" value="" class="form-control" style="width: 550px;" >
                <font color="red"><p id="msg-error-customer_name"></p></font>
            </div> 
            <div class="form-group">
                <label>Tax ID</label>
                <input type="text" name="tax_reg_no" id="tax_reg_no" value="" class="form-control" style="width: 550px;" >
                <font color="red"><p id="msg-error-tax_reg_no"></p></font>
            </div> 
            <div class="form-group">
                <label>Branch</label>
                <input type="text" name="customer_branch" id="customer_branch" value="" class="form-control" style="width: 550px;" >
            </div> 
            <div class="form-group">
                <label>Address - 1</label>
                <input type="text" name="customer_address" id="customer_address" value="" class="form-control" style="width: 550px;" >
                <font color="red"><p id="msg-error-customer_address"></p></font>
            </div>
            <div class="form-group">
                <label>Address - 2</label>
                <input type="text" name="customer_address2" id="customer_address2" value="" class="form-control" style="width: 550px;" >
            </div>
            <div class="form-group">
                <label>Address - 3</label>
                <input type="text" name="customer_address3" id="customer_address3" value="" class="form-control" style="width: 550px;" >
            </div>
             <div class="form-group">
                <label>Post Code</label>
                <input type="text" name="customer_post" id="customer_post" value="" class="form-control" style="width: 550px;" >
            </div>
             <div class="form-group">
                <label>Country</label>
                <input type="text" name="country" id="country" value="TH" class="form-control" style="width: 550px;" >
                
            </div>
            <!--<div class="form-group">
                <label>Contact Name</label>
                <input type="text" name="contact_person" id="contact_person" value="" class="form-control" style="width: 550px;" >
            </div>
             <div class="form-group">
                <label>Contact Email</label>
                <input type="text" name="customer_mail" id="customer_mail" value="" class="form-control" style="width: 550px;" >
            </div>
             <div class="form-group">
                <label>Contact Telphone No.</label>
                <input type="text" name="telephone_no" id="telephone_no" value="" class="form-control" style="width: 550px;" >
            </div>-->

            <div class="form-group" align="right">
                <button type="button" class="btn btn-outline btn-danger ClearCus" id="ClearCus">Cancle</button>
                <button type="button" class="btn btn-outline btn-success saveCus" id="saveCus">Save</button>
           </div>
          </div>    
    </div>

  </div>
</div>


<!-- Modal -->
<div id="c_del" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to delete ?</p>
          </div>
                <div class="modal-footer">
                    <input type="hidden" name="cusid" id="cusid">
                    <button type="button" class="btn btn-success cf-del">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>

        </div>

  </div>
</div>

<!-- Modal -->
<div id="sync_cus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div align="center">
                <h3>Sync Customer Profiles (LCIT)</h3>
            </div>
            <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-6" align="right">
                                        <input type="text" id="payer_s" name="payer_s" class="form-control" placeholder="Search Payer name"> 
                                        <br>
                                    </div>

                                    <table class="table table-striped table-bordered table-hover" style="font-size: 10px;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">Code</th>
                                            <th  width="40%">Name</th>
                                            <th >A/C No.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="t_payer_s" style="font-size: 10px;">
                                        <tr>
                                            <td colspan="5" style="text-align: center;">Please Search Customer</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
            <div class="form-group" align="right">
                <button type="button" class="btn btn-outline btn-danger sClear" id="ClearCus">Cancle</button>
           </div>
          </div>    
    </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/lib/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#rate_order').html('0');
$('#total_order').html('0');

$('#status_split').val('0');
$("#numpage_split").hide();

$(".type_split").bind("change",function(){
   var value = $(this).val();
   if(value == 1){

        $("#numpage_split").show();
        $('#status_split').val('1');

   } else {
        $("#numpage_split").hide();
        $('#status_split').val('0');
   }
});


var states = [
    'Aruba','Antigua and Barbuda','United Arab Emirates','Afghanistan','Algeria','Azerbaijan','Albania','Armenia','Andorra','Angola','American Samoa','Argentina','Australia','Ashmore and Cartier Islands','Austria','Anguilla','Åland Islands'
    ,'Antarctica','Bahrain','Barbados','Botswana','Bermuda','Belgium','Bahamas, The','Bangladesh','Belize','Bosnia and Herzegovina','Bolivia','Myanmar','Benin','Belarus','Solomon Islands','Navassa Island','Brazil','Bassas da India','Bhutan'
    ,'Bulgaria','Bouvet Island','Brunei','Burundi','Canada','Cambodia','Chad','Sri Lanka','Congo, Republic of the','Congo, Democratic Republic of the','China','Chile','Cayman Islands','Cocos (Keeling) Islands','Cameroon','Comoros','Colombia'
    ,'Northern Mariana Islands','Coral Sea Islands','Costa Rica','Central African Republic','Cuba','Cape Verde','Cook Islands','Cyprus','Denmark','Djibouti','Dominica','Jarvis Island','Dominican Republic','Dhekelia Sovereign Base Area'
    ,'Ecuador','Egypt','Ireland','Equatorial Guinea','Estonia','Eritrea','El Salvador','Ethiopia','Europa Island','Czech Republic','French Guiana','Finland','Fiji','Falkland Islands (Islas Malvinas)','Micronesia, Federated States of'
    ,'Faroe Islands','French Polynesia','Baker Island','France','French Southern and Antarctic Lands','Gambia, The','Gabon','Georgia','Ghana','Gibraltar','Grenada','Guernsey','Greenland','Germany','Glorioso Islands','Guadeloupe','Guam'
    ,'Greece','Guatemala','Guinea','Guyana','Gaza Strip','Haiti','Hong Kong','Heard Island and McDonald Islands','Honduras','Howland Island','Croatia','Hungary','Iceland','Indonesia','Isle of Man','India','British Indian Ocean Territory'
    ,'Clipperton Island','Iran','Israel','Italy','Cote d Ivoire','Iraq','Japan','Jersey','Jamaica','Jan Mayen','Jordan','Johnston Atoll','Juan de Nova Island','Kenya','Kyrgyzstan','Korea, North','Kingman Reef','Kiribati','KoreaSouth'
    ,'Christmas Island','Kuwait','Kosovo','Kazakhstan','Laos','Lebanon','Latvia','Lithuania','Liberia','Slovakia','Palmyra Atoll','Liechtenstein','Lesotho','Luxembourg','Libyan Arab','Madagascar','Martinique','Macau','Moldova, Republic of'
    ,'Mayotte','Mongolia','Montserrat','Malawi','Montenegro','The Former Yugoslav Republic of Macedonia','Mali','Monaco','Morocco','Mauritius','Midway Islands','Mauritania','Malta','Oman','Maldives','Mexico','Malaysia','Mozambique'
    ,'New Caledonia','Niue','Norfolk Island','Niger','Vanuatu','Nigeria','Netherlands','No Mans Land','Norway','Nepal','Nauru','Suriname','Netherlands Antilles','Nicaragua','New Zealand','Paraguay','Pitcairn Islands','Peru','Paracel Islands'
    ,'Spratly Islands','Pakistan','Poland','Panama','Portugal','Papua New Guinea','Palau','Guinea-Bissau','Qatar','Reunion','Serbia','Marshall Islands','Saint Martin','Romania','Philippines','Puerto Rico','Russia','Rwanda','Saudi Arabia'
    ,'Saint Pierre and Miquelon','Saint Kitts and Nevis','Seychelles','South Africa','Senegal','Saint Helena','Slovenia','Sierra Leone','San Marino','Singapore','Somalia','Spain','Saint Lucia','Sudan','Svalbard','Sweden'
    ,'South Georgia and the Islands','Syrian Arab Republic','Switzerland','Trinidad and Tobago','Tromelin Island','Thailand','Tajikistan','Turks and Caicos Islands','Tokelau','Tonga','Togo','Sao Tome and Principe','Tunisia'
    ,'East Timor','Turkey','Tuvalu','Taiwan','Turkmenistan','Tanzania, United Republic of','Uganda','United Kingdom','Ukraine','United States','Burkina Faso','Uruguay','Uzbekistan','Saint Vincent and the Grenadines','Venezuela'
    ,'British Virgin Islands','Vietnam','Virgin Islands (US)','Holy See (Vatican City)','Namibia','West Bank','Wallis and Futuna','Western Sahara','Wake Island','Samoa','Swaziland','Serbia and Montenegro','Yemen','Zambia','Zimbabwe'
];

$('#in_country').autocomplete({
    source:[states]
});


$('#in_country').on('input', function(e){
    var in_country = $(this).val();
    if(in_country != null){
        $('#msg-error-country').html('');
        $.ajax({
            url:'<?php echo site_url(); ?>Setting/CheckCountry',
            method:'POST',
            data:{ in_country:in_country }
        }).done(function(data){
              
        })
   
    } else {
         $('#msg-error-country').html('');
    }
});



$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();

            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id  = $row.data('ar_company_id');


           $.ajax({
                url:'<?php echo site_url(); ?>Setting/SavePayer',
                method:'POST',
                data:{ 
                    id_customer:id_customer ,
                    customer_address:customer_address,
                    customer_name:customer_name,
                    customer_code:customer_code,
                    telephone_an:telephone_an,
                    tax_reg_no:tax_reg_no,
                    ar_company_id:ar_company_id
                }
            }).done(function(data){

                $.ajax({
                    url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
                    method : 'POST',
                    data : {comp_code:customer_code}
                }).done(function(data){

                    $.ajax({
                        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
                        method : 'POST',
                        data : {comp_id:id_customer}
                    }).done(function(data){
                        $.ajax({
                                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                                method : 'POST',
                                data : {comp_id:id_customer}
                        }).done(function(data){
                            setTimeout(function() {
                                console.log('Pull Success');
                                $('.loader').fadeOut("slow");
                                alert('Customers Up-To-Date.');
                                location.reload();
                            }, 3500);

                        })
                    })

                })


            })

            $('#customer_name').html(customer_name);
            $('#customer_address').html(customer_address);  
            $('#id_comp').val(id_customer); 
            $('#payer_address').val(customer_address);
            $('#payer_name').val(customer_name); 
            $('#payer_code').val(customer_code);    
            $('#payer_tel').val(telephone_an);  
            $('#payer_tax').val(tax_reg_no);    



        });



$('#type_dr').change(function(){
    var id_type = $(this).val();
    var $row = $(this).parents('tr.r-dr');
    var size_con = $row.data('size_con');
    if(id_type == '0'){
         $('#rate_order').html('0');
         $('#total_order').html('0');
    }
            $.ajax({
                url:'<?php echo site_url(); ?>Setting/getRate',
                method:'POST',
                data:{ 
                    id_type:id_type,
                    size_con:size_con
                }
            }).done(function(data){
                var o = JSON.parse(data);
                var cur_rate = o.rate_con;
                var n_rate = parseInt(cur_rate);
                $('#rate_order').html(n_rate.toLocaleString('en'));
                $('#rate_n').val(n_rate);


                if(id_type){
                    var value_con = parseInt($('#value_split').val());
                    var rate_price = parseInt($('#rate_n').val());
                    var book_con = parseInt($('#book_con').val());

                    if(value_con > book_con){
                        alert('Too Much Value, Please Try Again.');
                        $('#value_split').val('');
                        $('#total_order').html('0');
                    } else {
                        var total_rate = value_con * rate_price;

                        if(total_rate > 0){
                            $('#total_order').html(total_rate.toLocaleString('en'));
                        } else {
                             $('#total_order').html('0');
                        }
                        
                    }
                } else {
                    $('#total_order').html('-');
                    $('#value_split').val('');
                    $('#total_order').html('-');
                }
            })
});


$("div").off("click", ".save-tax");
        $("div").on("click", ".save-tax", function(e) {
            e.preventDefault();
            
            var in_tax = $('#in_tax').val();
            var in_country = $('#in_country').val();
            if(in_tax == ''){
                $('#msg-error-tax').html('Please Input Tax ID.');
                console.log('null');
            } else {
                console.log('insert tax');
                $('#insert_tax').modal('hide');
                    var id_customer = $('#in_id_comp').val();
                    var customer_address = $('#in_customer_address').val();
                    var customer_name = $('#in_customer_name').val();
                    var customer_code = $('#in_payer_code').val();
                    var telephone_an  = $('#in_payer_tel').val();
                    var tax_reg_no  = in_tax;
                    var country = in_country;

                    $.ajax({
                            url:'<?php echo site_url(); ?>Setting/SavePayerTax',
                            method:'POST',
                            data:{ 
                                id_customer:id_customer ,
                                customer_address:customer_address,
                                customer_name:customer_name,
                                customer_code:customer_code,
                                telephone_an:telephone_an,
                                tax_reg_no:tax_reg_no,
                                country:country
                            }
                    }).done(function(data){

                    })

                        $('#customer_name').html(customer_name);
                        $('#customer_address').html(customer_address);  
                        $('#id_comp').val(id_customer); 
                        $('#payer_address').val(customer_address);
                        $('#payer_name').val(customer_name); 
                        $('#payer_code').val(customer_code);    
                        $('#payer_tel').val(telephone_an);  
                        $('#payer_tax').val(tax_reg_no);   
            }

          
});


$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_address2 = $row.data('customer_address2');
            var customer_address3 = $row.data('customer_address3');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id  = $row.data('ar_company_id');


            /*$.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            $.ajax({
                    url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
                    method : 'POST',
                    data : {comp_code:customer_code}
            })*/

            if(tax_reg_no != null){
                $.ajax({
                    url:'<?php echo site_url(); ?>Setting/SavePayer',
                    method:'POST',
                    data:{ 
                        id_customer:id_customer ,
                        customer_address:customer_address,
                        customer_name:customer_name,
                        customer_code:customer_code,
                        telephone_an:telephone_an,
                        tax_reg_no:tax_reg_no,
                        ar_company_id:ar_company_id
                    }
                }).done(function(data){

                    $.ajax({
                        url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
                        method : 'POST',
                        data : {comp_code:customer_code}
                    }).done(function(data){

                        $.ajax({
                            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
                            method : 'POST',
                            data : {comp_id:id_customer}
                        }).done(function(data){
                            $.ajax({
                                    url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                                    method : 'POST',
                                    data : {comp_id:id_customer}
                            }).done(function(data){
                                setTimeout(function() {
                                    console.log('Pull Success');
                                    $('.loader').fadeOut("slow");
                                    alert('Customers Up-To-Date.');
                                    location.reload();
                                }, 3500);

                            })
                        })

                    })


                })

                $('#customer_name').html(customer_name);
                $('#customer_address').html(customer_address+customer_address2+customer_address3);  
                $('#id_comp').val(id_customer); 
                $('#payer_address').val(customer_address);
                $('#payer_name').val(customer_name); 
                $('#payer_code').val(customer_code);    
                $('#payer_tel').val(telephone_an);  
                $('#payer_tax').val(tax_reg_no);   


            } else {

                $('#insert_tax').modal('show');
                $('#in_customer_name').val(customer_name); 
                $('#in_customer_address').val(customer_address+customer_address2+customer_address3);
                $('#in_id_comp').val(id_customer);   
                $('#in_payer_code').val(customer_code);  
                $('#in_payer_tel').val(telephone_an); 
            }

            $('.printDr').prop('disabled',false);   
            $('.can-dr').prop('disabled',false);

                alert('Sync Customer Profiles Completed.');
                location.reload();
});

$('#payer_nm').on('input', function(e){

    $('#payer').html('');
    $('.printDr').prop('disabled',true);
    $('.can-dr').prop('disabled',true);
    
    console.log('sync Zodiac');
    var payer = $('#payer_nm').val();
    var loading = '<tr align="center"><td colspan="6"><img src="<?php echo base_url(); ?>public/img/loading.gif" width="10%"></td></tr>';

    $('#payer').append(loading);

    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
        method : 'POST',
        data : {payer:payer}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;


        if(o.length == '0'){
             console.log('Not Found Record');
            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var COMPANY_ID = o[i]['COMPANY_ID'];
            var OPS_COMPANY_ID = o[i]['OPS_COMPANY_ID'];
            var ADDRESS = o[i]['STREET_ADDRESS1_DS'];
            var ADDRESS2 = o[i]['STREET_ADDRESS2_DS'];
            var ADDRESS3 = o[i]['STREET_ADDRESS3_DS'];
            var COMPANY_NM = o[i]['COMPANY_NM'];
            var TAX_REG_NO = o[i]['TAX_REG_NO'];
            var TELEPHONE_AN = o[i]['TELEPHONE_AN'];

            payer += '<tr class="r-payer" data-TELEPHONE_AN="'+TELEPHONE_AN+'" data-TAX_REG_NO="'+TAX_REG_NO+'" data-id_customer="'+COMPANY_ID+'"  data-customer_address3="'+ADDRESS3+'" data-customer_address2="'+ADDRESS2+'" data-customer_address="'+ADDRESS+'" data-customer_name="'+COMPANY_NM+'" data-customer_code="'+OPS_COMPANY_ID+'">';

            payer += '<td style="text-align:center; font-size:12px;">';
            payer += OPS_COMPANY_ID;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += COMPANY_NM;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += ADDRESS + ADDRESS2 + ADDRESS3;
            payer += '</td>';


            payer += '<td style="font-size:12px;">';
            payer += TAX_REG_NO;
            payer += '</td>';

            payer += '</tr>';


        }

         alert('Updated');


        }

        
        $('#payer').html('');
        $('#payer').append(payer);

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
            method : 'POST',
            data : {comp_code:OPS_COMPANY_ID}
        })

        $('.printDr').prop('disabled',false);
        $('.can-dr').prop('disabled',false);
    })
});

$("table").off("click", ".up-cus");
    $("table").on("click", ".up-cus", function(e) {

    var $row = $(this).parents('tr.r-cus');
    var comp_id =  $row.data('cus');
    var comp_code =  $row.data('code');

    var r = confirm("Updated This Data ?");

    if (r == true) {
        $('#loader').addClass('loader');
        $('.loader').fadeIn("slow");

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
            method : 'POST',
            data : {comp_code:comp_code}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
            method : 'POST',
            data : {comp_id:comp_id}
        })

        $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                method : 'POST',
                data : {comp_id:comp_id}
        }).done(function(data){

            console.log('Pull Success');
            $('.loader').fadeOut("slow");
            alert('Customers Up-To-Date.');
            location.reload();

        })
    }

     

});


    $('.c-cus').click(function(){

        $('#customers_id').val('');
        $('#ar_company').val('');
        $('#credit_term').val('');
        $('#customer_code').val('');
        $('#customer_branch').val('');
        $('#customer_name').val('');
        $('#customer_address').val('');
        $('#customer_address2').val('');
        $('#customer_address3').val('');
        $('#customer_post').val('');
        $('#tax_reg_no').val('');
        $('#country').val('');

        $('#insert_cus').modal('show');
          
    });


    $('.ClearCus').click(function(){

        $('#customers_id').val('');
        $('#ar_company').val('');
        $('#credit_term').val('');
        $('#customer_code').val('');
        $('#customer_branch').val('');
        $('#customer_name').val('');
        $('#customer_address').val('');
        $('#customer_address2').val('');
        $('#customer_address3').val('');
        $('#customer_post').val('');
        $('#tax_reg_no').val('');
        $('#country').val('');

        $('#insert_cus').modal('hide');
          
    });


    $('.re-cus').click(function(){

        $('#customers_id').val('');
        $('#ar_company').val('');
        $('#credit_term').val('');
        $('#customer_code').val('');
        $('#customer_branch').val('');
        $('#customer_name').val('');
        $('#customer_address').val('');
        $('#customer_address2').val('');
        $('#customer_address3').val('');
        $('#customer_post').val('');
        $('#tax_reg_no').val('');
        $('#country').val('');

        var $row = $(this).parents('tr.r-cus');
        var cusid = $row.data('cusid');
        var ar_company = $row.data('ar_company');
        var credit_term = $row.data('credit_term');
        var customer_code = $row.data('code');
        var customer_branch = $row.data('customer_branch');
        var customer_name = $row.data('customer_name');
        var customer_address = $row.data('customer_address');
        var customer_address2 = $row.data('customer_address2');
        var customer_address3 = $row.data('customer_address3');
        var customer_post = $row.data('customer_post');
        var tax_reg_no = $row.data('tax_reg_no');
        var country = $row.data('country');


        $('#customers_id').val(cusid);
        $('#ar_company').val(ar_company);
        $('#credit_term').val(credit_term);
        $('#customer_code').val(customer_code);
        $('#customer_branch').val(customer_branch);
        $('#customer_name').val(customer_name);
        $('#customer_address').val(customer_address);
        $('#customer_address2').val(customer_address2);
        $('#customer_address3').val(customer_address3);
        $('#customer_post').val(customer_post);
        $('#tax_reg_no').val(tax_reg_no);
        $('#country').val(country);

        $('#insert_cus').modal('show');
          
    });


    $('.ClearCus').click(function(){

        $('#insert_cus').modal('hide');
          
    });



    $('#saveCus').click(function(){

        var ar_company = $('#ar_company').val();
        var credit_term = $('#credit_term').val();
        var customer_code = $('#customer_code').val();
        var customer_name = $('#customer_name').val();
        var tax_reg_no = $('#tax_reg_no').val();
        var customer_branch = $('#customer_branch').val();
        var customer_address = $('#customer_address').val();
        var customer_address2 = $('#customer_address2').val();
        var customer_address3 = $('#customer_address3').val();
        var customer_post = $('#customer_post').val();
        var country = $('#country').val();
        var contact_person = $('#contact_person').val();
        var customer_mail = $('#customer_mail').val();
        var telephone_no = $('#telephone_no').val();
        var customers_id = $('#customers_id').val();

        if(customer_code == ''){
            $('#msg-error-customer_code').html('Please Specify Customer Code.');
        } 

        else if(customer_name == ''){
            $('#msg-error-customer_name').html('Please Specify Customer Name.');
        }

        else if(tax_reg_no == ''){
            $('#msg-error-tax_reg_no').html('Please Specify Tax Id.');
        }

        else if(customer_address == ''){
            $('#msg-error-customer_address').html('Please Specify Address.');
        }

        else {

            $.ajax({
                    url:'<?php echo site_url(); ?>Setting/CreatedCustomers',
                    method:'POST',
                    data:{ 
                        ar_company:ar_company,
                        credit_term:credit_term,
                        customer_code:customer_code,
                        customer_name:customer_name,
                        tax_reg_no:tax_reg_no,
                        customer_branch:customer_branch,
                        customer_address:customer_address,
                        customer_address2:customer_address2,
                        customer_address3:customer_address3,
                        customer_post:customer_post,
                        country:country,
                        contact_person:contact_person,
                        customer_mail:customer_mail,
                        telephone_no:telephone_no,
                        customers_id:customers_id
                    }

            }).done(function(data){

                var o = JSON.parse(data);
                var i = 0;

                if(o.msg == '100'){
                    $('#msg-error-customer_code').html('This customers code already exists.');
                } else {
                    location.reload();
                }

            })
        }
          
    });


    $('#sort').on('change', function (e) {

        var sort = $('#sort').val();

        $('#search').val(sort);
        $('#f_cus').submit();

    });

    $('.del-cus').click(function(){

        var $row = $(this).parents('tr.r-cus');
        var cusid =  $row.data('cusid');


        $('#cusid').val(cusid);

        $('#c_del').modal('show');
          
    });


    $('.cf-del').click(function(){

        var cusid =  $('#cusid').val();

        $('#c_del').modal('show');


        $.ajax({
                    url:'<?php echo site_url(); ?>Setting/DeletedCustomers',
                    method:'POST',
                    data:{ 
                        cusid:cusid
                    }

            }).done(function(data){

                alert('Success');
                location.reload();

            })
          
    });


     $('.s-cus').click(function(){

        $('#sync_cus').modal('show');
          
    });


    $('.sClear').click(function(){

        $('#sync_cus').modal('hide');
          
    });

    $('#payer_s').on('input', function(e){

    
    console.log('sync Zodiac');
    var payer = $('#payer_s').val();


    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
        method : 'POST',
        data : {payer:payer}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;


        if(o.length == '0'){
             console.log('Not Found Record');
            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var COMPANY_ID = o[i]['COMPANY_ID'];
            var OPS_COMPANY_ID = o[i]['OPS_COMPANY_ID'];
            var ADDRESS = o[i]['STREET_ADDRESS1_DS'];
            var ADDRESS2 = o[i]['STREET_ADDRESS2_DS'];
            var ADDRESS3 = o[i]['STREET_ADDRESS3_DS'];
            var COMPANY_NM = o[i]['COMPANY_NM'];
            var TAX_REG_NO = o[i]['TAX_REG_NO'];
            var TELEPHONE_AN = o[i]['TELEPHONE_AN'];
            var AR_COMPANY_ID = o[i]['AR_COMPANY_ID'];

            payer += '<tr class="r-payer" data-ar_company_id="'+AR_COMPANY_ID+'" data-TELEPHONE_AN="'+TELEPHONE_AN+'" data-TAX_REG_NO="'+TAX_REG_NO+'" data-id_customer="'+COMPANY_ID+'"  data-customer_address3="'+ADDRESS3+'" data-customer_address2="'+ADDRESS2+'" data-customer_address="'+ADDRESS+'" data-customer_name="'+COMPANY_NM+'" data-customer_code="'+OPS_COMPANY_ID+'" style="font-size: 10px;">';

            payer += '<td style="text-align:left; font-size:10px;" width="20%">';
            payer += OPS_COMPANY_ID;
            payer += '</td>';

            payer += '<td style="font-size:10px;"  width="45%">';
            payer += COMPANY_NM;
            payer += '</td>';


            payer += '<td style="font-size:10px;"  width="15%">';
            payer += AR_COMPANY_ID;
            payer += '</td>';

            payer += '<td>';
            payer += '<button class="btn btn-info btn-xs cus-data">Sync</button>'
            payer += '</td>';

            payer += '</tr>';

        }

        }

        
        $('#t_payer_s').html('');
        $('#t_payer_s').append(payer);

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/UpdatedCus',
            method : 'POST',
            data : {comp_code:OPS_COMPANY_ID}
        })

    })
});


});
</script>