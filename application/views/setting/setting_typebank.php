<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Type Bank
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Type</label>
                                                <input class="form-control" id="id" name="id" required="true" value="" type="hidden">                                           
                                                <input class="form-control" id="type" name="type" required="true" value="">
                                                <p id="validate-type" style="color: red;"></p>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="status" name="status">
                                                    <option value="0">ACTIVE</option>
                                                    <option value="1">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveType" id="saveSize">Save</button>
                                                <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-8">
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($setting_bank){
                                            $i = 1;
                                            foreach ($setting_bank as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type" data-id="<?php echo $rs['sb_id']; ?>"  data-type="<?php echo $rs['sb_type']; ?>" data-status="<?php echo $rs['status']; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $rs['sb_type']; ?></td>
                                                    <td><?php 
                                                        if($rs['status'] == '0'){
                                                            echo "<p style='color:green;'><b>ACTIVE</b></p>";
                                                        } else {
                                                            echo "<p style='color:red;'><b>INACTIVE</b></p>";
                                                        }
                                                     ?></td>
                                                    <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeleteTypeBank" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.EditType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var id = $row.data('id');
            var type = $row.data('type');
            var status = $row.data('status');

            $('#id').val('');
            $('#status').val('');
            $('#type').val('');
            $('#description').val('');

            $('#id').val(id);
            $('#status').val(status);
            $('#type').val(type);
            $('#description').val(des);
        });

         $('.RemoveType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var id = $row.data('id');
      
            $('#remove_id').val('');
            $('#remove_id').val(id);
            $('#myModal').modal('show');
        });

        $('.saveType').click(function(){
            
            var id = $('#id').val();
            var sb_type = $('#type').val();
            var status = $('#status').val();

            $.ajax({
                url:'<?php echo base_url(); ?>Setting/SaveTypeBank',
                method:'POST',
                data:{ id:id, sb_type:sb_type, status:status }
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-type').html('');
                    $('#validate-type').append('Please Enter Type Bank.');
                } 

                if(o.msg == 200){
                    $('#validate-type').html('');
                    $('#validate-type').append('Type Bank Already.');
                } 


                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        }); 
        

        $('.clearType').click(function(){
            
           $('#id').val('');

            $('#type').val('');
            $('#description').val('');


        }); 

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>