<style type="text/css">
    .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo base_url(); ?>public/img/loading.gif') 50% 50% no-repeat rgb(249,249,249);
}
</style> 
<div id="loader"></div>
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Customer <button class="btn btn-success btn-xs update-customer" style="float: right;"><i class="fa fa-database"></i>&nbsp;Update Data Customer</button>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-striped table-bordered table-hover"  id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Updated</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($customer){
                                            $i=1;
	                                    	foreach ($customer as $rs) { 
	                                    		
	                                    	?>

		                                         <tr>
                                                    <td><?php echo $i; ?></td>
		                                            <td><?php echo $rs['customer_code']; ?></td>
                                                    <td><?php echo $rs['customer_name']; ?></td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
		                                        </tr>

                                        <?php

		                                  $i++;  } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.update-customer').click(function(){

            var r = confirm("Start Sync Data ?");
            if (r == true) {
                 $('#loader').addClass('loader');

                console.log('Start Sync Data');

                var activeAjaxConnections = 0;
                 $('.loader').fadeIn("slow");
              ///////////////////////////////Get Customer From Zodiac///////////////////////////////////////

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
                    method:'POST',
                }).done(function(data){

                    var o = JSON.parse(data);
                    var i = 0;
                    
                    for(i=0; i < o.length; i++){
                                        
                        var customer_code =  o[i]['CUSTOMER_CODE'];
                        var customer_name = o[i]['CUSTOMER_NAME'];
                        var customer_address = o[i]['CUSTOMER_ADDRESS'];
                        var telephone_no = o[i]['TELEPHONE_NO'];
                        var contact_person = o[i]['CONTACT_PERSON'];
                        var customer_mail = o[i]['CUSTOMER_MAIL_ID'];

                        $.ajax({
                            beforeSend: function(xhr) {
                                    activeAjaxConnections++;
                            },
                            url:'<?php echo site_url(); ?>Setting/GetCustomerZodiac',
                            method:'POST',
                            data:{ 
                                customer_code:customer_code,
                                customer_name:customer_name,
                                customer_address:customer_address,
                                telephone_no:telephone_no,
                                contact_person:contact_person,
                                customer_mail:customer_mail
                            }
                        }).done(function(data){
                            var o = JSON.parse(data);
                            activeAjaxConnections--;
                               if (0 == activeAjaxConnections) {
                                        console.log('Pull Success');
                                        $('.loader').fadeOut("slow");
                                        alert('Customers Up-To-Date.');
                                    }
                        })

                    }


                });

                 ///////////////////////////////Get Customer From Zodiac///////////////////////////////////////
            } else {
                
            }

        });

    });
</script>