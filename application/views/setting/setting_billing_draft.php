<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Prefix Billing Draft
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label></label>
                                            <input class="form-control" name="prefix" id="prefix" required="true">
                                            <p id="validate-prefix" style="color: red;"></p>

                                        </div>
                                        <button type="button" class="btn btn-outline btn-success savePrefix">Save</button>
                                    </form>
                                </div>
                                <div class="col-lg-8">
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Prefix Invoice</th>
                                            <th>Updated</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($prefix_invoice){
	                                    	foreach ($prefix_invoice as $rs) { 
	                                    		
	                                    	?>

		                                        <tr>

		                                            <td><?php echo $rs['prefix']."-".date('Y').'0000'; ?></td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>

		                                        </tr>

                                        <?php

		                                    } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.savePrefix').click(function(){
            
            var prefix = $('#prefix').val();


            $.ajax({
                url:'<?php echo base_url(); ?>Setting/SavePrefixDraft',
                method:'POST',
                data:{ prefix:prefix}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if (o.msg == 100){
                    $('#validate-prefix').html('');
                    $('#validate-prefix').append('Please Enter Prefix.');
                }

                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        });

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>