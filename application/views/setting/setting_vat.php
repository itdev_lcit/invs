<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Vat
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Code</label>
                                                <input class="form-control"  id="code_vat" name="code_vat" required="true">
                                                <p id="validate-code-vat" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Vat</label>
                                                <input class="form-control"  id="vat" name="vat" required="true">
                                                <input type="hidden" id="id" name="id" value="">
                                                <p id="validate-vat" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="status" name="status">
                                                    <option value="0">ACTIVE</option>
                                                    <option value="1">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" align="right">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveVat">Save</button>
                                         <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                </div>
                                <div class="col-lg-8">
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Code</th>
                                            <th>Vat</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($setting_vat){
	                                    	foreach ($setting_vat as $rs) { 
	                                    		
	                                    	?>

		                                        <tr class="r-vat" data-code="<?php echo $rs['code_vat']; ?>" data-id="<?php echo $rs['id']; ?>" data-vat="<?php echo $rs['vat']; ?>" data-status="<?php echo $rs['status']; ?>">
                                                    <td style="text-align: center;"><?php echo $rs['code_vat']; ?></td>
		                                            <td><?php echo $rs['vat'].'%'; ?></td>
		                                            <td><?php 
		                                            	if($rs['status'] == '0'){
		                                            		echo '<p style="color:green;"><b>ACTIVE</b></p>';
		                                            	}else {
		                                            		echo '<p style="color:gray;"><b>INACTIVE</b></p>';
		                                            	}
		                                            ?></td>
                                                    <td><?php echo date("j-F-Y H:i", strtotime($rs['created'])); ?></td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>

                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>

		                                        </tr>

                                        <?php

		                                    } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<!-- Modal -->
<div id="rmvModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-danger alert-dismissable">
                <p id="rmv_vat"></p>
                <p style="color: red;">*If this vat is used with any Tariff, It'll be effect with Tariff and unavaliable.</p>
                <input type="hidden" id="rmv_id" name="rmv_id" value="">
            </div>
          </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-success yes_rmv">YES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.saveVat').click(function(){
            
            var vat = $('#vat').val();
            var id = $('#id').val();
            var status = $('#status').val();
            var code_vat = $('#code_vat').val();

                $.ajax({
                    url:'<?php echo base_url(); ?>Setting/SaveVat',
                    method:'POST',
                    data:{ vat:vat, status:status, id:id, code_vat:code_vat}
                }).done(function(data){
                    
                    var o = JSON.parse(data);

                        $('#validate-code-vat').html('');
                        $('#validate-vat').html('');

                    if(o.msg == 100){
                        $('#validate-code-vat').append('Code is existing in the systems.');
                    } 

                    if(o.msg == 200){
                        $('#validate-code-vat').append('Please specify Code.');
                    } 

                    if(o.msg == 400){
                        $('#validate-vat').append('Only Numeric.');
                    } 

                    if(o.msg == 500){
                        $('#validate-vat').append('Please specify Vat.');
                    } 


                    if(o.msg == 'success'){
                        $('#success').modal('show');
                    }
                })


        });

        $('.btn_succsess').click(function() {
            window.location = '<?php echo site_url(); ?>Setting/Vat';
        });

        $('.EditType').click(function(){

            var $row = $(this).parents('tr.r-vat');
            var id = $row.data('id');
            var vat = $row.data('vat');
            var status = $row.data('status');
            var code_vat = $row.data('code');


            $('#id').val('');
            $('#vat').val('');
            $('#status').val('');
            $('#code_vat').val('');

            $('#id').val(id);
            $('#vat').val(vat);
            $('#status').val(status);
            $('#code_vat').val(code_vat);
        });

        $('.clearType').click(function(){
            
            $('#id').val('');
            $('#vat').val('');
            $('#code_vat').val('');

        }); 

        $('.yes_rmv').click(function(){
            
            $('#rmvModal').modal('hide');

            var id = $('#rmv_id').val();


            $.ajax({
                url:'<?php echo base_url(); ?>Setting/RmvVat',
                method:'POST',
                data:{id:id}
                }).done(function(data){
                    
                     $('#success').modal('show');

                })


        }); 

        $('.RemoveType').click(function(){

            var $row = $(this).parents('tr.r-vat');
            var id = $row.data('id');
            var vat = $row.data('vat');

            $('#rmv_id').val(id);
            $('#rmv_vat').html('');
            $('#rmv_vat').html('Do you want to remove this vat <b>"'+vat+'%</b>" ?');

            $('#rmvModal').modal('show');

        });

    });
</script>