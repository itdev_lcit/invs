<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>
<div class="page">
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h5 align="center"><b>One Stop Logistics Co., Ltd.</b></h5>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h6 align="center"><b>TAX INVOICE / RECEIPT</b></h6>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 15px;"><b>Original</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>Receipt No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $select_cus->prefix_invoice.$select_cus->invoice_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Payment Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("M j, Y", strtotime($select_cus->date_note)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Customer Branch</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_branch; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Tax Number</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?></p></td>
					</tr>


				</table>
				<br>
				<table class="table">
				    <thead>
				        <tr>
				        	<th style="font-size: 11px; text-align: left;"  width="10%">Item</th>
				            <th style="font-size: 11px; text-align: left;"  width="20%">Date</th>
				            <th style="font-size: 11px; text-align: left;" width="20%" >Reference</th>
				            <th style="font-size: 11px; text-align: right;"width="20%"></th>
				            <th style="font-size: 11px; text-align: right;"  width="20%">Amount</th>
				            <th style="font-size: 11px; text-align: right;"  width="10%">Currency</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    		 $i = 1;
				    					$amount = 0;
                                    	$vat = 0;
                                    	$total = 0;
                                    	$withhold = 0;
                                    	$netpay = 0;
                                        foreach ($order  as $rs) {
                                        $amount += $rs['Amount'];
                                        $vat += $rs['Vat'];
                                        $total += $rs['Total'];
                                        $withhold += $rs['WithHolding'];
                                        $netpay += $rs['NetPay'];

				    		?>
				    		<tr>
				    			<td style="font-size: 11px; text-align: left;"><?php echo $i; ?></td>
				    			<td style="font-size: 11px; text-align: left;"> <?php echo date("M j, Y", strtotime($rs['created'])); ?></td>
				    			<td style="font-size: 11px; text-align: left;"><?php echo $rs['InvNo']; ?></td>
				    			<td style="font-size: 11px; text-align: right;"> (THB <?php echo number_format($rs['Amount'], 2); ?>)</td>
				    			<td style="font-size: 11px; text-align: right;"><?php echo number_format($rs['Amount'], 2); ?></td>
				    			<td style="font-size: 11px; text-align: right;">THB</td>
				    		</tr>
				    	<?php $i++; }  ?>
				    		<tr>
								<td colspan="4" rowspan="5"></td>
								<td style="font-size: 11px;" align="right"><b>Net Amount</b></td>
								<td style="font-size: 11px;"  align="right"><?php echo number_format($amount, 2); ?></td>
							</tr> 
							<tr>
								<td style="font-size: 11px;" align="right"><b>Vat Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($vat, 2); ?></td>
							</tr> 
							<tr>
								<td style="font-size: 11px;" align="right"><b>Gross Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($amount+$vat, 2); ?></td>
							</tr>
							<tr>
								<td style="font-size: 11px;" align="right"><b>Withholding Tax</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($withhold, 2); ?></td>
							</tr>
							<tr>
								<td style="font-size: 11px;" align="right"><b>Payment Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format(($amount+$vat)-$withhold, 2); ?></td>
							</tr>
				    </tbody>
				</table>

				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 13px;">
				            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

				            	$num = (($amount+$vat));

				            	$rest = substr(number_format(($amount+$vat), 2), -2);

				            	$num = str_replace(array(',', ' '), '' , trim($num));
							    if(! $num) {
							        return false;
							    }
							    $num = (int) $num;
							    $words = array();
							    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
							        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
							    );
							    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
							    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
							        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
							        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
							    );
							    $num_length = strlen($num);
							    $levels = (int) (($num_length + 2) / 3);
							    $max_length = $levels * 3;
							    $num = substr('00' . $num, -$max_length);
							    $num_levels = str_split($num, 3);
							    for ($i = 0; $i < count($num_levels); $i++) {
							        $levels--;
							        $hundreds = (int) ($num_levels[$i] / 100);
							        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
							        $tens = (int) ($num_levels[$i] % 100);
							        $singles = '';
							        if ( $tens < 20 ) {
							            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
							        } else {
							            $tens = (int)($tens / 10);
							            $tens = ' ' . $list2[$tens] . ' ';
							            $singles = (int) ($num_levels[$i] % 10);
							            $singles = ' ' . $list1[$singles] . ' ';
							        }
							        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
							    } //end for loop
							    $commas = count($words);
							    if ($commas > 1) {
							        $commas = $commas - 1;
							    }

							    echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";

				            	 ?></p>
				            	
				            </td>
				        </tr>
				</table>

				<p style="font-size: 10px;">This receipt will be valid only after the payment by cheque/draft/bank transfer is honored by the bank. This receipt will be valid when both
				signatures of Authorizer and Receiver are sign off. Please keep this receipt to contract with company when needed.</p>

				<table>
				    <tbody>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Payment by </td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;">Customer Check Payment</td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Cash/Bank Transfer </td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo number_format(($amount+$vat)-$withhold, 2); ?> THB</td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Bank</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo $select_cus->sb_type; ?></td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;">Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("M j, Y", strtotime($select_cus->date_note)); ?></td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Cheque No.</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo $select_cus->billing_note; ?></td>
				    	</tr>
				    </tbody>
				</table>
				<br><br>
				<div class="col-md-12">

					<div class="col-md-4">
						<table class="table  table-bordered">
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>

							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table class="table  table-bordered">
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

				</div>

				<div class="col-md-12">

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px; text-align: center;">
							           <p><b>Recevier</b></p><br>
							           <p>&nbsp;&nbsp;........................................................</p>
							       </td>
							      
							   </tr>
						</table>
					</div>
					
					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px; text-align: center;">
							           <p><b>Authorizer</b></p><br>
							           <p>&nbsp;&nbsp;........................................................</p>
							       </td>
							      
							   </tr>
						</table>
					</div>

				</div>
	

</div>


<div class="page">
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h5 align="center"><b>One Stop Logistics Co., Ltd.</b></h5>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h6 align="center"><b>TAX INVOICE / RECEIPT</b></h6>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 15px;"><b>Copy</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>Receipt No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $select_cus->prefix_invoice.$select_cus->invoice_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Payment Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("M j, Y", strtotime($select_cus->date_note)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Customer Branch</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_branch; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Tax Number</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?></p></td>
					</tr>


				</table>
				<br>
				<table class="table">
				    <thead>
				        <tr>
				        	<th style="font-size: 11px; text-align: left;"  width="10%">Item</th>
				            <th style="font-size: 11px; text-align: left;"  width="20%">Date</th>
				            <th style="font-size: 11px; text-align: left;" width="20%" >Reference</th>
				            <th style="font-size: 11px; text-align: right;"width="20%"></th>
				            <th style="font-size: 11px; text-align: right;"  width="20%">Amount</th>
				            <th style="font-size: 11px; text-align: right;"  width="10%">Currency</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    		 $i = 1;
				    					$amount = 0;
                                    	$vat = 0;
                                    	$total = 0;
                                    	$withhold = 0;
                                    	$netpay = 0;
                                        foreach ($order  as $rs) {
                                        $amount += $rs['Amount'];
                                        $vat += $rs['Vat'];
                                        $total += $rs['Total'];
                                        $withhold += $rs['WithHolding'];
                                        $netpay += $rs['NetPay'];

				    		?>
				    		<tr>
				    			<td style="font-size: 11px; text-align: left;"><?php echo $i; ?></td>
				    			<td style="font-size: 11px; text-align: left;"> <?php echo date("M j, Y", strtotime($rs['created'])); ?></td>
				    			<td style="font-size: 11px; text-align: left;"><?php echo $rs['InvNo']; ?></td>
				    			<td style="font-size: 11px; text-align: right;"> (THB <?php echo number_format($rs['Amount'], 2); ?>)</td>
				    			<td style="font-size: 11px; text-align: right;"><?php echo number_format($rs['Amount'], 2); ?></td>
				    			<td style="font-size: 11px; text-align: right;">THB</td>
				    		</tr>
				    	<?php $i++; }  ?>
				    		<tr>
								<td colspan="4" rowspan="5"></td>
								<td style="font-size: 11px;" align="right"><b>Net Amount</b></td>
								<td style="font-size: 11px;"  align="right"><?php echo number_format($amount, 2); ?></td>
							</tr> 
							<tr>
								<td style="font-size: 11px;" align="right"><b>Vat Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($vat, 2); ?></td>
							</tr> 
							<tr>
								<td style="font-size: 11px;" align="right"><b>Gross Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($amount+$vat, 2); ?></td>
							</tr>
							<tr>
								<td style="font-size: 11px;" align="right"><b>Withholding Tax</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format($withhold, 2); ?></td>
							</tr>
							<tr>
								<td style="font-size: 11px;" align="right"><b>Payment Amount</b></td>
								<td style="font-size: 11px;" align="right"><?php echo number_format(($amount+$vat)-$withhold, 2); ?></td>
							</tr>
				    </tbody>
				</table>

				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 13px;">
				            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

				            	$num = (($amount+$vat));

				            	$rest = substr(number_format(($amount+$vat), 2), -2);

				            	$num = str_replace(array(',', ' '), '' , trim($num));
							    if(! $num) {
							        return false;
							    }
							    $num = (int) $num;
							    $words = array();
							    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
							        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
							    );
							    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
							    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
							        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
							        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
							    );
							    $num_length = strlen($num);
							    $levels = (int) (($num_length + 2) / 3);
							    $max_length = $levels * 3;
							    $num = substr('00' . $num, -$max_length);
							    $num_levels = str_split($num, 3);
							    for ($i = 0; $i < count($num_levels); $i++) {
							        $levels--;
							        $hundreds = (int) ($num_levels[$i] / 100);
							        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
							        $tens = (int) ($num_levels[$i] % 100);
							        $singles = '';
							        if ( $tens < 20 ) {
							            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
							        } else {
							            $tens = (int)($tens / 10);
							            $tens = ' ' . $list2[$tens] . ' ';
							            $singles = (int) ($num_levels[$i] % 10);
							            $singles = ' ' . $list1[$singles] . ' ';
							        }
							        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
							    } //end for loop
							    $commas = count($words);
							    if ($commas > 1) {
							        $commas = $commas - 1;
							    }

							    echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";

				            	 ?></p>
				            	
				            </td>
				        </tr>
				</table>

				<p style="font-size: 10px;">This receipt will be valid only after the payment by cheque/draft/bank transfer is honored by the bank. This receipt will be valid when both
				signatures of Authorizer and Receiver are sign off. Please keep this receipt to contract with company when needed.</p>

				<table>
				    <tbody>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Payment by </td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;">Customer Check Payment</td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Cash/Bank Transfer </td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo number_format(($amount+$vat)-$withhold, 2); ?> THB</td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Bank</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo $select_cus->sb_type; ?></td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;">Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("M j, Y", strtotime($select_cus->date_note)); ?></td>
				    	</tr>
				    	<tr>
					    	<td style="font-size: 12px; text-align: left;">Cheque No.</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: left;">&nbsp;&nbsp;&nbsp;</td>
					    	<td style="font-size: 12px; text-align: right;"><?php echo $select_cus->billing_note; ?></td>
				    	</tr>
				    </tbody>
				</table>
				<br><br>
				<div class="col-md-12">

					<div class="col-md-4">
						<table class="table  table-bordered">
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>

							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table class="table  table-bordered">
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

				</div>

				<div class="col-md-12">

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px; text-align: center;">
							           <p><b>Recevier</b></p><br>
							           <p>&nbsp;&nbsp;........................................................</p>
							       </td>
							      
							   </tr>
						</table>
					</div>
					
					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px;">
							           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							       </td>
							   </tr>
						</table>
					</div>

					<div class="col-md-4">
						<table>
							   <tr>
							       <td style="font-size: 13px; text-align: center;">
							           <p><b>Authorizer</b></p><br>
							           <p>&nbsp;&nbsp;........................................................</p>
							       </td>
							      
							   </tr>
						</table>
					</div>

				</div>
	

</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    $(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Export/BillingNote";
	});
});
</script>