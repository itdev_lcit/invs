	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 420mm;
        min-height: 210mm;
        padding: 7mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 320mm;
            height: 210mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>


	<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 13px;">
_____________________________________________________________________________________________________________________</p>

		<p align="center" style="font-size: 13px;">Summary Of Outstanding Invoice</p>

		<p align="center" style="font-size: 13px;">Date Start : <?php echo date("j-M-Y", strtotime($first_date)); ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Date End : <?php echo date("j-M-Y", strtotime($second_date)); ?>
		</p>

						<table class="table table-striped table-bordered" style="<?php if($type_inv != 'INVOICE'){ ?> font-size: 9px; <?php } else { ?> font-size: 11px;  <?php  } ?>">
							<thead>
                               <tr>
                                <?php if($type_inv != 'INVOICE'){ ?>
                                	<!--<th style="text-align: center;">Check</th>-->
                                <?php } ?>
                                	<th style="text-align: left;">Invoice No.</th>
                                <?php if($type_inv != 'INVOICE'){ ?>
                                    <th style="text-align: left;">Tax Recipt.</th>
                                <?php } ?>
                                    <th style="text-align: left;">Noted No.</th>
                                    <th style="text-align: left;">Vessel / Berth</th>
                                	<th style="text-align: center;">Customer</th>
                                    <th style="text-align: right;">Payment</th>
                                	<th style="text-align: right;">Date Inv</th>
                                <?php if($type_inv != 'INVOICE'){ ?>
                                    <th style="text-align: right;">Date Recipt</th>
                                <?php } ?>
                                	<th style="text-align: right;">Amount</th>
                                	<th style="text-align: right;">VAT</th>
                                	<th style="text-align: right;">Total Amount</th>
                                	<th style="text-align: center;"  colspan="2">WithHolding Tax</th>
                                	<th style="text-align: right;">Net Pay</th>
                            </thead>
                            <tbody>
                            	<?php 
                                    $i = 1;

                                    if(!empty($outstand)){
                                    	$amount = 0;
                                    	$vat = 0;
                                    	$total = 0;
                                    	$withhold = 0;
                                    	$netpay = 0;
                                        foreach ($outstand  as $rs) {

                                        if($rs['type_noted'] == 'CREDIT'){

                                            $amount -= $rs['Amount'];
                                            $vat -= $rs['Vat'];
                                            $total -= $rs['Total'];
                                            $withhold -= $rs['WithHolding'];
                                            $netpay -= $rs['NetPay'];

                                        } else if ($rs['type_noted'] == 'DEBIT'){

                                            $amount += $rs['Amount'];
                                            $vat += $rs['Vat'];
                                            $total+= $rs['Total'];
                                            $withhold += $rs['WithHolding'];
                                            $netpay += $rs['NetPay'];

                                        } else {

                                            $amount += $rs['Amount'];
                                            $vat += $rs['Vat'];
                                            $total+= $rs['Total'];
                                            $withhold += $rs['WithHolding'];
                                            $netpay += $rs['NetPay'];

                                        }

                                        
                                        ?>
                                        <tr>
                                            <?php if($type_inv != 'INVOICE'){ ?>
                                                <!--<td style="text-align: center;"></td>-->
                                            <?php } ?>
	                                        <td style="text-align: left;">
                                                <?php echo $rs['InvNo']; ?></td>
                                            <?php if($type_inv != 'INVOICE'){ ?>
                                                <td style="text-align: left;"><?php echo $rs['RECEIPT_INVOICE']; ?></td>
                                            <?php } ?>
                                            <td style="text-align: left;">
                                                <?php 
                                                    if($rs['type_noted'] == 'DEBIT'){
                                                        echo $rs['NotedNo']." / DEBIT"; 
                                                    } else if($rs['type_noted'] == 'CREDIT'){
                                                        echo $rs['NotedNo']." / CREDIT";
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td style="text-align: left;">
                                                <?php 

                                                    if($rs['vessel']){
                                                        echo $rs['vessel']." / ".date("M j, Y", strtotime($rs['atb']));   
                                                  } else {
                                                        echo "-";
                                                  }
                                                    
                                                ?>
                                            </td> 
	                                        <td style="text-align: center;"><?php echo $rs['Cus']; ?></td> 
                                            <td style="text-align: center;"><?php echo $rs['payment']; ?></td> 
	                                        <td style="text-align: right;"> <?php echo date("j-M-Y", strtotime($rs['created'])); ?></td>
                                            <?php if($type_inv != 'INVOICE'){ ?>
                                                <td style="text-align: right;"> 
                                                <?php 
                                                    if($rs['RECEIPT_INVOICE'] != NULL){
                                                        echo date("j-M-Y", strtotime($rs['date_receipt'])); 
                                                    }
                                                
                                                ?>
                                            </td>
                                            <?php } ?>
	                                        <td style="text-align: right;<?php if($rs['type_noted'] == 'CREDIT'){ echo "color: red;"; } else if ($rs['type_noted'] == 'DEBIT') { echo "color: green;"; }  ?>">
                                                <?php 

                                                    if($rs['type_noted'] == 'CREDIT'){
                                                        echo "- ".number_format($rs['Amount'], 2);
                                                    } else if ($rs['type_noted'] == 'DEBIT'){
                                                        echo "+ ".number_format($rs['Amount'], 2);
                                                    } else {
                                                        echo number_format($rs['Amount'], 2); 
                                                    }
                                                ?>   
                                            </td>
	                                        <td style="text-align: right;<?php if($rs['type_noted'] == 'CREDIT'){ echo "color: red;"; } else if ($rs['type_noted'] == 'DEBIT') { echo "color: green;"; }  ?>">
                                                <?php 

                                                    if($rs['type_noted'] == 'CREDIT'){
                                                        echo "- ".number_format($rs['Vat'], 2);
                                                    } else if ($rs['type_noted'] == 'DEBIT'){
                                                        echo "+ ".number_format($rs['Vat'], 2);
                                                    } else {
                                                        echo number_format($rs['Vat'], 2); 
                                                    }
                                                ?> 
                                            </td>
	                                        <td style="text-align: right;<?php if($rs['type_noted'] == 'CREDIT'){ echo "color: red;"; } else if ($rs['type_noted'] == 'DEBIT') { echo "color: green;"; }  ?>">
                                                <?php 

                                                    if($rs['type_noted'] == 'CREDIT'){
                                                        echo "- ".number_format($rs['Total'], 2);
                                                    } else if ($rs['type_noted'] == 'DEBIT'){
                                                        echo "+ ".number_format($rs['Total'], 2);
                                                    } else {
                                                        echo number_format($rs['Total'], 2); 
                                                    }
                                                ?> 
                                            </td>
                                            <td style="text-align: right;">
                                                <?php 

                                                    echo number_format($rs['config_holdtax'], 2)."%"; 

                                                ?>
                                            </td>
	                                        <td style="text-align: right;<?php if($rs['type_noted'] == 'CREDIT'){ echo "color: red;"; } else if ($rs['type_noted'] == 'DEBIT') { echo "color: green;"; }  ?>">
                                                <?php 

                                                    if($rs['type_noted'] == 'CREDIT'){
                                                        echo "- ".number_format($rs['WithHolding'], 2);
                                                    } else if ($rs['type_noted'] == 'DEBIT'){
                                                        echo "+ ".number_format($rs['WithHolding'], 2);
                                                    } else {
                                                        echo number_format($rs['WithHolding'], 2); 
                                                    }
                                                ?>
                                            </td>
	                                        <td style="text-align: right;<?php if($rs['type_noted'] == 'CREDIT'){ echo "color: red;"; } else if ($rs['type_noted'] == 'DEBIT') { echo "color: green;"; }  ?>">
                                                <?php 

                                                    if($rs['type_noted'] == 'CREDIT'){
                                                        echo "- ".number_format($rs['NetPay'], 2);
                                                    } else if ($rs['type_noted'] == 'DEBIT'){
                                                        echo "+ ".number_format($rs['NetPay'], 2);
                                                    } else {
                                                        echo number_format($rs['NetPay'], 2); 
                                                    }
                                                ?>
                                            </td>
	                                     </tr>
                                		<?php } ?>
                                		<tr>
                                            <?php if($type_inv != 'INVOICE'){ ?>
                                                <td colspan="8" style="text-align: center;"><b>Total</b></td>
                                            <?php } else { ?>
                                                <td colspan="6" style="text-align: center;"><b>Total</b></td>
	                                        <?php } ?>
	                                        <td style="text-align: right;"><?php echo number_format($amount, 2); ?></td>
	                                        <td style="text-align: right;"><?php echo number_format($vat, 2); ?></td>
	                                        <td style="text-align: right;"><?php echo number_format($total, 2); ?></td>
	                                        <td style="text-align: right;" colspan="2"><?php echo number_format($withhold, 2); ?></td>
	                                        <td style="text-align: right;"><?php echo number_format($netpay, 2); ?></td>
	                                     </tr>
                                	<?php } else { ?>

                                	<?php } ?>
                            </tbody>
						</table>
						<br><br>
						<p align="right" style="font-size: 13px;"><b>__________________________</b></p>
						<p align="right" style="font-size: 13px;"><b>Acknowledgment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></p>
						<p align="right" style="font-size: 13px;">
						Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
	</div>

	
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Export/AllSSR";
	});
});
</script>