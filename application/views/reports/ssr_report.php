	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>

<?php $p_vat = $vat->vat; ?>
<?php 
	foreach ($g_order as $SsrOrder) {  
	$dr_no = $SsrOrder['dr_no'];

?>
	<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>
						
						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b>Original</b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Bill To</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_name'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Invoice No</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Date</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['created'])); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address2']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Corp. A/C No</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['ar_company']; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address3']; ?> &nbsp;&nbsp; <?php echo $SsrOrder['customer_post']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Oper. A/C ID</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_code']; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 12px;"><b>Tax ID</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['tax_reg_no']; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b>&nbsp;<?php echo $SsrOrder['customer_branch'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b></b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_name'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Payment Terms</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['payment']; ?> <?php if($SsrOrder['payment'] == 'CREDIT'){ echo $SsrOrder['credit_term'].' DAYS';} ?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Vessel</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;( <?php echo $SsrOrder['vsl_visit'];?> ) <?php echo $SsrOrder['vessel'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Voyage In/Out</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['voy_in'];?> / <?php echo $SsrOrder['voy_out'];?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>ATB</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['atb'])); ?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>ATD</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['atd'])); ?></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 12px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b></b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
							</tr>

						</table>
						<br><br><br>
						<table class="table">
						    <thead>
						        <tr>
						        	<th style="font-size: 12px; text-align: left;"  width="10%">Tariff Code</th>
						            <th style="font-size: 12px; text-align: left;"  width="20%" colspan="2">Description</th>
						            <th style="font-size: 12px; text-align: right;"  width="10%">QTY</th>
						            <th style="font-size: 12px; text-align: right;"  width="14%">Rate</th>
						            <th style="font-size: 12px; text-align: right;"  width="10%">Line Amount (THB)</th>
						        </tr>
						    </thead>
						    <tbody id="book" >
						    	<?php 
						    	$all_amount = 0;
						    	foreach ($order as $rs_order) { if($rs_order['dr_no'] == $dr_no){ 
						    		$is_vat =  'y';
						    	 ?>
						          <tr>
						          	<td style="font-size: 12px;" align="left"><?php echo $rs_order['tariff_code']; ?></td>
						          	<td style="font-size: 12px;" align="left" colspan="2"><?php echo $rs_order['tariff_des']."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 12px;" align="right"><?php echo number_format($rs_order['qty']); ?>
						          		&nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BH002') { echo "GRT"; } else { echo "Unit"; } ?></b>
						          	</td>
						          	<td style="font-size: 12px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?>
						          	&nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BH002') { echo "Days"; } else { echo "THB"; } ?></b></td>
						          	<td style="font-size: 12px;" align="right">
						          		<?php

						          			$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
						          			$all_amount += $line_amount;
						          			echo number_format($line_amount, 2);
						          		?>
						          	</td>
						          </tr>   
						        <?php } }  ?>

						        <?php if($is_vat == 'y'){ ?>
						         <tr>
						          	<td colspan="4" rowspan="5"></td>
						          	<td style="font-size: 12px;" align="right"><b>Sub Total</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 12px;" align="right"><b>VAT (<?php echo $p_vat ?>%)</b></td>
						          	<td style="font-size: 12px;"  align="right">
						          	<?php  
						          	
						          			$vat = $all_amount*$p_vat/100; 
						          		   	echo number_format($vat, 2); 

						          	?></td>
						          </tr> 
						          <?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 12px;" align="right"><b>Total (THB)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php  
						          			if($is_vat == 'y'){	
						          				$g_total = $all_amount + $vat;
						          				echo number_format($g_total, 2); 
						          			} else {
						          				echo number_format($all_amount, 2); 
						          			}
						          	  ?></td>
						          </tr>
						           	<?php } else { ?>
								        </tr> 

						           		<tr>
								          	<td colspan="4" rowspan="5"></td>
								          	<td style="font-size: 12px;" align="right"><b>Withholding Tax (3%)</b></td>
								          	<td style="font-size: 12px;"  align="right"><?php 
						          			$vat_hold = $all_amount*3/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
								        </tr> 
								        <tr>
								          	<td style="font-size: 12px;" align="right"><b>Total</b></td>
								          	<td style="font-size: 12px;"  align="right"><?php echo  number_format($all_amount - $vat_hold, 2); ?></td>
								        </tr> 

						           	<?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 12px;" align="right"><b>Withholding Tax (3%)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php 
						          			$vat_hold = $all_amount*3/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 12px;" align="right"><b>Net Pay (THB)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php 

						          			$net_total = $g_total - $vat_hold;
						          			echo number_format($net_total, 2); 

						          	?></td>

						           <?php }  ?>
						          </tr>  
						    </tbody>
						</table>
						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 12px;" width="50%">
						            	THIS IS A COMPUTER GENERATED DOCUMENT, NO SIGNATURE REQUIRED.
						            </td>
						            <td style="font-size: 12px;" width="50%">
						            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
						            </td>
						        </tr>
						</table>
						<br><br><br><br><br>
						<P style="color:gray; font-size: 12px;">Printed by <?php echo $SsrOrder['name']; ?> on <?php echo date("j-M-Y H:i", strtotime($SsrOrder['created'])); ?></P>
						<br>
	</div>




		<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>
						
						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b>Copy</b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Bill To</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_name'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Invoice No</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Date</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['created'])); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address2']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Corp. A/C No</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['ar_company']; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 12px;">&nbsp;&nbsp;<?php echo $SsrOrder['customer_address3']; ?> &nbsp;&nbsp; <?php echo $SsrOrder['customer_post']; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Oper. A/C ID</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_code']; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 12px;"><b>Tax ID</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['tax_reg_no']; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b>&nbsp;<?php echo $SsrOrder['customer_branch'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b></b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['customer_name'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Payment Terms</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['payment']; ?> <?php if($SsrOrder['payment'] == 'CREDIT'){ echo $SsrOrder['credit_term'].' DAYS';} ?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>Vessel</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;( <?php echo $SsrOrder['vsl_visit'];?> ) <?php echo $SsrOrder['vessel'];?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>Voyage In/Out</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo $SsrOrder['voy_in'];?> / <?php echo $SsrOrder['voy_out'];?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>ATB</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['atb'])); ?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 12px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b>ATD</b></p></td>
								<td width="50%"><p style="font-size: 12px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder['atd'])); ?></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 12px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 12px;"><b></b></p></td>
								<td width="50%"><p style="font-size: 12px;">&nbsp;&nbsp;</p></td>
							</tr>

						</table>
						<br><br><br>
						<table class="table">
						    <thead>
						        <tr>
						        	<th style="font-size: 12px; text-align: left;"  width="10%">Tariff Code</th>
						            <th style="font-size: 12px; text-align: left;"  width="20%" colspan="2">Description</th>
						            <th style="font-size: 12px; text-align: right;"  width="10%">QTY</th>
						            <th style="font-size: 12px; text-align: right;"  width="14%">Rate</th>
						            <th style="font-size: 12px; text-align: right;"  width="10%">Line Amount (THB)</th>
						        </tr>
						    </thead>
						    <tbody id="book" >
						    	<?php 
						    	$all_amount = 0;
						    	foreach ($order as $rs_order) { if($rs_order['dr_no'] == $dr_no){ 
						    		$is_vat =  'y';
						    	 ?>
						          <tr>
						          	<td style="font-size: 12px;" align="left"><?php echo $rs_order['tariff_code']; ?></td>
						          	<td style="font-size: 12px;" align="left" colspan="2"><?php echo $rs_order['tariff_des']."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 12px;" align="right"><?php echo number_format($rs_order['qty']); ?>
						          		&nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BH002') { echo "GRT"; } else { echo "Unit"; } ?></b>
						          	</td>
						          	<td style="font-size: 12px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?>
						          	&nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BH002') { echo "Days"; } else { echo "THB"; } ?></b></td>
						          	<td style="font-size: 12px;" align="right">
						          		<?php

						          			$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
						          			$all_amount += $line_amount;
						          			echo number_format($line_amount, 2);
						          		?>
						          	</td>
						          </tr>   
						        <?php } }  ?>

						        <?php if($is_vat == 'y'){ ?>
						         <tr>
						          	<td colspan="4" rowspan="5"></td>
						          	<td style="font-size: 12px;" align="right"><b>Sub Total</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 12px;" align="right"><b>VAT (<?php echo $p_vat ?>%)</b></td>
						          	<td style="font-size: 12px;"  align="right">
						          	<?php  
						          	
						          			$vat = $all_amount*$p_vat/100; 
						          		   	echo number_format($vat, 2); 

						          	?></td>
						          </tr> 
						          <?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 12px;" align="right"><b>Total (THB)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php  
						          			if($is_vat == 'y'){	
						          				$g_total = $all_amount + $vat;
						          				echo number_format($g_total, 2); 
						          			} else {
						          				echo number_format($all_amount, 2); 
						          			}
						          	  ?></td>
						          </tr>
						           	<?php } else { ?>
								        </tr> 

						           		<tr>
								          	<td colspan="4" rowspan="5"></td>
								          	<td style="font-size: 12px;" align="right"><b>Withholding Tax (3%)</b></td>
								          	<td style="font-size: 12px;"  align="right"><?php 
						          			$vat_hold = $all_amount*3/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
								        </tr> 
								        <tr>
								          	<td style="font-size: 12px;" align="right"><b>Total</b></td>
								          	<td style="font-size: 12px;"  align="right"><?php echo  number_format($all_amount - $vat_hold, 2); ?></td>
								        </tr> 

						           	<?php }  ?>

						          <?php if($is_vat == 'y'){ ?>
						          <tr>
						          	<td style="font-size: 12px;" align="right"><b>Withholding Tax (3%)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php 
						          			$vat_hold = $all_amount*3/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 12px;" align="right"><b>Net Pay (THB)</b></td>
						          	<td style="font-size: 12px;"  align="right"><?php 

						          			$net_total = $g_total - $vat_hold;
						          			echo number_format($net_total, 2); 

						          	?></td>

						           <?php }  ?>
						          </tr>  
						    </tbody>
						</table>
						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 12px;" width="50%">
						            	THIS IS A COMPUTER GENERATED DOCUMENT, NO SIGNATURE REQUIRED.
						            </td>
						            <td style="font-size: 12px;" width="50%">
						            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
						            </td>
						        </tr>
						</table>
						<br><br><br><br><br>
						<P style="color:gray; font-size: 12px;">Printed by <?php echo $SsrOrder['name']; ?> on <?php echo date("j-M-Y H:i", strtotime($SsrOrder['created'])); ?></P>
						<br>
	</div>


<?php } ?>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Speacial";
	});
});
</script>