<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Reports -> Log Print Draft</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Reports/LogprintDr'; ?>" method="POST">

                                <input class="form-control" type="text" name="search" value="" placeholder="Search Draft No">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12" id="draft">
                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">#</th>
                                            <th>Username</th>
                                            <th>#DRAFT</th>
                                            <th style="text-align: center;">Count</th>
                                            <!--<th>Created</th>
                                            <th>Last Print</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 


                                        if($results){
                                            $i=1;
                                            foreach ($results as $rs_dr) { 
                                                
                                            ?>

                                                 <tr>
                                                    <td style="text-align: center;"><?php echo $i; ?></td>
                                                    <td><?php echo $rs_dr->username; ?></td>
                                                    <td><?php echo $rs_dr->print_no; ?></td>
                                                    <td style="text-align: center;"><?php echo $rs_dr->num_print; ?></td>
                                                </tr>

                                        <?php

                                          $i++;  } 
                                        } else {
                                            echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                                 <div class="col-md-2" style="float: left;">
                                    <a href="<?php echo site_url(); ?>Reports/PrintLogDr" target="_blank"><button class="btn btn-success  btn-xs re-dr-normal"><span class="glyphicon glyphicon-print"></span>&nbsp;Print</button></a>
                                </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#table-draft').DataTable({
            //responsive: true
        });



    });
</script>