<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Reports Log Print Draft</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12" id="draft">
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Username</th>
                                            <th>#DRAFT</th>
                                            <th style="text-align: center;">Count</th>
                                            <!--<th>Created</th>
                                            <th>Last Print</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 


                                        if($log){
                                            $i=1;
                                            foreach ($log as $rs) { 
                                                
                                            ?>

                                                 <tr>
                                                    <td style="text-align: center;"><?php echo $i; ?></td>
                                                    <td><?php echo $rs['username']; ?></td>
                                                    <td><?php echo $rs['print_no']; ?></td>
                                                    <td style="text-align: center;"><?php echo $rs['num_print']; ?></td>
                                                    <!--<td><?php echo date("j-F-Y H:i", strtotime($rs_dr['created'])); ?></td>
                                                    <td><?php echo date("j-F-Y H:i", strtotime($rs_dr['updated'])); ?></td>-->
                                                </tr>

                                        <?php

                                          $i++;  } 
                                        }else {
                                            echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        <p style="font-size: 2px;"><?php echo date("j-F-Y H:i"); ?></p>
</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        window.print();
        window.close();
    });
</script>