<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">All Invoice 
            <a href="<?php echo site_url(); ?>Reports/PrintSSR/<?php echo $date_s; ?>/<?php echo $type; ?>"><button class="btn btn-primary print-inv"><i class="fa fa-print"></i>Print</button></a>
       </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url(); ?>Reports/AllSSR" method="POST">
                                <label>Date :</label>
                                <input class="form-control" type="date" name="search" value="<?php echo $date_s; ?>">
                                <label for="type">Payment</label>
                                  <select class="form-control" id="type" name="type">
                                    <option value="ALL" <?php if($type == 'ALL'){ echo "selected"; } ?> >ALL</option>
                                    <option value="CREDIT" <?php if($type == 'CREDIT'){ echo "selected"; } ?>>CREDIT</option>
                                    <option value="CASH" <?php if($type == 'CASH'){ echo "selected"; } ?>>CASH</option>
                                  </select>
                                <button class="btn btn-info">Go</button>
                                <button type="reset" class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                              
                        <div class="panel-body">
                            <div class="row">


                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Inv</th>
                                            <th style="text-align: left;">Customers</th>
                                            <th style="text-align: left;">Vessel</th>
                                            <th style="text-align: center;">Payment</th>
                                            <th>Created</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;
                                            if(!empty($results)){ ?>

                                            <input type="hidden" id="record" value="1">

                                            <?php foreach ($results as $rs_normal) {?>
                                            <tr class="r-normal">
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td style="text-align: left;"><?php echo $rs_normal->dr_no; ?></td>
                                                <td style="text-align: left;">
                                                    <?php echo $rs_normal->customer_code; ?>
                                                </td>
                                                <td style="text-align: left;">
                                                    (<?php echo $rs_normal->vsl_visit; ?>)<?php echo $rs_normal->vessel; ?>
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php echo $rs_normal->payment; ?>
                                                </td>
                                                <td>
                                                    <?php echo date("j-F-Y H:i", strtotime($rs_normal->created)); ?>
                                                        
                                                    </td>
                                                
                                            </tr>
                                         <?php $i++; } 
                                        } else {

                                        ?>
                                           <input type="hidden" id="record" value="0">
                                            <tr>
                                                <td colspan="8" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                     <!-- /.paging -->
                                    <?php echo $links; ?>
                                    <!-- /.paging -->
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>