<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 12mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<?php 
	$vat_inv = $vat->vat;
	foreach ($head as $rs_head) {  
	$hold_status = $rs_head['hold_tax'];
	$dr_no = $rs_head['dr_no'];
	$inv_no = $rs_head['prefix_invoice'].$rs_head['invoice_no'];
?>
		<div class="page">
			<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
			<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
			<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
			<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
			<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
			<br>
			<h4 align="center"><b>RECEIPT / TAX INVOICE</b></h4>

							<table>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 12px;"><b>Copy</b></td>
								</tr>
								<tr>

									<td width="10%"><p style="font-size: 10px;"><b>Customer</b></p></td>
									<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['company_bill'];?></p></td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="16%"><p style="font-size: 10px;"><b>No.</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['prefix_invoice'].$rs_head['invoice_no']; ?></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $rs_head['address_bill']; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
									<td width="40%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y"); ?></p></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $rs_head['address_bill2']; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Type</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['type']; ?></p></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $rs_head['address_bill3']; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Corp. A/C No</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;99999</p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 10px;"><b>Tax ID</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['tax_reg_no']; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b> HEAD OFFICE</p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Oper. A/C ID</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['code_comp']; ?></p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 10px;"><b>Remark</b></p></td>
									<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;</p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Payment Terms</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;</p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 10px;"><b></b></p></td>
									<td width="45%"><p style="font-size: 10px;"></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_head['terminal_doc_ref_an']; ?></p><p style="font-size: 10px;">&nbsp;&nbsp;DFT#<?php echo $rs_head['dr_no']; ?></p>
									<p style="font-size: 10px;">&nbsp;&nbsp;Booking#<?php echo $rs_head['book_an']; ?></p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 10px;"><b></b></p></td>
									<td></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b><!--ATD--></b></p></td>
									<td width="25%"></td>
								</tr>
							</table>
							<br><br><br>
							<table class="table">
							    <thead>
							        <tr>
							            <th style="font-size: 10px; text-align: left;"  colspan="2">Description</th>
							           
							            <th style="font-size: 10px; text-align: center;"  width="10%">QTY</th>
							            <th style="font-size: 10px; text-align: right;"  width="20%">Unit Rate (THB)</th>
							            <th style="font-size: 10px; text-align: right;"  width="25%">Line Amount (THB)</th>
							        </tr>
							    </thead>
							    <tbody id="book">
							    	<?php 
							    	$all_amount = 0;
							    	foreach ($order as $rs_order) { if($rs_order['dr_no'] == $dr_no){  ?>
							          <tr>
							          	<td style="font-size: 10px;" align="left" colspan="2"><?php echo $rs_order['description']."&nbsp;&nbsp;".$rs_order['size_con']."'"."&nbsp;&nbsp;".$rs_order['type_con']; ?></td>

							          	<td style="font-size: 10px;" align="center"><?php echo $rs_order['book_con']; ?></td>
							          	<td style="font-size: 10px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php

							          			$line_amount = $rs_order['book_con'] * $rs_order['cur_rate'];
							          			$all_amount += $line_amount;
							          			echo number_format($line_amount, 2);
							          		?>
							          	</td>
							          </tr>   
							        <?php } }  ?>

							         	<tr>
								          	<td colspan="3" rowspan="5"></td>
								          	<td style="font-size: 10px;" align="right"><b>Sub Total</b></td>
								          	<td style="font-size: 10px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
							          	</tr> 
							          	<tr>      	
								          	<td style="font-size: 10px;" align="right"><b>VAT (<?php echo $vat_inv; ?>%)</b></td>
								          	<td style="font-size: 10px;"  align="right">
								          	<?php  
								          			$vat = $all_amount*$vat_inv/100; 
								          		   	echo number_format($vat, 2); 

								          	?></td>
							          	</tr> 
							         	<tr>  	
								          	<td style="font-size: 10px;" align="right"><b>Total (THB)</b></td>
								          	<td style="font-size: 10px;"  align="right">
								          	<?php  

								          			$net_total = $all_amount + $vat;
								          			$amount = $all_amount + $vat;
								          			echo number_format($net_total, 2); 

								          	?></td>
							          	</tr> 
							          	<?php if($hold_status == '1'){ ?>
							          	<tr>  	
								          	<td style="font-size: 10px;" align="right"><b>Withholding Tax (3%)</b></td>
								          	<td style="font-size: 10px;"  align="right">
								          	<?php  

								          			$hold_tax = $all_amount*3/100; 
							          				echo number_format($hold_tax, 2); 

								          	?></td>
							          	</tr> 
							          	<tr>  	
								          	<td style="font-size: 10px;" align="right"><b>Net Pay(THB)</b></td>
								          	<td style="font-size: 10px;"  align="right">
								          	<?php  

								          			$net_total = ($all_amount + $vat) - $hold_tax;
							          			echo number_format($net_total, 2); 
								          	?></td>
							          	</tr> 
							          	<?php } else { ?>
							          	<tr>  	
								          	<td style="font-size: 10px;" align="right"><b>Withholding Tax (3%)</b></td>
								          	<td style="font-size: 10px;"  align="right">0.00</td>
							          	</tr> 
							          	<tr>  	
								          	<td style="font-size: 10px;" align="right"><b>Net Pay(THB)</b></td>
								          	<td style="font-size: 10px;"  align="right">
								          	<?php  

								          			$net_total = $all_amount + $vat;
							          			echo number_format($net_total, 2); 
								          	?></td>
							          	</tr> 
							          	<?php } ?>
							    </tbody>
							</table>
							<table class="table">
								<thead>
							        <tr>
							            <th style="font-size: 10px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
							        </tr>
							        <tr>
							            <th style="font-size: 10px; text-align: left;">Date</th>
							            <th style="font-size: 10px; text-align: center;">Payment Method</th>
							            <th style="font-size: 10px; text-align: left;">Ref. No.</th>
							            <th style="font-size: 10px; text-align: right;">Payment Amount</th>
							            <th style="font-size: 10px; text-align: center;">Exch. Rate</th>
							            <th style="font-size: 10px; text-align: right;">Amount (THB)</th>
							        </tr>
							    </thead>
							    <tbody>
							    	<tr>
							    		<td style="font-size: 10px; text-align: left;"><?php echo date("j-F-Y"); ?></td>
							    		<td style="font-size: 10px; text-align: center;">CASH</td>
							    		<td style="font-size: 10px; text-align: center;"></td>
							    		<td style="font-size: 10px; text-align: right;">THB&nbsp;<?php echo number_format($net_total, 2); ?></td>
							    		<td style="font-size: 10px; text-align: center;">1</td>
							    		<td style="font-size: 10px; text-align: right;"><?php echo number_format($amount, 2); ?></td>
							    	</tr>
							    	<tr>
							    		<td style="font-size: 10px; text-align: left;" colspan="2">Received By :
							    			<?php foreach ($log_print as $rs_logs ) { 

							    				if($rs_logs['invoice_no'] == $inv_no){
							    						echo $rs_logs['username']."&nbsp;&nbsp;On&nbsp;&nbsp;".date("j-F-Y H:i", strtotime($rs_head['created'])); 
							    				}
							    			
							    			} ?>
							    		</td>
							    		<td style="font-size: 10px; text-align: left;" colspan="2"></td>
							    		<td style="font-size: 10px; text-align: right;" >Amount Received (THB) :</td>
							    		<td style="font-size: 10px; text-align: right;" ><?php echo number_format($amount, 2); ?></td>
							    	</tr>
							    </tbody>
							</table>
							<table class="table table-striped table-bordered">
							        <tr>
							            <td style="font-size: 10px;" width="50%">
							            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
							            	
							            </td>
							            <td style="font-size: 10px;" width="50%">
							            	<p>This is a computer generated document, no signature required.</p>
							            	<!--<p>Contact detail:</p>
							            	<p>Email :</p>-->
							            	<p>Tel :  (66 38 ) 40 8200</p>
							            	<p>Fax : (66 38) 40 1021 - 2</p>
							            </td>
							        </tr>
							</table>

	</div>
<?php } ?>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  	//window.print();
    //document.location.href = "<?php echo site_url(); ?>Reports/AllInv";
});
</script>