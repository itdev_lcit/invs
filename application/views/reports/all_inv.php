<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">All Invoice 
            <a href="<?php echo site_url(); ?>Reports/PrintInv/<?php echo $date_s; ?>"><button class="btn btn-primary print-inv"><i class="fa fa-print"></i>Print</button></a>
       </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url(); ?>Reports/AllInv" method="POST">
                                <label>Date :</label>
                                <input class="form-control" type="date" name="search" value="<?php echo $date_s; ?>">
                                <button class="btn btn-info">Go</button>
                                <button type="reset" class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                              
                        <div class="panel-body">
                            <div class="row">


                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Draft</th>
                                            <th>Inv</th>
                                            <th style="text-align: center;">Customers</th>
                                            <th>Created</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;
                                            if(!empty($results)){ ?>

                                            <input type="hidden" id="record" value="1">

                                            <?php foreach ($results as $rs_normal) {?>
                                            <tr class="r-normal" data-prefix_invoice ="<?php echo $rs_normal->prefix_invoice; ?>" data-invoice_no ="<?php echo $rs_normal->invoice_no; ?>" <?php if($rs_normal->is_use == 1){ echo "style='color: red;'"; } ?>>
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td><?php echo $rs_normal->dr_no; ?></td>
                                                <td style="text-align: left;">
                                                    <?php 
                                                         if($rs_normal->is_use == '0'){
                                                            echo "<p style='color:green;'><b>Draft</b></p>";
                                                         } else if ($rs_normal->is_use == '2'){
                                                            if($rs_normal->is_reprint == 1){
                                                                echo "<p style='color:green;'><b>REPRINT-Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                            } else {
                                                                echo "<p style='color:green;'><b>Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                            }
                                                         } else {
                                                            echo "<p style='color:red;'><b>Cancel".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                         }
                                                    ?>
                                                    
                                                </td>
                                                <td style="text-align: center;"><?php echo $rs_normal->code_comp; ?></td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->create_order)); ?></td>
                                                
                                            </tr>
                                         <?php $i++; } 
                                        } else {

                                        ?>
                                           <input type="hidden" id="record" value="0">
                                            <tr>
                                                <td colspan="8" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                     <!-- /.paging -->
                                    <?php echo $links; ?>
                                    <!-- /.paging -->
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>