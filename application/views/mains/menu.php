 <!-- Navigation -->
        <nav id="navig-menu" class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;<?php if(!empty($is_change_pass)){if($is_change_pass == '0'){echo "display: none";}} ?>">

             <ul class="nav navbar-top-links navbar-left">


                <?php if($user->role == 'ADMIN' OR $user->role  == 'DOCUMENT' OR $user->role  == 'SUPBILLING'){ ?>
                <!--<li >
                    <a href="<?php echo site_url(''); ?>Createdraft" class="active"><i class="fa fa-plus-circle"></i>&nbsp;Create Draft</a>
                </li>-->
                <li>
                    <a href="<?php echo site_url(''); ?>Draft/AllDraft/" class="active"><i class="fa fa-newspaper-o"></i>&nbsp;Draft </a>
                </li>
                <li>
                    <a href="<?php echo site_url(''); ?>RemainOrder/AllRemain" class="active"><i class="fa fa-object-group"></i>&nbsp;Remain </a>
                </li>
                <!--<li>
                    <a href="<?php echo site_url(''); ?>Speacial" class="active"><i class="fa fa-plus-circle"></i>&nbsp;SSR </a>
                </li>-->
                <?php } ?>

                <?php 
                if($user->role == 'ADMIN'  OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING'){
                 ?>
               <!-- <li>
                    <a href="<?php echo site_url(''); ?>Inv/All" ><i class="fa fa-plus-circle"></i>&nbsp;INV Manual</a>
                </li>-->
                <li>
                    <a href="<?php echo site_url(''); ?>Billing/AllBill"><i class="fa fa-credit-card"></i>&nbsp;Billing</a>
                </li>

                <?php } ?>
                <?php if($user->role == 'ADMIN'  OR $user->role  == 'MT' OR $user->role == 'MNGOSL' OR $user->role == 'SUPOSL'){ ?>
                    <li>
                        <a href="<?php echo site_url(''); ?>Job/All" class="active"><i class="fa fa-plus-circle"></i>&nbsp;JOB</a>
                    </li>
                <?php } ?>
                <?php if($user->role == 'ADMIN'  OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING'){ ?>
                    <li>
                        <a href="<?php echo site_url(''); ?>Job/Orders" class="active"><i class="fa fa-plus-circle"></i>&nbsp;JOB</a>
                    </li>
                <?php } ?>
                <?php 
                if($user->role == 'ADMIN'  OR  $user->role  == 'SUPBILLING' OR $user->role  == 'DOCUMENT'){
                 ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-file"></i> &nbsp; Order&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php 
                        if($user->role == 'ADMIN'  OR $user->role  == 'DOCUMENT' OR  $user->role  == 'SUPBILLING'){
                         ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Speacial/SSR" class="active"><i class="fa fa-plus-circle"></i>&nbsp;SSR </a>
                        </li>
                        <?php } ?>
                         <?php 
                        if($user->role == 'ADMIN'  OR  $user->role  == 'SUPBILLING'){
                         ?>
                       <!-- <li>
                             <a href="<?php echo site_url(''); ?>Inv/All" ><i class="fa fa-plus-circle"></i>&nbsp;INV Manual</a>
                        </li>-->
                        <?php } ?>
                         <?php 
                        if($user->role == 'ADMIN'  OR  $user->role  == 'DOCUMENT'){
                         ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Createdraft" class="active"><i class="fa fa-plus-circle"></i>&nbsp;Create Draft</a>
                        </li>
                        <?php } ?>
                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <?php } ?>
                <?php
                 if($user->role == 'ADMIN'  OR $user->role  == 'SUPBILLING'){
                 ?>
                <li>
                    <a href="<?php echo site_url(''); ?>Admin/invoice"><i class="fa fa-undo"></i>&nbsp;Invoice</a>
                </li>
                <?php } ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-gear fa-fw"></i>&nbsp;Setting&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php if($user->role == 'ADMIN'  OR $user->role  == 'SUPBILLING'){  ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/Vat">Vat</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/Wh">Withholding Tax</a>
                        </li>
                        <?php }  ?>
                        <?php if($user->role == 'ADMIN'){  ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/TypeDoc">Doc Type</a>
                        </li>
                        <?php }  ?>
                         <?php if($user->role == 'ADMIN' OR $user->role  == 'SUPBILLING'){  ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/TypeBank">Bank Type</a>
                        </li>
                       <!-- <li>
                            <a href="<?php echo site_url(''); ?>Setting/TypeBill">INV Manual Type</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/PrefixDraft">Prefix INV Manual Draft</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/PrefixReceipt">Prefix INV Manual Receipt</a>
                        </li>-->

                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/TypeTariff">Tariff Type (SSR)</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/PrefixSsr">Prefix (SSR)</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/PrefixSsrReceipt">Receipt (SSR)</a>
                        </li>
                        <?php }  ?>

                        <?php if($user->role == 'ADMIN'){  ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/Prefix">Prefix Invoice</a>
                        </li>
                        <?php }  ?>
                        <?php if($user->role <> 'MT' OR $user->role == 'MNGOSL' OR $user->role == 'SUPOSL'){  ?>
                        <li>
                            <a href="<?php echo site_url(''); ?>Setting/UpdateCustomer">Customers</a>
                        </li>
                        <?php }  ?>
                        <?php if($user->role == 'ADMIN' OR $user->role == 'MT' OR $user->role == 'MNGOSL' OR $user->role == 'SUPOSL'){  ?>
                        <li>
                            <a href="<?php echo site_url(); ?>Job/JobPreFix">Job Prefix</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>Job/JobType">Job Type</a>
                        </li>
                        <?php }  ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <?php if($user->role == 'ADMIN'){ ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa  fa-user   fa-fw"></i>&nbsp;Admin&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo site_url(''); ?>Admin/Users">Users</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Admin/Permission">Permission</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Admin/invoice">Invoice</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                 <?php } ?>

                <?php if($user->role == 'ADMIN' OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING' OR $user->role  == 'MT' OR $user->role == 'MNGOSL' OR $user->role == 'SUPOSL'){ ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-list-alt"></i>&nbsp;Report&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">

                        <?php if($user->role == 'ADMIN' OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING'){ ?> 
                        <li>
                            <a href="<?php echo site_url(''); ?>Reports/AllSSR">All SSR</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Export/BillingNote">Billing Note</a>
                        </li> 
                        <li>
                            <a href="<?php echo site_url(''); ?>Export/AllSSR">Outstanding</a>
                        </li> 
                        <li>
                            <a href="<?php echo site_url(''); ?>Export/AllOrder">List Order</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Export/AllTariff">Tariff Order</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Reports/AllInv">All Inv</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Export/AllInv">Export</a>
                        </li>   
                        <?php } ?>
                        <?php if($user->role == 'ADMIN'){ ?> 
                        <li>
                            <a href="<?php echo site_url(''); ?>Reports/LogprintDr">Log Print Draft</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Reports/LogprintInv">Log Print Invoice</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(''); ?>Reports/LogreprintInv">Log Re-Print INV</a>
                        </li>
                         <li>
                            <a href="<?php echo site_url(''); ?>Reports/LogCancelInv">Log Cancel INV</a>
                        </li>
                        <?php } ?>
                        <?php if($user->role == 'MT' OR $user->role == 'ADMIN' OR $user->role == 'MNGOSL' OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING' OR $user->role == 'SUPOSL'){ ?> 
                        <li>
                            <a href="<?php echo site_url(''); ?>Job/Reports">Job Order</a>
                        </li>
                        <?php } ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
               <?php } ?>
            </ul>


            <ul class="nav navbar-top-links navbar-right">

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user fa-fw"></i>Welcome, <b><?php echo $user->name; ?></b>&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="<?php echo site_url();?>Logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                </li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>

            </ul>
            <!-- /.navbar-top-links -->

        </nav>