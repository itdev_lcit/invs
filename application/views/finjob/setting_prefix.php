<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting Prefix</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Prefix
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Type</label>
                                                <input class="form-control" id="jp_id" name="jp_id" required="true" value="" type="hidden">
                                                <input class="form-control" id="jp_type" name="jp_type" required="true" value="">
                                                <p id="validate-type" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Setting</label>
                                                <input class="form-control" id="jp_prefix" name="jp_prefix" required="true" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="is_delete" name="is_delete">
                                                    <option value="N">ACTIVE</option>
                                                    <option value="Y">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" align="right">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveType" id="saveType">Save</button>
                                                <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-12">
                                    
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Setting</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($job_type){
                                            $i = 1;
                                            foreach ($job_type as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type" data-jp_id="<?php echo $rs['jp_id']; ?>" data-jp_type="<?php echo $rs['jp_type']; ?>" data-jp_prefix="<?php echo $rs['jp_prefix']; ?>" data-is_delete="<?php echo $rs['is_delete']; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $rs['jp_type']; ?></td>
                                                    <td><?php echo $rs['jp_prefix'].date('Ym'); ?>-001</td>
                                                    <td style="text-align: center"><?php 
                                                        if($rs['is_delete'] == 'N'){
                                                            echo "<p style='color:green;'><b>ACTIVE</b></p>";
                                                        } else {
                                                            echo "<p style='color:red;'><b>INACTIVE</b></p>";
                                                        }
                                                     ?></td>
                                                     <td><?php echo date("j-M-Y H:i", strtotime($rs['created'])); ?></td>
                                                    <td><?php echo date("j-M-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="7">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeletePrefix" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.EditType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var jp_id = $row.data('jp_id');
            var jp_type = $row.data('jp_type');
            var jp_prefix = $row.data('jp_prefix');
            var is_delete = $row.data('is_delete');


            $('#jp_id').val('');
            $('#jp_type').val('');
            $('#jp_prefix').val('');
            $('#is_delete').val('');

            $('#jp_id').val(jp_id);
            $('#jp_type').val(jp_type);
            $('#jp_prefix').val(jp_prefix);
            $('#is_delete').val(is_delete);
        });

         $('.RemoveType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var job_id = $row.data('jp_id');
      
            $('#remove_id').val('');
            $('#remove_id').val(job_id);
            $('#myModal').modal('show');
        });

        $('.saveType').click(function(){
            
            var jp_id = $('#jp_id').val();
            var jp_type = $('#jp_type').val();
            var jp_prefix = $('#jp_prefix').val();

            $.ajax({
                url:'<?php echo base_url(); ?>Job/SavePrefix',
                method:'POST',
                data:{ jp_id:jp_id, jp_type:jp_type, jp_prefix:jp_prefix}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-type').html('');
                    $('#validate-type').append('Please specify Type.');
                } 

                if(o.msg == 200){
                    $('#validate-type').html('');
                    $('#validate-type').append("Type'S Already.");
                } 

                if(o.msg == 300){
                    $('#validate-description').html('');
                    $('#validate-description').append('Please specify Prefix.');
                } 


                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        }); 
        

        $('.clearType').click(function(){
            
            $('#jp_id').val('');
            $('#jp_type').val('');
            $('#jp_prefix').val('');


        }); 

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>