<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Setting Job</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Config Job
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>JOB CODE</label>
                                                <input class="form-control" id="job_id" name="job_id" required="true" value="" type="hidden">
                                                <input class="form-control" id="job_code" name="job_code" required="true" value="">
                                                <p id="validate-type" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>JOB DESC</label>
                                                <input class="form-control" id="job_desc" name="job_desc" required="true" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>CATEGORY</label>
                                                <select class="form-control" name="job_cat" id="job_cat">
                                                    <?php foreach($job_prefix as $rs) { ?>
                                                        <option value="<?php echo $rs['jp_type']; ?>"><?php echo $rs['jp_type']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>UNIT</label>
                                                <input class="form-control" id="job_unit" name="job_unit" required="true" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" id="is_delete" name="is_delete">
                                                    <option value="N">ACTIVE</option>
                                                    <option value="Y">INACTIVE</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" align="right">    
                                            <div class="form-group">
                                                <button type="button" class="btn btn-outline btn-success saveType" id="saveType">Save</button>
                                                <button type="button" class="btn btn-outline btn-danger clearType" id="clear">Clear</button>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="col-lg-12">
                                    
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Description</th>
                                            <th>Category</th>
                                            <th>Unit</th>
                                            <th>Is Use</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                        if($job_type){
                                            $i = 1;
                                            foreach ($job_type as $rs) { 
                                                
                                            ?>

                                                <tr class="r-type" data-job_id="<?php echo $rs['job_id']; ?>" data-job_code="<?php echo $rs['job_code']; ?>" data-job_desc="<?php echo $rs['job_desc']; ?>" data-job_cat="<?php echo $rs['job_cat']; ?>" data-job_unit="<?php echo $rs['job_unit']; ?>" data-is_delete="<?php echo $rs['is_delete']; ?>" >
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $rs['job_code']; ?></td>
                                                    <td><?php echo $rs['job_desc']; ?></td>
                                                    <td style="text-align: center"><?php echo $rs['job_cat']; ?></td>
                                                    <td style="text-align: right"><?php echo $rs['job_unit']; ?></td>
                                                    <td style="text-align: center"><?php 
                                                        if($rs['is_delete'] == 'N'){
                                                            echo "<p style='color:green;'><b>ACTIVE</b></p>";
                                                        } else {
                                                            echo "<p style='color:red;'><b>INACTIVE</b></p>";
                                                        }
                                                     ?></td>
                                                     <td><?php echo date("j-M-Y H:i", strtotime($rs['created'])); ?></td>
                                                    <td><?php echo date("j-M-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs EditType" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveType" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
                                                </tr>

                                        <?php

                                           $i++; } 
                                        }else {
                                            echo '<tr align="center"><td colspan="7">-No Data-</td></tr>';
                                       }?>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeleteTypeJob" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.EditType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var job_id = $row.data('job_id');
            var job_code = $row.data('job_code');
            var job_desc = $row.data('job_desc');
            var job_cat = $row.data('job_cat');
            var is_delete = $row.data('is_delete');
            var job_unit = $row.data('job_unit');


            $('#job_id').val('');
            $('#job_code').val('');
            $('#job_desc').val('');
            $('#job_cat').val('');
            $('#job_unit').val('');
            $('#is_delete').val('');

            $('#job_id').val(job_id);
            $('#job_code').val(job_code);
            $('#job_desc').val(job_desc);
            $('#job_cat').val(job_cat);
            $('#job_unit').val(job_unit);
            $('#is_delete').val(is_delete);
        });

         $('.RemoveType').click(function(){
          
            var $row = $(this).parents('tr.r-type');
            var job_id = $row.data('job_id');
      
            $('#remove_id').val('');
            $('#remove_id').val(job_id);
            $('#myModal').modal('show');
        });

        $('.saveType').click(function(){
            
            var job_id = $('#job_id').val();
            var job_code = $('#job_code').val();
            var job_desc = $('#job_desc').val();
            var job_cat = $('#job_cat').val();
            var job_unit = $('#job_unit').val();

            $.ajax({
                url:'<?php echo base_url(); ?>Job/SaveTypeJob',
                method:'POST',
                data:{ job_id:job_id, job_code:job_code, job_desc:job_desc, job_cat:job_cat, job_unit:job_unit}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    alert('Please specify Code');
                } 

                if(o.msg == 200){
                     alert("Code's already in use.");
                } 

                if(o.msg == 300){
                    alert('Please specify Desc');
                } 

                if(o.msg == 400){
                   alert('Please specify Category');
                } 

                if(o.msg == 500){
                   alert('Please specify Unit');
                } 



                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        }); 
        

        $('.clearType').click(function(){
            
            $('#job_id').val('');
            $('#job_code').val('');
            $('#job_desc').val('');
            $('#job_cat').val('');
            $('#job_unit').val('');


        }); 

        $('.btn_succsess').click(function() {
            location.reload();
        });
    });
</script>