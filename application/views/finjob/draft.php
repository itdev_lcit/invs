
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Job no : <?php echo $job_no; ?></h4>
       <input type="hidden" id="job_no" name="job_no" value="<?php echo $job_no; ?>">
       <input type="hidden" id="job_count" name="job_count" value="<?php echo count($order); ?>">
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">


                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Customers / Project 
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-6">
                                        <label>Name</label>
                                        <input type="text" id="customers_proj" name="customers_proj" class="form-control" value="<?php echo $customers_proj; ?>"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label>Booking</label>
                                        <input type="text" id="bkg" name="bkg" class="form-control" value="<?php echo $bkg; ?>"> 
                                    </div>
                                    <br><br><br><br>
                                    <div class="col-md-6">
                                        <label>From</label>
                                        <input type="text" id="dlv_from" name="dlv_from" class="form-control" value="<?php echo $dlv_from; ?>"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label>To</label>
                                        <input type="text" id="dlv_to" name="dlv_to" class="form-control" value="<?php echo $dlv_to; ?>"> 
                                    </div>
                                    <br><br><br><br>
                                    <div class="col-md-6">
                                        <label>Vessel Name</label>
                                        <input type="text" id="vessel" name="vessel" class="form-control" value="<?php echo $vessel; ?>"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label>Vessel Voy</label>
                                        <input type="text" id="voy_in" name="voy_in" class="form-control" value="<?php echo $voy_in; ?>"> 
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Detail
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th style="text-align: left; width: 10%;">CODE</th>
                                                <th style="text-align: left; width: 40%;">Description</th>
                                                <th style="text-align: left; width: 10%;">Currency</th>
                                                <th style="text-align: right; width: 10%;">QTY</th>
                                                <th style="text-align: right; width: 10%;">Pricing </th>
                                                <th style="text-align: right; width: 10%;">Total Price </th>
                                                <th style="text-align: center;">#</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                             <?php 
                                                $i=1;
                                                $th_rate = 0;
                                                $usd_rate = 0;
                                                foreach($order as $rs_o) {

                                                $remark = $rs_o['remark'];
                                                $job_date = $rs_o['job_date'];
                                                $roe = $rs_o['ex_rate'];

                                                $th_rate += $rs_o['th_rate'];
                                                $usd_rate += $rs_o['usd_rate'];

                                                ?>
                                                    <tr class="r-order" data-rang="<?php echo $i; ?>">
                                                        <input type="hidden" id="order_id-<?php echo $i; ?>" value="<?php echo $rs_o['order_id']; ?>">
                                                        <td>
                                                            <select class="form-control type" name="type" id="type-<?php echo $i; ?>">
                                                                    <option value="0">-----</option> 
                                                                <?php foreach ($job_type as $rs) { ?>
                                                                    <option value="<?php echo $rs['job_id']; ?>" <?php if($rs['job_id'] == $rs_o['job_id']){ echo 'selected'; } ?>><?php echo $rs['job_code']; ?> | <?php echo $rs['job_desc']; ?></option>      
                                                                <?php } ?>                 
                                                            </select>  
                                                        </td>
                                                        <td style="text-align: left;"><p id="tariff_des-<?php echo $i; ?>"><?php echo $rs_o['job_desc']; ?></p></td>
                                                        <td style="text-align: left;">
                                                            <select class="form-control proj_cur" name="proj_cur" id="proj_cur-<?php echo $i; ?>" >
                                                                <?php foreach ($job_cur_type as $rs_cur ) {  ?>
                                                                <option value="<?php echo $rs_cur['jc_cur']; ?>" <?php if($rs_o['currency'] == $rs_cur['jc_cur']){ echo 'selected'; } ?>><?php echo $rs_cur['jc_cur']; ?></option>     
                                                                <?php } ?>            
                                                            </select> 
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <input type="number" name="qty" class="form-control value-split" id="qty-<?php echo $i; ?>" value="<?php echo $rs_o['qty']; ?>" style="text-align: right;">  
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <input type="number" id="rate-pro-<?php echo $i; ?>" class="form-control unit_rate" style="float: right;text-align: right;"  value="<?php echo $rs_o['cur_rate']; ?>">

                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="rate_p-<?php echo $i; ?>"><?php echo $rs_o['pricing']; ?></p>
                                                            <input type="hidden" id="c_rate_p-<?php echo $i; ?>" name="c_rate_p-<?php echo $i; ?>" value="<?php echo $rs_o['pricing']; ?>">

                                                            <input type="hidden" id="th_rate-<?php echo $i; ?>" name="th_rate-<?php echo $i; ?>" value="<?php echo $rs_o['th_rate']; ?>">
                                                            <input type="hidden" id="usd_rate-<?php echo $i; ?>" name="usd_rate-<?php echo $i; ?>" value="<?php echo $rs_o['usd_rate']; ?>">
                                                        </td>
                                                        <td  style="text-align: center;"> 
                                                            <button class="btn btn-danger order_del"><i class="fa fa-remove"></i></button> 
                                                        </td>
                                                    </tr>  
                                            <?php 

                                                $i++;}  ?>  
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <td colspan="6" style="text-align: right;"></td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-success add-row-cntr"><i class="fa fa-plus-circle"></i></button>
                                                </td>
                                            </tr>
                                            
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"><b style="font-size: 17px;">USD<b></td>
                                                <td style="text-align: right;"><p id="sum_total"><?php echo number_format($usd_rate,2,".",","); ?></p></td>
                                            </tr>
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"><b style="font-size: 17px;">THB<b></td>
                                                <td style="text-align: right;"><p id="sum_total_TH"><?php echo number_format($th_rate,2,".",","); ?></p></td>
                                            </tr>
                                        </tbody>

                                    </table>

                                  
                                </div>
                                 <div class="col-lg-8">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Remark 
                                            </div>
                                            <?php 

                                                $pieces = explode("<br>", $remark);

                                            ?>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-1"><?php if(count($pieces) >= 1){ echo $pieces[0];}?></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-2"><?php if(count($pieces) >= 2){ echo $pieces[1]; } ?></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-3"><?php if(count($pieces) >= 3){ echo $pieces[2]; } ?></textarea>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->

                                    <div class="col-lg-4">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Option 
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12"> 
                                                        <div class="col-lg-6"> 
                                                            <label>Job Date</label>
                                                                <input class="form-control" type="date" name="job_date" id="job_date" value="<?php echo $job_date; ?>">
                                                            <br>
                                                        </div>
                                                        <div class="col-lg-6"> 
                                                            <label>Exchange Rate</label>
                                                            <input class="form-control" type="number" name="roe" id="roe" value="<?php echo $roe; ?>" style="text-align:right;">
                                                        </div>

                                                        <div class="col-lg-12" align="right"> 
                                                            <br>
                                                            
                                                            <button class="btn btn-warning printDr" title="Generate Job Order"><span class="glyphicon glyphicon glyphicon-floppy-saved"></span>&nbsp;&nbsp;Update</button>
                                                            <a href="<?php echo site_url(); ?>Job/All"><button class="btn btn-danger can-dr" title="Cancel"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;&nbsp;Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


                                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Vendor
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-6">
                                        <label>Name</label>
                                        <input type="text" id="vendor_nm" name="vendor_nm" class="form-control" value="<?php echo $vendor->vendor_nm; ?>"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label>Volume</label>
                                        <input type="text" id="vendor_qty" name="vendor_qty" class="form-control" value="<?php echo $vendor->vendor_qty; ?>"> 
                                    </div>
                                    
                                    <br><br><br><br>
                                    <div class="col-md-6">
                                        <label>Pricing</label>
                                        <input type="text" id="vendor_rate" name="vendor_rate" class="form-control" value="<?php echo $vendor->vendor_rate; ?>"> 
                                    </div>
                                    <div class="col-lg-6"> 
                                        <label>Currency</label>
                                        <select class="form-control type" name="vendor_cur" id="vendor_cur" >
                                            <?php foreach ($job_cur_type as $rs_cur ) {  ?>
                                                <option value="<?php echo $rs_cur['jc_cur']; ?>"><?php echo $rs_cur['jc_cur']; ?></option>     
                                            <?php } ?>                
                                        </select> 
                                   </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->

                            <br><br>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                   <div class="panel-heading">
                                        Remark 
                                    </div>
                                    <?php 

                                                $pieces_v = explode("<br>", $vendor->vendor_remark);

                                            ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4"> 
                                                <textarea class="form-control" rows="5" id="vendor_remark-1"><?php if(count($pieces_v) >= 1){ echo $pieces_v[0];}?></textarea>
                                            </div>
                                            <div class="col-md-4"> 
                                                <textarea class="form-control" rows="5" id="vendor_remark-2"><?php if(count($pieces_v) >= 2){ echo $pieces_v[1];}?></textarea>
                                            </div>
                                            <div class="col-md-4"> 
                                                <textarea class="form-control" rows="5" id="vendor_remark-3"><?php if(count($pieces_v) >= 3){ echo $pieces_v[2];}?></textarea>
                                            </div>
                                        </div>
                                                <!-- /.row (nested) -->
                                    </div>
                                            <!-- /.panel-body -->
                                </div>
                                        <!-- /.panel -->
                            </div>
                                    <!-- /.col-lg-12 -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row --> 

</div>
<input type="hidden" name="cur_usd" id="cur_usd" value="">

<input type="hidden" name="define_job" id="define_job" value="<?php echo $job_cat; ?>">
<input type="hidden" name="ref" id="ref" value="">
<input type="hidden" name="id_comp" id="id_comp" value="">
<input type="hidden" name="v_name" id="v_name" value="">
<input type="hidden" name="v_visit" id="v_visit" value="">
<input type="hidden" name="v_voyin" id="v_voyin" value="">
<input type="hidden" name="v_voyout" id="v_voyout" value="">
<input type="hidden" name="v_atb" id="v_atb" value="">
<input type="hidden" name="v_atd" id="v_atd" value="">
<input type="hidden" name="v_tonnage" id="v_tonnage" value="">

<input type="hidden" name="rm_order" id="rm_order" value="">

<input type="hidden" id="numOrder" value="<?php echo count($order); ?>">
<input type="hidden" name="row_c" id="row_c" value="<?php echo count($order); ?>">
<input type="hidden" name="row_last" id="row_last" value="<?php echo count($order); ?>">
<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/lib/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

            $('#rm_order').val('');

            $('#dr').on('click', 'button.order_del', function(events){

                var $row = $(this).parents('tr.r-order');
                var rang = $row.data('rang');

                var order_id = $('#order_id-'+rang).val();

                if(order_id != 'NA'){

                    var rm_order = $('#rm_order').val();

                    if(rm_order == null || rm_order == ''){
                        var rm_order = $('#rm_order').val(order_id);
                    } else {
                        var rm_order = $('#rm_order').val(rm_order+', '+order_id);
                    }
                }


                $(this).parents('.r-order').remove();
            });


            $(".add-row-cntr").click( function(e) {
                e.preventDefault();

                    var define_job = $('#define_job').val();
                    var row_c = parseInt($('#row_last').val())+1;
                    //var SVC_C = $('#svc_v').val();

                    /*$("#dr").append('<tr class="r-order" data-rang="'+row_c+'"><td> <select class="form-control type" name="type" id="type-'+row_c+'"> </select> </td> <td style="text-align: left;"><p id="tariff_des-'+row_c+'">-</p></td> <td style="text-align: right;"> <input type="number" name="qty" class="form-control value-split" id="qty-'+row_c+'" value="0" style="text-align: right;"> </td> <td style="text-align: right;"><p id="rate_u-'+row_c+'">0.00</p> <input type="number" id="rate-pro-'+row_c+'" class="form-control unit_rate" style=" display:none; float: right;text-align: right;"> </td>  <td  style="text-align: center;"> <button class="btn btn-danger order_del"><i class="fa fa-remove"></i></button> </td> </tr>');  */

                    $("#dr").append('<tr class="r-order" data-rang="'+row_c+'"><input type="hidden" id="order_id-'+row_c+'" value="NA"> <td> <select class="form-control type" name="type" id="type-'+row_c+'"> </select> </td> <td style="text-align: left;"><p id="tariff_des-'+row_c+'">-</p></td> <td style="text-align: left;"> <select class="form-control proj_cur" name="proj_cur" id="proj_cur-'+row_c+'"> </select> </td><td style="text-align: right;"> <input type="number" name="qty" class="form-control value-split" id="qty-'+row_c+'" value="0" style="text-align: right;"> </td> <td style="text-align: right;"><p id="rate_u-'+row_c+'">0.00</p> <input type="number" id="rate-pro-'+row_c+'" class="form-control unit_rate" style=" display:none; float: right;text-align: right;"> </td> <td style="text-align: right;"> <p id="rate_p-'+row_c+'">0.00</p> <input type="hidden" id="c_rate_p-'+row_c+'" name="c_rate_p-'+row_c+'"> <input type="hidden" id="th_rate-'+row_c+'" name="th_rate-'+row_c+'"> <input type="hidden" id="usd_rate-'+row_c+'" name="usd_rate-'+row_c+'"> </td> <td  style="text-align: center;"> <button class="btn btn-danger order_del"><i class="fa fa-remove"></i></button> </td> </tr>'); 

                    $.ajax({
                        url : '<?php echo site_url(); ?>Job/GetTariff',
                        data : {define_job:define_job},
                        method : 'POST',
                        cache: false

                    }).done(function(data){

                        var o = JSON.parse(data);
                        var i = 0;

                        var pod_order = '';

                                pod_order += '<option value="0">';
                                pod_order += '-----';
                                pod_order += '</option>';

                       for(i=0; i < o.length; i++){

                                pod_order += '<option value="'+o[i]['job_id']+'">';
                                pod_order += o[i]['job_code']+" | "+o[i]['job_desc'];
                                pod_order += '</option>';
                        }

                        $('#type-'+row_c).append(pod_order);

                        $.ajax({
                            url : '<?php echo site_url(); ?>Job/GetCur',
                            method : 'POST',
                            cache: false

                        }).done(function(data){

                            var o = JSON.parse(data);
                            var i = 0;

                            var proj_cur = '';

                           for(i=0; i < o.length; i++){

                                    proj_cur += '<option value="'+o[i]['jc_cur']+'">';
                                    proj_cur += o[i]['jc_cur'];
                                    proj_cur += '</option>';
                            }

                            $('#proj_cur-'+row_c).append(proj_cur);


                        });

                    });

                    $('#row_c').val(parseInt(row_c));
                    $('#row_last').val(parseInt(row_c));

                    return false;

            });





//$('.type').change(function(){
$("table").off("change", ".type");
        $("table").on("change", ".type", function(e) {

    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang');
    var id_type = $(this).val();

    $('#qty-' + rang).val('0');
    $('#rate-pro-' + rang).val('');
    $('#rate_u-' + rang).html('0.00');
    $('#total-' + rang).html('0.00');
    $('#vat-pro-' + rang).val('0.00');
    $('#vat_u-' + rang).html('0.00');
    $('#holdtax-pro-' + rang).val('0.00');
    $('#holdtax_u-' + rang).html('0.00'); 
    $('#tariff_des-' + rang).html('-'); 
    $('#unit_text-' + rang).html(' ');
    var numOrder = parseInt($('#row_c').val())-1;

            $.ajax({
                url:'<?php echo site_url(); ?>Job/getRate',
                method:'POST',
                data:{ 
                    id_type:id_type
                }
            }).done(function(data){
                var o = JSON.parse(data);
                var cur_rate = o.rate;
                var tariff_code = o.tariff_code;
                var tariff_des = o.tariff_des;
                var n_rate = parseFloat(cur_rate);
                var n_vat = parseFloat(o.vat);
                var n_holdtax = parseFloat(o.holdtax);

                $('#define_job').val(o.job_cat);

                console.log(n_vat.toFixed(2));
                console.log(n_holdtax.toFixed(2));

                if(cur_rate > 0){
      
                    if(tariff_code == 'BEPSL01'){
                        $('#unit_text-' + rang).html('Days');
                        $('#rate_u-' + rang).hide();
                        $('#rate-pro-' + rang).show();

                        

                        if(parseFloat($('#v_tonnage').val()) <= 750){
                            var v_tonnage = 750;
                        } else {
                            var v_tonnage = parseFloat($('#v_tonnage').val()); 
                        }

                        $('#qty-' + rang).val(v_tonnage);

                        var type = $('#type-' + rang).val();

                        var t_order = v_tonnage * 1;

                        $('#total-' + rang).html(t_order.toLocaleString('en'));
                        $('#cal_total-' + rang).val(t_order);


                        var sum_total = 0;

                        for(i=1; i <= numOrder; i++){

                            var caltotal = parseFloat($('#cal_total-'+i).val());

                            if(isNaN(caltotal)){
                                var check_total = 0;
                            } else {
                                var check_total = caltotal;
                            }

                            console.log(check_total.toFixed(2));

                            console.log(caltotal.toFixed(2));

                            sum_total += check_total;
                        }

                        $('#sum_total').html(numberWithCommas(sum_total.toFixed(2)));
                        $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        $('#tariff_des-' + rang).html(tariff_des); 

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2)); 
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        }
                        
                        console.log("Tax" + n_holdtax);
                        console.log("Vat" + cn_vat);
                        console.log("rang" + rang);
                        

                    } else {
                        $('#rate-pro-' + rang).hide();
                        $('#rate_u-' + rang).show();

                    }
                        $('#total-' + rang).html('0.00');
                        $('#unit_text-' + rang).html('THB');
                        $('#rate-pro-' + rang).val(n_rate);
                        $('#rate_u-' + rang).html(numberWithCommas(n_rate.toFixed(2)));
                        $('#tariff_des-' + rang).html(tariff_des); 
                        $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        //$('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        //$('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2));
                        //$('#total-' + rang).html(n_rate.toLocaleString('en')); 

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2)); 
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        }


                } else {

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2));
                            $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                            $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        }

                    $('#rate_u-' + rang).hide();
                    $('#rate-pro-' + rang).show();  
                    $('#rate_u-' + rang).html('0.00');
                    $('#total-' + rang).html('0.00');  
                    //$('#tariff_des-' + rang).html('-'); 
                     $('#tariff_des-' + rang).html(tariff_des); 
                }
                

            })
});



$('.printDr').click(function(){

    var numOrder = parseInt($('#row_last').val());

    var rm_order = $('#rm_order').val();
    var job_count = $('#job_count').val();
    var job_no = $('#job_no').val();
    var customers_proj = $('#customers_proj').val();
    var bkg = $('#bkg').val();
    var dlv_from = $('#dlv_from').val();
    var dlv_to = $('#dlv_to').val();
    var vessel = $('#vessel').val();
    var voy_in = $('#voy_in').val();

    var vendor_nm = $('#vendor_nm').val();
    var vendor_qty = $('#vendor_qty').val();
    var vendor_rate = $('#vendor_rate').val();
    var vendor_cur = $('#vendor_cur').val();
    var vendor_remark1 = $('#vendor_remark-1').val();
    var vendor_remark2 = $('#vendor_remark-2').val();
    var vendor_remark3 = $('#vendor_remark-3').val();


    var job_date = $('#job_date').val();
    var roe = $('#roe').val();

    var remark1 = $('#remark-1').val();
    var remark2 = $('#remark-2').val();
    var remark3 = $('#remark-3').val();

    if(customers_proj == null || customers_proj == ''){
        alert('Please fill in Customers / Project');
        abort();
    } 

    if(dlv_from == null || dlv_from == ''){
        alert('Please fill in Delivery From');
        abort();
    }

    if(dlv_to == null || dlv_to == ''){
        alert('Please fill in Delivery To');
        abort();
    }

    if(vendor_qty == null || vendor_qty == ''){
        alert('Please fill in Vendor Volume');
        abort();
    }

    if(vendor_rate == null || vendor_rate == ''){
        alert('Please fill in Vendor Pricing');
        abort();
    }

    if(job_date == null || job_date == ''){
        alert('Please fill in Job Date');
        abort();
    }

    var order_id = [];
    var qty = [];
    var type = [];
    var proj_cur = [];
    var rate_pro = [];
    var c_rate_p = [];

    var th_rate = [];
    var usd_rate = [];
 
    var r = confirm("Confirm to update ?");
    if (r == true) {  



                        for (var i = 1; i <= numOrder; i++) {

                            order_id.push($('#order_id-'+i).val());

                            qty.push($('#qty-'+i).val());

                            type.push($('#type-'+i).val());

                            rate_pro.push($('#rate-pro-'+i).val());

                            proj_cur.push($('#proj_cur-'+i).val());

                            c_rate_p.push($('#c_rate_p-'+i).val());

                            th_rate.push($('#th_rate-'+i).val());

                            usd_rate.push($('#usd_rate-'+i).val());
                        }

                            const order_id_needlea = '0';
                            const order_idInArray = order_id.includes(order_id_needlea);
                            console.log(order_idInArray); // true

                            const th_rate_needlea = '0';
                            const th_rateInArray = th_rate.includes(th_rate_needlea);
                            console.log(th_rateInArray); // true

                            const usd_rate_needlea = '0';
                            const usd_rateInArray = usd_rate.includes(usd_rate_needlea);
                            console.log(usd_rateInArray); // true

                            const proj_cur_needlea = '0';
                            const proj_curInArray = roe.includes(proj_cur_needlea);
                            console.log(proj_curInArray); // true

                            const needlea = '0';
                            const iqInArray = rate_pro.includes(needlea);
                            console.log(iqInArray); // true

                            const needle = '0';
                            const isInArray = type.includes(needle);
                            console.log(isInArray); // true

                            const needleq = '0';
                            const qInArray = qty.includes(needleq);
                            console.log(qInArray); // true

                            if(isInArray == true){

                                alert('Please fil in Type.');

                            } else if(qInArray == true){

                                alert('Please fil in Qty.');

                            } else if(iqInArray == true){

                                alert('Please fil in Pricing.');

                            } else if(order_idInArray == true){

                                alert('Unknown occurred ERROR, Please refresh page and try again.');

                            }   else {

                               $.ajax({
                                    url:'<?php echo site_url(); ?>Job/UpdateJobOrder',
                                    method:'POST',
                                    data:{ 
                                            rm_order:rm_order,
                                            job_count:job_count,
                                            job_no:job_no,
                                            order_id:order_id,
                                            type:type, 
                                            qty:qty,
                                            rate_pro:rate_pro,
                                            customers_proj:customers_proj,
                                            bkg:bkg,
                                            dlv_from:dlv_from,
                                            dlv_to:dlv_to,
                                            vessel:vessel,
                                            voy_in:voy_in,
                                            vendor_nm:vendor_nm,
                                            vendor_qty:vendor_qty,
                                            vendor_rate:vendor_rate,
                                            vendor_cur:vendor_cur,
                                            vendor_remark1:vendor_remark1,
                                            vendor_remark2:vendor_remark2,
                                            vendor_remark3:vendor_remark3,
                                            job_date:job_date,
                                            proj_cur:proj_cur,
                                            c_rate_p:c_rate_p,
                                            th_rate:th_rate,
                                            usd_rate:usd_rate,
                                            roe:roe,
                                            remark1:remark1,
                                            remark2:remark2,
                                            remark3:remark3

                                    }
                                }).done(function(data){

                                    var o = JSON.parse(data);
                                    var job_num = o.job_num;

                                    alert('Complete Update.');
                                    window.location = '<?php echo site_url(); ?>Job/Draft/'+job_num;
                                });

                            }
                           



           
    } 

    
});
    

    $("table").off("input", ".unit_rate");
        $("table").on("input", ".unit_rate", function(e) {
        var $row = $(this).parents('tr.r-order');
        var rang = $row.data('rang'); 

        var proj_cur = $('#proj_cur-' + rang).val();
        var roe = parseFloat($('#roe').val());
        var qty = parseFloat($('#qty-' + rang).val());
        var rate = parseFloat($('#rate-pro-' + rang).val());
        var numOrder = parseInt($('#row_c').val());


        if((roe == null || roe == '' || roe == 0) && proj_cur == 'USD'){
            alert('Please fill in USD Rate.');
            abort();
        }
            if(qty > 0){
                if(rate > 0){

                    var cal_price = (qty*rate);

                    $('#rate_p-' + rang).html(numberWithCommas(parseFloat(cal_price).toFixed(2)));
                    $('#c_rate_p-' + rang).val(parseFloat(cal_price).toFixed(2));
                    if(proj_cur == 'USD'){
                        var th_rate = (qty*rate)*roe;
                        var usd_rate = (qty*rate);
                        $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                        $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                    } else if (proj_cur == 'THB') {
                        if(roe == 0){
                            var th_rate = (qty*rate);
                            $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                            $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));
                        } else {
                            var th_rate = (qty*rate);
                            var usd_rate = (qty*rate)/roe;
                            $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                            $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                        }
                        
                    }

                    var sum_total_usd = 0;
                    var sum_total_th = 0;

                    for(i=1; i <= numOrder; i++){

                        var caltotal_usd = parseFloat($('#usd_rate-'+i).val());
                        var caltotal_th = parseFloat($('#th_rate-'+i).val());

                        if(isNaN(caltotal_usd)){
                            var check_total_usd = 0;
                        } else {
                            var check_total_usd = caltotal_usd;
                        }

                        sum_total_usd += check_total_usd;

                        if(isNaN(caltotal_th)){
                            var check_total_th = 0;
                        } else {
                            var check_total_th = caltotal_th;
                        }

                        sum_total_th += check_total_th;
                    }


                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                    $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));

                } else if(rate == 0) {
                    $('#th_rate-' + rang).val(parseFloat(0).toFixed(2));
                    $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));

                    for(i=1; i <= numOrder; i++){

                        var caltotal_usd = 0;
                        var caltotal_th = 0;

                        if(isNaN(caltotal_usd)){
                            var check_total_usd = 0;
                        } else {
                            var check_total_usd = caltotal_usd;
                        }

                        sum_total_usd += check_total_usd;

                        if(isNaN(caltotal_th)){
                            var check_total_th = 0;
                        } else {
                            var check_total_th = caltotal_th;
                        }

                        sum_total_th += check_total_th;
                    }

                    if(isNaN(sum_total_th)){
                        var sum_total_th = 0;
                    } else {
                        var sum_total_th = sum_total_th;
                    }

                    if(isNaN(sum_total_usd)){
                        var sum_total_usd = 0;
                    } else {
                        var sum_total_usd = sum_total_usd;
                    }

                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                    $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));
                } else {
                    $('#rate-pro-' + rang).val(0);
                    alert('Please fill in Pricing');
                }
            }

    });


    $("table").off("input", ".value-split");
        $("table").on("input", ".value-split", function(e) {
        var $row = $(this).parents('tr.r-order');
        var rang = $row.data('rang'); 

        var proj_cur = $('#proj_cur-' + rang).val();
        var roe = parseFloat($('#roe').val());
        var qty = parseFloat($('#qty-' + rang).val());
        var rate = parseFloat($('#rate-pro-' + rang).val());
        var numOrder = parseInt($('#row_c').val());

        if((roe == null || roe == '' || roe == 0) && proj_cur == 'USD'){
            alert('Please fill in USD Rate.');
            abort();
        }
            if(qty > 0){
                if(rate > 0){

                    var cal_price = (qty*rate);

                    $('#rate_p-' + rang).html(numberWithCommas(parseFloat(cal_price).toFixed(2)));
                    $('#c_rate_p-' + rang).val(parseFloat(cal_price).toFixed(2));
                    if(proj_cur == 'USD'){
                        var th_rate = (qty*rate)*roe;
                        var usd_rate = (qty*rate);
                        $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                        $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                    } else if (proj_cur == 'THB') {
                        if(roe == 0){
                            var th_rate = (qty*rate);
                            $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                            $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));
                        } else {
                            var th_rate = (qty*rate);
                            var usd_rate = (qty*rate)/roe;
                            $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                            $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                        }
                        
                    }

                    var sum_total_usd = 0;
                    var sum_total_th = 0;

                    for(i=1; i <= numOrder; i++){

                        var caltotal_usd = parseFloat($('#usd_rate-'+i).val());
                        var caltotal_th = parseFloat($('#th_rate-'+i).val());

                        if(isNaN(caltotal_usd)){
                            var check_total_usd = 0;
                        } else {
                            var check_total_usd = caltotal_usd;
                        }

                        sum_total_usd += check_total_usd;

                        if(isNaN(caltotal_th)){
                            var check_total_th = 0;
                        } else {
                            var check_total_th = caltotal_th;
                        }

                        sum_total_th += check_total_th;
                    }


                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                    $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));

                } else if(rate == 0) {
                    $('#th_rate-' + rang).val(parseFloat(0).toFixed(2));
                    $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));

                    for(i=1; i <= numOrder; i++){

                        var caltotal_usd = 0;
                        var caltotal_th = 0;

                        if(isNaN(caltotal_usd)){
                            var check_total_usd = 0;
                        } else {
                            var check_total_usd = caltotal_usd;
                        }

                        sum_total_usd += check_total_usd;

                        if(isNaN(caltotal_th)){
                            var check_total_th = 0;
                        } else {
                            var check_total_th = caltotal_th;
                        }

                        sum_total_th += check_total_th;
                    }

                    if(isNaN(sum_total_th)){
                        var sum_total_th = 0;
                    } else {
                        var sum_total_th = sum_total_th;
                    }

                    if(isNaN(sum_total_usd)){
                        var sum_total_usd = 0;
                    } else {
                        var sum_total_usd = sum_total_usd;
                    }

                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                    $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));
                } else {
                    $('#rate-pro-' + rang).val(0)
                    alert('Please fill in Pricing');
                }
            }  else if(qty == 0) {
                    $('#th_rate-' + rang).val(parseFloat(0).toFixed(2));
                    $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));

                    for(i=1; i <= numOrder; i++){

                        var caltotal_usd = 0;
                        var caltotal_th = 0;

                        if(isNaN(caltotal_usd)){
                            var check_total_usd = 0;
                        } else {
                            var check_total_usd = caltotal_usd;
                        }

                        sum_total_usd += check_total_usd;

                        if(isNaN(caltotal_th)){
                            var check_total_th = 0;
                        } else {
                            var check_total_th = caltotal_th;
                        }

                        sum_total_th += check_total_th;
                    }

                    if(isNaN(sum_total_th)){
                        var sum_total_th = 0;
                    } else {
                        var sum_total_th = sum_total_th;
                    }

                    if(isNaN(sum_total_usd)){
                        var sum_total_usd = 0;
                    } else {
                        var sum_total_usd = sum_total_usd;
                    }

                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                    $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));
                } else {
                    $('#qty-' + rang).val(0);
                    alert('Please fill in QTY');
                }

    });

    $("table").off("change", ".proj_cur");
            $("table").on("change", ".proj_cur", function(e) {  

            var $row = $(this).parents('tr.r-order');
            var rang = $row.data('rang'); 

            var proj_cur = $(this).val();
            var roe = parseFloat($('#roe').val());
            var qty = parseFloat($('#qty-' + rang).val());
            var rate = parseFloat($('#rate-pro-' + rang).val());
            var numOrder = parseInt($('#row_c').val());
            
           if((roe == null || roe == '' || roe == 0) && proj_cur == 'USD'){
                alert('Please fill in USD Rate.');
                abort();
            }
                if(qty > 0){
                    if(rate > 0){

                        var cal_price = (qty*rate);

                        $('#rate_p-' + rang).html(numberWithCommas(parseFloat(cal_price).toFixed(2)));
                        $('#c_rate_p-' + rang).val(parseFloat(cal_price).toFixed(2));
                        if(proj_cur == 'USD'){
                            var th_rate = (qty*rate)*roe;
                            var usd_rate = (qty*rate);
                            $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                            $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                        } else if (proj_cur == 'THB') {
                            if(roe == 0){
                                var th_rate = (qty*rate);
                                $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                                $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));
                            } else {
                                var th_rate = (qty*rate);
                                var usd_rate = (qty*rate)/roe;
                                $('#th_rate-' + rang).val(parseFloat(th_rate).toFixed(2));
                                $('#usd_rate-' + rang).val(parseFloat(usd_rate).toFixed(2));
                            }
                            
                        }

                        var sum_total_usd = 0;
                        var sum_total_th = 0;

                        for(i=1; i <= numOrder; i++){

                            var caltotal_usd = parseFloat($('#usd_rate-'+i).val());
                            var caltotal_th = parseFloat($('#th_rate-'+i).val());

                            if(isNaN(caltotal_usd)){
                                var check_total_usd = 0;
                            } else {
                                var check_total_usd = caltotal_usd;
                            }

                            sum_total_usd += check_total_usd;

                            if(isNaN(caltotal_th)){
                                var check_total_th = 0;
                            } else {
                                var check_total_th = caltotal_th;
                            }

                            sum_total_th += check_total_th;
                        }


                        $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                        $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));

                    } else if(rate == 0) {
                        $('#th_rate-' + rang).val(parseFloat(0).toFixed(2));
                        $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));

                        for(i=1; i <= numOrder; i++){

                            var caltotal_usd = 0;
                            var caltotal_th = 0;

                            if(isNaN(caltotal_usd)){
                                var check_total_usd = 0;
                            } else {
                                var check_total_usd = caltotal_usd;
                            }

                            sum_total_usd += check_total_usd;

                            if(isNaN(caltotal_th)){
                                var check_total_th = 0;
                            } else {
                                var check_total_th = caltotal_th;
                            }

                            sum_total_th += check_total_th;
                        }

                        if(isNaN(sum_total_th)){
                            var sum_total_th = 0;
                        } else {
                            var sum_total_th = sum_total_th;
                        }

                        if(isNaN(sum_total_usd)){
                            var sum_total_usd = 0;
                        } else {
                            var sum_total_usd = sum_total_usd;
                        }

                        $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                        $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));
                    } else {
                        $('#rate-pro-' + rang).val(0)
                        alert('Please fill in Pricing');
                    }
                }  else if(qty == 0) {
                        $('#th_rate-' + rang).val(parseFloat(0).toFixed(2));
                        $('#usd_rate-' + rang).val(parseFloat(0).toFixed(2));

                        for(i=1; i <= numOrder; i++){

                            var caltotal_usd = 0;
                            var caltotal_th = 0;

                            if(isNaN(caltotal_usd)){
                                var check_total_usd = 0;
                            } else {
                                var check_total_usd = caltotal_usd;
                            }

                            sum_total_usd += check_total_usd;

                            if(isNaN(caltotal_th)){
                                var check_total_th = 0;
                            } else {
                                var check_total_th = caltotal_th;
                            }

                            sum_total_th += check_total_th;
                        }

                        if(isNaN(sum_total_th)){
                            var sum_total_th = 0;
                        } else {
                            var sum_total_th = sum_total_th;
                        }

                        if(isNaN(sum_total_usd)){
                            var sum_total_usd = 0;
                        } else {
                            var sum_total_usd = sum_total_usd;
                        }

                        $('#sum_total').html(numberWithCommas(parseFloat(sum_total_usd).toFixed(2)));
                        $('#sum_total_TH').html(numberWithCommas(parseFloat(sum_total_th).toFixed(2)));
                    } else {
                        $('#qty-' + rang).val(0);
                        alert('Please fill in QTY');
                    }

    });

});
</script>
<script type="text/javascript">
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>