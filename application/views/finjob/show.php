<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Job Order</h4>


       
    </div>
    <!-- /.col-lg-12 -->
</div>


<?php if($user->role == 'ADMIN'  OR $user->role  == 'BILLING' OR $user->role  == 'SUPBILLING'){  ?>

<!-- /.row -->
<div class="row">

                 <div class="col-lg-12">
                        &nbsp;&nbsp;
                </div>
               
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url(); ?>Job/Orders" method="POST">
                                <label for="sel1">IS BILL </label>
                                <select class="form-control" name="sort">

                                    <option value="ALL" <?php if($sort == 'ALL'){ echo "selected"; } ?>>ALL</option>
                                    <option value="Y" <?php if($sort == 'Y'){ echo "selected"; } ?>>Y</option>
                                    <option value="N" <?php if($sort == 'N'){ echo "selected"; } ?>>N</option>

                                </select>
                                <label for="sel1">Search Job No. </label>
                                <input type="text" class="form-control" id="search" name="search" value="<?php echo $search; ?>">
                                <button class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i></button>
                                &nbsp;&nbsp;
                            </form>
                        </div>     
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" >
                                    <thead style="font-size:12px;">
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>JOB NO.</th>
                                            <th style="text-align: center;">Type</th>
                                            <th style="text-align: center;">IS BILL</th>
                                            <th style="text-align: Left;">Customers</th>
                                            <th style="text-align: center;">Status</th>
                                            <th>Job Date</th>
                                            <th style="text-align: center;">Supervisor</th>
                                            <th style="text-align: center;">Manager</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size:12px;">
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {

                                                if($rs_normal->type <> 'REJECT'){

                                                $date = new DateTime($rs_normal->job_date);
                                                $now = new DateTime();

                                             $date->diff($now)->format("%d");

                                            ?>
   
                                                    <tr class="r-invoice" data-jobid="<?php echo $rs_normal->job_no; ?>">
                                                        <td style="text-align: center;vertical-align: middle;"><?php echo $i; ?></td>
                                                        <td style="vertical-align: middle;">
                                                            <a href="<?php echo site_url(); ?>Job/Detail/<?php echo $rs_normal->job_no; ?>" target="_blank"><?php echo $rs_normal->job_no;  ?></a>
                                                        </td>
                                                        <td style="text-align: center;vertical-align: middle;"><?php echo $rs_normal->job_cat; ?></td>
                                                        <td style="text-align: center;vertical-align: middle;"><?php echo $rs_normal->fin_status; ?></td>
                                                        <td style="text-align: Left;vertical-align: middle;"><?php echo $rs_normal->customers_proj; ?></td>
                                                        <td style="text-align: center;vertical-align: middle;">
                                                            <?php 
                                                                 if($rs_normal->type == 'COMPLETE'){
                                                                    echo '<p style="color:green;"><b>COMPLETE</b></p>'; 
                                                                }  else if($rs_normal->type == 'DRAFT') {
                                                                    echo '<p style="color:orange;"><b>DRAFT</b></p>'; 
                                                                }  else if($rs_normal->type == 'PROCESS') {
                                                                    echo '<p style="color:#76c9e5;"><b>PROCESS</b></p>'; 
                                                                } else  {
                                                                    echo '<p style="color:red;"><b>CANCEL</b></p>'; 
                                                                }
                                                            ?>
                                                        </td>
                                                        <td style="vertical-align: middle;"><?php echo date("j-M-Y", strtotime($rs_normal->job_date)); ?></td>
                                                        <td style="text-align: center;vertical-align: middle;">
                                                            <?php 
                                                                if($rs_normal->sup_status == 'APPROVE'){
                                                                    echo '<p style="color:green;"><b>APPROVE</b> <br></p>'; 
                                                                }  else if($rs_normal->sup_status == 'WAIT') {
                                                                    echo '<p style="color:orange;"><b>WAIT</b></p>'; 
                                                                }  else  {
                                                                    echo '<p style="color:red;"><b>REJECT</b><br></p>'; 
                                                                }

                                                                echo date("j-M-Y H:i", strtotime($rs_normal->sup_tm));
                                                            ?>
                                                        </td>
                                                        <td style="text-align: center;vertical-align: middle;">
                                                            <?php 
                                                                if($rs_normal->mng_status == 'APPROVE'){
                                                                    echo '<p style="color:green;"><b>APPROVE</b> <br></p>'; 
                                                                }  else if($rs_normal->mng_status == 'WAIT') {
                                                                    echo '<p style="color:orange;"><b>WAIT</b></p>'; 
                                                                }  else  {
                                                                    echo '<p style="color:red;"><b>REJECT</b><br></p>'; 
                                                                }

                                                                echo date("j-M-Y H:i", strtotime($rs_normal->mng_tm));
                                                            ?>
                                                        </td>
                                                        <td style="vertical-align: middle;"><?php echo date("j-M-Y H:i", strtotime($rs_normal->created)); ?></td>
                                                        <td style="vertical-align: middle;"><?php echo date("j-M-Y H:i", strtotime($rs_normal->updated)); ?></td>
                                                        <td align="center">
                                                            <?php if($rs_normal->type == 'COMPLETE'){ ?>
                                                            <a href="<?php echo site_url(); ?>Job/Generate/<?php echo $rs_normal->job_no; ?>" target="_blank" title="Print Job Order No : <?php echo $rs_normal->job_no; ?>"><button class="btn btn-success"><span class="glyphicon glyphicon-print"></span></button></a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                            
                                          
                                        <?php 

                                                } //if Complete job order : Finance

                                        $i++; } 

                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="9" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>
<?php } ?>

<!-- Modal -->
<div id="remarkdr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p id="head-txt-cancel"></p>
            <p>*Please Remark for Cancel *</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="job_no_cancel" id="job_no_cancel" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default c-re" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>


<!-- Modal -->
<div id="reject_sup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p id="reg-rej-sup"></p>
            <p>*Please Remark for Reject *</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment_reject_sup"></textarea>
                    <input type="hidden" name="job_no_reject_sup" id="job_no_reject_sup" value="">
                    <font color="red"><p id="msg-error-remark-reject-sup"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark-rej-sup">Save</button>
                    <button type="button" class="btn btn-default c-re" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>

<!-- Modal -->
<div id="reject_mng" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p id="reg-rej-mng"></p>
            <p>*Please Remark for Reject *</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment_reject_mng"></textarea>
                    <input type="hidden" name="job_no_reject_mng" id="job_no_reject_mng" value="">
                    <font color="red"><p id="msg-error-remark-reject-mng"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark-rej-mng">Save</button>
                    <button type="button" class="btn btn-default c-re" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>

<form id="form-multi" action="<?php echo site_url(); ?>Speacial/MultiPreview" enctype="multipart/form-data" method="post" enctype="multipart/form-data">
    <input type="hidden" id="multiinv" name="multiinv" value="">
</form>

 <input type="hidden" id="origin_comp" name="origin_comp" value="">

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){

    $('.c-re').click(function(){

        window.location = '<?php echo site_url(); ?>Job/All/';
          
    });

    $("table").off("change", ".job_change_status");
        $("table").on("change", ".job_change_status", function(e) {

            var $row = $(this).parents('tr.r-invoice');
            var jobid = $row.data('jobid');

            var job_type = $(this).val();

            if(job_type == 'COMPLETE' || job_type == 'PROCESS' || job_type == 'APPROVE'){

                var r = confirm('Confirm change status of job number : "'+jobid+'" to "'+job_type+'" ?');
                if (r == true) {  

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/ChangeJobCat',
                        method:'POST',
                        data:{ 

                                jobid:jobid,
                                job_type:job_type

                             }
                    }).done(function(data){
                        var o = JSON.parse(data);
                        alert(o.msg);
                        location.reload();

                    });

                } else {
                    window.location = '<?php echo site_url(); ?>Job/All/';
                }

            } else if (job_type == 'CANCEL'){
                $('#head-txt-cancel').html('Cancel Job Number : '+jobid);
                $('#job_no_cancel').val(jobid);
                $('#remarkdr').modal('show');
            }
            
        });

    $("table").off("change", ".sup_change_status");
        $("table").on("change", ".sup_change_status", function(e) {

            var $row = $(this).parents('tr.r-invoice');
            var jobid = $row.data('jobid');

            var job_type = $(this).val();

            if(job_type == 'APPROVE'){

                var r = confirm('Confirm change status of job number : "'+jobid+'" to "'+job_type+'" ?');
                if (r == true) {  

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/ChangeSupJobCat',
                        method:'POST',
                        data:{ 

                                jobid:jobid,
                                job_type:job_type

                             }
                    }).done(function(data){
                        var o = JSON.parse(data);
                        alert(o.msg);
                        location.reload();

                    });

                } else {
                    window.location = '<?php echo site_url(); ?>Job/All/';
                }

            } else if (job_type == 'REJECT'){
                $('#reg-rej-sup').html('Reject Job Number : '+jobid);
                $('#job_no_reject_sup').val(jobid);
                $('#reject_sup').modal('show');
            }
            
        });


     $("table").off("change", ".mng_change_status");
        $("table").on("change", ".mng_change_status", function(e) {

            var $row = $(this).parents('tr.r-invoice');
            var jobid = $row.data('jobid');

            var job_type = $(this).val();

            if(job_type == 'APPROVE'){

                var r = confirm('Confirm change status of job number : "'+jobid+'" to "'+job_type+'" ?');
                if (r == true) {  

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/ChangeMngJobCat',
                        method:'POST',
                        data:{ 

                                jobid:jobid,
                                job_type:job_type

                             }
                    }).done(function(data){
                        var o = JSON.parse(data);
                        alert(o.msg);
                        location.reload();

                    });

                } else {
                    window.location = '<?php echo site_url(); ?>Job/All/';
                }

            } else if (job_type == 'REJECT'){
                $('#reg-rej-mng').html('Reject Job Number : '+jobid);
                $('#job_no_reject_mng').val(jobid);
                $('#reject_mng').modal('show');
            }
            
    });

    $('.save-remark').click(function(){

            var remark_job = $('#comment').val();
            var job_no_cancel = $('#job_no_cancel').val();

            if(remark_job == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel*');
            } else {

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/Cancel',
                            method:'POST',
                            data:{ job_no_cancel:job_no_cancel, remark_job:remark_job }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       window.location = '<?php echo site_url(); ?>Job/All/';
                    })
            }
          
    });

    $('.save-remark-rej-sup').click(function(){

            var comment_reject = $('#comment_reject_sup').val();
            var jobid = $('#job_no_reject_sup').val();
            var job_type = 'REJECT';

            if(comment_reject == ''){
                $('#msg-error-remark-reject-sup').html('*Please Remark for Reject*');
            } else {

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/ChangeSupJobCat',
                            method:'POST',
                            data:{ jobid:jobid, job_type:job_type, comment_reject:comment_reject }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       window.location = '<?php echo site_url(); ?>Job/All/';
                    })
            }
          
    });


    $('.save-remark-rej-mng').click(function(){

            var comment_reject = $('#comment_reject_mng').val();
            var jobid = $('#job_no_reject_mng').val();
            var job_type = 'REJECT';

            if(comment_reject == ''){
                $('#msg-error-remark-reject-mng').html('*Please Remark for Reject*');
            } else {

                    $.ajax({
                        url:'<?php echo site_url(); ?>Job/ChangeMngJobCat',
                            method:'POST',
                            data:{ jobid:jobid, job_type:job_type, comment_reject:comment_reject }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       window.location = '<?php echo site_url(); ?>Job/All/';
                    })
            }
          
    });


    });

</script>