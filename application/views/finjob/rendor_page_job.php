	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>

	<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b><u>JOB ORDER</u></b></h3>
		<h4 align="center"><b><?php echo $job_cat; ?></b></h4>
						
						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b></b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>Customers/Project</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $customers_proj; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-M-Y", strtotime($job_date)); ?></p></td>
							</tr>
							<?php if($vessel) { ?>
							<tr>
								<td colspan="1"><p style="font-size: 10px;"><b>Vessel</b></p></td>
								<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $vessel; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Voy</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $voy_in; ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan="1"><p style="font-size: 10px;"><b>Booking</b></p></td>
								<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $bkg; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Job No</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $job_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"><p style="font-size: 10px;"><b>From</b></p></td>
								<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $dlv_from; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>To</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $dlv_to; ?></td>
							</tr>
							<tr>
								<td colspan="1"><p style="font-size: 10px;"><b>Remark</b></p></td>
								<td colspan="3"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $remark; ?></p></td>
							</tr>

						</table>
						<table class="table">
						    <thead>
						    	<tr>
						            <th style="font-size: 15px; text-align: left;" colspan="5">Detail</th>
						        </tr>
						        <tr>
						        	<th style="font-size: 10px; text-align: center;" width="5%">#</th>
						        	<th style="font-size: 10px; text-align: left;" >Code</th>
						            <th style="font-size: 10px; text-align: left;" colspan="2">Description</th>
						            <th style="font-size: 10px; text-align: right;" >QTY</th>
						            <th style="font-size: 10px; text-align: right;"  width="15%">Pricing</th>
						            <th style="font-size: 10px; text-align: right;"  width="15%">Total Price </th>
						        </tr>
						    </thead>
						    <tbody id="book"> 
						    	<?php 
						    	$sum_vol = 0;
						    	$sum_price = 0;
						    	$th_rate = 0;
						    	$usd_rate = 0;
						    	$i = 1;
						    	foreach ($order as $rs_order) {  
						    		$remark = $rs_order['remark'];
						    		$sum_vol += $rs_order['qty'];
						    		$sum_price += $rs_order['cur_rate'];
						    		$job_unit = $rs_order['job_unit'];
						    		$created = $rs_order['created'];
						    		$users_created = $rs_order['users_created'];
						    		$auth_completed = $rs_order['auth_completed'];
						    		$auth_stamp = $rs_order['auth_stamp'];
						    		$cur_cus = $rs_order['currency'];

						    		$users_process = $rs_order['users_process'];
						    		$process_tm = $rs_order['process_tm'];

						    		$th_rate += $rs_order['th_rate'];
						    		$usd_rate += $rs_order['usd_rate'];

						    		$ex_rate = $rs_order['ex_rate'];

						    		$mng_auth = $rs_order['mng_auth'];
						    		$sup_auth = $rs_order['sup_auth'];

						    		$mng_tm = $rs_order['mng_tm'];
						    		$sup_tm = $rs_order['sup_tm'];
						    	 ?>
						          <tr>
						          	<td style="font-size: 10px; text-align:center;"><?php echo $i; ?></td>
						          	<td style="font-size: 10px; text-align:left;"><?php echo $rs_order['job_code']; ?></td>
						          	<td style="font-size: 10px; text-align:left;"colspan="2"><?php echo $rs_order['job_desc']."&nbsp;&nbsp;"; ?></td>
						          	<td style="font-size: 10px; text-align:right;">
						          		<?php echo number_format($rs_order['qty']).' / '.$rs_order['job_unit']; ?>
						          	</td>
						          	<td style="font-size: 10px; text-align:right;">
						          		<?php echo number_format($rs_order['cur_rate'], 2); ?>
						          	</td>
						          	<td style="font-size: 10px; text-align:right;">
						          		<?php echo number_format($rs_order['pricing'], 2); ?> / <?php echo $rs_order['currency']; ?>
						          	</td>
						          </tr>   
						        <?php $i++; }  ?>

						        
								<tr>
									<td colspan="2" style="text-align:right; font-size: 10px;">
										<?php if($ex_rate > 0) { ?>
										<b><?php echo "exc rate : ".number_format($ex_rate, 2); ?> (THB)</b>
										<?php } else { ?>
										&nbsp;&nbsp;
										<?php } ?>
									</td>
									<td>&nbsp;&nbsp;</td>
									<td style="text-align:right; font-size: 12px;"><b></b></td>
									<td style="text-align:right; font-size: 12px;"><b>Total</b></td>
									<td style="text-align:right; font-size: 12px;">
										<?php if($usd_rate > 0) { ?>
										<b><?php echo number_format($usd_rate, 2); ?> (USD)</b>
										<?php } else { ?>
										&nbsp;&nbsp;
										<?php } ?>
									</td>
									<td style="text-align:right; font-size: 12px;">
										<?php if($th_rate > 0) { ?>
										<b><?php echo number_format($th_rate, 2); ?> (THB)</b>
										<?php } else { ?>
										&nbsp;&nbsp;
										<?php } ?>
									</td>
								</tr>

						          <tr>
						          	<td colspan="7">
						          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						          	</td>
						          </tr>  
						    </tbody>
						</table>

						<table class="table">
							<thead>
						        <tr>
						            <th style="font-size: 15px; text-align: left;" colspan="6">Vendor</th>
						        </tr>
						        <tr>
						            <th style="font-size: 10px; text-align: left;">Name :</th>
						            <th style="font-size: 10px; text-align: left;">Volume :</th>
						            <th style="font-size: 10px; text-align: left;">Price :</th>
						            <th style="font-size: 10px; text-align: left;">Remark :</th>
						        </tr>
						    </thead>
						    <tbody>

						    	<tr>
						    		<td style="font-size: 10px; text-align: left;"><?php echo $vendor->vendor_nm; ?></td>
						    		<td style="font-size: 10px; text-align: left;"><?php echo $vendor->vendor_qty; ?></td>
						    		<td style="font-size: 10px; text-align: left;"><?php echo number_format($vendor->vendor_rate, 2); ?> /&nbsp;&nbsp;<?php echo $vendor->vendor_cur; ?></td>
						    		<td style="font-size: 10px; text-align: left;"><?php echo $vendor->vendor_remark; ?></td>
						    	</tr>
						    	<tr>
						          	<td colspan="6">
						          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						          	</td>
						          </tr>
						    </tbody>
						</table>
						<table class="table">
							<thead>
						        <tr>
						            <th style="font-size: 11px; text-align: center;">Created :</th>
						            <th style="font-size: 11px; text-align: center;">Process :</th>
						            <th style="font-size: 11px; text-align: center;">Completed :</th>
						        </tr>
						    </thead>
						    <tbody>

						    	<tr>
						    		<td style="font-size: 11px; text-align: center;">
						    			<?php echo $users_created; ?><br>
						    			<p style="color:#aaa;">Systems generated signature.</p>
						    			<p ><?php echo date("j-M-Y H:i", strtotime($created)); ?></p>
						    		</td>
						    		<td style="font-size: 11px; text-align: center;">
						    			<?php if($users_process){ ?>
						    				<?php echo $users_process; ?><br>
						    				<p style="color:#aaa;">Systems generated signature.</p>
						    				<p ><?php echo date("j-M-Y H:i", strtotime($process_tm)); ?></p>
						    			<?php } else { ?>
						    				<br>..................................................
						    			<?php } ?>
						    		</td>
						    		<td style="font-size: 11px; text-align: center;">
						    			<?php if($auth_completed){ ?>
						    				<?php echo $auth_completed; ?><br>
						    				<p style="color:#aaa;">Systems generated signature.</p>
						    				<p ><?php echo date("j-M-Y H:i", strtotime($auth_stamp)); ?></p>
						    			<?php } else { ?>
						    				<br>..................................................
						    			<?php } ?>
						    		</td>
						    	</tr>

						    </tbody>
						</table>
						<table class="table">
							<thead>
						        <tr>
						            <th style="font-size: 11px; text-align: center;">Supervisor :</th>
						            <th style="font-size: 11px; text-align: center;">Manager :</th>
						        </tr>
						    </thead>
						    <tbody>

						    	<tr>
						    		<td style="font-size: 11px; text-align: center;">
						    			<?php if($sup_auth){ ?>
							    			<?php echo $sup_auth; ?><br>
							    			<p style="color:#aaa;">Systems generated signature.</p>
							    			<p ><?php echo date("j-M-Y H:i", strtotime($sup_tm)); ?></p>
							    		<?php } else { ?>
						    				<br>..................................................
						    			<?php } ?>
						    		</td>
						    		<td style="font-size: 11px; text-align: center;">
						    			<?php if($mng_auth){ ?>
							    			<?php echo $mng_auth; ?><br>
							    			<p style="color:#aaa;">Systems generated signature.</p>
							    			<p ><?php echo date("j-M-Y H:i", strtotime($mng_tm)); ?></p>
							    		<?php } else { ?>
						    				<br>..................................................
						    			<?php } ?>
						    		</td>
						    	</tr>

						    </tbody>
						</table>

	</div>

	

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		location.reload();
		//document.location.href = "<?php echo site_url(); ?>Speacial/SSR";
	});
});
</script>