<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Covid-2019-Temperature</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

            <div class="col-md-2">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Report Date</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo site_url(); ?>Temperature/Report" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input class="form-control" name="s_date" type="date" autofocus required="true" value="<?php echo $s_date; ?>">
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input class="form-control" name="e_date" type="date" autofocus required="true" value="<?php echo $e_date; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Emp Code</label>
                                    <input class="form-control" name="emp_code" type="text" value="<?php echo $emp_code; ?>">
                                </div>
                                 <div class="form-group">
                                  <label for="sel1">Emp Dept</label>
                                  <select class="form-control" id="emp_dept" name="emp_dept">
                                        <option value="all">All</option>
                                    <?php foreach ($list_dept as $rs) { ?>
                                        <option value="<?php echo $rs['emp_dept']; ?>" <?php if($rs['emp_dept'] == $emp_dept){ echo "selected";} ?>><?php echo $rs['emp_dept']; ?></option>        
                                    <?php } ?>
                                  </select>
                                </div> 
                                  <input class="btn btn-sm btn-info btn-block" type="submit" value="Show"/>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>


            <div class="col-md-10"  id="dr-1">

                <h3>From : <?php echo date("j-F-Y", strtotime($s_date)); ?> To : <?php echo date("j-F-Y", strtotime($e_date)); ?></h3>
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;font-size: 12px;">#</th>
                                            <th style="text-align: left; font-size: 12px;">Emp Code</th>
                                            <th style="text-align: left; font-size: 12px;">Emp Name</th>
                                            <th style="text-align: left; font-size: 12px;">Date</th>
                                            <th style="text-align: left; font-size: 12px;">Emp Dept.</th>
                                            <th style="text-align: right; font-size: 12px;">Temp 09:00</th>
                                            <th style="text-align: left; font-size: 12px;">Time</th>
                                            <th style="text-align: right; font-size: 12px;">Temp 14:00</th>
                                            <th style="text-align: left; font-size: 12px;">Time</th>
                                            <th style="text-align: right; font-size: 12px;">Temp 19:00</th>
                                            <th style="text-align: left; font-size: 12px;">Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($list_emp) {  ?>
                                            <?php $i = 1; foreach ($list_emp as $rs) {  ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: center; font-size: 12px;"><?php echo $i; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_code']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_name']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo date("j-F-Y", strtotime($rs['created'])); ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_dept']; ?></td>
                                                    <td style="text-align: right; font-size: 12px;">
                                                        <?php 

                                                            if($rs['temp_1'] >= 37.5){
                                                                echo '<b style="color:red;">'.$rs['temp_1']; 
                                                            } else {
                                                                echo $rs['temp_1']; 
                                                            }
                                                        
                                                        ?>
                                                    </td>
                                                    <td style="text-align: left; font-size: 12px;">
                                                            <?php 
                                                                if($rs['temp_1']){
                                                                    echo date("H:i", strtotime($rs['stamp_t1'])); 
                                                                }
                                                            ?> 
                                                     </td>
                                                    <td style="text-align: right; font-size: 12px;">
                                                        <?php 

                                                            if($rs['temp_2'] >= 37.5){
                                                                echo '<b style="color:red;">'.$rs['temp_2']; 
                                                            } else {
                                                                echo $rs['temp_2']; 
                                                            }
                                                        
                                                        ?>
                                                   </td>
                                                    <td style="text-align: left; font-size: 12px;">
                                                        <?php 
                                                                if($rs['temp_2']){
                                                                    echo date("H:i", strtotime($rs['stamp_t2'])); 
                                                                }
                                                        ?>
                                                    </td>
                                                    <td style="text-align: right; font-size: 12px;">
                                                        <?php 

                                                            if($rs['temp_3'] >= 37.5){
                                                                echo '<b style="color:red;">'.$rs['temp_3']; 
                                                            } else {
                                                                echo $rs['temp_3']; 
                                                            }
                                                        
                                                        ?>
                                                     </td>
                                                    <td style="text-align: left; font-size: 12px;">
                                                        <?php 
                                                                if($rs['temp_3']){
                                                                    echo date("H:i", strtotime($rs['stamp_t3'])); 
                                                                }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        <?php } else { ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: center;" colspan="11">-No-Record-</td>
                                                </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                                </div>


    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/dist/js/sb-admin-2.js"></script>

</body>

</html>
