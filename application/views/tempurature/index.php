<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Covid-2019-Temperature</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>public/bootstap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>public/bootstap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Temperature</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo site_url(); ?>Temperature/SearchEmp" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="text" autofocus required="true">
                                </div>
                                <div>
                                  
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                  <input class="btn btn-sm btn-warning btn-block" type="submit" value="ค้นหา"/>
                            </fieldset>
                        </form>
                                <br>
                                 <a href="<?php echo site_url(); ?>Temperature/Report" target="_blank"><input class="btn btn-sm btn-success btn-block" type="button" value="รายงาน"/></a>

                    </div>
                </div>
            </div>

              <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"  id="dr-1">
                                    <h3>Today</h3>
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left; width: 7%; font-size: 12px;">Emp Code</th>
                                            <th style="text-align: left; width: 17%; font-size: 12px;">Emp Name</th>
                                            <th style="text-align: left; width: 15%; font-size: 12px;">Emp Dept.</th>
                                            <th style="text-align: right; width: 15%;">Temp 09:00</th>
                                            <th style="text-align: right; width: 15%;">Temp 14:00</th>
                                            <th style="text-align: right; width: 15%;">Temp 19:00</th>
                                            <th style="text-align: right; width: 15%;">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($list_emp) {  ?>
                                            <?php foreach ($list_emp as $rs) {  ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_code']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_name']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_dept']; ?></td>

                                                    <?php 

                                                    if($l_emp){  ?>
                                                            <td style="text-align: right; font-size: 12px;">
                                                                
                                                                <?php if($l_emp->temp_1 != null OR $l_emp->temp_1 != '' OR !empty($l_emp->temp_1)) { ?>
                                                                    <?php echo $l_emp->temp_1; ?>
                                                                <?php } else { ?>
                                                                    <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text" autofocus required="true">
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_1">
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/>
                                                                        </fieldset>
                                                                    </form>
                                                                <?php } ?>
                                                                    
                                                            </td>
                                                            <td style="text-align: right; font-size: 12px;">

                                                                 <?php if($l_emp->temp_2 != null OR $l_emp->temp_2 != '' OR !empty($l_emp->temp_2)) { ?>
                                                                    <?php echo $l_emp->temp_2; ?>
                                                                <?php } else { ?>
                                                                    <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text" autofocus required="true">
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_2">
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/>
                                                                        </fieldset>
                                                                    </form>
                                                                <?php } ?>
                                                                
                                                            </td>
                                                            <td style="text-align: right; font-size: 12px;">

                                                                <?php if($l_emp->temp_3 != null OR $l_emp->temp_3 != '' OR !empty($l_emp->temp_3)) { ?>
                                                                    <?php echo $l_emp->temp_3; ?>
                                                                <?php } else { ?>
                                                                    <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text" autofocus required="true" <?php if($l_emp->temp_2 == null OR $l_emp->temp_2 == '' OR empty($l_emp->temp_2)) { echo 'disabled="true"'; }?> >
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_3" <?php if($l_emp->temp_2 == null OR $l_emp->temp_2 == '' OR !empty($l_emp->temp_2)) { echo 'disabled="true"'; }?>>
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/>
                                                                        </fieldset>
                                                                    </form>
                                                                <?php } ?>

                                                            </td>

                                                            <td style="text-align: right; font-size: 12px;"><?php echo date("j-F-Y", strtotime($l_emp->created)); ?></td>

                                                    <?php } else { ?>   
                                                            
                                                            <td style="text-align: right; font-size: 12px;">
                                                                
                                                                <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text" autofocus required="true">
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_1">
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/>
                                                                        </fieldset>
                                                                    </form>
                                                                    
                                                            </td>
                                                            <td style="text-align: right; font-size: 12px;">

                                                                <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text"  autofocus required="true" disabled="true">
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_2">
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/ disabled="true">
                                                                        </fieldset>
                                                                    </form>
                                                                
                                                            </td>
                                                            <td style="text-align: right; font-size: 12px;">

                                                                <form action="<?php echo site_url(); ?>Temperature/SaveTemp" method="post" enctype="multipart/form-data">
                                                                        <fieldset>
                                                                            <div class="form-group">
                                                                                <input class="form-control" placeholder="อุณหภูมิ" name="emp_temp" type="text" autofocus required="true" disabled="true">
                                                                                <input class="form-control" placeholder="รหัสพนักงาน" name="emp_code" type="hidden" value="<?php echo $rs['emp_code']; ?>">
                                                                                <input class="form-control" name="type_temp" type="hidden" value="temp_3">
                                                                            </div>
                                                                            <div>
                                                                              
                                                                            </div>
                                                                            <!-- Change this to a button or input when using this as a form -->
                                                                              <input class="btn btn-sm btn-success btn-block" type="submit" value="บันทึก"/ disabled="true">
                                                                        </fieldset>
                                                                    </form>

                                                            </td>

                                                            <td style="text-align: right; font-size: 12px;">-</td>

                                                    <?php } ?>     

                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: center;" colspan="7">-No-Record-</td>
                                                </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>


                                <div class="col-lg-12"  id="dr-1">
                                    <h3>History</h3>
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left; width: 7%; font-size: 12px;">Emp Code</th>
                                            <th style="text-align: left; width: 17%; font-size: 12px;">Emp Name</th>
                                            <th style="text-align: left; width: 15%; font-size: 12px;">Emp Dept.</th>
                                            <th style="text-align: right; width: 15%;">Temp 09:00</th>
                                            <th style="text-align: right; width: 15%;">Temp 14:00</th>
                                            <th style="text-align: right; width: 15%;">Temp 19:00</th>
                                            <th style="text-align: right; width: 15%;">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($emp_temp) {  ?>
                                            <?php foreach ($emp_temp as $rs) {  ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_code']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_name']; ?></td>
                                                    <td style="text-align: left; font-size: 12px;"><?php echo $rs['emp_dept']; ?></td>
                                                    <td style="text-align: right; font-size: 12px;"><?php echo $rs['temp_1']; ?></td>
                                                    <td style="text-align: right; font-size: 12px;"><?php echo $rs['temp_2']; ?></td>
                                                    <td style="text-align: right; font-size: 12px;"><?php echo $rs['temp_3']; ?></td>
                                                    <td style="text-align: right; font-size: 12px;"><?php echo date("j-F-Y", strtotime($rs['created'])); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                                <tr class="r-normal">
                                                    <td style="text-align: center;" colspan="7">-No-Record-</td>
                                                </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>

        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>public/bootstap/dist/js/sb-admin-2.js"></script>

</body>

</html>
