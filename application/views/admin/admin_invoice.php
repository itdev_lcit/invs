<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Admin Control</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Manage Invoice
                        </div>
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Admin/invoice'; ?>" method="POST">
                                <label for="sel1">Search</label>
                                <input class="form-control" type="text" name="search" value="" placeholder=" Draft No. & Inv No.">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>  
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12">
                                    <table class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">#</th>
                                            <th width="15%">Invoice</th>
                                            <th>Draft</th>
                                            <th>Updated</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($results){
                                            $i=1; 
                                              foreach ($results as $rs) { 
	                                    	 
	                                    	?>

		                                      <tr class="r-invoice" data-multi="<?php echo $rs->is_multi; ?>" data-dr_no="<?php echo $rs->dr_no; ?>" data-prefix_invoice ="<?php echo $rs->prefix_invoice; ?>" data-invoice_no ="<?php echo $rs->invoice_no; ?>">
                                                    <td style="text-align: center; vertical-align:center;"><?php echo $i; ?></td>
		                                            <td><?php echo $rs->prefix_invoice.$rs->invoice_no; ?></td>
                                                    <td><?php echo $rs->dr_no; ?></td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs->updated)); ?></td>
                                                    <td style="text-align: center;">
                                                       <?php 
                                                       $type_draft = substr($rs->dr_no,0,3); 
                                                       if($type_draft == "DRN"){
                                                       ?>

                                                       <button class="btn btn-danger btn-xs cancel-normal-invoice" title="Cancel Invoice"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Cancel </button>
                                                       <?php } else { ?>


                                                       <button class="btn btn-danger btn-xs cancel-split-invoice" title="Cancel Invoice"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Cancel</button>
                                                       <?php } ?>
                                                    </td>
		                                        </tr>

                                        <?php

		                                  $i++;  } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>


<!-- Modal -->
<div id="remarkinv" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Remark for Cancel Invoice*</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="mprefix_invoice" id="mprefix_invoice" value="">
                    <input type="hidden" name="minvoice_no" id="minvoice_no" value="">
                    <input type="hidden" name="is_multi" id="is_multi" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("table").off("click", ".cancel-split-invoice");
        $("table").on("click", ".cancel-split-invoice", function(e) {
            e.preventDefault();
            var $row = $(this).parents('tr.r-invoice');


            var prefix_invoice = $row.data('prefix_invoice');
            var invoice_no = $row.data('invoice_no');



            var r = confirm("Confirm to Cancel-Invoice ?");
            if (r == true) {
                $.ajax({
                    url:'<?php echo site_url(); ?>Billing/CancelSplitInvoice',
                        method:'POST',
                        data:{ prefix_invoice:prefix_invoice , invoice_no:invoice_no }
                }).done(function(data){
                   var o = JSON.parse(data);
                   alert(o.msg);
                   location.reload();
                })
            } else {
                
            }
        });

        $("table").off("click", ".cancel-normal-invoice");
        $("table").on("click", ".cancel-normal-invoice", function(e) {
            e.preventDefault();
            
            $('#remarkinv').modal('show');
            var $row = $(this).parents('tr.r-invoice');
            var prefix_invoice = $row.data('prefix_invoice');
            var invoice_no = $row.data('invoice_no');
            var is_multi = $row.data('multi');


            $('#mprefix_invoice').val(prefix_invoice);
            $('#minvoice_no').val(invoice_no);
            $('#is_multi').val(is_multi);
            
        });


$('.save-remark').click(function(){

            var remark_inv = $('#comment').val();
            var prefix_invoice = $('#mprefix_invoice').val();
            var invoice_no = $('#minvoice_no').val();
            var is_multi = $('#is_multi').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel Invoice*');
                console.log('null');
            } else {
                console.log('insert remark');

                    if(is_multi != '1'){

                        $.ajax({
                            url:'<?php echo site_url(); ?>Billing/CancelNormalInvoice',
                                method:'POST',
                                data:{ prefix_invoice:prefix_invoice , invoice_no:invoice_no , remark_inv:remark_inv}
                        }).done(function(data){
                           var o = JSON.parse(data);
                           alert(o.msg);
                           location.reload();
                        })

                    } else {

                        $.ajax({
                            url:'<?php echo site_url(); ?>MultiInvoice/CancelNormalInvoice',
                                method:'POST',
                                data:{ prefix_invoice:prefix_invoice , invoice_no:invoice_no , remark_inv:remark_inv}
                        }).done(function(data){
                           var o = JSON.parse(data);
                           alert(o.msg);
                           location.reload();
                        })

                    }

            }

          
});

    });
</script>