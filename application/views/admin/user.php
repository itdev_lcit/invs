<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Admin Control</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Users
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">

                                		<p id="validate-name" style="color: red;"></p>
                                		<p id="validate-username" style="color: red;"></p>
                                		<p id="validate-password" style="color: red;"></p>
                                    <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="hidden" name="id" id="id">
                                                <input type="text" class="form-control" name="name" id="name" required="true">
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" class="form-control" name="username" id="username" required="true">
                                               
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" id="password" name="password" required="true">
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" id="confirm_password" class="form-control" name="confirm_password" required="true">
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-12">    
                                            <div class="form-group">
                                                <label>Type</label>
                                                <select class="form-control" id="role" name="role">
                                                    <option value="ADMIN">ADMIN</option>
                                                    <option value="DOCUMENT">DOCUMENT</option>
                                                    <option value="BILLING">BILLING</option>
                                                    <option value="SUPBILLING">SUPBILLING</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div align="right">
                                            <button type="button" class="btn btn-outline btn-success save">Add</button>
                                            <a href="<?php echo site_url(); ?>/Admin/Users"><button type="button" class="btn btn-outline btn-warning">Clear</button></a>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-8">
                                    <table class="table table-striped table-bordered table-hover"  id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Updated</th>
                                            <th style="text-align: center;">In Use</th>
                                            <th style="text-align: center;">In System</th>
                                            <th>Last Login</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($user){
                                            $i=1;
	                                    	foreach ($user as $rs) { 
	                                    		
	                                    	?>

		                                         <tr class="r-users" data-id="<?php echo $rs['id']; ?>" data-username="<?php echo $rs['username']; ?>" data-name="<?php echo $rs['name']; ?>" data-role="<?php echo $rs['role']; ?>">
                                                    <td><?php echo $i; ?></td>
		                                            <td><?php echo $rs['name']; ?></td>
                                                    <td><?php echo $rs['username']; ?></td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td style="text-align: center;"><?php if($rs['date_process'] != ''){ echo '<b><font color="green">Online</font></b>'; } else { echo '<b><font color="red">Offline</font></b>'; } ?></td>
                                                    <td style="text-align: center;"><?php 
                                                            if($rs['date_process'] != NULL){
                                                                $differenceFormat = '%i';
                                                                $date_now = date('Y-m-d H:i:s');

                                                                $datetime1 = $rs['date_process'];
                                                                $datetime2 = $date_now;

                                                                $date1=date_create($datetime1);
                                                                $date2=date_create($datetime2);
                                                                $diff=date_diff($date1,$date2);
                                                                echo $diff->format("%h : %i : %s H");
                                                                


                                                            } else {
                                                                echo "-No Active-";
                                                            }
                                                     ?></td>
                                                    <td><?php 
                                                            if($rs['last_date'] != NULL){
                                                                echo date("j-F-Y H:i", strtotime($rs['last_date']));
                                                            } else {
                                                                echo "-No Active-";
                                                            }
                                                     ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-success btn-xs AddUsers"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                                        <button class="btn btn-danger btn-xs RemoveUsers"><span class="glyphicon glyphicon-remove-sign"></span>Delete</button>
                                                    </td>
		                                        </tr>

                                        <?php

		                                  $i++;  } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<!-- Modal -->
<div id="RemoveUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>Confirm to Remove Data.</p>
          </div>
            <form id="FrmVat" role="form" action="<?php site_url();?>DeleteUsers" method="post" enctype="multipart/form-data">
                <input type="hidden" name="remove_id" id="remove_id" value="" class="form-control">
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.AddUsers').click(function(){

          	$('.AddUsers').prop('disabled',true);
            $('.AddUsers').prop('value','Save.....');
            var $row = $(this).parents('tr.r-users');
            var id = $row.data('id');
            var name = $row.data('name');
            var username = $row.data('username');
            var role = $row.data('role');


            $('#id').val('');
            $('#name').val('');
            $('#username').val('');
            $('#role').val();

            $('#id').val(id);
            $('#name').val(name);
            $('#username').val(username);
            $('#role').val(role);
        });

        $('.save').click(function(){
            
            var id = $('#id').val();
            var name = $('#name').val();
            var username = $('#username').val();
            var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();
            var role = $('#role').val();


            $.ajax({
                url:'<?php echo base_url(); ?>Admin/SaveUsers',
                method:'POST',
                data:{ id:id, name:name, username:username, password:password, confirm_password:confirm_password, role:role}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-username').html('');
                    $('#validate-username').append('Username Already.');
                } 

                if (o.msg == 200){
                    $('#validate-name').html('');
                    $('#validate-name').append('Name Already.');
                }

                if (o.msg == 300){
                    $('#validate-password').html('');
                    $('#validate-password').append('Password Not Match.');
                }

                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        });

        $('.btn_succsess').click(function() {
            location.reload();
        });

         $('.RemoveUsers').click(function(){

            var $row = $(this).parents('tr.r-users');
            var id = $row.data('id');
      
            $('#remove_id').val('');
            $('#remove_id').val(id);
            $('#RemoveUsers').modal('show');
        });
    });
</script>