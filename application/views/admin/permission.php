<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Admin Control</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Permission
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12">
                                    <table class="table table-striped table-bordered table-hover"  id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th style="text-align: center;">Type</th>
                                            <th style="text-align: center;">Special</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php 


                                    	if($user){
                                            $i=1;
	                                    	foreach ($user as $rs) { 
	                                    		
	                                    	?>

		                                         <tr class="r-users" data-id="<?php echo $rs['id']; ?>" data-username="<?php echo $rs['username']; ?>" data-name="<?php echo $rs['name']; ?>" data-role="<?php echo $rs['role']; ?>">
                                                    <td><?php echo $i; ?></td>
		                                            <td><?php echo $rs['name']; ?></td>
                                                    <td><?php echo $rs['username']; ?></td>
                                                    <td style="text-align: center;"><?php echo $rs['role']; ?></td>
                                                    <td style="text-align: center;">
                                                        <?php if($rs['reprint'] == 1){ echo "<p style='color:green'><b>Re-Print</b></p>"; } else { echo "<p style='color:red'><b>NONE</b></p>"; } ?>
                                                    </td>
		                                            <td><?php echo date("j-F-Y H:i", strtotime($rs['updated'])); ?></td>
                                                    <td align="center">
                                                        <button class="btn btn-warning btn-xs AddUsers"><span class="glyphicon glyphicon-eye-close"></span>Permission</button>
                                                        <?php if($rs['role'] == 'BILLING'){ ?>
                                                            <?php if($rs['reprint'] == 0) { ?>
                                                                <button class="btn btn-success btn-xs reprint-invoice" title="reprint Invoice"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;ON Special </button>
                                                            <?php } else { ?>
                                                                 <button class="btn btn-danger btn-xs reprint-invoice" title="reprint Invoice"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;OFF Special </button>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </td>
		                                        </tr>

                                        <?php

		                                  $i++;  } 
                                        }else {
                                        	echo '<tr align="center"><td colspan="5">-No Data-</td></tr>';
                                       }?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>



  <!-- Modal -->
  <div class="modal fade" id="Permission" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Config Permission</h4>
        </div>
        <div class="modal-body">
          <form id="FrmVat" role="form" action="<?php site_url();?>ConfigPermission" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">    
                    <div class="form-group">
                        <input type="hidden" name="id-per" id="id-per" value="" class="form-control">
                        <select class="form-control" id="role-per" name="role-per">
                            <option value="ADMIN">ADMIN</option>
                            <option value="DOCUMENT">DOCUMENT</option>
                            <option value="BILLING">BILLING</option>
                            <option value="SUPBILLING">SUPBILLING</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>



   <!-- Modal -->
  <div class="modal fade" id="reprint" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
            <h4>Confirm To Change Special Permission.</h4>
          <form id="FrmReprint" role="form" action="<?php site_url();?>RePrintINV" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">    
                    <div class="form-group">
                        <input type="hidden" name="id_user" id="id_user" value="" class="form-control">
                    </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("table").off("click", ".reprint-invoice");
        $("table").on("click", ".reprint-invoice", function(e) {
            e.preventDefault();
            var $row = $(this).parents('tr.r-users');

            var id = $row.data('id');

            $('#id_user').val(id);

            $('#reprint').modal('show');
 
        });


        $('.AddUsers').click(function(){
          
            var $row = $(this).parents('tr.r-users');
            var id = $row.data('id');
            var role = $row.data('role');


            $('#id').val('');
            $('#role').val();

            $('#id-per').val(id);
            $('#role-per').val(role);

            $('#Permission').modal('show');
        });

        $('.save').click(function(){
            
            var id = $('#id').val();
            var name = $('#name').val();
            var username = $('#username').val();
            var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();
            var role = $('#role').val();


            $.ajax({
                url:'<?php echo base_url(); ?>Admin/SaveUsers',
                method:'POST',
                data:{ id:id, name:name, username:username, password:password, confirm_password:confirm_password, role}
            }).done(function(data){
                
                var o = JSON.parse(data);

                if(o.msg == 100){
                    $('#validate-username').append('Username Already.');
                } 

                if (o.msg == 200){
                    $('#validate-name').append('Name Already.');
                }

                if (o.msg == 300){
                    $('#validate-password').append('Password Not Match.');
                }

                if(o.msg == 'success'){
                    $('#success').modal('show');
                }
            })


        });

        $('.btn_succsess').click(function() {
            location.reload();
        });

         $('.RemoveUsers').click(function(){

            var $row = $(this).parents('tr.r-users');
            var id = $row.data('id');
      
            $('#remove_id').val('');
            $('#remove_id').val(id);
            $('#RemoveUsers').modal('show');
        });
    });
</script>