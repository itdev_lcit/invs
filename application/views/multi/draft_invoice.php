	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>
	<div class="page">
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd.</b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>
						<table>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 13px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $payer_order->customer_name; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Draft No</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer_order->customer_address; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Date</b></p></td>
								<td width="40%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($date_create)); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer_order->customer_address2; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Type</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $type_dr; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer_order->customer_address3; ?>&nbsp;&nbsp;<?php echo $payer_order->customer_post; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Corp. A/C No</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;99999</p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 13px;"><b>Tax ID</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo$payer_order->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $payer_order->customer_branch; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Oper. A/C ID</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $payer_order->customer_code; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 13px;"><b>Remark</b></p></td>
								<td width="45%"><p style="font-size: 13px;">:&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Payment Terms</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;</p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 13px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 13px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Our Ref</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $terminal_doc_ref_an; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 13px;"><b></b></p></td>
								<td width="45%"><p style="font-size: 13px;">&nbsp;&nbsp;</p></td>
								<td></td>
								<td width="16%"><p style="font-size: 13px;"><b>Booking Ref</b></p></td>
								<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $bookan; ?></p></td>
							</tr>

						</table>
						<br><br><br>
						<table class="table">
						    <thead>
						        <tr>
						            <th style="font-size: 13px; text-align: left;"  width="40%" colspan="2">Description</th>
						            <th style="font-size: 13px; text-align: center;"  width="10%">QTY</th>
						            <th style="font-size: 13px; text-align: right;"  width="20%">Unit Rate (THB)</th>
						            <th style="font-size: 13px; text-align: right;"  width="15%">Line Amount (THB)</th>
						        </tr>
						    </thead>
						    <tbody id="book" >
						    	<?php 
						    	$all_amount = 0;
						    	foreach ($order as $rs_order) {  ?>
						          <tr>
						          	<td style="font-size: 13px;" align="left" colspan="2"><?php echo $dr_t->description."&nbsp;&nbsp;".$rs_order['size_con']."'"."&nbsp;&nbsp;".$rs_order['type_con']; ?></td>
						          	<td style="font-size: 13px;" align="center"><?php echo $rs_order['book_con']; ?></td>
						          	<td style="font-size: 13px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
						          	<td style="font-size: 13px;" align="right">
						          		<?php

						          			$line_amount = $rs_order['book_con'] * $rs_order['cur_rate'];
						          			$all_amount += $line_amount;
						          			echo number_format($line_amount, 2);
						          		?>
						          	</td>
						          </tr>   
						        <?php }  ?>
						         <tr>
						          	<td colspan="3" rowspan="5"></td>
						          	<td style="font-size: 13px;" align="right"><b>Sub Total</b></td>
						          	<td style="font-size: 13px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 13px;" align="right"><b>VAT (<?php echo $vat->vat; ?>%)</b></td>
						          	<td style="font-size: 13px;"  align="right">
						          	<?php  
						          	
						          			$vat = $all_amount*$vat->vat/100; 
						          		   	echo number_format($vat, 2); 

						          	?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 13px;" align="right"><b>Total (THB)</b></td>
						          	<td style="font-size: 13px;"  align="right"><?php  
						          				$g_total = $all_amount + $vat;
						          				echo number_format($g_total, 2); 
						          	  ?></td>
						          </tr>
						          <tr>
						          	
						          	<td style="font-size: 13px;" align="right"><b>Withholding Tax (3%)</b></td>
						          	<td style="font-size: 13px;"  align="right"><?php 
						          			$vat_hold = $all_amount*3/100; 
						          		   	echo number_format($vat_hold, 2); 
						          	 ?></td>
						          </tr> 
						          <tr>
						          	
						          	<td style="font-size: 13px;" align="right"><b>Net Pay (THB)</b></td>
						          	<td style="font-size: 13px;"  align="right"><?php 

						          			$net_total = $g_total - $vat_hold;
						          			echo number_format($net_total, 2); 

						          	?></td>
						          </tr>  
						    </tbody>
						</table>
						<table class="table table-striped table-bordered">
						        <tr>
						            <td style="font-size: 13px;" width="50%">
						            	REMARK : THE SERVICES QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THAILAND , USER HAS AGREED AND ACCEPTED.
						            </td>
						            <td style="font-size: 13px;" width="50%">
						            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
						            </td>
						        </tr>
						</table>
						<br><br><br><br><br><br>
						<P style="color:gray; font-size: 13px;">Printed by <?php echo $user_print->name; ?> on <?php echo date("j-F-Y g:i"); ?></P>
						<br>
	</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Draft/AllDraft/";
	});
});
</script>