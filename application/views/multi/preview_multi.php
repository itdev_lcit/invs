
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Document Register</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">

                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Customer 
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-3" align="right">
                                        <input type="text" id="payer_nm" name="payer_nm" class="form-control" placeholder="Search Payer name"> 
                                        <br>
                                    </div>

                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">Code</th>
                                            <th  width="40%">Name</th>
                                            <th  width="40%">Address</th>
                                            <th >TAX</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="payer">
                                        <tr>
                                            <td colspan="4" style="text-align: center;">Please Search Payer name</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->



                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          DR Option
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>DRAFT INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Customer</th>
                                                <th width="50%">Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td> <div id="customer_name"></td>
                                                        <td> <div id="customer_address"></td>
                                                        
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;"  width="15%">Doc Ref Book </th>
                                                <th style="text-align: center;">Size</th>
                                                <th style="text-align: center;">Type</th>         
                                                <th style="text-align: left;">Terminal Doc AN</th>
                                                <th style="text-align: left;" >Booking AN</th>
                                                <th style="text-align: center;" ><font color="green">Remain</font> / <font color="red">QTY</font></th>
                                                <th style="text-align: right;" >Unit Rate (THB)</th>
                                                <th style="text-align: right;" >Total (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <?php 
                                            $i=1;
                                            foreach ($order as $rs_order) {

                                                $doc = $rs_order['doc_ref_book'];

                                                $origin_book = $rs_order['book_con'];

                                                $id_comp = $rs_order['id_comp'];

                                                $book_con = $rs_order['book_con'];

                                                ?>
                                                    <tr class="r-dr" data-book_con="<?php echo $rs_order['book_con']; ?>" data-size_con="<?php echo $rs_order['size_con']; ?>" data-id="<?php echo $rs_order['id']; ?>" data-doc="<?php echo $rs_order['doc_ref_book']; ?>">
                                                        <td style="text-align: left;"><?php echo $rs_order['doc_ref_book']; ?></td>
                                                        <td style="text-align: center;"><?php echo $rs_order['size_con']."'"; ?></td>
                                                        <td style="text-align: center;"><?php echo $rs_order['type_con']; ?></td>
                                                        <td style="text-align: left;"><?php echo $rs_order['terminal_doc_ref_an']; ?></td>
                                                        <td style="text-align: left;"><?php echo $rs_order['book_an']; ?></td>
                                                        <td style="text-align: center;">
                                                            <font color="green"><?php echo $rs_order['book_con']; ?> / </font>
                                                             <input type="number" class="order_v" id="order_v<?php echo $rs_order['size_con']; ?>" style="width: 60px;">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="unit<?php echo $rs_order['size_con']; ?>"></p>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="total<?php echo $rs_order['size_con']; ?>"></p>
                                                        </td>
                                                    </tr> 
                                                            <input type="hidden" name="size" id="size<?php echo $i; ?>" value="<?php echo $rs_order['size_con']; ?>">
                                                            <input type="hidden" name="doc" id="doc<?php echo $i; ?>" value="<?php echo $doc; ?>">
                                                            <input type="hidden" name="origin_book" id="origin_book<?php echo $i; ?>" value="<?php echo $origin_book; ?>">
                                                            
                                                            <input type="hidden" name="book_con" id="book_con<?php echo $i; ?>" value="<?php echo $book_con; ?>">
                                                            <input type="hidden" name="rate_n" id="rate_n<?php echo $i; ?>" value="">


                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>   
                                    <div align="right">
                                        <select class="form-control" id="type_dr" style="width: 150px;">
                                            <option value="0" >Select Type</option>
                                            <?php foreach ($type as $rs_type) { ?>
                                            <option value="<?php echo $rs_type['id']; ?>"><?php echo $rs_type['type']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <br>
                                        <font size="4.5px"><b>Grand Total (THB)<p id="total_order"></p></b></font>

                                        <input type="hidden" name="count_order" id="count_order" value="<?php echo $count_order; ?>">
                                        <input type="hidden" name="order20" id="order20">
                                        <input type="hidden" name="order40" id="order40">
                                        <input type="hidden" name="order45" id="order45">

                                        <input type="hidden" name="result_order20" id="result_order20">
                                        <input type="hidden" name="result_order40" id="result_order40">
                                        <input type="hidden" name="result_order45" id="result_order45">

                                        <input type="hidden" name="order_id1" id="order_id1" value="<?php echo $order1; ?>">
                                        <input type="hidden" name="order_id2" id="order_id2" value="<?php echo $order2; ?>">
                                        <input type="hidden" name="order_id3" id="order_id3" value="<?php echo $order3; ?>">
                                        <input type="hidden" name="order_id4" id="order_id4" value="<?php echo $order4; ?>">
                                        <input type="hidden" name="order_id5" id="order_id5" value="<?php echo $order5; ?>">
                                        <input type="hidden" name="order_id6" id="order_id6" value="<?php echo $order6; ?>">

                                        <input type="hidden" name="id_comp" id="id_comp" value="">
  
                                                 <!--<div class="radio">
                                                  <label><input type="radio" class="type_split" name="type_split" value="1">Split</label>
                                                  <label><input type="radio" class="type_split" name="type_split" value="0" checked>None Split</label>
                                                  <input type="number" class="form-control" id="numpage_split" style="width: 150px;">
                                                  <input type="hidden" class="form-control" id="status_split" value="">
                                                </div>-->
                                                <button class="btn btn-success printDr"><span class="glyphicon glyphicon-print"></span>Generate DR</button>
                                                <a href="<?php echo site_url(); ?>Createdraft"><button class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>cancel</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

              

            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="insert_tax" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Input Tax ID, IF Customer live in Thailand input tax id 13 number , Else Input 0*</p>
          </div>

                <div class="form-group" align="center">
                    <label>TAX ID</label>

                    <input type="text" name="in_tax" id="in_tax" value="" class="form-control" style="width: 550px;" >
                    <font color="red"><p id="msg-error-tax"></p></font>
                    <input type="hidden" name="in_customer_name" id="in_customer_name" value="">
                    <input type="hidden" name="in_customer_address" id="in_customer_address" value="">
                    <input type="hidden" name="in_id_comp" id="in_id_comp" value="">
                    <input type="hidden" name="in_payer_code" id="in_payer_code" value="">
                    <input type="hidden" name="in_payer_tel" id="in_payer_tel" value="">
                </div>  
                <div class="form-group" align="center">
                    <label>Country</label>
                    <input type="text" name="in_country" id="in_country" value="Thailand" class="form-control" style="width: 550px;" placeholder="Auto Complete Country">
                    <font color="red"><p id="msg-error-country"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-tax">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

        </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/lib/jquery.autocomplete.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){

$('#rate_order').html('0');
$('#total_order').html('0');

$('#total20').html('0');
$('#total40').html('0');
$('#total45').html('0');

$('#unit20').html('0');
$('#unit40').html('0');
$('#unit45').html('0');

$('.order_v').prop('disabled', true); 

$('.printDr').prop('disabled', true);

$('#status_split').val('0');
$("#numpage_split").hide();

$(".type_split").bind("change",function(){
   var value = $(this).val();
   if(value == 1){

        $("#numpage_split").show();
        $('#status_split').val('1');

   } else {
        $("#numpage_split").hide();
        $('#status_split').val('0');
   }
});

var states = [
    'Aruba','Antigua and Barbuda','United Arab Emirates','Afghanistan','Algeria','Azerbaijan','Albania','Armenia','Andorra','Angola','American Samoa','Argentina','Australia','Ashmore and Cartier Islands','Austria','Anguilla','Åland Islands'
    ,'Antarctica','Bahrain','Barbados','Botswana','Bermuda','Belgium','Bahamas, The','Bangladesh','Belize','Bosnia and Herzegovina','Bolivia','Myanmar','Benin','Belarus','Solomon Islands','Navassa Island','Brazil','Bassas da India','Bhutan'
    ,'Bulgaria','Bouvet Island','Brunei','Burundi','Canada','Cambodia','Chad','Sri Lanka','Congo, Republic of the','Congo, Democratic Republic of the','China','Chile','Cayman Islands','Cocos (Keeling) Islands','Cameroon','Comoros','Colombia'
    ,'Northern Mariana Islands','Coral Sea Islands','Costa Rica','Central African Republic','Cuba','Cape Verde','Cook Islands','Cyprus','Denmark','Djibouti','Dominica','Jarvis Island','Dominican Republic','Dhekelia Sovereign Base Area'
    ,'Ecuador','Egypt','Ireland','Equatorial Guinea','Estonia','Eritrea','El Salvador','Ethiopia','Europa Island','Czech Republic','French Guiana','Finland','Fiji','Falkland Islands (Islas Malvinas)','Micronesia, Federated States of'
    ,'Faroe Islands','French Polynesia','Baker Island','France','French Southern and Antarctic Lands','Gambia, The','Gabon','Georgia','Ghana','Gibraltar','Grenada','Guernsey','Greenland','Germany','Glorioso Islands','Guadeloupe','Guam'
    ,'Greece','Guatemala','Guinea','Guyana','Gaza Strip','Haiti','Hong Kong','Heard Island and McDonald Islands','Honduras','Howland Island','Croatia','Hungary','Iceland','Indonesia','Isle of Man','India','British Indian Ocean Territory'
    ,'Clipperton Island','Iran','Israel','Italy','Cote d Ivoire','Iraq','Japan','Jersey','Jamaica','Jan Mayen','Jordan','Johnston Atoll','Juan de Nova Island','Kenya','Kyrgyzstan','Korea, North','Kingman Reef','Kiribati','KoreaSouth'
    ,'Christmas Island','Kuwait','Kosovo','Kazakhstan','Laos','Lebanon','Latvia','Lithuania','Liberia','Slovakia','Palmyra Atoll','Liechtenstein','Lesotho','Luxembourg','Libyan Arab','Madagascar','Martinique','Macau','Moldova, Republic of'
    ,'Mayotte','Mongolia','Montserrat','Malawi','Montenegro','The Former Yugoslav Republic of Macedonia','Mali','Monaco','Morocco','Mauritius','Midway Islands','Mauritania','Malta','Oman','Maldives','Mexico','Malaysia','Mozambique'
    ,'New Caledonia','Niue','Norfolk Island','Niger','Vanuatu','Nigeria','Netherlands','No Mans Land','Norway','Nepal','Nauru','Suriname','Netherlands Antilles','Nicaragua','New Zealand','Paraguay','Pitcairn Islands','Peru','Paracel Islands'
    ,'Spratly Islands','Pakistan','Poland','Panama','Portugal','Papua New Guinea','Palau','Guinea-Bissau','Qatar','Reunion','Serbia','Marshall Islands','Saint Martin','Romania','Philippines','Puerto Rico','Russia','Rwanda','Saudi Arabia'
    ,'Saint Pierre and Miquelon','Saint Kitts and Nevis','Seychelles','South Africa','Senegal','Saint Helena','Slovenia','Sierra Leone','San Marino','Singapore','Somalia','Spain','Saint Lucia','Sudan','Svalbard','Sweden'
    ,'South Georgia and the Islands','Syrian Arab Republic','Switzerland','Trinidad and Tobago','Tromelin Island','Thailand','Tajikistan','Turks and Caicos Islands','Tokelau','Tonga','Togo','Sao Tome and Principe','Tunisia'
    ,'East Timor','Turkey','Tuvalu','Taiwan','Turkmenistan','Tanzania, United Republic of','Uganda','United Kingdom','Ukraine','United States','Burkina Faso','Uruguay','Uzbekistan','Saint Vincent and the Grenadines','Venezuela'
    ,'British Virgin Islands','Vietnam','Virgin Islands (US)','Holy See (Vatican City)','Namibia','West Bank','Wallis and Futuna','Western Sahara','Wake Island','Samoa','Swaziland','Serbia and Montenegro','Yemen','Zambia','Zimbabwe'
];

$('#in_country').autocomplete({
    source:[states]
});

$('#in_country').on('input', function(e){
    var in_country = $(this).val();
    if(in_country != null){
        $('#msg-error-country').html('');
        $.ajax({
            url:'<?php echo site_url(); ?>Setting/CheckCountry',
            method:'POST',
            data:{ in_country:in_country }
        }).done(function(data){
              
        })
   
    } else {
         $('#msg-error-country').html('');
    }
});




$('.order_v').on('input', function(e){

    $('.printDr').prop('disabled', false);

    var $row = $(this).parents('tr.r-dr');
    var size_con = $row.data('size_con');
    var book_con = $row.data('book_con');

    var order_v = $('#order_v'+size_con).val();
    var rate_v = $('#order'+size_con).val();

    if(order_v > book_con){
        console.log('Too Much Value, Please Try Again.');
        $('#order_v'+size_con).val('');
        $('#total20').html('0');
        $('#total40').html('0');
        $('#total45').html('0');
        $('#total_order').html('0');
    } else if (order_v <= '0') {
        console.log('Order Cannot Input "0" , Please Try Again.');
        $('#order_v'+size_con).val('');
       $('#total_order').html('0');
    } else {

            var total_result = order_v*rate_v;
            $('#result_order').val(total_result);
            console.log('ready input ' + size_con + ' : '  + total_result);

            $('#total'+size_con).html(total_result.toLocaleString('en'));

            if(size_con == '20'){
                $('#result_order20').val(total_result);
            } else if (size_con == '40'){
                $('#result_order40').val(total_result);
            } else {
                $('#result_order45').val(total_result);
            }
    }
    
        var result1 =  $('#result_order20').val();
        var result2 =  $('#result_order40').val();
        var result3 =  $('#result_order45').val(); 

        var final_result = (Number(result1) + Number(result2) + Number(result3));

        console.log('Total : '  + final_result);

        $('#total_order').html(final_result.toLocaleString('en'));
});

$('#type_dr').change(function(){

    $('.order_v').prop('disabled', false);
    var count_order = parseInt($('#count_order').val());
    var id_type = $(this).val();

    $('#order20').val('');
    $('#order40').val('');
    $('#order45').val('');

    $('#order_v20').val('0');
    $('#order_v40').val('0');
    $('#order_v45').val('0');


       if(id_type != 0){

            $.ajax({
                url:'<?php echo site_url(); ?>Setting/getAllRate',
                method:'POST',
                data:{ 
                    id_type:id_type
                }
            }).done(function(data){
                var o = JSON.parse(data);
                
                for(i=0; i < o.length; i++){

                    if(o[i]['size'] == '20'){
                        var rate = parseInt(o[i]['rate']);
                        $('#unit20').html(rate.toLocaleString('en'));
                        $('#order20').val(rate);
                    } else if (o[i]['size'] == '40'){
                        var rate = parseInt(o[i]['rate']);
                        $('#unit40').html(rate.toLocaleString('en'));
                        $('#order40').val(rate);
                    } else {
                        var rate = parseInt(o[i]['rate']);
                        $('#unit45').html(rate.toLocaleString('en'));
                        $('#order45').val(rate);
                    }

                    console.log('SYNC Success');
                }
            });

        } else {
            $('.order_v').prop('disabled', true);
            $('.printDr').prop('disabled', true);
            $('#unit20').html('0');
            $('#unit40').html('0');
            $('#unit45').html('0');
            $('#order20').val('');
            $('#order40').val('');
            $('#order45').val('');
            $('#total20').html('0');
            $('#total40').html('0');
            $('#total45').html('0');
            $('#total_order').html('');
            console.log('No Select.');
        }

});

$("div").off("click", ".save-tax");
        $("div").on("click", ".save-tax", function(e) {
            e.preventDefault();
            
            var in_tax = $('#in_tax').val();
            var in_country = $('#in_country').val();
            if(in_tax == ''){
                $('#msg-error-tax').html('Please Input Tax ID.');
                console.log('null');
            } else {
                console.log('insert tax');
                $('#insert_tax').modal('hide');
                    var id_customer = $('#in_id_comp').val();
                    var customer_address = $('#in_customer_address').val();
                    var customer_name = $('#in_customer_name').val();
                    var customer_code = $('#in_payer_code').val();
                    var telephone_an  = $('#in_payer_tel').val();
                    var tax_reg_no  = in_tax;
                    var country = in_country;

                    $.ajax({
                            url:'<?php echo site_url(); ?>Setting/SavePayerTax',
                            method:'POST',
                            data:{ 
                                id_customer:id_customer ,
                                customer_address:customer_address,
                                customer_name:customer_name,
                                customer_code:customer_code,
                                telephone_an:telephone_an,
                                tax_reg_no:tax_reg_no,
                                country:country
                            }
                    }).done(function(data){

                    })

                        $('#customer_name').html(customer_name);
                        $('#customer_address').html(customer_address);  
                        $('#id_comp').val(id_customer); 
                        $('#payer_address').val(customer_address);
                        $('#payer_name').val(customer_name); 
                        $('#payer_code').val(customer_code);    
                        $('#payer_tel').val(telephone_an);  
                        $('#payer_tax').val(tax_reg_no);   
            }

          
});

$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();

            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_address2 = $row.data('customer_address2');
            var customer_address3 = $row.data('customer_address3');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');

            if(tax_reg_no != null){
                 $.ajax({
                    url:'<?php echo site_url(); ?>Setting/SavePayer',
                    method:'POST',
                    data:{ 
                        id_customer:id_customer ,
                        customer_address:customer_address,
                        customer_address2:customer_address2,
                        customer_address3:customer_address3,
                        customer_name:customer_name,
                        customer_code:customer_code,
                        telephone_an:telephone_an,
                        tax_reg_no:tax_reg_no
                    }
                }).done(function(data){

                })

                $('#customer_name').html(customer_name);
                $('#customer_address').html(customer_address+customer_address2+customer_address3);  
                $('#id_comp').val(id_customer); 
                $('#payer_address').val(customer_address+customer_address2+customer_address3);
                $('#payer_name').val(customer_name); 
                $('#payer_code').val(customer_code);    
                $('#payer_tel').val(telephone_an);  
                $('#payer_tax').val(tax_reg_no);   
            } else {

                $('#insert_tax').modal('show');
                $('#in_customer_name').val(customer_name); 
                $('#in_customer_address').val(customer_address+customer_address2+customer_address3);
                $('#in_id_comp').val(id_customer);   
                $('#in_payer_code').val(customer_code);  
                $('#in_payer_tel').val(telephone_an); 
            }

             
});


$('#payer_nm').on('input', function(e){

    $('#payer').html('');
    console.log('sync Zodiac');
    var payer = $('#payer_nm').val();
    var loading = '<tr align="center"><td colspan="6"><img src="<?php echo base_url(); ?>public/img/loading.gif" width="10%"></td></tr>';

    $('#payer').append(loading);

    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomer',
        method : 'POST',
        data : {payer:payer}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;


        if(o.length == '0'){
             console.log('Not Found Record');
            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var COMPANY_ID = o[i]['COMPANY_ID'];
            var OPS_COMPANY_ID = o[i]['OPS_COMPANY_ID'];
            var ADDRESS = o[i]['STREET_ADDRESS1_DS'];
            var ADDRESS2 = o[i]['STREET_ADDRESS2_DS'];
            var ADDRESS3 = o[i]['STREET_ADDRESS3_DS'];
            var COMPANY_NM = o[i]['COMPANY_NM'];
            var TAX_REG_NO = o[i]['TAX_REG_NO'];
            var TELEPHONE_AN = o[i]['TELEPHONE_AN'];

            payer += '<tr class="r-payer" data-TELEPHONE_AN="'+TELEPHONE_AN+'" data-TAX_REG_NO="'+TAX_REG_NO+'" data-id_customer="'+COMPANY_ID+'" data-customer_address3="'+ADDRESS3+'" data-customer_address2="'+ADDRESS2+'" data-customer_address="'+ADDRESS+'" data-customer_name="'+COMPANY_NM+'" data-customer_code="'+OPS_COMPANY_ID+'">';

            payer += '<td style="text-align:center; font-size:12px;">';
            payer += OPS_COMPANY_ID;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += COMPANY_NM;
            payer += '</td>';

            payer += '<td style="font-size:12px;">';
            payer += ADDRESS + ADDRESS2 + ADDRESS3;
            payer += '</td>';


            payer += '<td style="font-size:12px;">';
            payer += TAX_REG_NO;
            payer += '</td>';

            payer += '<td>';
            payer += '<button class="btn btn-info btn-xs cus-data">Select</button>'
            payer += '</td>';

            payer += '</tr>';

        }

        }

        
        $('#payer').html('');
        $('#payer').append(payer);

    })
});




$('.printDr').click(function(){

    var order_v20 = $('#order_v20').val();
    var order_v40 = $('#order_v40').val();
    var order_v45 = $('#order_v45').val();

    var order_id1 = $('#order_id1').val();
    var order_id2 = $('#order_id2').val();
    var order_id3 = $('#order_id3').val();
    var order_id4 = $('#order_id4').val();
    var order_id5 = $('#order_id5').val();
    var order_id6 = $('#order_id6').val();
    var id_comp = $('#id_comp').val();
    var type_dr = $('#type_dr').val();

    if(id_comp != 0){
         var r = confirm("Confirm to generate DRAFT ?");
            if (r == true) {  
                window.location = '<?php echo site_url(); ?>MultiDraft/GenrateDraft/'+id_comp+'/'+type_dr+'/'+order_v20+'/'+order_v40+'/'+order_v45+'/'+order_id1+'/'+order_id2+'/'+order_id3+'/'+order_id4+'/'+order_id5+'/'+order_id6;                            
            } else {
                alert('OK.');
            }
    } else {
        alert('Please Select Customer.');
    }


           
});



});
</script>