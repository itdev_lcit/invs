<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Speacial Service Request </h4>


       
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                        <?php if($role == 'ADMIN' OR $role == 'SUPBILLING' OR $role == 'DOCUMENT'){ ?>

                        <a href="<?php echo site_url(); ?>Speacial/Create">
                                <button class="btn btn-success"><i class="fa fa-plus-circle" title="Create"></i></button>
                        </a>
                        <?php } ?>
                            <?php if($role == 'ADMIN' OR $role == 'SUPBILLING'){ ?>
                                <a href="<?php echo site_url(); ?>Speacial/NotedList">
                                    <button class="btn btn" style="background-color:#7400ff;color: white;" title="Noted"><i class="fa fa-paperclip"></i></button>
                                </a>
                            <?php } ?>
                        <a href="<?php echo site_url(); ?>Speacial/SSR">
                            <button class="btn btn-warning"><i class="fa fa-refresh" title="Refresh"></i></button>
                        </a>

                        <button class="btn btn multi-receipt" style="background-color:#57a5bd;color: white;" title="Multiple Receipt">
                            <i class="fa fa-files-o" aria-hidden="true"></i>
                        </button>



                </div>

                 <div class="col-lg-12">
                        &nbsp;&nbsp;
                </div>
               
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url(); ?>Speacial/SSR" method="POST">
                                <label for="sel1">Search By </label>
                                  <select class="form-control" name="sort">
                                    <option value="dr_no" <?php if($sort == 'dr_no'){ echo "selected"; } ?>>SSR No.</option>
                                    <option value="code_comp" <?php if($sort == 'code_comp'){ echo "selected"; } ?>>Customers</option>                    
                                  </select>
                                <textarea class="form-control" id="search" name="search" rows="3" cols="40"><?php echo $search; ?></textarea>
                                <button class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i></button>
                                &nbsp;&nbsp;
                            </form>
                        </div>     
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;"><input type="checkbox" class="all-list" name="multi"></th>
                                            <th style="text-align: center;">#</th>
                                            <th>SSR NO.</th>
                                            <th style="text-align: center;">Customers</th>
                                            <th style="text-align: left;">Vessel</th>
                                            <th style="text-align: center;">Payment</th>
                                            <th style="text-align: center;">Status</th>
                                            <!--<th style="text-align: left;">Doc Remark</th>-->
                                            <th>Created</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;

                                            if(!empty($results)){
                                            foreach ($results  as $rs_normal) {

                                                $date = new DateTime($rs_normal->updated);
                                                $now = new DateTime();

                                             $date->diff($now)->format("%d");

                                            ?>
                                             <!--IF STATUS != CANCEL-->
                                            <?php if($rs_normal->type == 'CANCEL' OR ($rs_normal->type == 'INVOICE') OR ($rs_normal->type == 'RECEIPT')){  ?>
                                                    <tr class="r-invoice"  data-comp="<?php echo $rs_normal->id_comp; ?>" data-dr_no="<?php echo $rs_normal->dr_no; ?>" <?php if($rs_normal->type == 'CANCEL'){ echo "style='color: red;'"; } ?> >
                                                        <td style="text-align: center;">
                                                            <?php if($rs_normal->type == 'INVOICE'){ ?>
                                                                <input type="checkbox" class="chk" name="multi" value="<?php echo $rs_normal->dr_no; ?>">
                                                            <?php } else { ?>
                                                                <i class="fa fa-times" aria-hidden="true" style="color: #ca7777;"></i>
                                                            <?php } ?>
                                                        </td>
                                                        <td style="text-align: center;"><?php echo $i; ?></td>
                                                        <td>
                                                             <?php  if($rs_normal->type == 'CANCEL'){ ?>

                                                                   <a href="<?php echo site_url(); ?>Speacial/RePreview/<?php echo $rs_normal->dr_no; ?>" title="Preview Invoice" target="_blank"><p style="color: red;"><?php echo $rs_normal->dr_no; ?></p></a>

                                                            <?php } else {
                                                                    echo $rs_normal->dr_no;
                                                                }
                                                             ?>
                                                        </td>
                                                        <td style="text-align: center;"><?php echo $rs_normal->customer_code; ?></td>
                                                        <td style="text-align: left;">

                                                            <?php if($rs_normal->vsl_visit) { ?>
                                                                ( <?php echo $rs_normal->vsl_visit; ?> ) <?php echo $rs_normal->vessel; ?>
                                                            <?php } else { ?>
                                                                    -
                                                            <?php }?>
                                                        </td>
                                                        <td style="text-align: center;"><?php echo $rs_normal->payment; ?></td>
                                                        <td style="text-align: center;">
                                                            <?php

                                                                if($rs_normal->type == 'INVOICE'){
                                                                    echo '<p style="color:orange;"><b>INVOICE</b></p>'; 
                                                                } else if($rs_normal->type == 'RECEIPT'){
                                                                    echo '<p style="color:green;"><b>'.$rs_normal->receipt_type.'#'.$rs_normal->prefix_invoice.$rs_normal->invoice_no.'</b></p>'; 
                                                                } else  {
                                                                    echo '<p style="color:red;"><b>CANCEL</b></p>'; 
                                                                }
                                                            ?>
                                                        </td>
                                                       <!-- <td style="text-align: left;">
                                                            <?php 
                                                                if(empty($rs_normal->remark_doc)){
                                                                    echo '-'; 
                                                                } else  {
                                                                    echo $rs_normal->remark_doc;
                                                                }
                                                            ?>
                                                        </td>-->
                                                        <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->updated)); ?></td>
                                                        <td align="center">
                                                            <?php
                                                            if($rs_normal->type != 'CANCEL'){
                                                             if($role == 'ADMIN' OR $role == 'SUPBILLING' AND $rs_normal->is_use != 'NO') {?>
                                                               <!--<button class="btn btn-info btn-xs doc-remark" title="Remark Invoice"><span class="glyphicon glyphicon-comment"></span>&nbsp;Remark </button>-->
                                                            <?php } } ?>
                                                            <?php if($rs_normal->type != 'CANCEL'){ ?>
                                                                <?php if($role == 'SUPBILLING' OR $role == 'ADMIN' OR $role == 'DOCUMENT'){ ?>
                                                                    <a href="<?php echo site_url(); ?>Speacial/PrintDraft/<?php echo $rs_normal->ref_id; ?>" target="_blank" title="Generate Invoice"><button class="btn btn-success  btn re-dr-normal"><span class="glyphicon glyphicon-print"></span></button></a>
                                                                <?php } ?>
                                                            <?php } else if ($rs_normal->type == 'CANCEL') {
                                                                echo "Remark : ".$rs_normal->remark_cancel."<br>";
                                                                //echo 5-$date->diff($now)->format("%d")." days left to delete in the system.";
                                                            } ?>
                                                            <?php if($rs_normal->type == 'INVOICE' AND $role == 'SUPBILLING' OR $role == 'BILLING' OR $role == 'ADMIN' AND $rs_normal->type != 'CANCEL' AND $rs_normal->invoice_no == ''){ ?>
                                                                <a href="<?php echo site_url(); ?>Speacial/Preview/<?php echo $rs_normal->dr_no; ?>" title="Preview Receipt" target="_blank"><button class="btn btn-primary  btn re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span></button></a>
                                                            <?php } ?>
                                                            <?php if($rs_normal->type == 'RECEIPT'){ ?>
                                                                <?php if($special == 1   OR $role == 'SUPBILLING' OR $role == 'ADMIN' AND $rs_normal->payment == 'CASH') {?>
                                                                    <a href="<?php echo site_url(); ?>Speacial/ReMultiPreview/<?php echo $rs_normal->prefix_invoice.$rs_normal->invoice_no; ?>" title="Re-Print Receipt" target="_blank"><button class="btn btn-warning btn re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>t</button></a>
                                                                <?php } ?>
                                                            <?php } ?>

                                                            <?php if($rs_normal->type != 'CANCEL' AND $role == 'SUPBILLING' OR $role == 'BILLING' OR $role == 'ADMIN'){ ?>
                                                            <a href="<?php echo site_url(); ?>Speacial/CreateNote/<?php echo $rs_normal->ref_id; ?>" title="Create Noted" target="_blank"><button class="btn btn btn " style="background-color:#7400ff;color: white;"><span class="glyphicon glyphicon-paperclip"></span></button></a>
                                                            <?php } ?>

                                                            <?php
                                                            if($rs_normal->type != 'CANCEL'){
                                                             if($special == 1  OR $role == 'ADMIN' OR $role == 'SUPBILLING' AND $rs_normal->is_use != 'NO') {?>
                                                               <button class="btn btn-danger btn cancel-invoice" title="Cancel Invoice"><span class="glyphicon glyphicon-remove-sign"></span></button>
                                                            <?php } } ?>

                                                        </td>
                                                    </tr>
                                            
                                            <?php } ?>

                                            <!--IF STATUS != CANCEL-->
                                        <?php $i++; } 
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="8" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <!-- /.paging -->
                                <?php echo $links; ?>
                                <!-- /.paging -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="remarkdr" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Remark for Cancel *</p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                    <input type="hidden" name="dr_no" id="dr_no" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>


<!-- Modal -->
<div id="remarkdoc" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p></p>
          </div>
            <div class="modal-body">
                <div class="form-group" align="left">
                    <label>Remark</label>
                    <textarea class="form-control" rows="5" id="comment_doc"></textarea>
                    <input type="hidden" name="dr_no_doc" id="dr_no_doc" value="">
                    <font color="red"><p id="msg-error-remark"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-doc-remark">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>    
        </div>

  </div>
</div>


<form id="form-multi" action="<?php echo site_url(); ?>Speacial/MultiPreview" enctype="multipart/form-data" method="post" enctype="multipart/form-data">
    <input type="hidden" id="multiinv" name="multiinv" value="">
</form>

 <input type="hidden" id="origin_comp" name="origin_comp" value="">

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
	
$(document).ready(function(){

    $(".all-list").click(function () {
        $(".chk").prop('checked', $(this).prop('checked'));

                /* declare an checkbox array */
        var chkArray = [];
        
        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $(".chk:checked").each(function() {
            chkArray.push($(this).val());
        });
        
        /* we join the array separated by the comma */
        var selected;
        selected = chkArray.join(', ');

        $('#multiinv').val(selected);

    });

    $(".chk").click(function () {

        var $row = $(this).parents('tr.r-invoice');
        var comp = $row.data('comp');

        var origin_comp = $('#origin_comp').val();

        if(origin_comp == null || origin_comp == ''){

            /* declare an checkbox array */
            var chkArray = [];
            
            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".chk:checked").each(function() {
                chkArray.push($(this).val());
            });
            
            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(', ');

            $('#origin_comp').val(comp)

            $('#multiinv').val(selected);

        } else {

            if(comp == origin_comp){
               /* declare an checkbox array */
                var chkArray = [];
                
                /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
                $(".chk:checked").each(function() {
                    chkArray.push($(this).val());
                });
                
                /* we join the array separated by the comma */
                var selected;
                selected = chkArray.join(', ');

                $('#multiinv').val(selected); 
            } else {
                $(".chk").prop( "checked", false );
                $('#origin_comp').val('');
                alert("Please selected the same conmpany for multiple invoice.");
            }

        }
        //$(".chk").prop('checked', $(this).prop('checked'));
    });

    $('#normal-draft').DataTable({
            //responsive: true
    });



$('.multi-receipt').click(function(){

    var select_dr = $('#multiinv').val();


    if(select_dr == null || select_dr == ''){
        alert('No SSR selected, Please try again.');
    } else {
        $('#form-multi').submit();
    }
    
   
});

$("table").off("click", ".doc-remark");
$("table").on("click", ".doc-remark", function(e) {
            e.preventDefault();
            
            $('#remarkdoc').modal('show');

            var $row = $(this).parents('tr.r-invoice');
            var dr_no = $row.data('dr_no');
            $('#dr_no_doc').val(dr_no);
            
});


$("table").off("click", ".cancel-invoice");
$("table").on("click", ".cancel-invoice", function(e) {
            e.preventDefault();
            
            $('#remarkdr').modal('show');

            var $row = $(this).parents('tr.r-invoice');
            var dr_no = $row.data('dr_no');
            $('#dr_no').val(dr_no);
            
});



$('.save-remark').click(function(){

            var remark_inv = $('#comment').val();
            var dr_no = $('#dr_no').val();

            if(remark_inv == ''){
                $('#msg-error-remark').html('*Please Remark for Cancel*');
                console.log('null');
            } else {

                console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>Speacial/Cancel',
                            method:'POST',
                            data:{ dr_no:dr_no, remark_inv:remark_inv }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                    })
            }
          
});


$('.save-doc-remark').click(function(){

            var comment_doc = $('#comment_doc').val();
            var dr_no_doc = $('#dr_no_doc').val();

            if(comment_doc == ''){
                $('#msg-error-remark').html('*Please Remark*');
                console.log('null');
            } else {

                console.log('insert remark');

                    $.ajax({
                        url:'<?php echo site_url(); ?>Speacial/RemarkDoc',
                            method:'POST',
                            data:{ dr_no_doc:dr_no_doc, comment_doc:comment_doc }
                    }).done(function(data){
                       var o = JSON.parse(data);
                       alert(o.msg);
                       location.reload();
                })
            }
          
});


    });

</script>