	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>


<?php $p_vat = $vat->vat; ?>

<?php 
$start_rang = 0;


if((count($order) % 15) != '0'){
	$pag = ceil(count($order)/15);
} else {
	$pag = ceil(count($order)/15)+1;
}


for ($i_page=1; $i_page <= $pag; $i_page++) { ?>
		
	<div class="page">
		<div align="right">
			<b><?php echo $i_page.' / '.$pag; ?></b>
		</div>
		<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
		<h6 align="center"><b>One Stop Logistics Co., Ltd. </b></h6>
		<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
		<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
		<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
		<br>
		<h3 align="center"><b>INVOICE</b></h3>

						<div align="right">
							<img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $dr_no; ?>.png" width="25%">
							<br>
						</div>

						
						<table>
							<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 25px;"><b>Original</b></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>Bill To</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Invoice No</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $dr_no; ?></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($datePrint)); ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Corp. A/C No</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td><p style="font-size: 10px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> &nbsp;&nbsp; <?php echo $cus->customer_post; ?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Oper. A/C ID</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
							</tr>
							<tr>
								<td width="10%" colspan="1"><p style="font-size: 10px;"><b>Tax ID</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b>&nbsp;<?php echo $cus->customer_branch;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b></b></p></td>
								<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;</p></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>Customer</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Payment Terms</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $payment; ?> <?php if($payment == 'CREDIT'){ echo $cus->credit_term.' DAYS';} ?></td>
							</tr>

							<?php if($SsrOrder->vsl_visit){ ?>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>Barge</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;( <?php echo $SsrOrder->vsl_visit;?> ) <?php echo $SsrOrder->vessel;?></p></td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>Voyage In/Out</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $SsrOrder->voy_in;?> / <?php echo $SsrOrder->voy_out;?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>ATB</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder->atb)); ?></td>
							</tr>
							<tr>

								<td width="17%" colspan="1"><p style="font-size: 10px;"><b>&nbsp;&nbsp;</b></p></td>
								<td width="50%"><p style="font-size: 10px;">&nbsp;&nbsp;</td>
								<td></td>
								<td width="16%"><p style="font-size: 10px;"><b>ATD</b></p></td>
								<td width="50%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($SsrOrder->atd)); ?></td>
							</tr>
						<?php } ?>

						</table>
						<br>

						<table class="table">
							    <thead>
							        <tr>
							        	<th style="font-size: 10px; text-align: center;" >#</th>
							        	<th style="font-size: 10px; text-align: left;"  width="10%">Tariff Code</th>
							            <th style="font-size: 10px; text-align: left;"  width="15%" colspan="2">Description</th>
							            <th style="font-size: 10px; text-align: right;"  width="15%">QTY</th>
							            <th style="font-size: 10px; text-align: right;"  width="15%">Rate</th>
							            <th style="font-size: 10px; text-align: right;"  width="10%">Total</th>
							            <th style="font-size: 10px; text-align: right;"  width="10%">Vat</th>
							            <th style="font-size: 10px; text-align: right;"  width="10%">WH Tax</th>
							            <th style="font-size: 10px; text-align: right;"  width="15%">Line Amount</th>
							        </tr>
							    </thead>
							    <tbody id="book" >
							    	<?php 
							    	$all_amount = 0;
							    	$sub_total = 0;
							    	$vat_total = 0;
							    	$wh_total = 0;

							    	if($i_page > 1){
							    		$loop_begin = $loop_begin+15;
							    	} else {
							    		$loop_begin = $start_rang;
							    	}     



							    	if($i_page > 1){
							    		$i = $loop_begin+1;
							    	}else {
							    		$i = 1;
							    	}


							    	foreach (array_slice($order, $loop_begin, 15) as $rs_order) {  
							    		$is_vat =  $c_vat;
							    		$remark = $rs_order['remark'];
							    	 ?>
							          <tr>
							          	<td style="font-size: 10px;" align="center"><?php echo $i; ?></td>
							          	<td style="font-size: 10px;" align="left"><?php echo $rs_order['tariff_code']; ?></td>
							          	<td style="font-size: 10px;" align="left" colspan="2"><?php echo $rs_order['tariff_des']."&nbsp;&nbsp;"; ?></td>
							          	<td style="font-size: 10px;" align="right"><?php echo number_format($rs_order['qty'], 2); ?>
							          		&nbsp;&nbsp;<b><?php echo $rs_order['unit_type']; ?></b>
							          	</td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php echo number_format($rs_order['cur_rate'], 2); ?>
							          		&nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
							          	</td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php

							          			echo number_format(($rs_order['cur_rate'] * $rs_order['qty']), 2);
							          		?>
							          	</td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php if($rs_order['code_vat'] != 'N'){ ?>
							          			<?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
							          		<?php } else { ?>
							          				-
							          		<?php } ?>
							          	</td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php if($rs_order['code_wh'] != 'N'){ ?>
							          			<?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
							          		<?php } else { ?>
							          				-
							          		<?php } ?>
							          	</td>
							          	<td style="font-size: 10px;" align="right">
							          		<?php

							          			$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
							          			$all_amount += ($line_amount+$rs_order['cal_vat']-$rs_order['cal_wh']);
							          			echo number_format(($line_amount+$rs_order['cal_vat']-$rs_order['cal_wh']), 2);

							          			$sub_total += $rs_order['cur_rate'] * $rs_order['qty'];
							          			$vat_total += $rs_order['cal_vat'];
							          			$wh_total += $rs_order['cal_wh'];

							          			$InvRate = $rs_order['rate_type'];
							          		?>
							          		&nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
							          	</td>
							          </tr>   
							        <?php $i++; }  ?>


							    </tbody>
						    <tbody>
						    	<?php if($i_page == $pag){ ?>
									</tr> 
						    		<tr>
										<td colspan="8" rowspan="7" style="font-size: 10px;"><br><?php echo $SsrOrder->remark;?></td>
									</tr> 
									
									<?php if($s_vat_total > 0){ ?>
									<tr>
										<td style="font-size: 10px;" align="right"><b>Sub Total</b></td>
										<td style="font-size: 10px;"  align="right">
											<?php  
														          	
												echo number_format($s_sub_total, 2)."&nbsp;&nbsp;<b>".$InvRate."</b>"; 

											?>
										</td>
									</tr>
									<tr>
										<td style="font-size: 10px;" align="right"><b>Vat</b></td>
										<td style="font-size: 10px;"  align="right">
											<?php  
														          	
												echo number_format($s_vat_total, 2)."&nbsp;&nbsp;<b>".$InvRate."</b>"; 

											?>
										</td>
									</tr>
									<tr>
										<td style="font-size: 10px;" align="right"><b>Total (THB)</b></td>
										<td style="font-size: 10px;"  align="right">
											<?php  
														          	
												echo number_format($s_sub_total+$s_vat_total, 2)."&nbsp;&nbsp;<b>".$InvRate."</b>"; 

											?>
										</td>
									</tr>
									<?php } ?>
									<?php if($s_wh_total > 0){ ?>
									<tr>
										<td style="font-size: 10px;" align="right"><b>WH Tax</b></td>
										<td style="font-size: 10px;"  align="right">
											<?php  
														          	
												echo number_format($s_wh_total, 2)."&nbsp;&nbsp;<b>".$InvRate."</b>"; 

											?>
										</td>
									</tr>
									<?php } ?>
									<tr>
										<td style="font-size: 10px;" align="right"><b>Net Pay</b></td>
										<td style="font-size: 10px;"  align="right">
											<?php  
														          	
												echo number_format($s_all_amount, 2)."&nbsp;&nbsp;<b>".$InvRate."</b>"; 
											?>
										</td>
								</tr>
						    	<?php } ?>
						    </tbody>
						</table>
						<?php if($i_page == $pag){ ?>
						<br><br>
						<table class="table table-striped">
						        <tr>
						            <td style="font-size: 10px;">
						            	In absence of alternative arrangements being agreed in writing with Operator: <br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-	All bills or part thereof remaining unpaid beyond the due date will be charged interest up to the maximum rate permitted by law. <br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-	Disputed charges should be notified to the Operator within 7 days of receipt of invoice, else invoices will be deemed correct.  <br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-	This is a computer generated document, No signature required.
						            </td>
						        </tr>
						</table>
						<P style="color:gray; font-size: 10px;">Printed by <?php echo $user_print->name; ?> on <?php echo date("j-M-Y H:i", strtotime($datePrint)); ?></P>
						<?php } ?>
	</div>

<?php } ?>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Speacial/SSR";
	});
});
</script>