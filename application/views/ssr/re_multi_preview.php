<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Speacial Service Request</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Re Print - Preview Receipt</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Code</th>
                                                <th >Customer</th>
                                                <th >Branch</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody  style="font-size: 13px;">
                                           
                                                    <tr>
                                                        <td><?php echo  $cus->customer_code; ?></td>
                                                        <td><?php echo  $cus->customer_name; ?></td>
                                                        <td><?php echo  $cus->customer_branch; ?></td>
                                                        <td><?php echo  $cus->customer_address; ?><?php echo  $cus->customer_address2; ?><?php echo  $cus->customer_address3; ?> <?php echo  $cus->customer_post; ?></td>
                                                        <td><?php echo  $cus->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  

                                    <?php if($v_data->vsl_visit){  ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th >Vessel</th>
                                                    <th >Voy In</th>
                                                    <th >Voy Out</th>
                                                    <th >ATB</th>
                                                    <th >ATD</th>
                                                </tr>
                                            </thead>
                                            <tbody  style="font-size: 13px;">
                                               
                                                        <tr>
                                                            <td>( <?php echo $v_data->vsl_visit;?> ) <?php echo $v_data->vessel;?></td>
                                                            <td><?php echo  $v_data->voy_in; ?></td>
                                                            <td><?php echo  $v_data->voy_out; ?></td>
                                                            <td><?php echo  $v_data->atb; ?></td>
                                                            <td><?php echo  $v_data->atd; ?></td>
                                                        </tr> 
                                             
                                            </tbody>
                                        </table> 
                                    <?php } ?>
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Inv No.</th>
                                                <th style="text-align: left;">Tariff Code</th>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: right;">QTY</th>
                                                <th style="text-align: right;">Rate</th>
                                                <!--<th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: right;">Vat (THB)</th>-->
                                                <th style="text-align: right;">W / T (THB)</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr"  style="font-size: 13px;">
                                            <?php 
                                                    $all_amount= 0;
                                                    $payment= 0;
                                                    $y_wh = 0;
                                                    $y_vat = 0;
                                                foreach ($order as $rs_order) { 
                                                    $is_vat =  $v_data->vat;
                                                    $line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
                                                    $all_amount += $line_amount;
                                                    
                                                    if($rs_order['code_vat'] != 'N'){
                                                        $y_vat += $rs_order['cal_vat'];
                                                    }

                                                    if($rs_order['code_wh'] != 'N'){
                                                        $y_wh += $rs_order['cal_wh'];
                                                    }

                                            ?>
                                                <tr>
                                                    <td style="text-align: left;"><?php 
                                                            
                                                            echo $rs_order['dr_no']; 
                                                            

                                                    ?></td>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_code']; ?></td>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_des']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['qty'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "GRT"; } else { echo "Unit"; } ?></b>
                                                    </td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "Days"; } else { echo "THB"; } ?></b>
                                                    </td>
                                                    <!--<td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'] * $rs_order['qty'],2); ?></td>
                                                    <td align="right">
                                                        <?php if($rs_order['code_vat'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>-->
                                                    <td align="right">
                                                        <?php if($rs_order['code_wh'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php

                                                            $line_amount = (($rs_order['cur_rate'] * $rs_order['qty']));
                                                            $payment += $line_amount;
                                                            echo number_format(($line_amount), 2);
                                                        ?>
                                                        &nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
                                                    </td>
                                                <tr>
                                            <?php } ?>  
                                                <tr>
                                                    <td style="text-align: right;" colspan="6"><b>Sub Total</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($all_amount,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6"><b>VAT 7%</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6"><b>Total</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($all_amount+$y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6"><b>Less Witholding Tax</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($y_wh,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="6"><b>Net Pay</b></td>
                                                    <td style="text-align: right;"><?php echo number_format(($all_amount+$y_vat)-$y_wh,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                        </tbody>
                                    </table>  
                                    <div align="right">
                                        <!--<label>Receipt Date</label>
                                        <input class="form-control" type="date" name="inv_date" id="inv_date" value="" style="width: 200px;">-->
                                        <br>
                                        <?php if(count($count_multi) <> "1"){ ?>
                                            <button class="btn btn-warning btn gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Reprint- Receipt</button>
                                        <?php } else { ?>
                                            <a href="<?php echo site_url(); ?>Speacial/RePrintReceipt/<?php echo $dr_no; ?>"><button class="btn btn-warning btn"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Reprint- Receipt</button></a>
                                        <?php } ?>
                                        <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger btn ">Cancel</button></a>
                                    </div>
                                </div>


                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<form id="form-multi" action="<?php echo site_url(); ?>Speacial/ReMultiReceipt" enctype="multipart/form-data" method="post" enctype="multipart/form-data">
    <input type="hidden" id="res" name="res" value="<?php echo $res; ?>">
    <input type="hidden" id="date_res" name="date_res" value="<?php echo $date_res; ?>">
</form>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){


    $('.gen-invoice').click(function(){

        if($('#date_res').val() != '' || $('#date_res').val() != null){
            $('#form-multi').submit();
        } else {
            alert('Please specify date of receipt.');
        }
       
       
    });


});
</script>