<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Speacial Service Request</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div style="float: left;">
                            <h6>&nbsp;&nbsp;&nbsp;<b>SSR NO : </b><?php echo $dr_no; ?></h6>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview Receipt</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Code</th>
                                                <th >Customer</th>
                                                <th >Branch</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody  style="font-size: 13px;">
                                           
                                                    <tr>
                                                        <td><?php echo  $cus->customer_code; ?></td>
                                                        <td><?php echo  $cus->customer_name; ?></td>
                                                        <td><?php echo  $cus->customer_branch; ?></td>
                                                        <td><?php echo  $cus->customer_address; ?><?php echo  $cus->customer_address2; ?><?php echo  $cus->customer_address3; ?> <?php echo  $cus->customer_post; ?></td>
                                                        <td><?php echo  $cus->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  

                                    <?php if($v_data->vsl_visit){  ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th >Vessel</th>
                                                    <th >Voy In</th>
                                                    <th >Voy Out</th>
                                                    <th >ATB</th>
                                                    <th >ATD</th>
                                                </tr>
                                            </thead>
                                            <tbody  style="font-size: 13px;">
                                               
                                                        <tr>
                                                            <td>( <?php echo $v_data->vsl_visit;?> ) <?php echo $v_data->vessel;?></td>
                                                            <td><?php echo  $v_data->voy_in; ?></td>
                                                            <td><?php echo  $v_data->voy_out; ?></td>
                                                            <td><?php echo  $v_data->atb; ?></td>
                                                            <td><?php echo  $v_data->atd; ?></td>
                                                        </tr> 
                                             
                                            </tbody>
                                        </table> 
                                    <?php } ?>
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Tariff Code</th>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: right;">QTY</th>
                                                <th style="text-align: right;">Rate</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: right;">Vat (THB)</th>
                                                <th style="text-align: right;">WH Tax (THB)</th>
                                                <th style="text-align: right;">Line Amount (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr"  style="font-size: 13px;">
                                            <?php 
                                                $all_amount= 0;
                                                $payment= 0;
                                                foreach ($order as $rs_order) { 
                                                $is_vat =  'YES';
                                                $line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
                                                $all_amount += $line_amount;
                                            ?>
                                                <tr>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_code']; ?></td>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_des']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['qty'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "GRT"; } else { echo "Unit"; } ?></b>
                                                    </td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "Days"; } else { echo "THB"; } ?></b>
                                                    </td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'] * $rs_order['qty'],2); ?></td>
                                                    <td align="right">
                                                        <?php if($rs_order['code_vat'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php if($rs_order['code_wh'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php

                                                            $line_amount = (($rs_order['cur_rate'] * $rs_order['qty'])+$rs_order['cal_vat'])-$rs_order['cal_wh'];
                                                            $payment += $line_amount;
                                                            echo number_format(($line_amount), 2);
                                                        ?>
                                                        &nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
                                                    </td>
                                                <tr>
                                            <?php } ?>  
                                        </tbody>
                                    </table>   
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <?php if($is_vat == 'YES'){ ?>
                                        <div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1">VAT(<?php echo $vat->vat; ?>%) (THB)</label>
                                              <p><?php echo number_format($Tax->all_vat,2); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                        <b>Remark : </b> <?php echo  $v_data->remark; ?>
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1"><?php if($cus->country == null or $cus->country == 'TH'){ ?> Total (THB) <?php } else { ?> Net Pay(THB) <?php } ?> </label>
                                              <p><?php echo number_format( $all_amount + $Tax->all_vat,2); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($cus->country == null or $cus->country == 'TH'){ ?>
                                        <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="holdtax"><input type="checkbox" id="holdtax" value="1" <?php if($all_amount >= 1000) { echo "checked"; } ?>>&nbsp; &nbsp; Withholding Tax (<?php echo $with_hold; ?>%) (THB)</label>
                                                  <p id="hold_v"><?php echo number_format($Tax->all_wh,2); ?></p>
                                                  <input type="hidden" name="hold_total" id="hold_total" value="<?php echo number_format($Tax->all_wh, 2, '.', ''); ?>">
                                                  <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo number_format($all_amount + $Tax->all_vat - $Tax->all_wh, 2, '.', ''); ?>">
                                                  <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  number_format($all_amount + $Tax->all_vat, 2, '.', ''); ?>">
                                                </div>
                                            </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <?php if($all_amount >= 1000){ ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount + $Tax->all_vat - $Tax->all_wh,2); ?></p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3">
                                                <div class="form-group"  align="right">
                                                  <label for="sel1">Net Pay(THB)</label>
                                                  <p id="net_p"><?php echo  number_format($all_amount + $Tax->all_vat,2); ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                     <?php } ?>
                                    <?php } else { ?>

                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1"><?php if($cus->country == null or $cus->country == 'TH'){ ?> Total (THB) <?php } else { ?> Net Pay(THB) <?php } ?> </label>
                                              <p><?php echo number_format( $all_amount,2); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                        
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>

                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            
                                        </div>
                                    <?php } ?> 
                                    <!--<div class="col-lg-3">
                                            <div class="form-group"  align="right">
                                              <label for="sel1"><?php if($cus->country == null or $cus->country == 'TH'){ ?> Total (THB) <?php } else { ?> Net Pay(THB) <?php } ?> </label>
                                              <p><?php echo number_format($payment,2); ?></p>
                                            </div>
                                        </div>-->
                                    <div align="right">
                                        <label>Receipt Type</label>
                                        <select class="form-control" aria-label="Default select example" style="width: 200px;" id="receipt_type">
                                                <option value="0">-Select-</option>
                                                <option value="R">Receipt</option>
                                                <option value="RT">Receipt / Tax Invoice</option>
                                            </select>
                                        <br>
                                        <label>Receipt Date</label>
                                        <input class="form-control" type="date" name="inv_date" id="inv_date" value="" style="width: 200px;">
                                        <br>
                                        <input type="hidden" id="is_vat" value="<?php echo $is_vat; ?>">
                                        <input type="hidden" id="none_hold" value="1">
                                        <input type="hidden" id="dr_no" value="<?php echo $dr_no; ?>">
                                        <button class="btn btn-info btn-xs gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Genarate Receipt</button>
                                        <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger btn-xs ">Cancel</button></a>
                                    </div>
                                </div>


                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    var is_vat = $('#is_vat').val();



$("#holdtax").bind("change",function(){
   var value = $(this).is(":checked");
   if(value){
        var hold_total = parseFloat($('#hold_total').val()).toFixed( 2 );
        var net_total_h = parseFloat($('#net_total_h').val()).toFixed( 2 );
        $('#hold_v').html(hold_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#net_p').html(net_total_h.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

        $('#none_hold').val('1');
   } else {

        var net_total_n = parseFloat($('#net_total_n').val()).toFixed( 2 );
        //$('#hold_v').html('0.00');
        $('#net_p').html(net_total_n.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#none_hold').val('0');
   }
});

 $('.gen-invoice').click(function(){

        var dr_no = $('#dr_no').val();
        var none_hold = $('#none_hold').val();
        var inv_date = $('#inv_date').val();
        var receipt_type = $('#receipt_type').val();

        if(inv_date == null || inv_date == ''){

            alert('Please specify receipt date.');

        } else if (receipt_type == '0') {

            alert('Please specify receipt type.');

        } else {

            var r = confirm("Confirm to generate Receipt Invoice ?");
                if (r == true) {  
                    
                    window.location = '<?php echo site_url(); ?>Speacial/SetReceipt/'+dr_no+'/'+none_hold+'/'+inv_date+'/'+receipt_type;
                       
                } 

        }
           
        });



});
</script>