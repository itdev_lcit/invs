<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>

<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>

<?php 
$start_rang = 0;


if((count($order) % 22) != '0'){
	if(((count($order) % 22) >= '7') && ((count($order) % 22) <= '22')){
		$pag = ceil(count($order)/22)+1;
	} else {
		$pag = ceil(count($order)/22);
	}
	
} else {
	$pag = ceil(count($order)/22)+1;
}


for ($i_page=1; $i_page <= $pag; $i_page++) { 
?>
<div class="page">
		<div align="right">
			<b ><?php echo $i_page.' / '.$pag; ?></b>
		</div>
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h4 align="center"><b><?php echo $v_data->receipt_type; ?></b></h4>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 25px;"><b>Original</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $res_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($receipt_date)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $cus->customer_branch;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Payment Terms </b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $v_data->payment." ".$cus->credit_term.' DAYS';;?></p></td>
					</tr>

					<tr>
						<td width="10%"><p style="font-size: 11px;"></p></td>
						<td width="25%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
						<td></td>
						<td width="16%"></td>
						<td width="25%"></td>
					</tr>

				</table>
				<br>
				<table class="table">
				    <thead style="font-size: 11px;">
				        <tr>
				        	<th style="text-align: center;">Item.</th>
				        	<th style="text-align: left;">Description Ref.</th>
				        	<th style="text-align: right;">Date Inv.</th>
				        	<th style="text-align: right;">Vat (THB)</th>
                            <th style="text-align: right;">W / T (THB)</th>
                            <th style="text-align: right;">Total (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="dr" style="font-size: 11px;">
                         <?php 
                            $all_amount= 0;
                            $payment= 0;
                            $y_wh = 0;
                            $y_vat = 0;
                            
                            

                            if($i_page > 1){
							    $loop_begin = $loop_begin+22;
							} else {
							    $loop_begin = $start_rang;
							}   

							if($i_page > 1){
							   $i = $loop_begin+1;
							}	else {
							   $i = 1;
							}
                            
                            foreach (array_slice($order, $loop_begin, 22) as $rs_order) { 
                                $is_vat =  $v_data->vat;
                                $line_amount = $rs_order['TotalInv'];
                                $all_amount += $line_amount;
                                                    
                                $y_vat += $rs_order['cal_vat'];
                                $y_wh += $rs_order['cal_wh'];

                        ?>
		                        <tr>
		                        	<td style="text-align: center;">
		                            	<?php echo $i; ?>
		                            </td>
		                            <td style="text-align: left;">
		                            	<?php echo $rs_order['dr_no']; ?>
		                            </td>
		                            <td style="text-align: right;">
		                            	<?php echo date("j-M-y H:i", strtotime($rs_order['created'])); ?>
		                            </td>
		                            <td align="right">
		                                <?php if($rs_order['code_vat'] != 'N'){ ?>
		                                	<?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
		                                <?php } else { ?>
		                                     -
		                                <?php } ?>
		                            </td>
		                            <td align="right">
		                                <?php if($rs_order['code_wh'] != 'N'){ ?>
		                                	<?php if($chk_wht == '1'){ ?>
		                                		<?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
		                                	<?php } else { ?>
		                                	    -
		                                	<?php } ?>
		                                <?php } else { ?>
		                                     -
		                                <?php } ?>
		                            </td>
		                            <td align="right">
		                                <?php
											$line_amount = $rs_order['TotalInv'];
		                                    $payment += $line_amount;
		                                     echo number_format(($line_amount), 2);
		                                ?>
		                                &nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
		                            </td>
		                        <tr>
                           	<?php $i++;} ?>  

                           	<?php if($i_page == $pag){ ?>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Sub Total</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_all_amount,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>VAT 7%</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Total</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_all_amount+$s_y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <?php if($chk_wht == '1'){ ?>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Less Witholding Tax</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_y_wh,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                	<td colspan="4">
                                		<table class="table table-striped table-bordered">
									        <tr>
									            <td style="font-size: 13px; text-align: center;">
									            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

									            	$originDate = new DateTime($receipt_date);
					 								$chkDate = new DateTime("01/10/2022");

									            	if($originDate >= $chkDate) {
									            		$net = $s_all_amount+$s_y_vat;

									            		$num = $net;

									            		$rest = substr(number_format(($net), 2), -2);
									            	} else {
									            		$net = ($s_all_amount+$s_y_vat)-$s_y_wh;

									            		$num = $net;

									            		$rest = substr(number_format(($net), 2), -2);	
									            	}

									            	$num = str_replace(array(',', ' '), '' , trim($num));
									            	
												    if(! $num) {
												        return false;
												    }
												    $num = (int) $num;
												    $words = array();
												    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
												        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
												    );
												    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
												    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
												        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
												        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
												    );
												    $num_length = strlen($num);
												    $levels = (int) (($num_length + 2) / 3);
												    $max_length = $levels * 3;
												    $num = substr('00' . $num, -$max_length);
												    $num_levels = str_split($num, 3);
												    for ($i = 0; $i < count($num_levels); $i++) {
												        $levels--;
												        $hundreds = (int) ($num_levels[$i] / 100);
												        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
												        $tens = (int) ($num_levels[$i] % 100);
												        $singles = '';
												        if ( $tens < 20 ) {
												            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
												        } else {
												            $tens = (int)($tens / 10);
												            $tens = ' ' . $list2[$tens] . ' ';
												            $singles = (int) ($num_levels[$i] % 10);
												            $singles = ' ' . $list1[$singles] . ' ';
												        }
												        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
												    } //end for loop
												    $commas = count($words);
												    if ($commas > 1) {
												        $commas = $commas - 1;
												    }

												    if($rest > 0){
												    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
												    } else {
												    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
												    }
												    

									            	 ?></p>
									            	
									            </td>
									        </tr>
									</table>
                                	</td>
                                    <td style="text-align: right;">
                                    	<b>Net Pay</b>
                                    </td>
                                    <td style="text-align: right;">
                                    	<?php echo number_format(($s_all_amount+$s_y_vat)-$s_y_wh,2); ?>&nbsp;&nbsp;<b>THB</b>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
				</table>

				<?php if($i_page == $pag){ ?>
				<table class="table">
					<thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
				        </tr>
				        <tr>
				            <th style="font-size: 11px; text-align: left;">Paid by:</th>
				            <th style="font-size: 11px; text-align: left;">Bank :</th>
				            <th style="font-size: 11px; text-align: left;">Cheque no. :</th>
				            <th style="font-size: 11px; text-align: left;">Branch :</th>
				            <th style="font-size: 11px; text-align: left;">Date :</th>
				        </tr>
				    </thead>
				    <tbody>

				    	<tr>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    	</tr>

				    	
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">Received By : <?php echo $user_print->name; ?> on <?php echo date("j-M-Y H:i", strtotime($datePrint)); ?></td>

				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 11px;" width="50%">
				            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
				            	
				            </td>
				            <td style="font-size: 11px;" width="50%">
				            	<p>This is a computer generated document, no signature required.</p>
				            	<!--<p>Contact detail:</p>
				            	<p>Email :</p>-->
				            	<p>Tel :  (66 38 ) 40 8200</p>
				            	<p>Fax : (66 38) 40 1021 - 2</p>
				            </td>
				        </tr>
				</table>
				 <?php } ?>
</div>

<?php } ?>

<!---COPY-->





<?php 
$start_rang = 0;


if((count($order) % 22) != '0'){
	if(((count($order) % 22) >= '7') && ((count($order) % 22) <= '22')){
		$pag = ceil(count($order)/22)+1;
	} else {
		$pag = ceil(count($order)/22);
	}
	
} else {
	$pag = ceil(count($order)/22)+1;
}


for ($i_page=1; $i_page <= $pag; $i_page++) { 
?>
<div class="page">
		<div align="right">
			<b ><?php echo $i_page.' / '.$pag; ?></b>
		</div>
<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
<br>
<h4 align="center"><b><?php echo $v_data->receipt_type; ?></b></h4>


				<table>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="25%"><p style="font-size: 25px;"><b>Copy</b></td>
					</tr>
					<tr>

						<td width="10%"><p style="font-size: 11px;"><b>Customer</b></p></td>
						<td width="50%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_name;?></p></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td width="16%"><p style="font-size: 11px;"><b>No.</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $res_no; ?></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo date("j-M-y H:i", strtotime($receipt_date)); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address2; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->ar_company; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td width="50%"><p style="font-size: 11px;">&nbsp;&nbsp;<?php echo $cus->customer_address3; ?> <?php echo $cus->customer_post; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->customer_code; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 11px;"><b>Tax ID</b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $cus->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $cus->customer_branch;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 11px;"><b>Payment Terms </b></p></td>
						<td width="25%"><p style="font-size: 11px;">:&nbsp;&nbsp;<?php echo $v_data->payment." ".$cus->credit_term.' DAYS';;?></p></td>
					</tr>

					<tr>
						<td width="10%"><p style="font-size: 11px;"></p></td>
						<td width="25%"><p style="font-size: 11px;">&nbsp;&nbsp;</p></td>
						<td></td>
						<td width="16%"></td>
						<td width="25%"></td>
					</tr>

				</table>
				<br>
				<table class="table">
				    <thead style="font-size: 11px;">
				        <tr>
				        	<th style="text-align: center;">Item.</th>
				        	<th style="text-align: left;">Description Ref.</th>
				        	<th style="text-align: right;">Date Inv.</th>
				        	<th style="text-align: right;">Vat (THB)</th>
                            <th style="text-align: right;">W / T (THB)</th>
                            <th style="text-align: right;">Total (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="dr" style="font-size: 11px;">
                         <?php 
                            $all_amount= 0;
                            $payment= 0;
                            $y_wh = 0;
                            $y_vat = 0;
                            
                            

                            if($i_page > 1){
							    $loop_begin = $loop_begin+22;
							} else {
							    $loop_begin = $start_rang;
							}   

							if($i_page > 1){
							   $i = $loop_begin+1;
							}	else {
							   $i = 1;
							}
                            
                            foreach (array_slice($order, $loop_begin, 22) as $rs_order) { 
                                $is_vat =  $v_data->vat;
                                $line_amount = $rs_order['TotalInv'];
                                $all_amount += $line_amount;
                                                    
                                $y_vat += $rs_order['cal_vat'];
                                $y_wh += $rs_order['cal_wh'];

                        ?>
		                        <tr>
		                        	<td style="text-align: center;">
		                            	<?php echo $i; ?>
		                            </td>
		                            <td style="text-align: left;">
		                            	<?php echo $rs_order['dr_no']; ?>
		                            </td>
		                            <td style="text-align: right;">
		                            	<?php echo date("j-M-y H:i", strtotime($rs_order['created'])); ?>
		                            </td>
		                            <td align="right">
		                                <?php if($rs_order['code_vat'] != 'N'){ ?>
		                                	<?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
		                                <?php } else { ?>
		                                     -
		                                <?php } ?>
		                            </td>
		                            <td align="right">
		                                <?php if($rs_order['code_wh'] != 'N'){ ?>
		                                	<?php if($chk_wht == '1'){ ?>
		                                		<?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
		                                	<?php } else { ?>
		                                	    -
		                                	<?php } ?>
		                                <?php } else { ?>
		                                     -
		                                <?php } ?>
		                            </td>
		                            <td align="right">
		                                <?php
											$line_amount = $rs_order['TotalInv'];
		                                    $payment += $line_amount;
		                                     echo number_format(($line_amount), 2);
		                                ?>
		                                &nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
		                            </td>
		                        <tr>
                           	<?php $i++;} ?>  

                           	<?php if($i_page == $pag){ ?>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Sub Total</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_all_amount,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>VAT 7%</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Total</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_all_amount+$s_y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <?php if($chk_wht == '1'){ ?>
                                <tr>
                                    <td style="text-align: right;" colspan="5"><b>Less Witholding Tax</b></td>
                                    <td style="text-align: right;"><?php echo number_format($s_y_wh,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                	<td colspan="4">
                                		<table class="table table-striped table-bordered">
									        <tr>
									            <td style="font-size: 13px; text-align: center;">
									            	<p>Amount In Letter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php 

									            	$originDate = new DateTime($receipt_date);
					 								$chkDate = new DateTime("01/10/2022");

									            	if($originDate >= $chkDate) {
									            		$net = $s_all_amount+$s_y_vat;

									            		$num = $net;

									            		$rest = substr(number_format(($net), 2), -2);
									            	} else {
									            		$net = ($s_all_amount+$s_y_vat)-$s_y_wh;

									            		$num = $net;

									            		$rest = substr(number_format(($net), 2), -2);	
									            	}

									            	$num = str_replace(array(',', ' '), '' , trim($num));
												    if(! $num) {
												        return false;
												    }
												    $num = (int) $num;
												    $words = array();
												    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
												        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
												    );
												    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
												    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
												        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
												        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
												    );
												    $num_length = strlen($num);
												    $levels = (int) (($num_length + 2) / 3);
												    $max_length = $levels * 3;
												    $num = substr('00' . $num, -$max_length);
												    $num_levels = str_split($num, 3);
												    for ($i = 0; $i < count($num_levels); $i++) {
												        $levels--;
												        $hundreds = (int) ($num_levels[$i] / 100);
												        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
												        $tens = (int) ($num_levels[$i] % 100);
												        $singles = '';
												        if ( $tens < 20 ) {
												            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
												        } else {
												            $tens = (int)($tens / 10);
												            $tens = ' ' . $list2[$tens] . ' ';
												            $singles = (int) ($num_levels[$i] % 10);
												            $singles = ' ' . $list1[$singles] . ' ';
												        }
												        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
												    } //end for loop
												    $commas = count($words);
												    if ($commas > 1) {
												        $commas = $commas - 1;
												    }

												    if($rest > 0){
												    	echo implode(' ', $words)."and&nbsp;&nbsp;".$rest."/100&nbsp;&nbsp;THB";
												    } else {
												    	echo implode(' ', $words)."&nbsp;&nbsp;THB";
												    }
												    

									            	 ?></p>
									            	
									            </td>
									        </tr>
									</table>
                                	</td>
                                    <td style="text-align: right;">
                                    	<b>Net Pay</b>
                                    </td>
                                    <td style="text-align: right;">
                                    	<?php echo number_format(($s_all_amount+$s_y_vat)-$s_y_wh,2); ?>&nbsp;&nbsp;<b>THB</b>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
				</table>

				<?php if($i_page == $pag){ ?>
				<table class="table">
					<thead>
				        <tr>
				            <th style="font-size: 11px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
				        </tr>
				        <tr>
				            <th style="font-size: 11px; text-align: left;">Paid by:</th>
				            <th style="font-size: 11px; text-align: left;">Bank :</th>
				            <th style="font-size: 11px; text-align: left;">Cheque no. :</th>
				            <th style="font-size: 11px; text-align: left;">Branch :</th>
				            <th style="font-size: 11px; text-align: left;">Date :</th>
				        </tr>
				    </thead>
				    <tbody>

				    	<tr>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    		<td style="font-size: 11px; text-align: center;"><br>..................................................</td>
				    	</tr>

				    	
				    	<tr>
				    		<td style="font-size: 11px; text-align: left;" colspan="3">Received By : <?php echo $user_print->name; ?> on <?php echo date("j-M-Y H:i", strtotime($datePrint)); ?></td>

				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 11px;" width="50%">
				            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
				            	
				            </td>
				            <td style="font-size: 11px;" width="50%">
				            	<p>This is a computer generated document, no signature required.</p>
				            	<!--<p>Contact detail:</p>
				            	<p>Email :</p>-->
				            	<p>Tel :  (66 38 ) 40 8200</p>
				            	<p>Fax : (66 38) 40 1021 - 2</p>
				            </td>
				        </tr>
				</table>
				 <?php } ?>
</div>

<?php } ?>





<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  	
    $(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		location.reload();
		//document.location.href = "<?php echo site_url(); ?>Speacial/SSR";
	});
});
</script>