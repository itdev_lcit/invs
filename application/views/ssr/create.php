
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/autocomplete.css">   
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Create SSR</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">

                
               <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Customer 
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-6">
                                        <input type="text" id="payer_nm" name="payer_nm" class="form-control" placeholder="Search Payer name"> 
                                    </div>
                                    <div class="col-md-6" >
                                        <button class="btn btn-success search-cus"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                     <div class="col-md-12" >
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    </div>


                                    <div class="col-md-12" >
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;" width="5%">Code</th>
                                                    <th  width="40%">Name</th>
                                                    <th  width="40%">Address</th>
                                                    <th >A/C No.</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="payer">
                                                <tr>
                                                    <td colspan="5" style="text-align: center;">Please Search Payer name</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                Vessel 
                        </div>
                        <br>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"> 
                                    <div class="col-md-4" align="right">
                                        <input type="text" id="vsl_visit" name="vsl_visit" class="form-control" placeholder="Search Vessle Visit Code"> 
                                        <br>
                                    </div>

                                    <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="35%">Vessel Visit</th>
                                            <th  width="65%">Vessel Name</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="vsl_order">
                                        <tr>
                                            <td colspan="3" style="text-align: center;">Please Search Vessle Visit Code</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Option
                        </div>
                       <!-- <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Speacial Service Request</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Customer</th>
                                                <th width="50%">Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td> <div id="customer_name"></td>
                                                        <td> <div id="customer_address"></td>
                                                        
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>

                                <div class="col-lg-12">
                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <th width="50%">Vessel</th>
                                                <th width="">Voy In</th>
                                                <th width="">Voy Out</th>
                                                <th width="">ATB</th>
                                                <th width="">ATD</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                           <tr>
                                                <td> <div id="vessel"> </td>
                                                <td> <div id="vsL_voyin"></td>
                                                <td> <div id="vsl_voyout"></td>
                                                <td> <div id="vsl_atb"></td>
                                                <td> <div id="vsl_atd"></td>       
                                            </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <!--<th style="text-align: center;">#</th>-->
                                                <th style="text-align: left; width: 10%;">TARIFF CODE </th>
                                                <th style="text-align: left; width: 20%;">Description</th>
                                                <th style="text-align: right; width: 10%;">QTY</th>
                                                <th style="text-align: right; width: 10%;">Rate </th>
                                                <th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: right;">Vat </th>
                                                <th style="text-align: right;">Withholding Tax </th>
                                                <th style="text-align: right;">Net (THB)</th>
                                                <th style="text-align: center;"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <?php for ($i=1; $i <= $numOrder; $i++) {  ?>
                                                    <tr class="r-order" data-rang="<?php echo $i; ?>">
                                                        <!--<td style="text-align: center;"><?php echo $i; ?></td>-->
                                                        <td>
                                                            <select class="form-control type" name="type" id="type-<?php echo $i; ?>">
                                                                    <option value="0">-----</option> 
                                                                <?php foreach ($tariff_type as $rs) { ?>
                                                                    <option value="<?php echo $rs['tariff_id']; ?>"><?php echo $rs['tariff_code']; ?> | <?php echo $rs['tariff_des']; ?></option>      
                                                                <?php } ?>                 
                                                            </select>  
                                                        </td>
                                                        <td style="text-align: left;"><p id="tariff_des-<?php echo $i; ?>">-</p></td>
                                                        <td style="text-align: right;">
                                                            <input type="number" name="qty" class="form-control value-split" id="qty-<?php echo $i; ?>" value="0" style="text-align: right;">  
                                                        </td>
                                                        <td style="text-align: right;"><p id="rate_u-<?php echo $i; ?>">0.00</p>
                                                            <input type="number" id="rate-pro-<?php echo $i; ?>" class="form-control unit_rate" style=" display:none; float: right;text-align: right;">

                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="total-<?php echo $i; ?>">0.00</p>
                                                            <input type="hidden" name="cal_total-<?php echo $i; ?>" id="cal_total-<?php echo $i; ?>" value="">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="vat_u-<?php echo $i; ?>">0.00 (0.00 %) </p>
                                                            <input type="hidden" name="vat-pro-<?php echo $i; ?>" id="vat-pro-<?php echo $i; ?>" value="">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <p id="holdtax_u-<?php echo $i; ?>">0.00 (0.00 %) </p>
                                                            <input type="hidden" name="holdtax-pro-<?php echo $i; ?>" id="holdtax-pro-<?php echo $i; ?>" value="">
                                                        </td>

                                                        <td style="text-align: right;">
                                                            <p id="net-<?php echo $i; ?>">0.00</p>
                                                            <input type="hidden" name="net_total-<?php echo $i; ?>" id="net_total-<?php echo $i; ?>" value="">
                                                        </td>
                                                        <td  style="text-align: center;"> 
                                                            <button class="btn btn-danger order_del"><i class="fa fa-remove"></i></button> 
                                                            <!--<?php if($i > 1) { ?>
                                                                <form class="form-inline" action="<?php echo site_url() . 'Speacial/Create'; ?>" method="POST">
                                                                    <input type="hidden" name="sort" value="<?php echo $type; ?>">
                                                                    <input type="hidden" name="numOrder" value="<?php echo $numOrder-1; ?>">
                                                                    <input type="hidden" name="visit_his" id="visit_his" value="<?php echo $visit_his; ?>">
                                                                    <input type="hidden" name="comp_his" id="comp_his" value="<?php echo $comp_his; ?>">
                                                                    <input type="hidden" name="id_comp_b" id="id_comp_b" value="<?php echo $id_comp; ?>">
                                                                    <button class="btn btn-danger delrow"><i class="fa fa-remove"></i></button>
                                                                    &nbsp;&nbsp;
                                                                </form>
                                                            <?php } ?>-->
                                                        </td>
                                                    </tr>  
                                            <?php 

                                                }  ?>
                                                    <!--<tr>
                                                        <td colspan="9">
                                                            <form class="form-inline" action="<?php echo site_url() . 'Speacial/Create'; ?>" method="POST">
                                                                <input type="hidden" name="sort" value="<?php echo $type; ?>">
                                                                <input type="hidden" name="visit_his" id="visit_his" value="<?php echo $visit_his; ?>">
                                                                <input type="hidden" name="comp_his" id="comp_his" value="<?php echo $comp_his; ?>">
                                                                <input type="hidden" name="id_comp_f" id="id_comp_f" value="<?php echo $id_comp; ?>">
                                                                <input type="hidden" name="numOrder" value="<?php echo $numOrder+1; ?>">
                                                                <button class="btn btn-success add-row-cntr"><i class="fa fa-plus-circle"></i></button>
                                                                &nbsp;&nbsp;
                                                            </form>
                                                            </td>
                                                    </tr>-->  

                                                   
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" style="text-align: right;">
                                                </td>
                                                <td style="text-align: center;">
                                                    <button class="btn btn-success add-row-cntr"><i class="fa fa-plus-circle"></i></button>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" style="text-align: right;"><b style="font-size: 17px;">Net Total<b></td>
                                                <td style="text-align: right;"><p id="sum_total">0.00</p></td>
                                            </tr>
                                        </tbody>
                                    </table>   

                                    <div class="col-lg-8">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Remark 
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-1"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-2"></textarea>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <textarea class="form-control" rows="5" id="remark-3"></textarea>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->

                                    <div class="col-lg-4">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                    Option 
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12"> 
                                                        <div class="col-lg-6"> 
                                                            <label>Inv Date</label>
                                                                <input class="form-control" type="date" name="inv_date" id="inv_date" value="">
                                                            <br>
                                                        </div>

                                                        <div class="col-lg-6"> 
                                                            <label>Payment</label>
                                                            <select class="form-control type" name="payment" id="payment" >
                                                               <option value="0">-----</option> 
                                                               <option value="CASH">CASH</option>     
                                                               <option value="CREDIT">CREDIT</option>                 
                                                            </select> 
                                                        </div>

                                                        <!--<div class="col-lg-6"> 
                                                            <label>VAT</label>
                                                            <select class="form-control type" name="vat" id="vat">
                                                               <option value="0">-----</option> 
                                                               <option value="YES" selected>YES</option>     
                                                               <option value="NO">NO</option>                 
                                                            </select> 
                                                        </div>-->

                                                        <div class="col-lg-12" align="right"> 
                                                            <br>
                                                            <button class="btn btn-info pre-inv" title="Preview Invoice"><span class="glyphicon glyphicon-eye-open"></span></button>
                                                            <button class="btn btn-success printDr" title="Generate Invoice"><span class="glyphicon glyphicon-print"></span></button>
                                                            <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger can-dr" title="Cancel"><span class="glyphicon glyphicon-remove"></span></button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-lg-12 -->

                                    <!--<div align="right">
                                        <label>Payment</label>
                                        <select class="form-control type" name="payment" id="payment" style="width: 200px;">
                                           <option value="0">-----</option> 
                                           <option value="CASH">CASH</option>     
                                           <option value="CREDIT">CREDIT</option>                 
                                        </select>  
                                        <br>
                                        <button class="btn btn-success printDr"><span class="glyphicon glyphicon-print"></span>Generate</button>
                                        <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger can-dr"><span class="glyphicon glyphicon-remove"></span>cancel</button></a>
                                    </div>-->
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->

</div>

<!-- Modal -->
<div id="insert_tax" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <p>*Please Input Tax ID, IF Customer live in Thailand input tax id 13 number , Else Input 0*</p>
          </div>

                <div class="form-group" align="center">
                    <label>TAX ID</label>

                    <input type="text" name="in_tax" id="in_tax" value="" class="form-control" style="width: 550px;" >
                    <font color="red"><p id="msg-error-tax"></p></font>
                    <input type="hidden" name="in_customer_name" id="in_customer_name" value="">
                    <input type="hidden" name="in_customer_address" id="in_customer_address" value="">
                    <input type="hidden" name="in_id_comp" id="in_id_comp" value="">
                    <input type="hidden" name="in_payer_code" id="in_payer_code" value="">
                    <input type="hidden" name="in_payer_tel" id="in_payer_tel" value="">
                </div>  
                <div class="form-group" align="center">
                    <label>Country</label>
                    <input type="text" name="in_country" id="in_country" value="Thailand" class="form-control" style="width: 550px;" placeholder="Auto Complete Country">
                    <font color="red"><p id="msg-error-country"></p></font>
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-success save-tax">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

        </div>

  </div>
</div>
<input type="hidden" name="ref" id="ref" value="">
<input type="hidden" name="id_comp" id="id_comp" value="">
<input type="hidden" name="v_name" id="v_name" value="">
<input type="hidden" name="v_visit" id="v_visit" value="">
<input type="hidden" name="v_voyin" id="v_voyin" value="">
<input type="hidden" name="v_voyout" id="v_voyout" value="">
<input type="hidden" name="v_atb" id="v_atb" value="">
<input type="hidden" name="v_atd" id="v_atd" value="">
<input type="hidden" name="v_tonnage" id="v_tonnage" value="">

<input type="hidden" id="numOrder" value="<?php echo $numOrder; ?>">
<input type="hidden" name="row_c" id="row_c" value="1">
<input type="hidden" name="row_last" id="row_last" value="1">
<script src="<?php echo base_url(); ?>public/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/lib/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

            $('#dr').on('click', 'button.order_del', function(events){

                /*var $row = $(this).parents('tr.r-order');
                var rang = $row.data('rang');


                var row_c = $('#row_c').val();
                var row_last = $('#row_last').val();

                if(rang == row_last){
                    $('#row_last').val(parseInt(row_last)-1);
                } else if(rang > row_last){
                    $('#row_last').val(parseInt(row_last)+1);
                }

                $('#row_c').val(parseInt(row_c)-1);*/
                $(this).parents('.r-order').remove();
            });


            $(".add-row-cntr").click( function(e) {
                e.preventDefault();

                    var row_c = parseInt($('#row_last').val())+1;

                    //var SVC_C = $('#svc_v').val();

                    $("#dr").append('<tr class="r-order" data-rang="'+row_c+'"><td> <select class="form-control type" name="type" id="type-'+row_c+'"> </select> </td> <td style="text-align: left;"><p id="tariff_des-'+row_c+'">-</p></td> <td style="text-align: right;"> <input type="number" name="qty" class="form-control value-split" id="qty-'+row_c+'" value="0" style="text-align: right;"> </td> <td style="text-align: right;"><p id="rate_u-'+row_c+'">0.00</p> <input type="number" id="rate-pro-'+row_c+'" class="form-control unit_rate" style=" display:none; float: right;text-align: right;"> </td> <td style="text-align: right;"> <p id="total-'+row_c+'">0.00</p> <input type="hidden" name="cal_total-'+row_c+'" id="cal_total-'+row_c+'" value=""> </td> <td style="text-align: right;"> <p id="vat_u-'+row_c+'">0.00 (0.00 %) </p> <input type="hidden" name="vat-pro-'+row_c+'" id="vat-pro-'+row_c+'" value=""> </td> <td style="text-align: right;"> <p id="holdtax_u-'+row_c+'">0.00 (0.00 %) </p> <input type="hidden" name="holdtax-pro-'+row_c+'" id="holdtax-pro-'+row_c+'" value=""> </td> <td style="text-align: right;"> <p id="net-'+row_c+'">0.00</p> <input type="hidden" name="net_total-'+row_c+'" id="net_total-'+row_c+'" value=""> </td> <td  style="text-align: center;"> <button class="btn btn-danger order_del"><i class="fa fa-remove"></i></button> </td> </tr>');  

                    $.ajax({
                        url : '<?php echo site_url(); ?>Speacial/GetTariff',
                        method : 'POST',
                        cache: false

                    }).done(function(data){

                        var o = JSON.parse(data);
                        var i = 0;

                        var pod_order = '';

                                pod_order += '<option value="0">';
                                pod_order += '-----';
                                pod_order += '</option>';

                       for(i=0; i < o.length; i++){

                                pod_order += '<option value="'+o[i]['tariff_id']+'">';
                                pod_order += o[i]['tariff_code']+" | "+o[i]['tariff_des'];
                                pod_order += '</option>';
                        }

                        $('#type-'+row_c).append(pod_order);

                    });

                    $('#row_c').val(parseInt(row_c));
                    $('#row_last').val(parseInt(row_c));

                    return false;

            });


///autoCustomerHistory

var comp_his = '<?php echo $comp_his; ?>'

var visit_his = '<?php echo $visit_his; ?>'

var check_list_price = '<?php echo $numOrder; ?>'

var id_comp = '<?php echo $id_comp; ?>'

if(id_comp == 0  && comp_his != 0 && check_list_price == 1){
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 
            $('#id_comp_f').val('');
            $('#id_comp_b').val('');

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomerReNew',
                method : 'POST',
                data : {payer:comp_his}
            }).done(function(data){
                var o = JSON.parse(data);
                var i = 0;

                for(i=0; i < o.length; i++){

                    $('#customer_name').html(o[i]['COMPANY_NM']);
                    $('#customer_address').html(o[i]['STREET_ADDRESS1_DS']+o[i]['STREET_ADDRESS2_DS']+o[i]['STREET_ADDRESS3_DS']);  
                    $('#id_comp').val(o[i]['COMPANY_ID']);   
                    $('#id_comp_f').val(o[i]['COMPANY_ID']); 
                    $('#id_comp_b').val(o[i]['COMPANY_ID']); 

                }

            })

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
} else {

            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            var id_comp = '<?php echo $id_comp; ?>'

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomerReNew',
                method : 'POST',
                data : {payer:id_comp}
            }).done(function(data){
                var o = JSON.parse(data);
                var i = 0;

                for(i=0; i < o.length; i++){

                    $('#customer_name').html(o[i]['COMPANY_NM']);
                    $('#customer_address').html(o[i]['STREET_ADDRESS1_DS']+o[i]['STREET_ADDRESS2_DS']+o[i]['STREET_ADDRESS3_DS']);  
                    $('#id_comp').val(o[i]['COMPANY_ID']);   
                    $('#id_comp_f').val(o[i]['COMPANY_ID']); 
                    $('#id_comp_b').val(o[i]['COMPANY_ID']); 

                }

            })

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
}

if(visit_his != 0){
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacVsl',
                method : 'POST',
                data : {vsl_order:visit_his}
            }).done(function(data){
                var o = JSON.parse(data);
                var i = 0;

                for(i=0; i < o.length; i++){

                    $('#v_name').val(o[i]['VSL_NAME']);
                    $('#v_visit').val(o[i]['VSL_VISIT']);
                    $('#v_voyin').val(o[i]['VOY_IN']);
                    $('#v_voyout').val(o[i]['VOY_OUT']);
                    $('#v_atb').val(o[i]['ATB']);
                    $('#v_atd').val(o[i]['ATD']);
                    $('#v_tonnage').val(o[i]['VISIT_VSL_TONNAGE_NR']);


                    $('#vessel').html('('+o[i]['VSL_VISIT']+') ' + ' ' + o[i]['VSL_NAME']);
                    $('#vsL_voyin').html(o[i]['VOY_IN']);
                    $('#vsl_voyout').html(o[i]['VOY_OUT']);
                    $('#vsl_atb').html(o[i]['ATB']);
                    $('#vsl_atd').html(o[i]['ATD']);

                }

            })

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
}
            

//autoCustomerHistory


var states = [
    'Aruba','Antigua and Barbuda','United Arab Emirates','Afghanistan','Algeria','Azerbaijan','Albania','Armenia','Andorra','Angola','American Samoa','Argentina','Australia','Ashmore and Cartier Islands','Austria','Anguilla','Åland Islands'
    ,'Antarctica','Bahrain','Barbados','Botswana','Bermuda','Belgium','Bahamas, The','Bangladesh','Belize','Bosnia and Herzegovina','Bolivia','Myanmar','Benin','Belarus','Solomon Islands','Navassa Island','Brazil','Bassas da India','Bhutan'
    ,'Bulgaria','Bouvet Island','Brunei','Burundi','Canada','Cambodia','Chad','Sri Lanka','Congo, Republic of the','Congo, Democratic Republic of the','China','Chile','Cayman Islands','Cocos (Keeling) Islands','Cameroon','Comoros','Colombia'
    ,'Northern Mariana Islands','Coral Sea Islands','Costa Rica','Central African Republic','Cuba','Cape Verde','Cook Islands','Cyprus','Denmark','Djibouti','Dominica','Jarvis Island','Dominican Republic','Dhekelia Sovereign Base Area'
    ,'Ecuador','Egypt','Ireland','Equatorial Guinea','Estonia','Eritrea','El Salvador','Ethiopia','Europa Island','Czech Republic','French Guiana','Finland','Fiji','Falkland Islands (Islas Malvinas)','Micronesia, Federated States of'
    ,'Faroe Islands','French Polynesia','Baker Island','France','French Southern and Antarctic Lands','Gambia, The','Gabon','Georgia','Ghana','Gibraltar','Grenada','Guernsey','Greenland','Germany','Glorioso Islands','Guadeloupe','Guam'
    ,'Greece','Guatemala','Guinea','Guyana','Gaza Strip','Haiti','Hong Kong','Heard Island and McDonald Islands','Honduras','Howland Island','Croatia','Hungary','Iceland','Indonesia','Isle of Man','India','British Indian Ocean Territory'
    ,'Clipperton Island','Iran','Israel','Italy','Cote d Ivoire','Iraq','Japan','Jersey','Jamaica','Jan Mayen','Jordan','Johnston Atoll','Juan de Nova Island','Kenya','Kyrgyzstan','Korea, North','Kingman Reef','Kiribati','KoreaSouth'
    ,'Christmas Island','Kuwait','Kosovo','Kazakhstan','Laos','Lebanon','Latvia','Lithuania','Liberia','Slovakia','Palmyra Atoll','Liechtenstein','Lesotho','Luxembourg','Libyan Arab','Madagascar','Martinique','Macau','Moldova, Republic of'
    ,'Mayotte','Mongolia','Montserrat','Malawi','Montenegro','The Former Yugoslav Republic of Macedonia','Mali','Monaco','Morocco','Mauritius','Midway Islands','Mauritania','Malta','Oman','Maldives','Mexico','Malaysia','Mozambique'
    ,'New Caledonia','Niue','Norfolk Island','Niger','Vanuatu','Nigeria','Netherlands','No Mans Land','Norway','Nepal','Nauru','Suriname','Netherlands Antilles','Nicaragua','New Zealand','Paraguay','Pitcairn Islands','Peru','Paracel Islands'
    ,'Spratly Islands','Pakistan','Poland','Panama','Portugal','Papua New Guinea','Palau','Guinea-Bissau','Qatar','Reunion','Serbia','Marshall Islands','Saint Martin','Romania','Philippines','Puerto Rico','Russia','Rwanda','Saudi Arabia'
    ,'Saint Pierre and Miquelon','Saint Kitts and Nevis','Seychelles','South Africa','Senegal','Saint Helena','Slovenia','Sierra Leone','San Marino','Singapore','Somalia','Spain','Saint Lucia','Sudan','Svalbard','Sweden'
    ,'South Georgia and the Islands','Syrian Arab Republic','Switzerland','Trinidad and Tobago','Tromelin Island','Thailand','Tajikistan','Turks and Caicos Islands','Tokelau','Tonga','Togo','Sao Tome and Principe','Tunisia'
    ,'East Timor','Turkey','Tuvalu','Taiwan','Turkmenistan','Tanzania, United Republic of','Uganda','United Kingdom','Ukraine','United States','Burkina Faso','Uruguay','Uzbekistan','Saint Vincent and the Grenadines','Venezuela'
    ,'British Virgin Islands','Vietnam','Virgin Islands (US)','Holy See (Vatican City)','Namibia','West Bank','Wallis and Futuna','Western Sahara','Wake Island','Samoa','Swaziland','Serbia and Montenegro','Yemen','Zambia','Zimbabwe'
];

$('#in_country').autocomplete({
    source:[states]
});


$('#in_country').on('input', function(e){
    var in_country = $(this).val();
    if(in_country != null){
        $('#msg-error-country').html('');
        $.ajax({
            url:'<?php echo site_url(); ?>Setting/CheckCountry',
            method:'POST',
            data:{ in_country:in_country }
        }).done(function(data){
              
        })
   
    } else {
         $('#msg-error-country').html('');
    }
});


/*$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();

            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 
            $('#id_comp_f').val(''); 
            $('#id_comp_b').val(''); 

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id  = $row.data('ar_company_id');

             $.ajax({
                url:'<?php echo site_url(); ?>Setting/SavePayer',
                method:'POST',
                data:{ 
                    id_customer:id_customer ,
                    customer_address:customer_address,
                    customer_name:customer_name,
                    customer_code:customer_code,
                    telephone_an:telephone_an,
                    tax_reg_no:tax_reg_no,
                    ar_company_id:ar_company_id
                }
            }).done(function(data){

            })
            
            $('#customer_name').html(customer_name);
            $('#customer_address').html(customer_address);  
            $('#id_comp').val(id_customer);  
            $('#id_comp_f').val(id_customer);  
            $('#id_comp_b').val(id_customer);  

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'Please Search Payer name';
            payer += '</td>';

            payer += '</tr>';
            $('#payer').html('');
            $('#payer').append(payer);


        });

*/

$("div").off("click", ".save-tax");
        $("div").on("click", ".save-tax", function(e) {
            e.preventDefault();
            
            var in_tax = $('#in_tax').val();
            var in_country = $('#in_country').val();
            if(in_tax == ''){
                $('#msg-error-tax').html('Please Input Tax ID.');
                console.log('null');
            } else {
                console.log('insert tax');
                $('#insert_tax').modal('hide');
                    var id_customer = $('#in_id_comp').val();
                    var customer_address = $('#in_customer_address').val();
                    var customer_name = $('#in_customer_name').val();
                    var customer_code = $('#in_payer_code').val();
                    var telephone_an  = $('#in_payer_tel').val();
                    var tax_reg_no  = in_tax;
                    var country = in_country;

                    $.ajax({
                            url:'<?php echo site_url(); ?>Setting/SavePayerTax',
                            method:'POST',
                            data:{ 
                                id_customer:id_customer ,
                                customer_address:customer_address,
                                customer_name:customer_name,
                                customer_code:customer_code,
                                telephone_an:telephone_an,
                                tax_reg_no:tax_reg_no,
                                country:country
                            }
                    }).done(function(data){

                    })

                        $('#customer_name').html(customer_name);
                        $('#customer_address').html(customer_address);  
                        $('#id_comp').val(id_customer); 
                        $('#payer_address').val(customer_address);
                        $('#payer_name').val(customer_name); 
                        $('#payer_code').val(customer_code);    
                        $('#payer_tel').val(telephone_an);  
                        $('#payer_tax').val(tax_reg_no);   
            }

          
});


$("table").off("click", ".cus-data");
        $("table").on("click", ".cus-data", function(e) {
            e.preventDefault();
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);
            $('#customer_name').html('');
            $('#customer_address').html('');  
            $('#id_comp').val(''); 
            $('#id_comp_f').val(''); 
            $('#id_comp_b').val('');

            var $row = $(this).parents('tr.r-payer');
            var id_customer = $row.data('id_customer');
            var customer_address = $row.data('customer_address');
            var customer_address2 = $row.data('customer_address2');
            var customer_address3 = $row.data('customer_address3');
            var customer_name = $row.data('customer_name');
            var customer_code = $row.data('customer_code');
            var telephone_an  = $row.data('telephone_an');
            var tax_reg_no  = $row.data('tax_reg_no');
            var ar_company_id  = $row.data('ar_company_id');

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            $.ajax({
                url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
                method : 'POST',
                data : {comp_id:id_customer}
            })

            if(tax_reg_no != null){
                 $.ajax({
                    url:'<?php echo site_url(); ?>Setting/SavePayer',
                    method:'POST',
                    data:{ 
                        id_customer:id_customer ,
                        customer_address:customer_address,
                        customer_address2:customer_address2,
                        customer_address3:customer_address3,
                        customer_name:customer_name,
                        customer_code:customer_code,
                        telephone_an:telephone_an,
                        tax_reg_no:tax_reg_no,
                        ar_company_id:ar_company_id
                    }
                }).done(function(data){

                })

                $('#comp_his').val(id_customer);
                $('#customer_name').html(customer_name);
                $('#customer_address').html(customer_address+customer_address2+customer_address3);  
                $('#id_comp').val(id_customer); 
                $('#id_comp_f').val(id_customer);  
                $('#id_comp_b').val(id_customer);  
                $('#payer_address').val(customer_address);
                $('#payer_name').val(customer_name); 
                $('#payer_code').val(customer_code);    
                $('#payer_tel').val(telephone_an);  
                $('#payer_tax').val(tax_reg_no);   
            } else {

                $('#insert_tax').modal('show');
                $('#in_customer_name').val(customer_name); 
                $('#in_customer_address').val(customer_address+customer_address2+customer_address3);
                $('#in_id_comp').val(id_customer);   
                $('#in_payer_code').val(customer_code);  
                $('#in_payer_tel').val(telephone_an); 
            }

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';
            $('#payer').html('');
            $('#payer').append(payer);

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
             
});

$('.search-cus ').click(function(){

    $('#payer').html('');
    $('.printDr').prop('disabled',true);
    $('.can-dr').prop('disabled',true);
    
    console.log('sync Zodiac');
    var payer = $('#payer_nm').val();
    var loading = '<tr align="center"><td colspan="6"><img src="<?php echo base_url(); ?>public/img/loading.gif" width="10%"></td></tr>';

    $('#payer').append(loading);

    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCustomerdb',
        method : 'POST',
        data : {payer:payer}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;


        if(o.length == '0'){
             console.log('Not Found Record');
            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            payer += '<tr class="r-payer">';

            payer += '<td style="text-align:center;" colspan="4">';
            payer += 'No Data';
            payer += '</td>';

            payer += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var COMPANY_ID = o[i]['custom_id'];
            var OPS_COMPANY_ID = o[i]['customer_code'];
            var ADDRESS = o[i]['customer_address'];
            var ADDRESS2 = o[i]['customer_address2'];
            var ADDRESS3 = o[i]['customer_address3'];
            var COMPANY_NM = o[i]['customer_name'];
            var TAX_REG_NO = o[i]['tax_reg_no'];
            var TELEPHONE_AN = o[i]['telephone_no'];
            var AR_COMPANY_ID = o[i]['ar_company'];

            payer += '<tr class="r-payer" data-ar_company_id="'+AR_COMPANY_ID+'" data-TELEPHONE_AN="'+TELEPHONE_AN+'" data-TAX_REG_NO="'+TAX_REG_NO+'" data-id_customer="'+COMPANY_ID+'"  data-customer_address3="'+ADDRESS3+'" data-customer_address2="'+ADDRESS2+'" data-customer_address="'+ADDRESS+'" data-customer_name="'+COMPANY_NM+'" data-customer_code="'+OPS_COMPANY_ID+'">';

            payer += '<td style="text-align:left; font-size:10px;">';
            payer += OPS_COMPANY_ID;
            payer += '</td>';

            payer += '<td style="font-size:10px;">';
            payer += COMPANY_NM;
            payer += '</td>';

            payer += '<td style="font-size:10px;">';
            payer += ADDRESS + ADDRESS2 + ADDRESS3;
            payer += '</td>';


            payer += '<td style="font-size:10px;">';
            payer += AR_COMPANY_ID;
            payer += '</td>';

            payer += '<td>';
            payer += '<button class="btn btn-info btn-xs cus-data">Select</button>'
            payer += '</td>';

            payer += '</tr>';

        }

        }

        
        $('#payer').html('');
        $('#payer').append(payer);

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusBranch',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })

        $.ajax({
            url : '<?php echo site_url(); ?>FetchZodiac/ZodiacCusPost',
            method : 'POST',
            data : {comp_id:COMPANY_ID}
        })
        $('.printDr').prop('disabled',false);
        $('.can-dr').prop('disabled',false);
    })
});

//$('.type').change(function(){
$("table").off("change", ".type");
        $("table").on("change", ".type", function(e) {

    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang');
    var id_type = $(this).val();

    $('#qty-' + rang).val('0');
    $('#rate-pro-' + rang).val('');
    $('#rate_u-' + rang).html('0.00');
    $('#total-' + rang).html('0.00');
    $('#vat-pro-' + rang).val('0.00');
    $('#vat_u-' + rang).html('0.00');
    $('#holdtax-pro-' + rang).val('0.00');
    $('#holdtax_u-' + rang).html('0.00'); 
    $('#tariff_des-' + rang).html('-'); 
    $('#unit_text-' + rang).html(' ');
    var numOrder = parseInt($('#row_c').val())-1;

            $.ajax({
                url:'<?php echo site_url(); ?>Speacial/getRate',
                method:'POST',
                data:{ 
                    id_type:id_type
                }
            }).done(function(data){
                var o = JSON.parse(data);
                var cur_rate = o.rate;
                var tariff_code = o.tariff_code;
                var tariff_des = o.tariff_des;
                var n_rate = parseFloat(cur_rate);
                var n_vat = parseFloat(o.vat);
                var n_holdtax = parseFloat(o.holdtax);

                console.log(n_vat.toFixed(2));
                console.log(n_holdtax.toFixed(2));

                if(cur_rate > 0){
      
                    if(tariff_code == 'BEPSL01'){
                        $('#unit_text-' + rang).html('Days');
                        $('#rate_u-' + rang).hide();
                        $('#rate-pro-' + rang).show();

                        

                        if(parseFloat($('#v_tonnage').val()) <= 750){
                            var v_tonnage = 750;
                        } else {
                            var v_tonnage = parseFloat($('#v_tonnage').val()); 
                        }

                        $('#qty-' + rang).val(v_tonnage);

                        var type = $('#type-' + rang).val();

                        var t_order = v_tonnage * 1;

                        $('#total-' + rang).html(t_order.toLocaleString('en'));
                        $('#cal_total-' + rang).val(t_order);


                        var sum_total = 0;

                        for(i=1; i <= numOrder; i++){

                            var caltotal = parseFloat($('#cal_total-'+i).val());

                            if(isNaN(caltotal)){
                                var check_total = 0;
                            } else {
                                var check_total = caltotal;
                            }

                            console.log(check_total.toFixed(2));

                            console.log(caltotal.toFixed(2));

                            sum_total += check_total;
                        }

                        $('#sum_total').html(numberWithCommas(sum_total.toFixed(2)));
                        $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        $('#tariff_des-' + rang).html(tariff_des); 

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2)); 
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        }
                        
                        console.log("Tax" + n_holdtax);
                        console.log("Vat" + cn_vat);
                        console.log("rang" + rang);
                        

                    } else {
                        $('#rate-pro-' + rang).hide();
                        $('#rate_u-' + rang).show();

                    }
                        $('#total-' + rang).html('0.00');
                        $('#unit_text-' + rang).html('THB');
                        $('#rate-pro-' + rang).val(n_rate);
                        $('#rate_u-' + rang).html(numberWithCommas(n_rate.toFixed(2)));
                        $('#tariff_des-' + rang).html(tariff_des); 
                        $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        //$('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        //$('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2));
                        //$('#total-' + rang).html(n_rate.toLocaleString('en')); 

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2)); 
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                        }


                } else {

                        if(n_holdtax == "0.00"){
                            $('#holdtax-pro-' + rang).val('0');
                        } else {
                            $('#holdtax-pro-' + rang).val(n_holdtax.toFixed(2));
                            $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_holdtax).toFixed(2))+' %)');
                        }

                        if(n_vat == "0.00"){
                            $('#vat-pro-' + rang).val('0');
                        } else {
                            $('#vat-pro-' + rang).val(n_vat.toFixed(2));
                            $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(n_vat).toFixed(2))+' %)');
                        }

                    $('#rate_u-' + rang).hide();
                    $('#rate-pro-' + rang).show();  
                    $('#rate_u-' + rang).html('0.00');
                    $('#total-' + rang).html('0.00');  
                    //$('#tariff_des-' + rang).html('-'); 
                     $('#tariff_des-' + rang).html(tariff_des); 
                }
                

            })
});

$('.pre-inv').click(function(){

    var numOrder = parseInt($('#row_last').val());
    var id_comp = $('#id_comp').val();

    var v_name = $('#v_name').val();
    var v_visit = $('#v_visit').val();
    var v_voyin = $('#v_voyin').val();
    var v_voyout = $('#v_voyout').val();
    var v_atb = $('#v_atb').val();
    var v_atd = $('#v_atd').val();
    var inv_date = $('#inv_date').val();

    var vat = $('#vat').val();
    


    var payment = $('#payment').val();
    var remark1 = $('#remark-1').val();
    var remark2 = $('#remark-2').val();
    var remark3 = $('#remark-3').val();

    var qty = [];
    var type = [];
    var rate_pro = [];

 
    var r = confirm("Do you want to preview Invoice ?");
    if (r == true) {  
        
        if(id_comp != 0){

            if(payment != 0){
               /* if(v_name != null || v_name != ''){*/
                    if(numOrder > 1){


                        for (var i = 0; i <= numOrder; i++) {

                            qty.push($('#qty-'+i).val());

                            type.push($('#type-'+i).val());

                            rate_pro.push($('#rate-pro-'+i).val());
                        }

                            const needle = '0';
                            const isInArray = type.includes(needle);
                            console.log(isInArray); // true

                            const needleq = '0';
                            const qInArray = qty.includes(needleq);
                            console.log(qInArray); // true

                            if(isInArray == true){

                                alert('Please Select Type.');

                            } else if(qInArray == true){

                                alert('Please Input Qty.');

                            } else {

                               $.ajax({
                                    url:'<?php echo site_url(); ?>Speacial/PreviewSaveMultiOrder',
                                    method:'POST',
                                    data:{ 
                                        type:type, qty:qty , id_comp:id_comp, v_name:v_name, v_visit:v_visit, v_voyin:v_voyin, v_voyout:v_voyout, v_atb:v_atb, v_atd:v_atd, payment:payment, remark1:remark1, remark2:remark2, remark3:remark3, rate_pro:rate_pro, inv_date:inv_date, vat:vat
                                    }
                                }).done(function(data){

                                    var o = JSON.parse(data);
                                    var refId = o.refId;

                                    window.open("<?php echo site_url(); ?>Speacial/PreviewInvoice/"+refId);
                                });

                            }
                           

                    } else {

                            var type = $('#type-1').val();
                            var qty = $('#qty-1').val();
                            var rate_pro = $('#rate-pro-1').val();

                            if(type != 0){

                                $.ajax({
                                    url:'<?php echo site_url(); ?>Speacial/PreviewSaveOnceOrder',
                                    method:'POST',
                                    data:{ 
                                        type:type, qty:qty , id_comp:id_comp, v_name:v_name, v_visit:v_visit, v_voyin:v_voyin, v_voyout:v_voyout, v_atb:v_atb, v_atd:v_atd, payment:payment, remark1:remark1, remark2:remark2, remark3:remark3, rate_pro:rate_pro, inv_date:inv_date, vat:vat
                                    }
                                }).done(function(data){

                                    var o = JSON.parse(data);
                                    var refId = o.refId;
                                    window.open("<?php echo site_url(); ?>Speacial/PreviewInvoice/"+refId);
                                });

                            } else {

                                alert('Please Select Type.');

                            }

                            
                    }
               /* } else {
                    alert('Please Select Vessel.');
                }*/
            } else {
                alert('Please Select Payment.');
            }

        } else {
            alert('Please Select Customer.');
        }
           
    } 

});


$('.printDr').click(function(){

    var numOrder = parseInt($('#row_last').val());
    var id_comp = $('#id_comp').val();

    var v_name = $('#v_name').val();
    var v_visit = $('#v_visit').val();
    var v_voyin = $('#v_voyin').val();
    var v_voyout = $('#v_voyout').val();
    var v_atb = $('#v_atb').val();
    var v_atd = $('#v_atd').val();
    var inv_date = $('#inv_date').val();

    var vat = $('#vat').val();
    


    var payment = $('#payment').val();
    var remark1 = $('#remark-1').val();
    var remark2 = $('#remark-2').val();
    var remark3 = $('#remark-3').val();

    var qty = [];
    var type = [];
    var rate_pro = [];

 
    var r = confirm("Confirm to generate ?");
    if (r == true) {  
        
        if(id_comp != 0){

            if(payment != 0){
                if(v_name != null || v_name != ''){
                    if(numOrder > 1){

                        for (var i = 1; i <= numOrder; i++) {

                            qty.push($('#qty-'+i).val());

                            type.push($('#type-'+i).val());

                            rate_pro.push($('#rate-pro-'+i).val());
                        }

                            const needle = '0';
                            const isInArray = type.includes(needle);
                            console.log(isInArray); // true

                            const needleq = '0';
                            const qInArray = qty.includes(needleq);
                            console.log(qInArray); // true

                            if(isInArray == true){

                                alert('Please Select Type.');

                            } else if(qInArray == true){

                                alert('Please Input Qty.');

                            } else {

                               $.ajax({
                                    url:'<?php echo site_url(); ?>Speacial/SaveMultiOrder',
                                    method:'POST',
                                    data:{ 
                                        type:type, qty:qty , id_comp:id_comp, v_name:v_name, v_visit:v_visit, v_voyin:v_voyin, v_voyout:v_voyout, v_atb:v_atb, v_atd:v_atd, payment:payment, remark1:remark1, remark2:remark2, remark3:remark3, rate_pro:rate_pro, inv_date:inv_date, vat:vat
                                    }
                                }).done(function(data){

                                    var o = JSON.parse(data);
                                    var refId = o.refId;

                                    window.location = '<?php echo site_url(); ?>Speacial/Generate/'+refId;
                                });

                            }
                           

                    } else {
                            var type = $('#type-1').val();
                            var qty = $('#qty-1').val();
                            var rate_pro = $('#rate-pro-1').val();

                            if(type != 0){

                                $.ajax({
                                    url:'<?php echo site_url(); ?>Speacial/SaveOnceOrder',
                                    method:'POST',
                                    data:{ 
                                        type:type, qty:qty , id_comp:id_comp, v_name:v_name, v_visit:v_visit, v_voyin:v_voyin, v_voyout:v_voyout, v_atb:v_atb, v_atd:v_atd, payment:payment, remark1:remark1, remark2:remark2, remark3:remark3, rate_pro:rate_pro, inv_date:inv_date, vat:vat
                                    }
                                }).done(function(data){

                                    var o = JSON.parse(data);
                                    var refId = o.refId;
                                    window.location = '<?php echo site_url(); ?>Speacial/Generate/'+refId;
                                });

                            } else {

                                alert('Please Select Type.');

                            }

                            
                    }
                } else {
                    alert('Please Select Vessel.');
                }
            } else {
                alert('Please Select Payment.');
            }

        } else {
            alert('Please Select Customer.');
        }
           
    } 

    
});



//$('.value-split').on('input', function(e){
$("table").off("input", ".value-split");
        $("table").on("input", ".value-split", function(e) {
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseFloat($('#rate-pro-' + rang).val());
    var qty = parseFloat($('#qty-' + rang).val());
    var numOrder = parseInt($('#row_last').val());

    var holdtax = parseFloat($('#holdtax-pro-' + rang).val());
    var vat = parseFloat($('#vat-pro-' + rang).val());

    var type = $('#type-' + rang).val();

    var t_order = rate * qty;

    console.log(parseFloat(t_order).toFixed(2));

    if(type == null || type == 0){
        alert('Please select type.');
        $('#qty-' + rang).val('0');
    } else {
        if(qty < 0){
            alert('Wrong Value, Please Try Again.');
            $('#qty-' + rang).val('');
            $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(vat).toFixed(2))+' %)');
            $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)');
        } else {
            if(qty != 0 || qty == null || rate == null){

                if(isNaN(t_order)){
                        var check_t_order = 0;
                    } else {
                        var check_t_order = t_order;
                    }


                if(vat != "0.00" && holdtax != "0.00"){ //Have Vat&Holdtax

                    var cal_vat = (check_t_order*vat)/100;
                    var cal_holdtax = (check_t_order*holdtax)/100;
                    
                    var net = check_t_order + cal_vat - cal_holdtax;

                    $('#vat_u-' + rang).html(numberWithCommas(parseFloat(cal_vat).toFixed(2))+' ('+numberWithCommas(parseFloat(vat).toFixed(2))+' %)');

                    $('#holdtax_u-' + rang).html(numberWithCommas(parseFloat(cal_holdtax).toFixed(2))+' ('+numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)' );

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(net);

                } else if(vat != "0.00" && holdtax == "0.00"){ //Have Vat& No Holdtax

                    var cal_vat = (check_t_order*vat)/100;
                    
                    var net = check_t_order + cal_vat;

                    $('#vat_u-' + rang).html(numberWithCommas(parseFloat(cal_vat).toFixed(2))+' ('+numberWithCommas(parseFloat(vat).toFixed(2))+' %)');

                    $('#holdtax_u-' + rang).html('-');

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(net);

                }  else if(vat == "0.00" && holdtax != "0.00"){ //No Vat& Have Holdtax

                    var cal_holdtax = (check_t_order*holdtax)/100;
                    
                    var net = check_t_order- cal_holdtax;

                    $('#vat_u-' + rang).html('-');

                    $('#holdtax_u-' + rang).html(numberWithCommas(parseFloat(cal_holdtax).toFixed(2))+' ('+numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)' );

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(net);

                } else { //No Vat&Holdtax

                    var net = check_t_order;

                    $('#vat_u-' + rang).html('-');

                    $('#holdtax_u-' + rang).html('-');

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(net);

                }

                var sum_total = 0;

                for(i=1; i <= numOrder; i++){

                    var caltotal = parseFloat($('#cal_total-'+i).val());

                    if(isNaN(caltotal)){
                        var check_total = 0;
                    } else {
                        var check_total = caltotal;
                    }

                    console.log(check_total.toFixed(2));

                    console.log(caltotal.toFixed(2));

                    sum_total += check_total;
                }

                    
                    //console.log(sum_total);



                    $('#sum_total').html(numberWithCommas(sum_total.toFixed(2)));


            } else {
                $('#total-' + rang).html('0.00');
                $('#qty-' + rang).val('');
                $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(vat).toFixed(2))+' %)');
                $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)');
            }
        }
    }


});


//$('.unit_rate').on('input', function(e){
$("table").off("input", ".unit_rate");
        $("table").on("input", ".unit_rate", function(e) {
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseFloat($('#rate-pro-' + rang).val());
    var qty = parseFloat($('#qty-' + rang).val());
    var numOrder = parseInt($('#row_last').val());

    var type = $('#type-' + rang).val();

    var holdtax = parseFloat($('#holdtax-pro-' + rang).val());
    var vat = parseFloat($('#vat-pro-' + rang).val());

    var t_order = parseFloat(rate*qty);

    if(type == null || type == 0){
        alert('Please select type.');
        $('#qty-' + rang).val('0');
    } else {
        if(qty < 0){
            alert("The QTY's Wrong Value, Please Try Again.");
            $('#qty-' + rang).val('');
        } else if(rate < 0){
            alert("The Rate's Wrong Value, Please Try Again.");
            $('#rate-pro-' + rang).val('');
        } else {
            if(qty != 0 || qty == null || rate == null){

                if(isNaN(t_order)){
                        var check_t_order = 0;
                    } else {
                        var check_t_order = t_order;
                    }

                $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));


                $('#cal_total-'+rang).val(check_t_order);

                if(vat != "0.00" && holdtax != "0.00"){ //Have Vat&Holdtax

                    var cal_vat =  parseFloat((check_t_order*vat)/100).toFixed(2);
                    var cal_holdtax =  parseFloat((check_t_order*holdtax)/100).toFixed(2);
                    
                    var net = parseFloat(check_t_order)+parseFloat(cal_vat)-parseFloat(cal_holdtax);

                    $('#vat_u-' + rang).html(numberWithCommas(parseFloat(cal_vat).toFixed(2))+' ('+numberWithCommas(parseFloat(vat).toFixed(2))+' %)');

                    $('#holdtax_u-' + rang).html(numberWithCommas(parseFloat(cal_holdtax).toFixed(2))+' ('+numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)' );

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(parseFloat(net).toFixed(2));

                } else if(vat != "0.00" && holdtax == "0.00"){ //Have Vat& No Holdtax

                    var cal_vat = parseFloat((check_t_order*vat)/100).toFixed(2);

                    var net = parseFloat(check_t_order)+parseFloat(cal_vat);

                    $('#vat_u-' + rang).html(numberWithCommas(parseFloat(cal_vat).toFixed(2))+' ('+numberWithCommas(parseFloat(vat).toFixed(2))+' %)');

                    $('#holdtax_u-' + rang).html('-');

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(parseFloat(net).toFixed(2));

                }  else if(vat == "0.00" && holdtax != "0.00"){ //No Vat& Have Holdtax

                    var cal_holdtax = parseFloat((check_t_order*holdtax)/100).toFixed(2);
                    
                    var net = parseFloat(check_t_order)-parseFloat(cal_holdtax);

                    $('#vat_u-' + rang).html('-');

                    $('#holdtax_u-' + rang).html(numberWithCommas(parseFloat(cal_holdtax).toFixed(2))+' ('+numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)' );

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(parseFloat(net).toFixed(2));

                } else { //No Vat&Holdtax

                    var net = parseFloat(check_t_order).toFixed(2);

                    $('#vat_u-' + rang).html('-');

                    $('#holdtax_u-' + rang).html('-');

                    $('#total-' + rang).html(numberWithCommas(parseFloat(check_t_order).toFixed(2)));

                    $('#net-' + rang).html(numberWithCommas(parseFloat(net).toFixed(2)));

                    $('#cal_total-'+rang).val(parseFloat(net).toFixed(2));

                }

                var sum_total = 0;

                for(i=1; i <= numOrder; i++){

                    var caltotal = parseFloat($('#cal_total-'+i).val());

                    if(isNaN(caltotal)){
                        var check_total = 0;
                    } else {
                        var check_total = caltotal;
                    }

                    console.log(parseFloat(check_total).toFixed(2));

                    console.log(caltotal);

                    sum_total += check_total;
                }

                    
                    //console.log(parseFloat(sum_total).toFixed(2));

                    $('#sum_total').html(numberWithCommas(parseFloat(sum_total).toFixed(2)));


            } else {
                $('#total-' + rang).html('0.00');
                $('#qty-' + rang).val('');
                $('#vat_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(vat).toFixed(2))+' %)');
                $('#holdtax_u-' + rang).html('0.00 '+'( ' +numberWithCommas(parseFloat(holdtax).toFixed(2))+' %)');
            }
        }
    }


});


/*$('.unit_rate').on('input', function(e){
    var $row = $(this).parents('tr.r-order');
    var rang = $row.data('rang'); 
    var rate = parseInt($('#rate-pro-' + rang).val());
    var qty = parseInt($('#qty-' + rang).val());

    var type = $('#type-' + rang).val();

    var t_order = rate * qty;

    if(qty == 0 || qty == null){
        alert("The QTY's Wrong Value, Please Try Again.");
    } else {

        if(type == null || type == 0){
            alert('Please select type.');
            $('#qty-' + rang).val('0');
        } else {
            if(qty < 0){
                alert('Wrong Value, Please Try Again.');
                $('#qty-' + rang).val('');
            } else {
                if(qty != 0 || qty == null || rate == null){
                    $('#total-' + rang).html(parseFloat(t_order).toFixed(2).toLocaleString('en'));
                } else {
                    $('#total-' + rang).html('0.00');
                    $('#qty-' + rang).val('');
                }
            }
        }

    }

});*/



$('#vsl_visit').on('input', function(e){

    $('#vsl_order').html('');
    $('.printDr').prop('disabled',true);
    $('.can-dr').prop('disabled',true);
    
    console.log('sync Zodiac');
    var vsl_order = $('#vsl_visit').val();
    var loading = '<tr align="center"><td colspan="6"><img src="<?php echo base_url(); ?>public/img/loading.gif" width="10%"></td></tr>';

    $('#vsl_order').append(loading);

    $.ajax({
        url : '<?php echo site_url(); ?>FetchZodiac/ZodiacVsl',
        method : 'POST',
        data : {vsl_order:vsl_order}
    }).done(function(data){
        var o = JSON.parse(data);
        var i = 0;

        if(o.length == '0'){
             console.log('Not Found Record');
            vsl_order += '<tr class="r-vsl"  >';

            vsl_order += '<td style="text-align:center;" colspan="3">';
            vsl_order += 'No Data';
            vsl_order += '</td>';

            vsl_order += '</tr>';

        } else if(data== '0'){
            console.log('Not Found Record');

            vsl_order += '<tr class="r-vsl"  >';

            vsl_order += '<td style="text-align:center;" colspan="3">';
            vsl_order += 'No Data';
            vsl_order += '</td>';

            vsl_order += '</tr>';

        } else {

            console.log('Found Record : ' +  o.length);

            for(i=0; i < o.length; i++){

            var VSL_VISIT = o[i]['VSL_VISIT'];
            var VSL_NAME = o[i]['VSL_NAME'];
            var ATB = o[i]['ATB'];
            var ATD = o[i]['ATD'];
            var VOY_IN = o[i]['VOY_IN'];
            var VOY_OUT = o[i]['VOY_OUT'];
            var TONNAGE = o[i]['VISIT_VSL_TONNAGE_NR'];

            vsl_order += '<tr class="r-vsl" data-vsl_visit="'+VSL_VISIT+'" data-vsl_name="'+VSL_NAME+'" data-atb="'+ATB+'" data-atd="'+ATD+'" data-voy_in="'+VOY_IN+'" data-voy_out="'+VOY_OUT+'" data-tonnage="'+TONNAGE+'">';

            vsl_order += '<td style="font-size:12px; text-align:center;">';
            vsl_order += VSL_VISIT;
            vsl_order += '</td>';

            vsl_order += '<td style="font-size:12px;">';
            vsl_order += VSL_NAME;
            vsl_order += '</td>';


            vsl_order += '<td>';
            vsl_order += '<button class="btn btn-info btn-xs vsl-data">Select</button>'
            vsl_order += '</td>';

            vsl_order += '</tr>';

        }

        }

        
        $('#vsl_order').html('');
        $('#vsl_order').append(vsl_order);

        
        $('.printDr').prop('disabled',false);
        $('.can-dr').prop('disabled',false);
    })
});

$("table").off("click", ".vsl-data");
        $("table").on("click", ".vsl-data", function(e) {
            e.preventDefault();
            $('.printDr').prop('disabled',true);
            $('.can-dr').prop('disabled',true);

            $('#vessel').html('');
            $('#vsL_voyin').html('');  
            $('#vsl_voyout').html('');
            $('#vsl_atb').html('');
            $('#vsl_atd').html('');

            var $row = $(this).parents('tr.r-vsl');
            var vsl_visit = $row.data('vsl_visit');
            var vsl_name = $row.data('vsl_name');
            var vsl_voyout = $row.data('voy_out');
            var vsL_voyin = $row.data('voy_in');
            var atd = $row.data('atd');
            var atb = $row.data('atb');
            var tonnage = parseInt($row.data('tonnage'));



            if(tonnage <= 750){
                var c_tonnage = 750;
                
            } else {
                var c_tonnage = tonnage;

            }

            $('#visit_his').val(vsl_visit);
            $('#v_name').val(vsl_name);
            $('#v_visit').val(vsl_visit);
            $('#v_voyin').val(vsL_voyin);
            $('#v_voyout').val(vsl_voyout);
            $('#v_atb').val(atb);
            $('#v_atd').val(atd);
            $('#v_tonnage').val(c_tonnage);


                /* $.ajax({
                    url:'<?php echo site_url(); ?>Setting/SavePayer',
                    method:'POST',
                    data:{ 
                        id_customer:id_customer ,
                        customer_address:customer_address,
                        customer_address2:customer_address2,
                        customer_address3:customer_address3,
                        customer_name:customer_name,
                        customer_code:customer_code,
                        telephone_an:telephone_an,
                        tax_reg_no:tax_reg_no
                    }
                }).done(function(data){

                })*/

                $('#vessel').html('('+vsl_visit+') ' + ' ' + vsl_name);
                $('#vsL_voyin').html(vsL_voyin);
                $('#vsl_voyout').html(vsl_voyout);
                $('#vsl_atb').html(atb);
                $('#vsl_atd').html(atd);

            $('.printDr').prop('disabled',false);
            $('.can-dr').prop('disabled',false);
             
});


});
</script>
<script type="text/javascript">
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>