<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Speacial Service Request</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview Multiple Receipt</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Code</th>
                                                <th >Customer</th>
                                                <th >Branch</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody  style="font-size: 13px;">
                                           
                                                    <tr>
                                                        <td><?php echo  $cus->customer_code; ?></td>
                                                        <td><?php echo  $cus->customer_name; ?></td>
                                                        <td><?php echo  $cus->customer_branch; ?></td>
                                                        <td><?php echo  $cus->customer_address; ?><?php echo  $cus->customer_address2; ?><?php echo  $cus->customer_address3; ?> <?php echo  $cus->customer_post; ?></td>
                                                        <td><?php echo  $cus->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  

                                    <?php if($v_data->vsl_visit){  ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th >Vessel</th>
                                                    <th >Voy In</th>
                                                    <th >Voy Out</th>
                                                    <th >ATB</th>
                                                    <th >ATD</th>
                                                </tr>
                                            </thead>
                                            <tbody  style="font-size: 13px;">
                                               
                                                        <tr>
                                                            <td>( <?php echo $v_data->vsl_visit;?> ) <?php echo $v_data->vessel;?></td>
                                                            <td><?php echo  $v_data->voy_in; ?></td>
                                                            <td><?php echo  $v_data->voy_out; ?></td>
                                                            <td><?php echo  $v_data->atb; ?></td>
                                                            <td><?php echo  $v_data->atd; ?></td>
                                                        </tr> 
                                             
                                            </tbody>
                                        </table> 
                                    <?php } ?>
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Inv No.</th>
                                                <th style="text-align: left;">Tariff Code</th>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: right;">QTY</th>
                                                <th style="text-align: right;">Rate</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                                <th style="text-align: right;">Vat (THB)</th>
                                                <th style="text-align: right;">W / T (THB)</th>
                                                <th style="text-align: right;">Total (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr"  style="font-size: 13px;">
                                            <?php 
                                                    $all_amount= 0;
                                                    $payment= 0;
                                                    $y_wh = 0;
                                                    $y_vat = 0;
                                                foreach ($order as $rs_order) { 
                                                    $is_vat =  $v_data->vat;
                                                    $line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
                                                    $all_amount += $line_amount;
                                                    
                                                    $y_vat += $rs_order['cal_vat'];
                                                    $y_wh += $rs_order['cal_wh'];

                                            ?>
                                                <tr>
                                                    <td style="text-align: left;"><?php 
                                                            
                                                            echo $rs_order['dr_no']; 
                                                            

                                                    ?></td>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_code']; ?></td>
                                                    <td style="text-align: left;"><?php echo $rs_order['tariff_des']; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['qty'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "GRT"; } else { echo "Unit"; } ?></b>
                                                    </td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'],2); ?>
                                                        &nbsp;&nbsp;<b><?php if($rs_order['tariff_code'] == 'BHS001') { echo "Days"; } else { echo "THB"; } ?></b>
                                                    </td>
                                                    <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'] * $rs_order['qty'],2); ?></td>
                                                    <td align="right">
                                                        <?php if($rs_order['code_vat'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_vat'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['vat'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php if($rs_order['code_wh'] != 'N'){ ?>
                                                            <?php echo number_format($rs_order['cal_wh'], 2); ?>&nbsp;&nbsp;(<?php echo number_format($rs_order['holdtax'], 2); ?>%)</b>
                                                        <?php } else { ?>
                                                                -
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php

                                                            $line_amount = (($rs_order['cur_rate'] * $rs_order['qty']));
                                                            $payment += $line_amount;
                                                            echo number_format(($line_amount), 2);
                                                        ?>
                                                        &nbsp;&nbsp;<b><?php echo $rs_order['rate_type']; ?></b>
                                                    </td>
                                                <tr>
                                            <?php } ?>  
                                                <tr>
                                                    <td style="text-align: right;" colspan="8"><b>Sub Total</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($all_amount,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="8"><b>VAT 7%</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="8"><b>Total</b></td>
                                                    <td style="text-align: right;"><?php echo number_format($all_amount+$y_vat,2); ?>&nbsp;&nbsp;<b>THB</b></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="8">
                                                        <input type="checkbox" id="holdtax" value="1" <?php if($all_amount >= 1000) { echo "checked"; } ?>>
                                                        <b>Less Witholding Tax</b>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <font id="hold_v"><?php echo number_format($y_wh,2); ?></font>&nbsp;&nbsp;<b>THB</b>
                                                        <input type="hidden" name="hold_total" id="hold_total" value="<?php echo number_format($y_wh, 2, '.', ''); ?>">
                                                        <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo number_format($all_amount + $y_vat - $y_wh, 2, '.', ''); ?>">
                                                        <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  number_format($all_amount + $y_vat, 2, '.', ''); ?>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;" colspan="8"><b>Net Pay</b></td>
                                                    <td style="text-align: right;">
                                                        <font id="net_p"><?php echo number_format(($all_amount+$y_vat)-$y_wh,2); ?></font>&nbsp;&nbsp;<b>THB</b>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>  
                                    <div align="right">
                                         <label>Receipt Type</label>
                                            <select class="form-control" aria-label="Default select example" style="width: 200px;" id="receipt_type">
                                                <option value="0">-Select-</option>
                                                <option value="R">Receipt</option>
                                                <option value="RT">Receipt / Tax Invoice</option>
                                            </select>
                                        <br>
                                        <label>Receipt Date</label>
                                            <input type="hidden" id="none_hold" value="1">
                                            <input class="form-control" type="date" name="inv_date" id="inv_date" value="" style="width: 200px;">
                                            <br>
                                        <button class="btn btn-info btn gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Genarate Receipt</button>
                                        <a href="<?php echo site_url(); ?>Speacial/SSR"><button class="btn btn-danger btn ">Cancel</button></a>
                                    </div>
                                </div>


                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<form id="form-multi" action="<?php echo site_url(); ?>Speacial/SetMultiReceipt" enctype="multipart/form-data" method="post" enctype="multipart/form-data">
    <input type="hidden" id="multiinv" name="multiinv" value="<?php echo $multiinv; ?>">
    <input type="hidden" id="chk_wht" name="chk_wht" value="">
    <input type="hidden" id="re_type" name="re_type" value="">
    <input type="hidden" id="date_multiinv" name="date_multiinv" value="">
</form>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    $("#holdtax").bind("change",function(){
       var value = $(this).is(":checked");
       if(value){
            var hold_total = parseFloat($('#hold_total').val()).toFixed( 2 );
            var net_total_h = parseFloat($('#net_total_h').val()).toFixed( 2 );
            $('#hold_v').html(hold_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#net_p').html(net_total_h.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

            $('#none_hold').val('1');
       } else {

            var net_total_n = parseFloat($('#net_total_n').val()).toFixed( 2 );
            //$('#hold_v').html('0.00');
            $('#net_p').html(net_total_n.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#none_hold').val('0');
       }
    });


    $('.gen-invoice').click(function(){

        $('#date_multiinv').val($('#inv_date').val());
        $('#chk_wht').val($('#none_hold').val());
        $('#re_type').val($('#receipt_type').val());

        if($('#receipt_type').val() == '0' || $('#receipt_type').val() == ''){

            alert('Please specify type of receipt.');

        } else {

            if($('#inv_date').val() != '' || $('#inv_date').val() != null){
                $('#form-multi').submit();
            } else {
                alert('Please specify date of receipt.');
            }

        }
       
       
    });


});
</script>