<br><br><br>
<h3 align="center"><b>DRAFT INVOICE</b></h3>


				<table>

					<tr>

						<td width="10%"><p style="font-size: 10px;"><b>Bill To</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->company_bill;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Draft No</b></p></td>
						<td width="25%"><p style="font-size: 10px;"><img src="<?php echo site_url(); ?>public/img/barcode/<?php echo $split_order->dr_no;?>.png" width="55%"></td>
					</tr>
					<tr>
						<td></td>
						<td><p style="font-size: 10px;"><?php echo $head->address_bill; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y"); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Type</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->type; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;99999</p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Tax ID</b></p></td>
						<td>:&nbsp;&nbsp;</td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->code_comp."-".$head->id_comp; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Branch</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;HEAD OFFICE</p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Payment Terms</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->payment; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Customer</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->company_bill;?></p></td>
						<td></td>
						<!--<td width="16%"><p style="font-size: 10px;"><b>Voyage In/Out</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php if(!empty($split_order->voy_in) AND $split_order->voy_out) { echo $split_order->voy_in."&nbsp;/&nbsp;".$split_order->voy_out; } else { echo "-"; } ?></p></td>-->
						<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->terminal_doc_ref_an; ?></p></td>
					</tr>
					<!--<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Vessel</b></p></td>
						<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php if(!empty($split_order->vessel_code)) { echo $split_order->vessel_code; } else { echo "-";} ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>ATB</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($split_order->atb)); ?></p></td>
					</tr>-->
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Remark </b></p></td>
						<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->remark; ?></p></td>
						<td></td>
						<td width="16%"><!--<p style="font-size: 10px;"><b>ATD</b></p>--></td>
						<td width="25%"><!--<p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($split_order->atd));?></p>--></td>
					</tr>
					<!--<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->terminal_doc_ref_an; ?></p></td>
					</tr>-->

				</table>
				<br><br><br>
				<table class="table">
				    <thead>
				        <tr>
				            <th style="font-size: 10px; text-align: center;"  width="25%">Size</th>
				            <th style="font-size: 10px; text-align: center;"  width="25%">QTY</th>
				            <th style="font-size: 10px; text-align: right;"  width="20%">Unit Rate (THB)</th>
				            <th style="font-size: 10px; text-align: right;"  width="25%">Line Amounth (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    	$all_amount = 0; ?>
				          <tr>
				          	<td style="font-size: 10px;" align="center"><?php echo $split_order->size_con."''"; ?></td>
				          	<td style="font-size: 10px;" align="center"><?php echo $split_order->book_con; ?></td>
				          	<td style="font-size: 10px;" align="right"><?php echo number_format($split_order->cur_rate, 2); ?></td>
				          	<td style="font-size: 10px;" align="right">
				          		<?php

				          			$line_amount = $split_order->book_con * $split_order->cur_rate;
				          			$all_amount += $line_amount;
				          			echo number_format($line_amount, 2);
				          		?>
				          	</td>
				          </tr>   

				         <tr>
				          	<td colspan="2" rowspan="5"></td>
				          	<td style="font-size: 10px;" align="right">Sub Total</td>
				          	<td style="font-size: 10px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
				          </tr> 
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right">VAT (VAT)</td>
				          	<td style="font-size: 10px;"  align="right">
				          	<?php  
				          		
				          			$vat = $all_amount*$vat->vat/100; 
				          		   	echo number_format($vat, 2); 

				          	?></td>
				          </tr> 
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Total (THB)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php  
				          				$g_total = $all_amount + $vat;
				          				echo number_format($g_total, 2); 
				          	  ?></td>
				          </tr>
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Withholding Tax (3%)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php 
				          			$vat_hold = $all_amount*3/100; 
				          		   	echo number_format($vat_hold, 2); 
				          	 ?></td>
				          </tr> 
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Net Pay (THB)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php 

				          			$net_total = $g_total - $vat_hold;
				          			echo number_format($net_total, 2); 

				          	?></td>
				          </tr>  
				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 10px;" width="50%">
				            	REMARK : THE SERVICE QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THILAND , USER HAS AGREED AND ACCEPTED.
				            </td>
				            <td style="font-size: 10px;" width="50%">
				            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
				            </td>
				        </tr>
				</table>
				<br><br><br><br><br><br><br><br><br>
				<P style="color:gray; font-size: 10px;">Printed by <?php echo $user_print->name; ?> on <?php echo date("j-F-Y g:i"); ?></P>
				<br>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    window.print();
    document.location.href = "<?php echo site_url(); ?>Createdraft"; 
});
</script>