<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Document Register</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          DR Option
                        </div>
                        <div class="col-md-2" style="float: right;">
                            <label>Search Customer</label>
                            <input id="skills" class="form-control">
                        </div>   
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>DRAFT INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Customer</th>
                                                <th >Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td> <div id="customer_name"></td>
                                                        <td> <div id="customer_address"></td>
                                                        
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table" 
                                            <tr>
                                                <th style="text-align: left;">Doc Ref Book </th>
                                                <th style="text-align: center;" width="10%">Type</th>
                                                <th style="text-align: center;">Size</th>
                                                <th style="text-align: center;">QTY</th>
                                                <th style="text-align: left;">Terminal Doc AN</th>
                                                <th style="text-align: left;">Booking AN</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <?php 
                                            $i=0;
                                            foreach ($order as $rs_order) {

                                                $trm = $rs_order['terminal_doc_ref_id'];

                                                $id_comp = $rs_order['id_comp'];

                                                ?>
                                                    <tr class="r-dr<?php echo $i; ?>" data-id="<?php echo $rs_order['id']; ?>" data-doc="<?php echo $rs_order['doc_ref_book']; ?>">
                                                        <td style="text-align: left;"><?php echo $rs_order['doc_ref_book']; ?></td>
                                                        <td>
                                                            <select class="form-control" id="type_dr">
                                                                <option value="0" >Select Type</option>
                                                            <?php foreach ($type as $rs_type) { ?>
                                                                <option value="<?php echo $rs_type['id']; ?>"><?php echo $rs_type['type']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        </td>
                                                        <td style="text-align: center;"><?php echo $rs_order['size_con']."''"; ?></td>
                                                        <td style="text-align: center;"><?php echo $rs_order['book_con']; ?></td>
                                                        <td style="text-align: left;"><?php echo $rs_order['terminal_doc_ref_an']; ?></td>
                                                        <td style="text-align: left;"><?php echo $rs_order['book_an']; ?></td>
                                                        <!--<td style="text-align: center;"> <button class="btn btn-info splitDR"><span class="glyphicon glyphicon-print"></span> Generate Split</button></td>-->
                                                    </tr> 
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>   
                                    <div align="right">
                                        <input type="hidden" name="trm" id="trm" value="<?php echo $trm; ?>">
                                        <input type="hidden" name="id_comp" id="id_comp" value="">
                                        <?php if($status_dr){ ?>
                                            <?php if($status_dr->is_split == '1') {?>
                                            <a href="<?php echo site_url(); ?>Createdraft/printBySplit/<?php echo $trm; ?>/<?php echo $id_comp; ?>" target="_blank"><button class="btn btn-success"><span class="glyphicon glyphicon-print"></span> &nbsp;Preview DR</button></a>
                                            <?php } else if ($status_dr->is_split == '0'){ ?>
                                            <a href="<?php echo site_url(); ?>Createdraft/PrintDR/<?php echo $trm; ?>/<?php echo $id_comp; ?>" target="_blank"><button class="btn btn-success"><span class="glyphicon glyphicon-print"></span> &nbsp;Preview DR</button></a>
                                        <?php } ?>
                                        <?php } else { ?>

                                                <div class="form-group" id="s-input" >
                                                <label>QTY SPLIT / PAGE</label>
                                                <input type="text" name="value_split" class="form-control" id="value_split" value="" style="width: 150px;">
                                                </div>
                                                <label class="radio-inline"><input type="radio" name="types" class="types" value="split">Split</label>                                        
                                                <label class="radio-inline"><input type="radio" name="types" class="types" value="none">None Split</label> &nbsp; &nbsp;
                                                <button class="btn btn-success printDr"><span class="glyphicon glyphicon-print"></span>Generate DR</button>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('#s-input').hide();
$("input[name=types]").change(function(){
    var types = $('input[name=types]:checked').val();
    if(types == 'split'){
        $('#s-input').show();
    } else {
        $('#s-input').hide();
    }
});

$('#skills').on('input',function(e){
    $('#customer_name').html('');  
    $('#customer_address').html('');   
    var skill = $('#skills').val();

    $.ajax({
        url:'<?php echo site_url(); ?>Setting/AutoCustomer',
        method:'POST',
        data:{ auto_customer:skill }
    }).done(function(data){
        var o = JSON.parse(data);             
        $('#customer_name').append(o.customer_name);
        $('#customer_address').append(o.customer_address);  
        $('#id_comp').val(o.customer_id);                
    })
});



 $('.printDr').click(function(){

        var trm = $('#trm').val();
        var id_comp = $('#id_comp').val();

        var types = $('input[name=types]:checked').val();

        var type_dr = $('#type_dr').val();


        if(types == null){
            alert('Please Select Split or None Split Data.');
        } else {
            if(type_dr != 0){
                if(id_comp != 0){
                     var r = confirm("Confirm to generate DRAFT ?");
                    if (r == true) {  
                         if(types == 'split'){
                          
                               var num_split = $('#value_split').val();  
                                //window.open('<?php echo site_url(); ?>Createdraft/printBySplit/'+trm+'/'+id_comp+'/'+num_split+'', '_blank');
                                window.location = '<?php echo site_url(); ?>Createdraft/printBySplit/'+trm+'/'+id_comp+'/'+num_split+'/'+type_dr+'';
                           
                        } else {
                         
                                //window.open('<?php echo site_url(); ?>Createdraft/PrintDR/'+trm+'/'+id_comp+'', '_blank');
                                window.location = '<?php echo site_url(); ?>Createdraft/PrintDR/'+trm+'/'+id_comp+'/'+type_dr+'';

                        }
                        
                    } else {
                        alert('OK.');
                    }
                } else {
                    alert('Please Select Customer.');
                }
            } else {
                alert('Please Select Type Draft');
            }
        }

           /* var r = confirm("Split Data ?");
            if (r == true) {
               
                var rowCount = $('#dr tr').length;

                for (var i = 1; i <= rowCount; i++) {
                    var $row = $('.r-dr'+i+'');
                    var id = $row.data('id');
                    var doc = $row.data('doc');

                    var id_a = new Array();
                    var doc_a = new Array();
                    var id_a = id;
                    var doc_a = doc; 

                }
                var trm = $('#trm').val();
                var id_comp = $('#id_comp').val();
                //window.open('<?php echo site_url(); ?>Createdraft/PrintDR/'+trm+'/'+id_comp+'', 'popUpWindow','left=300, top=100, resizable=no, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');
                window.open('<?php echo site_url(); ?>Createdraft/printBySplit/'+trm+'/'+id_comp+'', '_blank');
           
            } else {
                
                //window.open('<?php echo site_url(); ?>Createdraft/PrintDR/'+trm+'/'+id_comp+'', 'popUpWindow','left=300, top=100, resizable=no, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');
                window.open('<?php echo site_url(); ?>Createdraft/PrintDR/'+trm+'/'+id_comp+'', '_blank');
            }*/
           
        });

        $('.gen').click(function(){

        var doc = $('#trm').val();
        var id = $('#id_comp').val();

            //window.open('<?php echo site_url(); ?>Createdraft/SplitDR/'+doc+'/'+id+'', 'popUpWindow','left=300, top=100, resizable=no, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');
            //window.open('<?php echo site_url(); ?>Createdraft/PrintDR/'+doc+'/'+id+'', '_blank');
            window.location = '<?php echo site_url(); ?>Createdraft/PrintDR/'+doc+'/'+id+'';
        });


});
</script>