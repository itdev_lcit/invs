<br><br><br>
<h3 align="center"><b>DRAFT INVOICE</b></h3>

<?php foreach ($barcode as $rs_barcode) {
	echo $rs_barcode;
} ?>

<?php foreach ($order as $rs_order) { ?>

	<?php foreach ($comp as $rs_comp) { ?>

		<?php foreach ($size as $rs_size) { ?>


		<?php foreach ($vat as $rs_vat) { ?>

				<table>
					<tr>

						<td width="10%"><p style="font-size: 10px;"><b>Bill To :</b></p></td>
						<td width="50%"><p style="font-size: 10px;"><?php echo substr($rs_comp['company_bill'],0,30)."...";?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Draft No</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['dr_no']; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p style="font-size: 10px;"><?php echo $rs_comp['address_bill']; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['created']; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Type</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['type']; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['crop']; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Tax ID :</b></p></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['oper']; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Branch :</b></p></td>
						<td width="45%"><p style="font-size: 10px;">HEAD OFFICE</p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Payment Terms</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['payment']; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Customer :</b></p></td>
						<td width="45%"><p style="font-size: 10px;"><?php echo substr($rs_comp['company_bill'],0,30)."...";?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Voyage In/Out</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['voy_in']."&nbsp;/&nbsp;".$rs_order['voy_out']; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Vessel :</b></p></td>
						<td><p style="font-size: 10px;"><?php echo $rs_order['vessel_code']; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>ATB</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['atb']; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Remark :</b></p></td>
						<td><p style="font-size: 10px;"><?php echo $rs_order['remark']; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>ATD</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['atd']; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $rs_order['terminal_doc_ref_an']; ?></p></td>
					</tr>
				</table>
				<br><br><br>
				<table class="table">
				    <thead>
				        <tr>
				            <th style="font-size: 10px;">Type</th>
				            <th style="font-size: 10px; text-align: center;">Size</th>
				            <th style="font-size: 10px; text-align: center;">QTY</th>
				            <th style="font-size: 10px; text-align: right;">Unit Rate (THB)</th>
				            <th style="font-size: 10px; text-align: right;">Line Amount (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				          <tr>
				          	<td style="font-size: 10px;"><?php echo $rs_order['type']; ?></td>
				          	<td style="font-size: 10px;" align="center"><?php echo $rs_order['size_con']."''"; ?></td>
				          	<td style="font-size: 10px;" align="center"><?php echo $rs_order['book_con']; ?></td>
				          	<td style="font-size: 10px;" align="right"><?php echo  number_format($rs_size['rate'], 2); ?></td>
				          	<td style="font-size: 10px;" align="right"><?php
				          						$total_1 = $rs_order['book_con']*$rs_size['rate']; 
				          						echo  number_format($total_1, 2);
				          	?></td>
				          </tr>   
				          <tr>
				          	<td colspan="3" rowspan="5"></td>
				          	<td style="font-size: 10px;" align="right">Sub Total</td>
				          	<td style="font-size: 10px;"  align="right"><?php echo  number_format($total_1, 2); ?></td>
				          </tr>  
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right">VAT (VAT)</td>
				          	<td style="font-size: 10px;"  align="right">
				          	<?php  $vat = $total_1*$rs_vat['vat']/100; 
				          		   echo number_format($vat, 2); 
				          	?></td>
				          </tr>      
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Total (THB)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php  
				          				$g_total = $total_1 + $vat;
				          				echo number_format($g_total, 2); 
				          	  ?></td>
				          </tr>  
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Withholding Tax (3%)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php 
				          			$vat_hold = $total_1*3/100; 
				          		   	echo number_format($vat_hold, 2); 
				          	 ?></td>
				          </tr> 
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right"><b>Net Pay (THB)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php 

				          			$net_total = $g_total - $vat_hold;
				          			echo number_format($net_total, 2); 

				          	?></td>
				          </tr>                     
				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 13px;" width="50%">
				            	REMARK : THE SERVICE QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THILAND , USER HAS AGREED AND ACCEPTED.
				            </td>
				            <td style="font-size: 13px;" width="50%">
				            	โปรดตรวจสอบความถูกต้องในทันที่ที่ได้รับเอกสารนี้ หากไม่ทักท้วงจะถือว่ารายการดังกล่าวข้างต้นสมบูรณ์
				            </td>
				        </tr>
				</table>
	
<?php

}

}
	
} 

}

?>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    window.print();
    document.location.href = "<?php echo site_url(); ?>Createdraft"; 
});
</script>