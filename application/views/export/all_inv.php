<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Export Invoice  </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="left">
                            <form class="form-inline" action="<?php echo site_url(); ?>Export/Invoice" method="POST">
                                &nbsp;&nbsp;
                                <label for="type">Type</label>
                                  <select class="form-control" id="type" name="type">
                                    <option value="1">All</option>
                                    <option value="2">User</option>
                                  </select>
                                &nbsp;&nbsp;
                                <label>From Date :</label>
                                <input class="form-control" type="date" name="first_date" value="<?php echo $first_date; ?>">
                                &nbsp;&nbsp;
                                <label>To Date :</label>
                                <input class="form-control" type="date" name="second_date" value="<?php echo $second_date; ?>">
                                <button class="btn btn-info"><i class="fa fa-download"></i>Export</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                          <br>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>