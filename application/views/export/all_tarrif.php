<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Export Tariff Orders </h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="left">
                            <form class="form-inline" action="<?php echo site_url(); ?>Export/TarrifOrder" target="_blank" method="POST">
                                <label>Line :</label>
                                <input class="form-control" type="text" name="liner" required="true">
                                &nbsp;&nbsp;
                                <label>Tariff :</label>
                                <select class="form-control" id="tariff_type" name="tariff_type">
                                    <option value="ALL">ALL</option>
                                    <?php foreach ($tariff_type as $rs) { ?>
                                        <option value="<?php echo $rs['tariff_id']; ?>"><?php echo $rs['tariff_code']; ?> || <?php echo $rs['tariff_des']; ?></option>
                                    <?php } ?>
                                  </select>
                                &nbsp;&nbsp;
                                <label for="type">Type</label>
                                  <select class="form-control" id="type_inv" name="type_inv">
                                    <option value="ALL">ALL</option>
                                    <option value="INVOICE">INVOICE</option>
                                    <option value="RECEIPT">RECEIPT</option>
                                  </select>
                                <label for="type">Payment</label>
                                  <select class="form-control" id="type" name="type">
                                    <option value="ALL">ALL</option>
                                    <option value="CREDIT">CREDIT</option>
                                    <option value="CASH">CASH</option>
                                  </select>
                                &nbsp;&nbsp;
                                <label>From Date :</label>
                                <input class="form-control" type="date" name="first_date" value="<?php echo $first_date; ?>">
                                &nbsp;&nbsp;
                                <label>To Date :</label>
                                <input class="form-control" type="date" name="second_date" value="<?php echo $second_date; ?>">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="btn btn-info"><i class="fa fa-download"></i>Export</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                          <br>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>