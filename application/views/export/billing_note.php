<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing Note</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="left">
                            <form class="form-inline" action="<?php echo site_url(); ?>Export/ProcessNote" method="POST">
                                
                                <label>Line :</label>
                                <input class="form-control" type="text" name="liner" required="true">
                                &nbsp;&nbsp;
                                <label>Bank</label>
                                    <select class="form-control" id="bank_type" name="bank_type">
                                        <?php foreach ($setting_bank as $rs) { ?>
                                            <option value="<?php echo $rs['sb_id']; ?>"><?php echo $rs['sb_type']; ?></option>
                                        <?php } ?>
                                    </select>
                                &nbsp;&nbsp;
                                <label>Cheque No :</label>
                                <input class="form-control" type="text" name="cheque_no" >
                                &nbsp;&nbsp;
                                <label>Receipt Date :</label>
                                <input class="form-control" type="date" name="receipt_date" value="<?php echo $second_date; ?>">
                                &nbsp;&nbsp;
                                <label>From Date :</label>
                                <input class="form-control" type="date" name="first_date" value="<?php echo $first_date; ?>">
                                &nbsp;&nbsp;
                                <label>To Date :</label>
                                <input class="form-control" type="date" name="second_date" value="<?php echo $second_date; ?>">
                                <button class="btn btn-info"><i class="fa fa-eye"></i>&nbsp;&nbsp;Show</button>
                                
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                          <br>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var record = $('#record').val();

        if(record == '0'){
            $('.print-inv').prop('disabled',true);
        }


    });
</script>