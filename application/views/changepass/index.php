
<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Change Password</h3>
                    </div>
                    <div class="panel-body">
                        <p id="validate-password" style="color: red;"></p>
                         <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" required="true" placeholder="Password">
                                    <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
                                </div>
                                <div class="form-group">
                                    <input type="password" id="confirm_password" class="form-control" name="confirm_password" required="true" placeholder="Confirm Password">
                                </div>
                                <div>
                                </div>

                                  <button type="button" class="btn btn-block btn-success save">Save</button>
                            </fieldset>
                        </form>
                    </div>

                    <div class="panel-body">
                                <div>
                                  <p style="color:#5bc0de;">The password require at least 8 character with 1 upper character, 1 numbers and 1 special character</p>
                                </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#navig-menu').css('display','none');


        $('.save').click(function(){

            var id = $('#user_id').val();
            var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();

            if(password == ''){
                $('#validate-password').html('');
                $('#validate-password').append('Password Cannot be null.');
            } else if (confirm_password == '') {
                $('#validate-password').html('');
                $('#validate-password').append('Password Cannot be null.');
            } else {
                $.ajax({
                    url:'<?php echo base_url(); ?>ChangePass/SavePass',
                    method:'POST',
                    data:{ id:id, password:password, confirm_password:confirm_password}
                }).done(function(data){
                     var o = JSON.parse(data);

                    if (o.msg == 300){
                        $('#validate-password').html('');
                        $('#validate-password').append('Password Not Match.');
                    } else if (o.msg == 400){
                        $('#validate-password').html('');
                        $('#validate-password').append('Password will be at lest 8 characters.');
                    } else if (o.msg == 500){
                        $('#validate-password').html('');
                        $('#validate-password').append('Password require 1 upper character, 1 numbers and 1 special character.');
                    }

                    if(o.msg == 'success'){
                        $('#success').modal('show');
                    }
                    
                });
            }

        });

        $('.btn_succsess').click(function() {
            location.reload();
        });

    });
</script>