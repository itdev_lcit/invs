<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div style="float: left;">
                            <h6>&nbsp;&nbsp;&nbsp;<b>DRAFT NO : </b><?php echo $head->dr_no; ?></h6>
                            <h6>&nbsp;&nbsp;&nbsp;<b>Trm Doc Ref : </b><?php echo $head->terminal_doc_ref_an; ?></h6>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Customer</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td><?php echo  $head->company_bill; ?></td>
                                                        <td><?php echo  $head->address_bill; ?></td>
                                                        <td><?php echo  $head->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: center;">Size</th>
                                                <th style="text-align: center;">Type</th>
                                                <th style="text-align: center;">QTY</th>
                                                <th style="text-align: center;">Type</th>
                                                <th style="text-align: center;">Rate (THB)</th>
                                                <th style="text-align: center;">Total (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <?php foreach ($order as $rs_order) { 
                                                    $total_order =  $rate->rate *$rs_order['book_con'];
                                                    $holdtax = $rs_order['config_holdtax'];
                                                ?>
                                                <td style="text-align: center;"><?php echo $rs_order['size_con']."'"; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_order['type_con']; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_order['book_con']; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_order['type']; ?></td>
                                                <td style="text-align: center;"><?php echo number_format($rate->rate,2); ?></td>
                                                <td style="text-align: center;"><?php echo number_format($rate->rate * $rs_order['book_con'],2); ?></td>
                                            <?php } ?>  
                                        </tbody>
                                    </table>   
                                     <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">VAT(<?php echo $vat->vat; ?>%) (THB)</label>
                                          <p><?php echo number_format($total_order* $vat->vat / 100,2); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">Total (THB)</label>
                                          <p><?php echo number_format( $total_order + ($total_order* $vat->vat / 100),2); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="holdtax"><input type="checkbox" id="holdtax" value="1" checked>&nbsp; &nbsp; Withholding Tax (<?php echo $holdtax; ?>%) (THB)</label>
                                          <p id="hold_v"><?php echo number_format($total_order*$holdtax / 100,2); ?></p>
                                          <input type="hidden" name="hold_total" id="hold_total" value="<?php echo $total_order* 3 / 100; ?>">
                                          <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo $total_order + ($total_order * $vat->vat / 100) - ($total_order*$holdtax / 100); ?>">
                                          <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  $total_order + ($total_order * $vat->vat / 100); ?>">
                                        </div>
                                    </div>
                                     <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">Net Pay(THB)</label>
                                          <p id="net_p"><?php echo  number_format($total_order + ($total_order * $vat->vat / 100)- ($total_order* 3 / 100),2); ?></p>
                                        </div>
                                    </div>
                                    <div align="right">
                                        <input type="hidden" id="none_hold" value="">
                                        <input type="hidden" id="dr_no" value="<?php echo $head->dr_no; ?>">
                                        <input type="hidden" id="trm" value="<?php echo $trm; ?>">
                                        <button class="btn btn-info btn-xs gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Re Print Invoice</button>
                                        <a href="<?php echo site_url(); ?>Billing"><button class="btn btn-danger btn-xs ">Cancel</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#none_hold').val('1');

$("#holdtax").bind("change",function(){
   var value = $(this).is(":checked");
   if(value){
        var hold_total = parseInt($('#hold_total').val());
        var net_total_h = parseInt($('#net_total_h').val());
        $('#hold_v').html(hold_total.toLocaleString('en'));
        $('#net_p').html(net_total_h.toLocaleString('en'));
        $('#none_hold').val('1');
   } else {

        var net_total_n = parseInt($('#net_total_n').val());
        $('#hold_v').html('0');
        $('#net_p').html(net_total_n.toLocaleString('en'));
         $('#none_hold').val('0');
   }
});

$('.gen-invoice').click(function(){

        var trm = $('#trm').val();
        var dr_no = $('#dr_no').val();
        var none_hold = $('#none_hold').val();

                var r = confirm("Confirm to Re Print Invoice ?");
                if (r == true) {  
                    
                    //window.open('<?php echo site_url(); ?>Billing/SetInvoiceNormal/'+dr_no+'/'+trm+'');
                    window.location = '<?php echo site_url(); ?>Billing/ReSetInvoiceNormal/'+dr_no+'/'+trm+'/'+none_hold;
                       
                } else {
                    alert('Cancel Re Print');
                }
           
        });



});
</script>