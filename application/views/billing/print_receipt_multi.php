	<style type="text/css">
	    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /*font: 12pt "Tahoma";*/
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 250mm;
        min-height: 310mm;
        padding: 15mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 250mm;
            height: 310mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<div align="center" id="zone-p">
	<br><br>
	<button type="button" class="btn btn-success print-dr">Print</button>
</div>
	<div class="page">

			<img src="<?php echo site_url(); ?>public/img/One-stop-logo.png" width="18%">
			<h4 align="center"><b>One Stop Logistics Co., Ltd.</b></h4>
			<p align="center" style="font-size: 10px;">Laem Chabang International Terminal Co., Ltd. Building 2Nd Fl. Room No.218</p>
			<p align="center" style="font-size: 10px;">Laem Chabang Port B5 Tungsukhla Sriracha Chonburi 20231</p>
			<p align="center" style="font-size: 10px;">Tax Id 0205560028833  Head Office Tel. (66 38 ) 40 8200 Fax : (66 38) 40 1021 - 2</p>
			<br>
			<h4 align="center"><b>RECEIPT / TAX INVOICE</b></h4>


							<table>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="25%"><p style="font-size: 12px;"><b>Original</b></td>
								</tr>
								<tr>

									<td width="10%"><p style="font-size: 13px;"><b>Customer</b></p></td>
									<td width="50%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $payer->customer_name;?></p></td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="16%"><p style="font-size: 13px;"><b>No.</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $invoice_ref; ?></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer->customer_address; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Date</b></p></td>
									<td width="40%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo date("j-F-Y", strtotime($print_date)); ?></p></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer->customer_address2; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Type</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $type; ?></p></td>
								</tr>
								<tr>
									<td></td>
									<td width="50%"><p style="font-size: 13px;">&nbsp;&nbsp;<?php echo $payer->customer_address3; ?>&nbsp;&nbsp;<?php echo $payer->customer_post; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Corp. A/C No</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;99999</p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 13px;"><b>Tax ID</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $payer->tax_reg_no; ?> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <b>Branch :</b><?php echo $payer->customer_branch; ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Oper. A/C ID</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $payer->customer_code ; ?></p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 13px;"><b>Remark</b></p></td>
									<td width="45%"><p style="font-size: 13px;">:&nbsp;&nbsp;</p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Payment Terms</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<!--<?php echo $rs_head['payment']; ?>--></p></td>
								</tr>
								<tr>
									<td width="10%"><p style="font-size: 13px;"><b></b></p></td>
									<td width="45%"><p style="font-size: 13px;"></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b>Our Ref</b></p></td>
									<td width="25%"><p style="font-size: 13px;">:&nbsp;&nbsp;<?php echo $trm; ?></p><p style="font-size: 13px;">&nbsp;&nbsp;DFT#<?php echo $dr_no; ?></p>
									<p style="font-size: 13px;">&nbsp;&nbsp;Booking#<?php echo $bookan; ?></p></td>
								</tr>
								<!--<tr>
									<td width="10%"><p style="font-size: 10px;"><b>Vessel</b></p></td>
									<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php if(!empty($rs_head['vessel_code'])) { echo $rs_head['vessel_code']; } else { echo "-";} ?></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 10px;"><b>ATB</b></p></td>
									<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($rs_head['atb'])); ?></p></td>
								</tr>-->
								<tr>
									<td width="10%"><p style="font-size: 13px;"><b></b></p></td>
									<td><!--<p style="font-size: 10px;"><?php echo $rs_head['remark']; ?>--></p></td>
									<td></td>
									<td width="16%"><p style="font-size: 13px;"><b><!--ATD--></b></p></td>
									<td width="25%"><!--<p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($rs_head['atd']));?></p>--></td>
								</tr>

							</table>
							<br>
							<table class="table">
							    <thead>
							        <tr>
							            <th style="font-size: 13px; text-align: left;"  colspan="2">Description</th>
							           
							            <th style="font-size: 13px; text-align: center;"  width="10%">QTY</th>
							            <th style="font-size: 13px; text-align: right;"  width="20%">Unit Rate (THB)</th>
							            <th style="font-size: 13px; text-align: right;"  width="25%">Line Amount (THB)</th>
							        </tr>
							    </thead>
							    <tbody id="book">
							    	<?php 
							    	$all_amount = 0;
							    	foreach ($order as $rs_order) {  ?>
							          <tr>
							          	<td style="font-size: 13px;" align="left" colspan="2"><?php echo $dr_t->description."&nbsp;&nbsp;".$rs_order['size_con']."'"."&nbsp;&nbsp;".$rs_order['type_con']; ?></td>

							          	<td style="font-size: 13px;" align="center"><?php echo $rs_order['book_con']; ?></td>
							          	<td style="font-size: 13px;" align="right"><?php echo number_format($rs_order['cur_rate'], 2); ?></td>
							          	<td style="font-size: 13px;" align="right">
							          		<?php

							          			$line_amount = $rs_order['book_con'] * $rs_order['cur_rate'];
							          			$all_amount += $line_amount;
							          			echo number_format($line_amount, 2);
							          		?>
							          	</td>
							          </tr>   
							        <?php }  ?>

							         	<tr>
								          	<td colspan="3" rowspan="5"></td>
								          	<td style="font-size: 13px;" align="right"><b>Sub Total</b></td>
								          	<td style="font-size: 13px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
							          	</tr> 
							          	<tr>      	
								          	<td style="font-size: 13px;" align="right"><b>VAT (<?php echo $vat->vat; ?>%)</b></td>
								          	<td style="font-size: 13px;"  align="right">
								          	<?php  
								          			$vat = $all_amount*$vat->vat/100; 
								          		   	echo number_format($vat, 2); 

								          	?></td>
							          	</tr> 
							         	<tr>  	
								          	<td style="font-size: 13px;" align="right"><b>Total (THB)</b></td>
								          	<td style="font-size: 13px;"  align="right">
								          	<?php  

								          			$net_total = $all_amount + $vat;
								          			echo number_format($net_total, 2); 

								          	?></td>
							          	</tr> 
							          	
							    </tbody>
							</table>
							<table class="table">
								<thead>
							        <tr>
							            <th style="font-size: 13px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
							        </tr>
							        <tr>
							            <th style="font-size: 13px; text-align: left;">Date</th>
							            <th style="font-size: 13px; text-align: center;">Payment Method</th>
							            <th style="font-size: 13px; text-align: left;">Ref. No.</th>
							            <th style="font-size: 13px; text-align: right;">Payment Amount</th>
							            <th style="font-size: 13px; text-align: center;">Exch. Rate</th>
							            <th style="font-size: 13px; text-align: right;">Amount (THB)</th>
							        </tr>
							    </thead>
							    <tbody>
							    	<tr>
							    		<td style="font-size: 13px; text-align: left;"><?php echo date("j-F-Y"); ?></td>
							    		<td style="font-size: 13px; text-align: center;">CASH</td>
							    		<td style="font-size: 13px; text-align: center;"></td>
							    		<td style="font-size: 13px; text-align: right;">THB&nbsp;<?php echo number_format($net_total, 2); ?></td>
							    		<td style="font-size: 13px; text-align: center;">1</td>
							    		<td style="font-size: 13px; text-align: right;"><?php echo number_format($net_total, 2); ?></td>
							    	</tr>
							    	<?php if($hold_status == '1'){ ?>
							    	<tr>
							    		<td style="font-size: 13px; text-align: right;" colspan="5"><b>Withholding Tax (3%) :</b></td>
							    		<td style="font-size: 13px; text-align: right;">&nbsp;<?php $hold_tax = $all_amount*3/100; echo number_format($hold_tax, 2);  ?></td>
							    	</tr>
							    	<tr>
							    		<td style="font-size: 13px; text-align: left;" colspan="2">Received By : <?php echo $user_print->name; ?> </td>
							    		<td style="font-size: 13px; text-align: left;" colspan="2"></td>
							    		<td style="font-size: 13px; text-align: right;" ><b>Amount Received (THB) :</b></td>
							    		<td style="font-size: 13px; text-align: right;" ><?php echo number_format($net_total - $hold_tax, 2); ?></td>
							    	</tr>
							    	<?php } else { ?>	
							    	<tr>
							    		<td style="font-size: 13px; text-align: right;" colspan="5"><b>Withholding Tax (3%) :</b></td>
							    		<td style="font-size: 13px; text-align: right;">&nbsp;0.00</td>
							    	</tr>
							    	<tr>
							    		<td style="font-size: 13px; text-align: left;" colspan="2">Received By : <?php echo $user_print->name; ?> </td>
							    		<td style="font-size: 13px; text-align: left;" colspan="2"></td>
							    		<td style="font-size: 13px; text-align: right;" ><b>Amount Received (THB) :</b></td>
							    		<td style="font-size: 13px; text-align: right;" ><?php echo number_format($net_total, 2); ?></td>
							    	</tr>	
							    	<?php } ?>
							    </tbody>
							</table>
							<table class="table table-striped table-bordered">
							        <tr>
							            <td style="font-size: 13px;" width="50%">
							            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวนสิทธิ ในการไม่คืนเงิน</p>
							            	
							            </td>
							            <td style="font-size: 13px;" width="50%">
							            	<p>This is a computer generated document, no signature required.</p>
							            	<!--<p>Contact detail:</p>
							            	<p>Email :</p>-->
							            	<p>Tel :  (66 38 ) 40 8200</p>
							            	<p>Fax : (66 38) 40 1021 - 2</p>
							            </td>
							        </tr>
							</table>
	</div>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  	//window.print();
    //document.location.href = "<?php echo site_url(); ?>Billing/AllBill";
    $(".print-dr").click(function(){
		$("#zone-p").hide();
		window.print();
		document.location.href = "<?php echo site_url(); ?>Billing/AllBill";
	});
});
</script>