<br><br><br>
<h3 align="center"><b>RECEIPT / TAX INVOICE</b></h3>


				<table>

					<tr>

						<td width="10%"><p style="font-size: 10px;"><b>Bill To</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->company_bill;?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Invoice No</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->prefix_invoice.$split_order->invoice_no;?></td>
					</tr>
					<tr>
						<td></td>
						<td><p style="font-size: 10px;"><?php echo $head->address_bill; ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Date</b></p></td>
						<td width="40%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y"); ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Type</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->type; ?></p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Corp. A/C No</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;99999</p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Tax ID</b></p></td>
						<td>:&nbsp;&nbsp;</td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Oper. A/C ID</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->code_comp."-".$head->id_comp; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Branch</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->company_branch; ?>/p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Payment Terms</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->payment; ?></p></td>
					</tr>
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Customer</b></p></td>
						<td width="45%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $head->company_bill;?></p></td>
						<td></td>
						<!--<td width="16%"><p style="font-size: 10px;"><b>Voyage In/Out</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php if(!empty($split_order->voy_in) AND $split_order->voy_out) { echo $split_order->voy_in."&nbsp;/&nbsp;".$split_order->voy_out; } else { echo "-"; } ?></p></td>-->
						<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->terminal_doc_ref_an; ?></p>
							<p style="font-size: 10px;">&nbsp;&nbsp;DFT#<?php echo $split_order->dr_no; ?></p>
						</td>
					</tr>
					<!--<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Vessel</b></p></td>
						<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php if(!empty($split_order->vessel_code)) { echo $split_order->vessel_code; } else { echo "-";} ?></p></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>ATB</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($split_order->atb)); ?></p></td>
					</tr>-->
					<tr>
						<td width="10%"><p style="font-size: 10px;"><b>Remark </b></p></td>
						<td><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->remark; ?></p></td>
						<td></td>
						<td width="16%"><!--<p style="font-size: 10px;"><b>ATD</b></p>--></td>
						<td width="25%"><!--<p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo date("j-F-Y g:i", strtotime($split_order->atd));?></p>--></td>
					</tr>
					<!--<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="16%"><p style="font-size: 10px;"><b>Our Ref</b></p></td>
						<td width="25%"><p style="font-size: 10px;">:&nbsp;&nbsp;<?php echo $split_order->terminal_doc_ref_an; ?></p></td>
					</tr>-->

				</table>
				<br><br><br>
				<table class="table">
				    <thead>
				        <tr>
				            <th style="font-size: 10px; text-align: center;"  width="25%">Size</th>
				            <th style="font-size: 10px; text-align: center;"  width="25%">QTY</th>
				            <th style="font-size: 10px; text-align: right;"  width="20%">Unit Rate (THB)</th>
				            <th style="font-size: 10px; text-align: right;"  width="25%">Line Amounth (THB)</th>
				        </tr>
				    </thead>
				    <tbody id="book">
				    	<?php 
				    	$all_amount = 0; ?>
				          <tr>
				          	<td style="font-size: 10px;" align="center"><?php echo $split_order->size_con."''"; ?></td>
				          	<td style="font-size: 10px;" align="center"><?php echo $split_order->book_con; ?></td>
				          	<td style="font-size: 10px;" align="right"><?php echo number_format($split_order->cur_rate, 2); ?></td>
				          	<td style="font-size: 10px;" align="right">
				          		<?php

				          			$line_amount = $split_order->book_con * $split_order->cur_rate;
				          			$all_amount += $line_amount;
				          			echo number_format($line_amount, 2);
				          		?>
				          	</td>
				          </tr>   

				         <tr>
				          	<td colspan="2" rowspan="5"></td>
				          	<td style="font-size: 10px;" align="right">Sub Total</td>
				          	<td style="font-size: 10px;"  align="right"><?php echo  number_format($all_amount, 2); ?></td>
				          </tr> 
				          <tr>
				          	
				          	<td style="font-size: 10px;" align="right">VAT (<?php echo $vat->vat."%"; ?>)</td>
				          	<td style="font-size: 10px;"  align="right">
				          	<?php  
				          		
				          			$vat = $all_amount*$vat->vat/100; 
				          		   	echo number_format($vat, 2); 

				          	?></td>
				          </tr> 
				          <tr>     	
				          	<td style="font-size: 10px;" align="right"><b>Net Pay (THB)</b></td>
				          	<td style="font-size: 10px;"  align="right"><?php 

				          			$net_total = $all_amount + $vat;
				          			echo number_format($net_total, 2); 

				          	?></td>
				          </tr>  
				    </tbody>
				</table>
				<table class="table">
					<thead>
				        <tr>
				            <th style="font-size: 10px; text-align: left;" colspan="6">PAYMENT DETAILS</th>
				        </tr>
				        <tr>
				            <th style="font-size: 10px; text-align: left;">Date</th>
				            <th style="font-size: 10px; text-align: center;">Payment Method</th>
				            <th style="font-size: 10px; text-align: left;">Ref. No.</th>
				            <th style="font-size: 10px; text-align: right;">Payment Amount</th>
				            <th style="font-size: 10px; text-align: center;">Exch. Rate</th>
				            <th style="font-size: 10px; text-align: right;">Amounth (THB)</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<tr>
				    		<td style="font-size: 10px; text-align: left;"><?php echo date("j-F-Y"); ?></td>
				    		<td style="font-size: 10px; text-align: center;">CASH</td>
				    		<td style="font-size: 10px; text-align: center;"></td>
				    		<td style="font-size: 10px; text-align: right;">THB&nbsp;<?php echo number_format($net_total, 2); ?></td>
				    		<td style="font-size: 10px; text-align: center;">1</td>
				    		<td style="font-size: 10px; text-align: right;"><?php echo number_format($net_total, 2); ?></td>
				    	</tr>
				    	<tr>
				    		<td style="font-size: 10px; text-align: left;" colspan="2">Received By : <?php echo $user_print->name; ?> </td>
				    		<td style="font-size: 10px; text-align: left;" colspan="2"></td>
				    		<td style="font-size: 10px; text-align: right;" >Amount Received (THB) :</td>
				    		<td style="font-size: 10px; text-align: right;" ><?php echo number_format($net_total, 2); ?></td>
				    	</tr>
				    </tbody>
				</table>
				<table class="table table-striped table-bordered">
				        <tr>
				            <td style="font-size: 10px;" width="50%">
				            	<p>กรณีขอคืนเงิน กรุณายื่นเรื่องที่แคชเชียร์ ภายใน 30 วัน นับจากวันที่ชำระเงิน หากเกิน 30 วัน ทางบริษัท ขอสงวน ในการไม่คืนเงิน</p>
				            	<p>REMARK : THE SERVICE QUOTED/CHARGED ABOVE ARE SUBJECT TO TARIFF OF PORT AUTHORITY OF THILAND , USER HAS AGREED AND ACCEPTED.</p>
				            </td>
				            <td style="font-size: 10px;" width="50%">
				            	<p>This is a computer generated document, no signature required.</p>
				            	<p>Contact detail:</p>
				            	<p>Email :</p>
				            	<p>Tel :</p>
				            	<p>Fax :</p>
				            </td>
				        </tr>
				</table>

<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    window.print();
    document.location.href = "<?php echo site_url(); ?>Billing"; 
});
</script>