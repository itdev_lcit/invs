<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                 <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <br>
                        <div align="right">
                            <form class="form-inline" action="<?php echo site_url() . 'Billing/AllBill'; ?>" method="POST">
                                <label for="sel1">Sort By</label>
                                  <select class="form-control" name="sort">
                                    <option value="dr_no">Draft No.</option>
                                    <option value="code_comp">Customers</option>
                                    <option value="book_an">Booking</option>
                                    <option value="created">Created</option>                        
                                  </select>
                                <input class="form-control" type="text" name="search" value="" placeholder="Search Draft No...">
                                <button class="btn btn-info">Go</button>
                                <button class="btn btn-warning">Clear</button>
                                &nbsp;&nbsp;
                            </form>
                        </div>   
                              
                        <div class="panel-body">
                            <div class="row">


                                <div class="col-lg-12"  id="dr-1">
                                    <table class="table table-striped table-bordered" id="normal-draft">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">#</th>
                                            <th>Draft</th>
                                            <th style="text-align: center;">Customers</th>
                                            <!--<th style="text-align: center;">Size</th>
                                            <th style="text-align: center;">QTY</th>-->
                                            <th style="text-align: center;">Status</th>
                                            <th>Created</th>
                                            <th style="text-align: left;">Comment</th>
                                            <!--<th>Updated</th>-->
                                            <th style="text-align: center;">Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;
                                            if(!empty($results)){
                                            foreach ($results as $rs_normal) {?>
                                            <tr class="r-normal" data-prefix_invoice ="<?php echo $rs_normal->prefix_invoice; ?>" data-invoice_no ="<?php echo $rs_normal->invoice_no; ?>" <?php if($rs_normal->is_use == 1){ echo "style='color: red;'"; } ?>>
                                                <td style="text-align: center;"><?php echo $i; ?></td>
                                                <td><?php echo $rs_normal->dr_no; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->code_comp; ?></td>
                                                <!--<td style="text-align: center;"><?php echo $rs_normal->size_con; ?></td>
                                                <td style="text-align: center;"><?php echo $rs_normal->book_con; ?></td>-->
                                                <td style="text-align: center;">
                                                    <?php 
                                                         if($rs_normal->is_use == '0'){
                                                            echo "<p style='color:green;'><b>Draft</b></p>";
                                                         } else if ($rs_normal->is_use == '2'){
                                                            if($rs_normal->is_reprint == 1){
                                                                echo "<p style='color:green;'><b>REPRINT-Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                            } else {
                                                                echo "<p style='color:green;'><b>Invoice#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                            }
                                                         } else {
                                                            echo "<p style='color:red;'><b>Cancel#".$rs_normal->prefix_invoice.$rs_normal->invoice_no."</b></p>";
                                                         }
                                                    ?>
                                                    
                                                </td>
                                                <td><?php echo date("j-F-Y H:i", strtotime($rs_normal->create_order)); ?></td>
                                                <td style="text-align: left;">

                                                    <?php if ($rs_normal->is_use == '1') { ?>
                                                        <p style='color:red;'><?php echo $rs_normal->remark_cancel; ?></p>
                                                    <?php } else { ?>
                                                        <p style='color:green;'><?php echo $rs_normal->remark_cancel; ?></p>
                                                    <?php } ?>   
                                                </td>
                                                <!--<td><?php echo date("j-F-Y H:i", strtotime($rs_normal->update_order)); ?></td>-->
                                                <td style="text-align: center;">
                                                    <?php if($rs_normal->is_use == '2'){ ?>                                                  
                                                             <?php if($special == 1  AND $rs_normal->is_reprint == 0 OR $role == 'SUPBILLING') {?>
                                                            <a href="<?php echo site_url(); ?>Billing/RePreviewNormalInvoice/<?php echo $rs_normal->dr_no; ?>/<?php echo $rs_normal->id_order; ?>" title="Preview Invoice"><button class="btn btn-warning btn-xs re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Re - Preview Invoice</button></a>
                                                            <?php } ?>
                                                    
                                                    <?php } else { ?>
                                                            <?php if($rs_normal->is_multi == '0' and $rs_normal->is_use != '1'){ ?>
                                                                <a href="<?php echo site_url(); ?>Billing/PreviewNormalInvoice/<?php echo $rs_normal->dr_no; ?>/<?php echo $rs_normal->id_order; ?>" title="Generate Invoice"><button class="btn btn-success  btn-xs re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Preview Invoice</button></a>
                                                            <?php } else if($rs_normal->is_use != '1') { ?>
                                                                <a href="<?php echo site_url(); ?>MultiInvoice/PreviewInvoice/<?php echo $rs_normal->dr_no; ?>" title="Generate Invoice"><button class="btn btn-success  btn-xs re-dr-normal"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Preview Invoice</button></a>
                                                            <?php } ?>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                         <?php $i++; } 
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="8" style="text-align: center;">-No data available-</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                     <!-- /.paging -->
                                    <?php echo $links; ?>
                                    <!-- /.paging -->
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>



<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        /*$('#normal-draft').DataTable({
           // responsive: true
        });*/

        $('#split-draft').DataTable({
            //responsive: true
        });

        $('#dr-1').show();
        $('#dr-2').hide();

        $("#type-draft").change(function(){
            var type = $('#type-draft').val();
            if(type == 1){
                $('#dr-1').show();
                $('#dr-2').hide();
            } else if (type == 2){
                $('#dr-1').hide();
                $('#dr-2').show();
            } else {
                $('#dr-1').hide();
                $('#dr-2').hide();
            }
        });

        

    });
</script>