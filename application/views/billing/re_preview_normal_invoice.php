<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div style="float: left;">
                            <h6>&nbsp;&nbsp;&nbsp;<b>DRAFT NO : </b><?php echo $split_order->dr_no; ?></h6>
                            <h6>&nbsp;&nbsp;&nbsp;<b>Trm Doc Ref : </b><?php echo $head->terminal_doc_ref_an; ?></h6>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Customer</th>
                                                <th >Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td><?php echo  $head->company_bill; ?></td>
                                                        <td><?php echo  $head->address_bill; ?></td>
                                                        
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: center;">Size</th>
                                                <th style="text-align: center;">QTY</th>
                                                <th style="text-align: center;">Type</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                                <td style="text-align: center;"><?php echo $split_order->size_con; ?></td>
                                                <td style="text-align: center;"><?php echo $split_order->book_con; ?></td>
                                                <td style="text-align: center;"><?php echo $split_order->type; ?></td>
                                        </tbody>
                                    </table>   
                                    <div align="right">
                                        <input type="hidden" id="dr_no" value="<?php echo $split_order->dr_no; ?>">
                                        <button class="btn btn-info btn-xs gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Re Print Invoice</button>
                                        <a href="<?php echo site_url(); ?>Billing"><button class="btn btn-danger btn-xs ">Cancel</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

 $('.gen-invoice').click(function(){

        var trm = $('#trm').val();
        var dr_no = $('#dr_no').val();


                var r = confirm("Confirm to Re Print Invoice ?");
                if (r == true) {  
                    
                    //window.open('<?php echo site_url(); ?>Billing/SetInvoiceSplit/'+dr_no+'');

                    window.location = '<?php echo site_url(); ?>Billing/ReSetInvoiceSplit/'+dr_no+'';
                       
                } else {
                    alert('Cancel Re Print');
                }
           
        });

});
</script>