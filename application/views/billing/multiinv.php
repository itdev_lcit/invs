<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
       <h4 class="page-header">Billing</h4>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Invoice
                        </div>
                        <div style="float: left;">
                            <h6>&nbsp;&nbsp;&nbsp;<b>DRAFT NO : </b><?php echo $dr_ref; ?></h6>
                            <h6>&nbsp;&nbsp;&nbsp;<b>Trm Doc Ref : </b><?php echo $trm; ?></h6>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 align="center"><b>Preview INVOICE</b></h3>
                                    <br><br>
                                   
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th >Customer</th>
                                                <th >Address</th>
                                                <th >TAX</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                                    <tr>
                                                        <td><?php echo  $payer->customer_name; ?></td>
                                                        <td><?php echo  $payer->customer_address; ?><?php echo $payer->customer_address2; ?><?php echo $payer->customer_address3; ?></td>
                                                        <td><?php echo  $payer->tax_reg_no; ?></td>
                                                    </tr> 
                                         
                                        </tbody>
                                    </table>  
                                </div>
  
                                <div class="col-lg-12">
                                    <table class="table">
                                            <tr>
                                                <th style="text-align: left;">Description</th>
                                                <th style="text-align: center;">QTY</th>
                                                <th style="text-align: right;">Unit Rate (THB)</th>
                                                <th style="text-align: right;">Line Amount (THB)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="dr">
                                            <?php 
                                                $all_amount = 0;
                                                foreach ($order as $rs_order) { 

                                                $total_order =  $rs_order['book_con'] * $rs_order['cur_rate'];
                                                $all_amount += $total_order;

                                            ?>
                                            <tr>
                                                <td style="text-align: left;"><?php echo $type->description."&nbsp;&nbsp;".$rs_order['size_con']."'"."&nbsp;&nbsp;".$rs_order['type_con']; ?> 
                                                </td>
                                                <td style="text-align: center;"><?php echo $rs_order['book_con']; ?></td>
                                                <td style="text-align: right;"><?php echo number_format($rs_order['cur_rate'],2); ?></td>
                                                <td style="text-align: right;"><?php echo number_format($rs_order['book_con'] * $rs_order['cur_rate'],2); ?></td>
                                            <tr>
                                            <?php } ?>  
                                        </tbody>
                                    </table>   
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">VAT(<?php echo $vat->vat; ?>%) (THB)</label>
                                          <p><?php echo number_format($all_amount* $vat->vat / 100); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">Total (THB)</label>
                                          <p><?php echo number_format( $all_amount + ($all_amount* $vat->vat / 100)); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="holdtax"><input type="checkbox" id="holdtax" value="1" checked>&nbsp; &nbsp; Withholding Tax (3%) (THB)</label>
                                          <p id="hold_v"><?php echo number_format($all_amount* 3 / 100); ?></p>
                                          <input type="hidden" name="hold_total" id="hold_total" value="<?php echo $all_amount* 3 / 100; ?>">
                                          <input type="hidden" name="net_total_h" id="net_total_h" value="<?php echo $all_amount + ($all_amount * $vat->vat / 100) - ($all_amount* 3 / 100); ?>">
                                          <input type="hidden" name="net_total_n" id="net_total_n" value="<?php echo  $all_amount + ($all_amount * $vat->vat / 100); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"  align="right">
                                          <label for="sel1">Net Pay(THB)</label>
                                          <p id="net_p"><?php  echo  number_format($all_amount + ($all_amount * $vat->vat / 100) - ($all_amount* 3 / 100));  ?></p>
                                        </div>
                                    </div>
                                    <div align="right">
                                        <input type="hidden" id="none_hold" value="">
                                        <input type="hidden" id="dr_no" value="<?php echo $dr_ref; ?>">
                                        <input type="hidden" id="trm" value="<?php echo $trm; ?>">
                                        <button class="btn btn-info btn-xs gen-invoice"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Genarate Invoice</button>
                                        <a href="<?php echo site_url(); ?>Billing"><button class="btn btn-danger btn-xs ">Cancel</button></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

</div>
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#none_hold').val('1');

$("#holdtax").bind("change",function(){
   var value = $(this).is(":checked");
   if(value){
        var hold_total = parseInt($('#hold_total').val());
        var net_total_h = parseInt($('#net_total_h').val());
        $('#hold_v').html(hold_total.toLocaleString('en'));
        $('#net_p').html(net_total_h.toLocaleString('en'));
        $('#none_hold').val('1');
   } else {

        var net_total_n = parseInt($('#net_total_n').val());
        $('#hold_v').html('0');
        $('#net_p').html(net_total_n.toLocaleString('en'));
         $('#none_hold').val('0');
   }
});

 $('.gen-invoice').click(function(){

        var trm = $('#trm').val();
        var dr_no = $('#dr_no').val();
        var none_hold = $('#none_hold').val();


                var r = confirm("Confirm to generate Invoice ?");
                if (r == true) {  
                    
                    //window.open('<?php echo site_url(); ?>Billing/SetInvoiceNormal/'+dr_no+'/'+trm+'');
                    window.location = '<?php echo site_url(); ?>MultiInvoice/SetInvoiceNormal/'+dr_no+'/'+none_hold;
                       
                } else {
                    alert('Cancel Generate');
                }
           
        });



});
</script>