<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class LaunchPatch extends Welcome {

	public function index(){

		
		$this->db->order_by('ssr_order.ssr_id','desc');
		$this->db->where('type !=','CANCEL');
		$this->db->where('cal_vat','0.00');
		$ssr = $this->db->get('ssr_order')->result_array();

			foreach ($ssr as $rs) {

				$this->db->limit(1);
				$trf = $this->db->get('tariff_type')->row();

				$cal_vat = ($rs['cur_rate']*$rs['qty'])*($trf->vat/100);
				$cal_wh = ($rs['cur_rate']*$rs['qty'])*($trf->holdtax/100);


				if($trf->code_wh != 'N'){
					$wh = 'YES';
				} else {
					$wh = 'NO';
				}

				if($trf->code_vat != 'N'){
					$vat = 'YES';
				} else {
					$vat = 'NO';
				}

					$data = array(
						"wh" => $wh,
						"vat" => $vat,
						"cal_vat" => (float)(($rs['cur_rate']*$rs['qty'])*($trf->vat/100)),
						"cal_wh" => (float)(($rs['cur_rate']*$rs['qty'])*($trf->holdtax/100)),
						"updated" => date('Y-m-d H:i:s')
					);

					$this->db->where('ssr_id', $rs['ssr_id']);
					$this->db->update('ssr_order',$data);

					echo  $rs['ssr_id']." | Update record : ".$rs['dr_no']." | On Time : ".date('Y-m-d H:i:s')."<br>";
			
			}

		$this->db->where('type !=','CANCEL');
		$this->db->where('cal_vat','0.00');
		$this->db->select('count(ssr_id) as RemainSSR');
		$remain = $this->db->get('ssr_order')->row();

		echo "Remain : ".$remain->RemainSSR;


	}



}