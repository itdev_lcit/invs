<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
        parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');

	}

	public function view(){

		$check_data = $this->session->userdata('logged_in');

		$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$this->view['header'] =  $this->load->view('mains/header','',true);
		$this->view['menu'] =  $this->load->view('mains/menu',$data,true);

		return $this->load->view('mains/index',$this->view);
	}
}
