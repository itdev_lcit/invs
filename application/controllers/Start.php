<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Start extends Welcome {

	public function index(){
		
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');

			$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$date_user = date_create($user->date_process);

	        if(!empty($user->date_process)){
	          if(date_format($date_user,'Y-m-d') < date('Y-m-d')){
	            redirect('Logout', 'refresh');
	          } 
	        }

			$this->view['main'] =  $this->load->view('pro/index','',true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}
	
	
}