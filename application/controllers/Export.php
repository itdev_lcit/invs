<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Welcome.php');


class Export extends Welcome {

        public function __construct() {
            parent:: __construct();
            $this->load->helper("url");
            $this->load->library("pagination");
            $this->load->model('M_Billing');
        }

    public function AllInv($type = null){
        if($this->session->userdata('logged_in')) {     

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

           
            $this->view['main'] =  $this->load->view('export/all_inv',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }

    public function AllSSR($type = null){
        if($this->session->userdata('logged_in')) {     

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

            
            $this->view['main'] =  $this->load->view('export/all_ssr',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }

    public function AllOrder($type = null){
        if($this->session->userdata('logged_in')) {     

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

            
            $this->view['main'] =  $this->load->view('export/all_order',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }



        public function Invoice(){

            $type = $this->input->post('type');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');

            $check_data = $this->session->userdata('logged_in');
            $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $this->db->join('normal_order','normal_order.id = status_dr.normal_id','left');
            $this->db->join('log_print_invoice','log_print_invoice.invoice_no = concat(`normal_order`.`prefix_invoice`, `normal_order`.`invoice_no` )','left');
            $this->db->join('customer','customer.id = normal_order.id_comp','left');  
            $this->db->join('book_order','book_order.id = status_dr.trm_doc','left');
            $this->db->join('status_invoice','status_invoice.dr_no = normal_order.dr_no','left');
            $this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);
            $this->db->where('normal_order.prefix_invoice is NOT NULL', NULL, FALSE);
            $this->db->where('normal_order.invoice_no is NOT NULL', NULL, FALSE);
            $this->db->select('concat(`normal_order`.`prefix_invoice`, `normal_order`.`invoice_no` ) as inv, `normal_order`.`dr_no`, `customer`.`customer_code`, `customer`.`customer_name`, CAST( `customer`.`customer_branch` AS CHAR), CAST( `customer`.`tax_reg_no` AS CHAR), `book_order`.`book_an`, `book_order`.`type`, `book_order`.`size_con`, `book_order`.`book_con`, ((`book_order`.book_con*`book_order`.cur_rate) * 7 / 100) , (`book_order`.book_con*`book_order`.cur_rate) , (`book_order`.book_con*`book_order`.cur_rate) + ((`book_order`.book_con*`book_order`.cur_rate) * 7 / 100) , IF(`normal_order`.hold_tax = 1, (`book_order`.book_con*`book_order`.cur_rate) + ((`book_order`.book_con*`book_order`.cur_rate) * 7 / 100) - (((`book_order`.book_con*`book_order`.cur_rate) * 3 / 100)) , (`book_order`.book_con*`book_order`.cur_rate) + ((`book_order`.book_con*`book_order`.cur_rate) * 7 / 100) ) , IF(`normal_order`.hold_tax = 1, "WithholdingTax", "NO") , `log_print_invoice`.`username` , `log_print_invoice`.`created`');
            if($type == 2){
                $this->db->where('log_print_invoice.username', $username->username);
            }
            $this->db->where("log_print_invoice.created BETWEEN '".$first_date." 00:00:01"."' AND '".$second_date." 23:59:59"."'");
            //$this->db->group_by("`normal_order`.`invoice_no`");
            $query = $this->db->get('status_dr');
            $data = $query->result_array();
            
            header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=\"PaymentReport".$first_date.".csv\"");
            header("Pragma: no-cache");
            header("Expires: 0");

            $handle = fopen('php://output', 'w');

            foreach ($data as $data) {
                fputcsv($handle, $data);
            }
                fclose($handle);
            exit;
        }

    public function SSR(){

            $type = $this->input->post('type');
            $type_inv = $this->input->post('type_inv');
            $liner = $this->input->post('liner');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');
            $f_ex = $this->input->post('f_ex');

            $check_data = $this->session->userdata('logged_in');
            $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $data['type_inv'] = $type_inv;
            $data['first_date'] = $first_date;
            $data['second_date'] = $second_date;

            $data['customer_code'] = $liner;
            $this->db->order_by("`ssr_order`.`dr_no`");
            $this->db->group_by("`ssr_order`.`dr_no`");  

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_order.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("status_ssr_invoice.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_order.type !=', 'CANCEL');
                $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->where('ssr_order.dr_no is NOT NULL', NULL, FALSE);
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->join('status_ssr_invoice','status_ssr_invoice.dr_no = ssr_order.dr_no','left');
            $this->db->select(' `ssr_order`.`dr_no` as `InvNo`, CONCAT(ssr_order.prefix_invoice, ssr_order.invoice_no) AS RECEIPT_INVOICE, "" as `NotedNo`, `ssr_order`.`vessel`, `ssr_order`.`atb`, `ssr_order`.`payment`, `customer`.`customer_code` as `Cus`, `ssr_order`.`created`, `status_ssr_invoice`.`created` as `date_receipt`, sum(ssr_order.qty*ssr_order.cur_rate) as Amount, (if(ssr_order.vat = "YES", sum(cal_vat),0)) as Vat, (if(ssr_order.vat = "YES", Sum((ssr_order.qty*ssr_order.cur_rate)+(cal_vat)), Sum((ssr_order.qty*ssr_order.cur_rate)))) as Total, `ssr_order`.`config_holdtax`, (if((ssr_order.wh = "YES"), Sum(cal_wh),0)) as WithHolding, (if(ssr_order.vat = "YES", Sum((ssr_order.qty*ssr_order.cur_rate)+(cal_vat)), Sum((ssr_order.qty*ssr_order.cur_rate)))) - (if((ssr_order.wh = "YES"), sum(cal_wh), 0)) as NetPay, "" as `type_noted`');

            

            $query2 = $this->db->get('ssr_order')->result_array();


            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            $this->db->where('ssr_noted.status_noted !=', 'CANCEL');

            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->select('ssr_noted.dr_no as InvNo, CONCAT(ssr_noted.prefix_invoice,ssr_noted.invoice_no) AS RECEIPT_INVOICE, ssr_noted.noted_no as NotedNo, ssr_noted.vessel, ssr_noted.atb ,ssr_noted.payment, customer.customer_code as Cus, ssr_noted.created, ssr_noted.created as date_receipt, sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount, sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat ,Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total, ssr_noted.config_holdtax, sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding, Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay ,ssr_noted.type_noted');
             $this->db->group_by("`ssr_noted`.`noted_no`");

            

            $query_noted = $this->db->get('ssr_noted')->result_array();

            //echo '<pre>',print_r($query2,1),'</pre>'; die;


             /////////////////////////SSR//////////////////////////////////////////////////////////////////////


            $this->db->group_by("`ssr_order`.`vat`"); 

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_order.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("status_ssr_invoice.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_order.type !=', 'CANCEL');
                $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->join('status_ssr_invoice','status_ssr_invoice.dr_no = ssr_order.dr_no','left');
            $this->db->select('sum(ssr_order.qty*ssr_order.cur_rate) as Amount , (if(ssr_order.vat = "YES", sum((ssr_order.qty*ssr_order.cur_rate)*7/100), 0))  as Vat , (if(ssr_order.vat = "YES", Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)), Sum((ssr_order.qty*ssr_order.cur_rate)))) as Total , (if((ssr_order.wh = "YES" OR ssr_order.wh is null) and ssr_order.vat = "YES", sum((ssr_order.qty*ssr_order.cur_rate)*ssr_order.config_holdtax/100), 0)) as WithHolding ,(if(ssr_order.vat = "YES", Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)), Sum((ssr_order.qty*ssr_order.cur_rate)))) - (if(ssr_order.wh = "YES" OR ssr_order.wh is null and ssr_order.vat = "YES", sum((ssr_order.qty*ssr_order.cur_rate)*ssr_order.config_holdtax/100), 0)) as NetPay ,ssr_order.vat');

            $sum_ssr_csv = $this->db->get('ssr_order')->result_array();




            ////////////////////////////////////NOTED/////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_noted.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_noted.type !=', 'CANCEL');
                $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->where('ssr_noted.type_noted', 'DEBIT');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->select('ssr_noted.type_noted, sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat
                     ,Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total
                     , ssr_noted.config_holdtax
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding
                     , Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay');
            $this->db->group_by("`ssr_noted`.`type_noted`");

            $sum_noted_debit = $this->db->get('ssr_noted')->row();



            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_noted.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_noted.type !=', 'CANCEL');
                $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->where('ssr_noted.type_noted', 'CREDIT');
            $this->db->where('ssr_noted.is_use', 'YES');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->select('ssr_noted.type_noted, sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat
                     ,Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total
                     , ssr_noted.config_holdtax
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding
                     , Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay');
            $this->db->group_by("`ssr_noted`.`type_noted`");

            $sum_noted_credit = $this->db->get('ssr_noted')->row();


            $this->db->limit(1);
            $this->db->select("'InvNo' as InvNo , 'RECEIPT_INVOICE' as RECEIPT_INVOICE, 'NotedNo' as NotedNo, 'Vessel Name' as vessel, 'Berth Date' as atb, 'payment' as payment , 'Cus' as Cus, 'Date Inv' as created , 'Date Receipt' as date_receipt, 'Amount' as Amount, 'Vat' as Vat, 'Total' as Total, 'With Holding Tax' as config_holdtax, 'Total With Holding' as WithHolding, 'Net Pay' as Netpay");

            $query1 = $this->db->get('ssr_order')->result_array();
   
            $data['outstand'] = array_merge($query2,$query_noted);


                    $total_csv_amount = 0;
                    $total_csv_vat = 0;
                    $total_csv_total = 0;
                    $total_csv_wh = 0;
                    $total_csv_np = 0;

            foreach ($sum_ssr_csv as $rs_csv) {

                    $total_csv_amount += $rs_csv['Amount'];
                    $total_csv_vat += $rs_csv['Vat'];
                    $total_csv_total += $rs_csv['Total'];
                    $total_csv_wh += $rs_csv['WithHolding'];
                    $total_csv_np += $rs_csv['NetPay'];

            }


            ////////CALCULATE SUM CSV/////////
                if($sum_noted_debit){
                    $chk_debit_Amount = $sum_noted_debit->Amount;
                } else {
                    $chk_debit_Amount = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Amount = $sum_noted_credit->Amount;
                } else {
                    $chk_credit_Amount = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_Vat = $sum_noted_debit->Vat;
                } else {
                    $chk_debit_Vat = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Vat = $sum_noted_credit->Vat;
                } else {
                    $chk_credit_Vat = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_Total = $sum_noted_debit->Total;
                } else {
                    $chk_debit_Total = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Total = $sum_noted_credit->Total;
                } else {
                    $chk_credit_Total = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_WithHolding = $sum_noted_debit->WithHolding;
                } else {
                    $chk_debit_WithHolding = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_WithHolding = $sum_noted_credit->WithHolding;
                } else {
                    $chk_credit_WithHolding = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_NetPay = $sum_noted_debit->NetPay;
                } else {
                    $chk_debit_NetPay = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_NetPay = $sum_noted_credit->NetPay;
                } else {
                    $chk_credit_NetPay = 0;
                }


                    $csv_amount =  $total_csv_amount+$chk_debit_Amount-$chk_credit_Amount;
                    $csv_vat = $total_csv_vat+$chk_debit_Vat-$chk_credit_Vat;
                    $csv_total = $total_csv_total+$chk_debit_Total-$chk_credit_Total;
                    $csv_holdtax = $total_csv_wh+$chk_debit_WithHolding-$chk_credit_WithHolding;
                    $csv_netpay = $total_csv_np+$chk_debit_NetPay-$chk_credit_NetPay;

            /////////////////////////////////
            $this->db->limit(1);
            $this->db->select("'' as InvNo , '' as RECEIPT_INVOICE, '' as NotedNo, '' as vessel, '' as atb, '' as payment , '' as Cus, '' as created , 'Total' as date_receipt, '".$csv_amount ."' as Amount, '".$csv_vat."' as Vat, '".$csv_total."' as Total, '' as config_holdtax, '".$csv_holdtax."' as WithHolding, '".$csv_netpay."' as Netpay");

            $revise_csv = $this->db->get('ssr_order')->result_array();


            ////////CALCULATE SUM CSV/////////

            //$csv = array_merge($query1,$query2,$query_noted,$revise_csv);
            $csv = array_merge($query1,$query2,$query_noted,$revise_csv);
           
            //echo '<pre>',print_r($csv,1),'</pre>'; die;

            if($f_ex == 'PDF'){
                $this->view['main'] =  $this->load->view('reports/reportOutStanding',$data,true);
                $this->view();
            } else {

                header("Content-type: application/csv");
                header("Content-Disposition: attachment; filename=\"SummaryOutstanding".$first_date.".csv\"");
                header("Pragma: no-cache");
                header("Expires: 0");

                $handle = fopen('php://output', 'w');

                foreach ($csv as $data) {
                    fputcsv($handle, $data);
                }
                    fclose($handle);
                exit;
            }
            
    }


    public function BillingNote($type = null){
        if($this->session->userdata('logged_in')) {     

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

            $query_size = $this->db->get_where('setting_bank', array('is_delete' => 0));
            $data['setting_bank'] = $query_size->result_array();

           
            $this->view['main'] =  $this->load->view('export/billing_note',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }

        public function ProcessNote(){

            $bank_type = $this->input->post('bank_type');
            $liner = $this->input->post('liner');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');
            $cheque_no = $this->input->post('cheque_no');
            $receipt_date = $this->input->post('receipt_date');

            $check_data = $this->session->userdata('logged_in');
            $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $bank_type= $this->db->get_where('setting_bank', array('sb_id' => $bank_type))->row();

            $data['cus'] = $this->db->get_where('customer', array('customer_code' => $liner))->row();

            $this->db->order_by("`ssr_order`.`dr_no`");
            $this->db->group_by("`ssr_order`.`dr_no`");

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            $this->db->where('ssr_order.payment', 'CREDIT');
            $this->db->where('ssr_order.type', 'INVOICE');
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->where('ssr_order.type !=', 'CANCEL');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->select('ssr_order.dr_no as InvNo, customer.customer_code as Cus, ssr_order.prefix_invoice, ssr_order.invoice_no , ssr_order.created, sum(ssr_order.qty*ssr_order.cur_rate) as Amount, sum((ssr_order.qty*ssr_order.cur_rate)*7/100) as Vat ,Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)) as Total, sum((ssr_order.qty*ssr_order.cur_rate)*3/100) as WithHolding, Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)-((ssr_order.qty*ssr_order.cur_rate)*3/100)) as NetPay');

            $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");

            $query_order = $this->db->get('ssr_order')->result_array();


            ///////////////////////////setReceipt////////////////////////////////////////////

            

            if($query_order){

                $this->db->where('status',0);
                $this->db->where('is_delete',0);
                $query_prefix= $this->db->get('prefix_ssr_receipt')->row();


                $this->db->limit(1);
                $this->db->where('invoice_no is NOT NULL', NULL, FALSE);
                $this->db->order_by("invoice_no", "desc");
                $last_invoice = $this->db->get('status_ssr_invoice')->row();


                if(empty($last_invoice->invoice_no)){
                    $rand_invoice = date('Y')."000001";
                } else {
                    $rand_invoice = $last_invoice->invoice_no+1;
                }

                 foreach ($query_order as $rs_order) {

                $this->db->where('dr_no', $rs_order['InvNo']);
                $this->db->order_by("invoice_no", "desc");
                $check_receipt = $this->db->get('status_ssr_invoice')->row();

                if(!$check_receipt){
                    $status_invoice = array(    
                        "dr_no" => $rs_order['InvNo'],
                        "prefix_invoice" => $query_prefix->prefix,
                        "invoice_no" => $rand_invoice,
                        "created" => date('Y-m-d H:i:s'),
                        "updated" => date('Y-m-d H:i:s')
                    );
                                        
                $this->db->insert('status_ssr_invoice', $status_invoice);

                    $data = array(
                        "prefix_invoice" => $query_prefix->prefix,
                        "invoice_no" => $rand_invoice,
                        "type" => 'RECEIPT',
                        "bank_note" => $bank_type->sb_id,
                        "date_note" => $receipt_date,
                        "billing_note" => $cheque_no,
                        "updated" => date('Y-m-d H:i:s')
                    );

                    $this->db->where('dr_no', $rs_order['InvNo']);
                    $this->db->update('ssr_order',$data);


                }


            }

                $data = array(
                            "username" => $username->username,
                            "invoice_no" => $query_prefix->prefix.$rand_invoice,
                            "created" => date('Y-m-d H:i:s'),
                            "updated" => date('Y-m-d H:i:s')
                        );
                        
                $this->db->insert('log_print_ssr_invoice', $data);
            }

           

            ///////////////////////////setReceipt////////////////////////////////////////////
           
            redirect('Export/SetReceiptSSR/'.$rand_invoice, 'refresh');
        
    }

    public function SetReceiptSSR($invoice_no = null){

        if($invoice_no){
            $this->db->order_by("`ssr_order`.`dr_no`");
            $this->db->group_by("`ssr_order`.`dr_no`");

            $this->db->where('ssr_order.invoice_no', $invoice_no);

            $this->db->where('ssr_order.payment', 'CREDIT');
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->where('ssr_order.type !=', 'CANCEL');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->select('ssr_order.dr_no as InvNo, customer.customer_code as Cus, ssr_order.prefix_invoice, ssr_order.invoice_no , ssr_order.created, sum(ssr_order.qty*ssr_order.cur_rate) as Amount, sum((ssr_order.qty*ssr_order.cur_rate)*7/100) as Vat ,Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)) as Total, sum((ssr_order.qty*ssr_order.cur_rate)*3/100) as WithHolding, Sum((ssr_order.qty*ssr_order.cur_rate)+((ssr_order.qty*ssr_order.cur_rate)*7/100)-((ssr_order.qty*ssr_order.cur_rate)*3/100)) as NetPay');

            $data['order'] = $this->db->get('ssr_order')->result_array();


            $this->db->order_by("`ssr_order`.`dr_no`");
            $this->db->group_by("`ssr_order`.`dr_no`");

            $this->db->where('ssr_order.invoice_no', $invoice_no);

            $this->db->where('ssr_order.payment', 'CREDIT');
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->where('ssr_order.type !=', 'CANCEL');
            $this->db->join('setting_bank','setting_bank.sb_id = ssr_order.bank_note','left');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->select('customer.customer_code, ssr_order.date_note, ssr_order.bank_note, setting_bank.sb_type, ssr_order.billing_note, ssr_order.prefix_invoice, ssr_order.invoice_no');

            $data['select_cus'] = $this->db->get('ssr_order')->row();

                    $data['cus'] = $this->db->get_where('customer', array('customer_code' => $data['select_cus'] ->customer_code))->row();

        
            $this->view['main'] =  $this->load->view('reports/receipt_note',$data,true);
            $this->view();
        } else {
            $this->view['main'] =  $this->load->view('reports/none','',true);
            $this->view();
        }
        
            

    }


    public function ListOrder(){

            set_time_limit(600);
            ini_set('memory_limit', '2048M');

            $type = $this->input->post('type');
            $type_inv = $this->input->post('type_inv');
            $liner = $this->input->post('liner');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');

            $check_data = $this->session->userdata('logged_in');
            $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $data['type_inv'] = $type_inv;
            $data['first_date'] = $first_date;
            $data['second_date'] = $second_date;

            $data['customer_code'] = $liner;
            $this->db->order_by("`ssr_order`.`dr_no`");
            $this->db->group_by("`ssr_order`.`ssr_id`");  

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_order.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("status_ssr_invoice.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_order.type !=', 'CANCEL');
                $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->where('ssr_order.dr_no is NOT NULL', NULL, FALSE);
            $this->db->join('tariff_type','tariff_type.tariff_id = ssr_order.tariff_id','left');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            //$this->db->join('status_ssr_invoice','status_ssr_invoice.dr_no = ssr_order.dr_no','left');
            $this->db->join('status_ssr_invoice','status_ssr_invoice.invoice_no = ssr_order.invoice_no','left');
            $this->db->select('`ssr_order`.`dr_no` as `InvNo`, CONCAT(ssr_order.prefix_invoice, ssr_order.invoice_no) AS RECEIPT_INVOICE, "" as `NotedNo`, `ssr_order`.`vessel`, (if(ssr_order.vessel <> "", CONCAT(ssr_order.voy_in, " / ", ssr_order.voy_out), "")) AS voy, (if(ssr_order.vessel <> "", `ssr_order`.`atb`, "")) AS atb, (if(ssr_order.vessel <> "", `ssr_order`.`atd`, "")) AS atd, `ssr_order`.`payment`, `customer`.`customer_code` as `Cus`, (if(ssr_order.type = "INVOICE", DATEDIFF(DATE_FORMAT("'.date('Y-m-d').'", "%Y-%m/%d"), ssr_order.created), "")) as AgeInvoice, `ssr_order`.`created`, `status_ssr_invoice`.`created` as `date_receipt`, `tariff_type`.`tariff_code`, `tariff_type`.`tariff_des`, `ssr_order`.`qty`, `tariff_type`.`unit_type`, FORMAT(`ssr_order`.`cur_rate`, 2) as cur_rate, FORMAT((ssr_order.qty*ssr_order.cur_rate), 2) as Amount, FORMAT((if(ssr_order.vat = "YES", (ssr_order.cal_vat), 0)), 2) as Vat, FORMAT((if(ssr_order.vat = "YES", ((ssr_order.qty*ssr_order.cur_rate)+(ssr_order.cal_vat)), ((ssr_order.qty*ssr_order.cur_rate)))), 2) as Total, `ssr_order`.`config_holdtax`, FORMAT((if(ssr_order.wh = "YES", (ssr_order.cal_wh), 0)), 2) as WithHolding, FORMAT((if(ssr_order.vat = "YES", ((ssr_order.qty*ssr_order.cur_rate)+(ssr_order.cal_vat)), ((ssr_order.qty*ssr_order.cur_rate)))) - (if(ssr_order.wh = "YES", (ssr_order.cal_wh), 0)), 2) as NetPay, `ssr_order`.`remark`, "" as `type_noted`');

            

            $query2 = $this->db->get('ssr_order')->result_array();


            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            $this->db->where('ssr_noted.status_noted !=', 'CANCEL');

            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->join('tariff_type','tariff_type.tariff_id = ssr_noted.tariff_id','left');
            $this->db->select('ssr_noted.dr_no as InvNo , CONCAT(ssr_noted.prefix_invoice, ssr_noted.invoice_no) AS RECEIPT_INVOICE , ssr_noted.noted_no as NotedNo , ssr_noted.vessel ,(if(ssr_noted.vessel <> "", CONCAT(ssr_noted.voy_in," / ", ssr_noted.voy_out),"")) AS voy ,(if(ssr_noted.vessel <> "", ssr_noted.atb,"")) AS atb ,(if(ssr_noted.vessel <> "", ssr_noted.atd,"")) AS atd , ssr_noted.payment , customer.customer_code as Cus, "" as AgeInvoice, ssr_noted.created , ssr_noted.created as date_receipt ,tariff_type.tariff_code ,tariff_type.tariff_des ,ssr_noted.qty ,tariff_type.unit_type ,ssr_noted.cur_rate , sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount, sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat, Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total, ssr_noted.config_holdtax, sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding, Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay, ssr_noted.type_noted');
             $this->db->group_by("`ssr_noted`.`noted_no`");

            

            $query_noted = $this->db->get('ssr_noted')->result_array();

            //echo '<pre>',print_r($query2,1),'</pre>'; die;


             /////////////////////////SSR//////////////////////////////////////////////////////////////////////



            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_order.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("status_ssr_invoice.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_order.type !=', 'CANCEL');
                $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->join('status_ssr_invoice','status_ssr_invoice.dr_no = ssr_order.dr_no','left');
            $this->db->select('sum(ssr_order.qty*ssr_order.cur_rate) as Amount, sum(ssr_order.cal_vat) as Vat, Sum((ssr_order.qty*ssr_order.cur_rate)+(ssr_order.cal_vat)) as Total, `ssr_order`.`config_holdtax`, sum(ssr_order.cal_wh) as WithHolding, Sum((ssr_order.qty*ssr_order.cur_rate)+(ssr_order.cal_vat)-(ssr_order.cal_wh)) as NetPay');

            $sum_ssr_csv = $this->db->get('ssr_order')->row();



            ////////////////////////////////////NOTED/////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_noted.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_noted.type !=', 'CANCEL');
                $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->where('ssr_noted.type_noted', 'DEBIT');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->select('ssr_noted.type_noted, sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat
                     ,Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total
                     , ssr_noted.config_holdtax
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding
                     , Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay');
            $this->db->group_by("`ssr_noted`.`type_noted`");

            $sum_noted_debit = $this->db->get('ssr_noted')->row();



            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_noted.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_noted.type', $type_inv);
                    $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_noted.type !=', 'CANCEL');
                $this->db->where("ssr_noted.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            $this->db->where('ssr_noted.status_noted', 'USE');
            $this->db->where('ssr_noted.type_noted', 'CREDIT');
            $this->db->where('ssr_noted.is_use', 'YES');
            $this->db->join('customer','ssr_noted.id_comp = customer.custom_id','left');
            $this->db->select('ssr_noted.type_noted, sum(ssr_noted.qty*ssr_noted.cur_rate) as Amount
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*7/100) as Vat
                     ,Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)) as Total
                     , ssr_noted.config_holdtax
                     , sum((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100) as WithHolding
                     , Sum((ssr_noted.qty*ssr_noted.cur_rate)+((ssr_noted.qty*ssr_noted.cur_rate)*7/100)-((ssr_noted.qty*ssr_noted.cur_rate)*ssr_noted.config_holdtax/100)) as NetPay');
            $this->db->group_by("`ssr_noted`.`type_noted`");

            $sum_noted_credit = $this->db->get('ssr_noted')->row();


            $this->db->limit(1);
            $this->db->select("'InvNo' as InvNo , 'RECEIPT_INVOICE' as RECEIPT_INVOICE, 'NotedNo' as NotedNo, 'Vessel Name' as vessel,'Voyage In/Out' as voy , 'ATB' as atb, 'ATD' as atd, 'payment' as payment , 'Cus' as Cus , 'AgeInvoice/Days' as AgeInvoice, 'Date Inv' as created , 'Date Receipt' as date_receipt, 'Tariff Code' as tariff_code, 'Description' as tariff_des, 'QTY' as qty, 'QTY/TYPE' as unit_type, 'Rate' as cur_rate, 'Amount' as Amount, 'Vat' as Vat, 'Total' as Total, 'With Holding Tax' as config_holdtax, 'Total With Holding' as WithHolding, 'Net Pay' as Netpay, 'Remark' as remark");

            $query1 = $this->db->get('ssr_order')->result_array();
   
            $data['outstand'] = array_merge($query2,$query_noted);




            ////////CALCULATE SUM CSV/////////
                if($sum_noted_debit){
                    $chk_debit_Amount = $sum_noted_debit->Amount;
                } else {
                    $chk_debit_Amount = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Amount = $sum_noted_credit->Amount;
                } else {
                    $chk_credit_Amount = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_Vat = $sum_noted_debit->Vat;
                } else {
                    $chk_debit_Vat = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Vat = $sum_noted_credit->Vat;
                } else {
                    $chk_credit_Vat = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_Total = $sum_noted_debit->Total;
                } else {
                    $chk_debit_Total = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_Total = $sum_noted_credit->Total;
                } else {
                    $chk_credit_Total = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_WithHolding = $sum_noted_debit->WithHolding;
                } else {
                    $chk_debit_WithHolding = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_WithHolding = $sum_noted_credit->WithHolding;
                } else {
                    $chk_credit_WithHolding = 0;
                }

                if($sum_noted_debit){
                    $chk_debit_NetPay = $sum_noted_debit->NetPay;
                } else {
                    $chk_debit_NetPay = 0;
                }

                if($sum_noted_credit){
                    $chk_credit_NetPay = $sum_noted_credit->NetPay;
                } else {
                    $chk_credit_NetPay = 0;
                }


                    $csv_amount =  $sum_ssr_csv->Amount+$chk_debit_Amount-$chk_credit_Amount;
                    $csv_vat = $sum_ssr_csv->Vat+$chk_debit_Vat-$chk_credit_Vat;
                    $csv_total = $sum_ssr_csv->Total+$chk_debit_Total-$chk_credit_Total;
                    $csv_holdtax = $sum_ssr_csv->WithHolding+$chk_debit_WithHolding-$chk_credit_WithHolding;
                    $csv_netpay = $sum_ssr_csv->NetPay+$chk_debit_NetPay-$chk_credit_NetPay;

            /////////////////////////////////
            $this->db->limit(1);
            $this->db->select("'' as InvNo , '' as RECEIPT_INVOICE, '' as NotedNo, '' as vessel, '' as voy, '' as atb, '' as atd, '' as payment , '' as Cus , '' as AgeInvoice, '' as created , 'Total' as date_receipt,'' as tariff_code, '' as tariff_des, '' as qty, '' as unit_type, '' as cur_rate, '".$csv_amount ."' as Amount, '".$csv_vat."' as Vat, '".$csv_total."' as Total, '' as config_holdtax, '".$csv_holdtax."' as WithHolding, '".$csv_netpay."' as Netpay, '' as remark");

            $revise_csv = $this->db->get('ssr_order')->result_array();


            ////////CALCULATE SUM CSV/////////

            $csv = array_merge($query1,$query2,$query_noted,$revise_csv);

           
            //echo '<pre>',print_r($csv,1),'</pre>'; die;


                header("Content-type: application/csv");
                header("Content-Disposition: attachment; filename=\"SummaryOutstanding".$first_date.".csv\"");
                header("Pragma: no-cache");
                header("Expires: 0");

                $handle = fopen('php://output', 'w');

                foreach ($csv as $data) {
                    fputcsv($handle, $data);
                }
                
                fclose($handle);
                exit;

    }

    public function TarrifOrder(){

            $tariff_type = $this->input->post('tariff_type');
            $type = $this->input->post('type');
            $type_inv = $this->input->post('type_inv');
            $liner = $this->input->post('liner');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');

            $check_data = $this->session->userdata('logged_in');
            $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $data['type_inv'] = $type_inv;
            $data['first_date'] = $first_date;
            $data['second_date'] = $second_date;

            $data['customer_code'] = $liner;
            

            $this->db->order_by("tariff_type.tariff_id","ASC");
            $this->db->group_by("tariff_type.tariff_id");  

            if($liner != '%'){
                $this->db->where('customer.customer_code', $liner);
            }

            if($type != 'ALL'){
               $this->db->where('ssr_order.payment', $type);
            }

            if($type_inv != 'ALL'){
                if($type_inv == 'INVOICE'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                } else if($type_inv == 'RECEIPT'){
                    $this->db->where('ssr_order.type', $type_inv);
                    $this->db->where("status_ssr_invoice.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
                }
               
            } else {
                $this->db->where('ssr_order.type !=', 'CANCEL');
                $this->db->where("ssr_order.created BETWEEN '".$first_date." 00:00"."' AND '".$second_date." 23:59"."'");
            }
            
            if($tariff_type != 'ALL'){
               $this->db->where('ssr_order.tariff_id', $tariff_type);
            }
            $this->db->where('ssr_order.is_use', 'YES');
            $this->db->join('tariff_type','tariff_type.tariff_id = ssr_order.tariff_id','left');
            $this->db->join('customer','ssr_order.id_comp = customer.custom_id','left');
            $this->db->join('status_ssr_invoice','status_ssr_invoice.dr_no = ssr_order.dr_no','left');
            $this->db->select('`tariff_type`.`tariff_code` , `tariff_type`.`tariff_des` ,sum(`ssr_order`.`qty`) qty, `tariff_type`.`unit_type` , `ssr_order`.`cur_rate` , sum(ssr_order.qty*ssr_order.cur_rate) as Amount');

            

            $query2 = $this->db->get('ssr_order')->result_array();


           //echo '<pre>',print_r($query2,1),'</pre>'; die;


             /////////////////////////SSR//////////////////////////////////////////////////////////////////////



            /////////////////////////////////
            $this->db->limit(1);
            $this->db->select("'Tariff Code' as tariff_code, 'Tariff Description' as tariff_des, 'QTY' as qty, 'Type' as unit_type, 'Rate' as cur_rate, 'Amount' as Amount");

            $revise_csv = $this->db->get('ssr_order')->result_array();


            ////////CALCULATE SUM CSV/////////

            $csv = array_merge($revise_csv, $query2);

           
            //echo '<pre>',print_r($csv,1),'</pre>'; die;


                header("Content-type: application/csv");
                header("Content-Disposition: attachment; filename=\"TariffOrder".$first_date."_to_".$second_date."_Customer_".$liner.".csv\"");
                header("Pragma: no-cache");
                header("Expires: 0");

                $handle = fopen('php://output', 'w');

                foreach ($csv as $data) {
                    fputcsv($handle, $data);
                }
                    fclose($handle);
                exit;

    }


    public function AllTariff($type = null){
        if($this->session->userdata('logged_in')) {     

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

            $query_size = $this->db->get_where('tariff_type', array('is_delete' => 'N'));
            $data['tariff_type'] = $query_size->result_array();

            $this->view['main'] =  $this->load->view('export/all_tarrif',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }
}