<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class MultiInvoice extends Welcome {

	public function index(){

		
	}

	public function PreviewInvoice($dr_no=null){

		if($this->session->userdata('logged_in')) { 	

			$data['dr_ref'] = $dr_no;

			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$bookref = $this->db->get('book_order')->row();

			$data['trm'] = $bookref->terminal_doc_ref_an;


			$date['trm'] = $bookref->terminal_doc_ref_an;

			$data['type'] = $this->db->get_where('setting_type', array('type' => $bookref->type))->row();

			$data['payer'] = $this->db->get_where('customer', array('id' => $bookref->id_comp))->row();

			$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ))->row();

			$data['order'] = $this->db->get_where('book_order', array('is_use' => 0 , 'dr_no' => $dr_no))->result_array();


			$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->view['main'] =  $this->load->view('billing/multiinv',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SetInvoiceNormal($dr_no=null , $none_hold = null){

		if($this->session->userdata('logged_in')) { 	


			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$bookref = $this->db->get('book_order')->row();

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_invoice')->row();

			/////////Gen Invoice Number////////////////////////////////////////////////////////
						if(empty($bookref->invoice_no)){

							$this->db->limit(1);
							$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
							$this->db->order_by("invoice_no", "desc");
							$last_invoice = $this->db->get('status_invoice')->row();


							if(empty($last_invoice->invoice_no)){
								$rand_invoice = date('Y')."000001";
							} else {
								$rand_invoice = $last_invoice->invoice_no+1;
							}

							$status_invoice = array(	
								"dr_no" => $dr_no,
								"prefix_invoice" => $query_prefix->prefix,
								"invoice_no" => $rand_invoice,
								"created" => date('Y-m-d H:i:s'),
								"updated" => date('Y-m-d H:i:s')
							);
										
							$this->db->insert('status_invoice', $status_invoice);

							$this->db->where('dr_no',$dr_no);
							$fetch_query = $this->db->get('book_order')->result_array();

							foreach ($fetch_query as $rs) {

								$data_invoice = array(
									"prefix_invoice" => $query_prefix->prefix,
									"invoice_no" => $rand_invoice,
									"is_use" => 2,
									"hold_tax" => $none_hold,
									"updated" => date('Y-m-d H:i:s')
								);

								$this->db->where('id_order', $rs['id']);
								$this->db->update('normal_order',$data_invoice);

								$data_order = array(
									"prefix_invoice" => $query_prefix->prefix,
									"invoice_no" => $rand_invoice,
									"is_use" => 2,
									"updated" => date('Y-m-d H:i:s')
								);

								$this->db->where('id', $rs['id']);
								$this->db->update('book_order',$data_order);

							}
						}

					/////////Gen Invoice Number////////////////////////////////////////////////////////

				redirect('MultiInvoice/PrintInvoice/'.$dr_no.'/'.$none_hold, 'refresh');

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function PrintInvoice($dr_no=null , $none_hold = null){

		if($this->session->userdata('logged_in')) {

			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$bookref = $this->db->get('book_order')->row();

			$check_print = $this->db->get_where('log_print_invoice', array('invoice_no' => $bookref->prefix_invoice.$bookref->invoice_no))->row();

			if($check_print){

					echo "Cannot Generat Invoice Again.<br>";
					echo "<a href='".site_url()."Billing/'>Back To Main</a>";
					return false;

			} else {

				///////////////////LogPrint/////////////////////////

				$check_data = $this->session->userdata('logged_in');		
				$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
									
					$data = array(
						"username" => $username->username,
						"invoice_no" => $bookref->prefix_invoice.$bookref->invoice_no,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
										
				$this->db->insert('log_print_invoice', $data);

				///////////////////LogPrint/////////////////////////

				$data['print_date'] = date('Y-m-d H:i:s');
				$data['type'] = $bookref->type;
				$data['invoice_ref'] = $bookref->prefix_invoice.$bookref->invoice_no;
				$data['bookan'] = $bookref->book_an;
				$data['trm'] = $bookref->terminal_doc_ref_an;
				$data['dr_no'] = $dr_no;
				$data['hold_status'] = $none_hold;

				$query_dr_t = $this->db->get_where('setting_type', array('type' => $bookref->type ,'is_delete' => 0 , 'status' => 0 ));
				$data['dr_t'] = $query_dr_t->row();

				$this->db->limit(1);
				$this->db->where('id',$bookref->id_comp);
				$data['payer'] = $this->db->get('customer')->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->db->where('dr_no',$bookref->dr_no);
				$data['order'] = $this->db->get('book_order')->result_array();

				$check_data = $this->session->userdata('logged_in');

				$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$this->view['main'] =  $this->load->view('billing/print_receipt_multi',$data,true);
				$this->view();

			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function CancelNormalInvoice(){

		$prefix_invoice = $this->input->post('prefix_invoice');
		$invoice_no = $this->input->post('invoice_no');
		$remark_inv = $this->input->post('remark_inv');


		$check_data = $this->session->userdata('logged_in');

		$date_user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$data_invoice = $this->db->get_where('status_invoice', array('invoice_no' => $invoice_no))->row();

		$cancelInvoice = array(	
						"invoice_no" => $prefix_invoice.$invoice_no,
						"dr_no" => $data_invoice->dr_no,
						"username" => $date_user->username,
						"remark_cancel" => "Billing | ".$remark_inv,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
		);

		$this->db->insert('log_cancel_invoice', $cancelInvoice);

		$data_log = array(
						"is_delete" => 1,
					    "updated" => date('Y-m-d H:i:s')
			          );
			         
		$this->db->where('invoice_no', $prefix_invoice.$invoice_no);
		$this->db->update('log_print_invoice',$data_log);

		$data_inv = array(
						"is_use" => 1,
					    "updated" => date('Y-m-d H:i:s')
			          );
		$this->db->where('prefix_invoice', $prefix_invoice);
		$this->db->where('invoice_no', $invoice_no);
		$this->db->update('status_invoice',$data_inv);

				$data = array(
	      				"remark_cancel" => "Billing | ".$remark_inv,
						"is_use" => 1,
						"updated" => date('Y-m-d H:i:s')
					);
			    $this->db->where('dr_no',  $data_invoice->dr_no);
				$this->db->update('normal_order',$data);

				$data_order = array(
						"remark_cancel" => "Billing | ".$remark_inv,
			          	"is_use" => 1,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
				$this->db->where('dr_no', $data_invoice->dr_no);
				$this->db->update('book_order',$data_order);

			$this->db->limit(1);
			$this->db->where('dr_no',$data_invoice->dr_no);
			$this->db->order_by("id", "ASC");
			$change_dr = $this->db->get('normal_order')->row();

			$datastatus_dr = array(

			          	"is_use" => 1,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('normal_id', $change_dr->id);
			$this->db->update('status_dr',$datastatus_dr);

			$current_fetch = $this->db->get_where('book_order', array('is_sub' => '1', 'dr_no' => $data_invoice->dr_no))->result_array();

			foreach ($current_fetch as $rs_origin) {

				$origin_order = $this->db->get_where('book_order',array('doc_ref_book' => $rs_origin['doc_ref_book'], 'is_sub' => 2))->row();

				$data_order = array(

			          	"book_con" => $origin_order->book_con + $rs_origin['book_con'],
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
				$this->db->where('doc_ref_book', $rs_origin['doc_ref_book']);
				$this->db->where('is_sub', 2);
				$this->db->update('book_order',$data_order);

			}

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

	}

}