<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Admin extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Billing');
    }

	public function Users(){
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');	
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			if($username->role == "ADMIN"){ 	

				//show vat list
				$query_vat = $this->db->get_where('user', array('is_delete' => 0));
				$data['user'] = $query_vat->result_array();

				$this->view['main'] =  $this->load->view('admin/user',$data,true);
				$this->view();
			} else {
				redirect('Start');
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveUsers(){

		$id = $this->input->post('id');
	 	$username = $this->input->post('username');
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$role = $this->input->post('role');


		if(empty($id)){

			$check_username = $this->db->get_where('user', array('username' => $username , 'is_delete' => 0));
			$check_name = $this->db->get_where('user', array('name' => $name , 'is_delete' => 0));

			if($check_username->num_rows() > 0){
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			} 

			if($check_name->num_rows() > 0){
				$result['msg'] = "200";
				echo json_encode($result);
				return false;
			} 

			if($password != $confirm_password){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}

			$data = array(
          			"username" => $username,
          			"name" => $name,
          			"password" => md5($password),
          			"role" => $role,
          			"is_change_pass" => 0,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);

			$this->db->insert('user', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;


		} else {

			if($password != $confirm_password){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}


			$data = array(

	          	"username" => $username,
          		"name" => $name,
          		"password" => md5($password),
          		"is_change_pass" => 0,
          		"role" => $role,
          		"updated" => date('Y-m-d H:i:s')
	          );
	         
	        $this->db->where('id', $id);
			$this->db->update('user',$data);

			$result['msg'] = "success";
			echo json_encode($result);
			return false;

		}

	}

	public function DeleteUsers(){
		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 1,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('user',$data);

		redirect('Admin\Users');
	}

	public function Permission(){
		
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');		
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				if($username->role == "ADMIN"){ 

					$check_data = $this->session->userdata('logged_in');
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$this->db->where('username !=',$username->username);
					$this->db->where('is_delete',0);
					$query_user = $this->db->get('user');
					$data['user']= $query_user->result_array();

					$this->view['main'] =  $this->load->view('admin/permission',$data,true);
					$this->view();

				} else {
					redirect('Start');
				}


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');	
		}
	}

	public function ConfigPermission(){

		$id = $this->input->post('id-per');
		$role = $this->input->post('role-per');
		
		$data = array(
	          	"role" => $role,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('user',$data);

		redirect('Admin\Permission');

	}

	public function invoice(){
	
		if($this->session->userdata('logged_in')) { 	

				$check_data = $this->session->userdata('logged_in');		
				$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$data['member'] = $this->db->get_where('user', array('is_delete' => 0 , 'role' => 'BILLING'))->result_array();

				if($username->role == "ADMIN" OR $username->role == "SUPBILLING"){ 

					$search = $this->input->post('search');

				        $config = array();
				        $config["base_url"] = site_url() . "Admin/invoice";
				        $config["total_rows"] = $this->M_Billing->CountStatusInv($search);

				        $config["per_page"] = 20;
				        $config["uri_segment"] = 3;
				        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
				        $config['full_tag_close'] = '</ul></div><!--pagination-->';
				        $config['first_link'] = false;
				        $config['last_link'] = false;
				        $config['first_tag_open'] = '<li>';
				        $config['first_tag_close'] = '</li>';
				        $config['prev_link'] = 'Previous';
				        $config['prev_tag_open'] = '<li class="prev">';
				        $config['prev_tag_close'] = '</li>';
				        $config['next_link'] = 'Next';
				        $config['next_tag_open'] = '<li>';
				        $config['next_tag_close'] = '</li>';
				        $config['last_tag_open'] = '<li>';
				        $config['last_tag_close'] = '</li>';
				        $config['cur_tag_open'] = '<li class="active"><a href="#">';
				        $config['cur_tag_close'] = '</a></li>';
				        $config['num_tag_open'] = '<li>';
				        $config['num_tag_close'] = '</li>';


				        $this->pagination->initialize($config);

				        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				        $data["results"] = $this->M_Billing->fetch_status_inv($config["per_page"], $page , $search);
				        $data["links"] = $this->pagination->create_links();

				        $check_data = $this->session->userdata('logged_in');

						$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

						$data['special'] = $user->reprint;

						$data['role'] = $user->role;
				       
				        $this->view['main'] =  $this->load->view('admin/admin_invoice',$data,true);
						$this->view();

				} else {
					redirect('Start');
				}

		} else {
				$this->load->helper(array('form'));
				$this->load->view('login_view');
		}
	}


	public function RePrintINV(){
		
		if($this->session->userdata('logged_in')) { 	

				$id_user = $this->input->post('id_user');

				$check_data = $this->session->userdata('logged_in');		
				$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				if($username->role == "ADMIN"){ 

					$check_permission = $this->db->get_where('user', array('id' => $id_user))->row();

					if($check_permission->reprint != 1){

						$data = array(
		          			"reprint" => 1,
		          			"updated" => date('Y-m-d H:i:s')
		          		);
		          		$this->db->where('id', $id_user);
						$this->db->update('user', $data);
					} else {
						$data = array(
		          			"reprint" => 0,
		          			"updated" => date('Y-m-d H:i:s')
		          		);
		          		$this->db->where('id', $id_user);
						$this->db->update('user', $data);
					}

					redirect('Admin/Permission');

				} else {
					redirect('Start');
				}

		} else {
				$this->load->helper(array('form'));
				$this->load->view('login_view');
		}
		
	}

}