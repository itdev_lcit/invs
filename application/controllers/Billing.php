<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Billing extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Billing');
    }


	public function index(){
		if($this->session->userdata('logged_in')) { 	

			$this->db->join('normal_order','normal_order.id = status_dr.normal_id');
			$this->db->join('book_order','book_order.id = status_dr.trm_doc');
			$this->db->where('normal_order.dr_no is NOT NULL', NULL, FALSE);

			$this->db->group_by('normal_order.dr_no');
			$this->db->order_by("normal_order.dr_no", "desc");
			$this->db->select('normal_order.dr_no,normal_order.is_reprint ,normal_order.code_comp, book_order.id as id_order, book_order.id_comp,normal_order.prefix_invoice, normal_order.created as Create_Dr , normal_order.updated as Updated_Dr, normal_order.invoice_no, book_order.terminal_doc_ref_id, book_order.book_an , book_order.size_con , book_order.type_con, book_order.remark_cancel , book_order.book_con , book_order.created as create_order , book_order.updated as update_order , status_dr.id, normal_order.is_use , status_dr.is_use as status_use , status_dr.created, status_dr.updated , normal_order.is_multi');
			$query_normal = $this->db->get('status_dr');
			$data['normal']= $query_normal->result_array();

			/*$this->db->join('customer','customer.id = split_order.id_comp');
			$this->db->join('status_dr','status_dr.normal_id = split_order.id');
			$this->db->where('status_dr.is_split',1);
			$this->db->order_by("split_order.dr_no", "desc");
			$this->db->select('split_order.dr_no ,customer.customer_code as code_comp , split_order.prefix_invoice , split_order.invoice_no , split_order.book_con ,split_order.is_reprint , split_order.size_con, split_order.type_con , split_order.id,split_order.is_use, split_order.created as create_order , split_order.updated as update_order , status_dr.created , status_dr.updated');
			$query_split = $this->db->get('split_order');
			$data['split']= $query_split->result_array();*/

			$check_data = $this->session->userdata('logged_in');

			$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;


			$this->view['main'] =  $this->load->view('billing/index',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function AllBill() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Billing/AllBill";
	        $config["total_rows"] = $this->M_Billing->CountDraft($search);

	        $config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Billing->fetch_draft($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

			$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
	       
	        $this->view['main'] =  $this->load->view('billing/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }


	public function SetInvoiceSplit($dr_no=null){

		if($this->session->userdata('logged_in')) { 	
			$this->db->where('dr_no',$dr_no);
			$query_split = $this->db->get('split_order');
			$data['split'] = $query_split->row();

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_invoice')->row();


			/////////Gen Invoice Number////////////////////////////////////////////////////////
				if(empty($data['split']->invoice_no)){

					$this->db->limit(1);
					$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
					$this->db->order_by("invoice_no", "desc");
					$last_invoice = $this->db->get('status_invoice')->row();



					if(empty($last_invoice->invoice_no)){
						$rand_invoice = date('Y')."000001";
					} else {
						$rand_invoice = $last_invoice->invoice_no+1;
					}

					$status_invoice = array(	
						"dr_no" => $dr_no,
						"prefix_invoice" => $query_prefix->prefix,
						"invoice_no" => $rand_invoice,
						"updated" => date('Y-m-d H:i:s')
					);
								
					$this->db->insert('status_invoice', $status_invoice);

					$data = array(
						"prefix_invoice" => $query_prefix->prefix,
						"invoice_no" => $rand_invoice,
						"is_use" => 2,
						"updated" => date('Y-m-d H:i:s')
					);

					$this->db->where('dr_no', $dr_no);
					$this->db->update('split_order',$data);
				}
			/////////Gen Invoice Number////////////////////////////////////////////////////////

			
			 redirect('Billing/PrintSplitInvoice/'.$rand_invoice, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	} 

	public function PrintSplitInvoice($invoice_no = null){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

			$check_print = $this->db->get_where('log_print_invoice', array('invoice_no' => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no))->row();
			
			if($check_print){
					echo "Cannot Generat Invoice Again.<br>";
					echo "<a href='".site_url()."Billing/AllBill'>Back To Main</a>";
					return false;
			} else {
				///////////////////LogPrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');		
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
							
						$data = array(
							"username" => $username->username,
							"invoice_no" => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
								
						$this->db->insert('log_print_invoice', $data);

					///////////////////LogPrint/////////////////////////


					$data['split_order'] = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

					$split_comp = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

					$data['head'] = $this->db->get_where('head_all_order', array('id_comp' => $split_comp->id_comp))->row();

					$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0))->row();

					$check_data = $this->session->userdata('logged_in');

					$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$this->view['main'] =  $this->load->view('billing/split_invoice',$data,true);
					$this->view();
			}
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

		
	}

	public function CancelSplitInvoice(){


		if($this->session->userdata('logged_in')) { 	

			$prefix_invoice = $this->input->post('prefix_invoice');
			$invoice_no = $this->input->post('invoice_no');

			$check_data = $this->session->userdata('logged_in');

			$date_user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data_invoice = $this->db->get_where('status_invoice', array('invoice_no' => $invoice_no))->row();

			$cancelInvoice = array(	
						"invoice_no" => $prefix_invoice.$invoice_no,
						"dr_no" => $data_invoice->dr_no,
						"username" => $date_user->username,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);

			$this->db->insert('log_cancel_invoice', $cancelInvoice);

			$this->db->where('invoice_no', $prefix_invoice.$invoice_no);
	      	$this->db->delete('log_print_invoice');

	      	$this->db->where('invoice_no', $invoice_no);
	      	$this->db->delete('status_invoice');

	      	$data = array(
						"prefix_invoice" => '',
						"invoice_no" => '',
						"is_use" => 0,
						"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('invoice_no', $invoice_no);
			$this->db->update('split_order',$data);

			$result['msg'] = "success";
			echo json_encode($result);
			return false;

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SetInvoiceNormal($dr_no=null , $id_order = null , $none_hold = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('dr_no',$dr_no);
			$query_split = $this->db->get('normal_order');
			$data['normal_order'] = $query_split->row();

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_invoice')->row();



					/////////Gen Invoice Number////////////////////////////////////////////////////////
						if(empty($data['normal_order']->invoice_no)){

							$this->db->limit(1);
							$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
							$this->db->order_by("invoice_no", "desc");
							$last_invoice = $this->db->get('status_invoice')->row();


							if(empty($last_invoice->invoice_no)){
								$rand_invoice = date('Y')."000001";
							} else {
								$rand_invoice = $last_invoice->invoice_no+1;
							}

							$status_invoice = array(	
								"dr_no" => $dr_no,
								"prefix_invoice" => $query_prefix->prefix,
								"invoice_no" => $rand_invoice,
								"created" => date('Y-m-d H:i:s'),
								"updated" => date('Y-m-d H:i:s')
							);
										
							$this->db->insert('status_invoice', $status_invoice);

							$data = array(
								"prefix_invoice" => $query_prefix->prefix,
								"invoice_no" => $rand_invoice,
								"is_use" => 2,
								"hold_tax" => $none_hold,
								"updated" => date('Y-m-d H:i:s')
							);

							$this->db->where('dr_no', $dr_no);
							$this->db->update('normal_order',$data);
						}
					/////////Gen Invoice Number////////////////////////////////////////////////////////

					 redirect('Billing/PrintNormalInvoice/'.$id_order.'/'.$dr_no.'/'.$none_hold, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	} 

	public function PrintNormalInvoice($id_order=null ,$dr_no=null , $none_hold = null){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('normal_order', array('dr_no' => $dr_no))->row();

			$check_print = $this->db->get_where('log_print_invoice', array('invoice_no' => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no))->row();

			if($check_print){
					echo "Cannot Generat Invoice Again.<br>";
					echo "<a href='".site_url()."Billing/AllBill'>Back To Main</a>";
					return false;
			} else {

					///////////////////LogPrint/////////////////////////

							$check_data = $this->session->userdata('logged_in');		
							$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
									
								$data = array(
									"username" => $username->username,
									"invoice_no" => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
										
								$this->db->insert('log_print_invoice', $data);

					///////////////////LogPrint/////////////////////////

				$pull_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();

					//$id_order = $pull_order->id;
				$trm_an = $pull_order->terminal_doc_ref_an;

				$data['with_hold'] = $this->db->get('setting_holdtax')->row();
				
				$data['bookan'] = $pull_order->book_an;

				$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
				$data['order'] = $query_order->result_array();

				$query_dr_t = $this->db->get_where('setting_type', array('type' => $pull_order->type ,'is_delete' => 0 , 'status' => 0 ));
				$data['dr_t'] = $query_dr_t->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$data['hold_status'] = $none_hold;

				$data['datePrint'] = $this->db->get_where('status_invoice', array('is_use' => 0 , 'dr_no' => $dr_no))->row();

				$check_data = $this->session->userdata('logged_in');

				$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$this->db->limit(1);
				$this->db->where('id_order',$id_order);
				$query = $this->db->get('normal_order');
				$data['head']= $query->result_array();

				$this->db->where('id_order',$id_order);
				$query_barcode = $this->db->get('normal_order');
				$gen_barcode = $query_barcode->row();

				$this->view['main'] =  $this->load->view('billing/normal_invoice',$data,true);
				$this->view();
			}
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function CancelNormalInvoice(){

		if($this->session->userdata('logged_in')) { 

			$prefix_invoice = $this->input->post('prefix_invoice');
			$invoice_no = $this->input->post('invoice_no');
			$remark_inv = $this->input->post('remark_inv');

			$check_data = $this->session->userdata('logged_in');

			$date_user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data_invoice = $this->db->get_where('status_invoice', array('invoice_no' => $invoice_no))->row();



			$cancelInvoice = array(	
						"invoice_no" => $prefix_invoice.$invoice_no,
						"dr_no" => $data_invoice->dr_no,
						"username" => $date_user->username,
						"remark_cancel" => "Billing | ".$remark_inv,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);

			$this->db->insert('log_cancel_invoice', $cancelInvoice);

			/*$this->db->where('invoice_no', $prefix_invoice.$invoice_no);
	      	$this->db->delete('log_print_invoice');*/

	      	$data_log = array(
						"is_delete" => 1,
					    "updated" => date('Y-m-d H:i:s')
			          );
			         
			$this->db->where('invoice_no', $prefix_invoice.$invoice_no);
			$this->db->update('log_print_invoice',$data_log);

			$data_inv = array(
						"is_use" => 1,
					    "updated" => date('Y-m-d H:i:s')
			          );
			$this->db->where('prefix_invoice', $prefix_invoice);
			$this->db->where('invoice_no', $invoice_no);
			$this->db->update('status_invoice',$data_inv);


	      	/*$this->db->where('invoice_no', $invoice_no);
	      	$this->db->delete('status_invoice');*/

	      	$data = array(
	      				"remark_cancel" => "Billing | ".$remark_inv,
						"is_use" => 0,
						"updated" => date('Y-m-d H:i:s')
			);
	      	$this->db->where('prefix_invoice', $prefix_invoice);
			$this->db->where('invoice_no', $invoice_no);
			$this->db->update('normal_order',$data);

			///////////////////////////Cancel///////////////////////////////////

			$pull_data_order = $this->db->get_where('normal_order',array('invoice_no' => $invoice_no, 'prefix_invoice' => $prefix_invoice))->row();


			$datastatus_dr = array(

			          	"is_use" => 1,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('id', $pull_data_order->id_order);
			$this->db->update('status_dr',$datastatus_dr);

			$change_status = $this->db->get_where('status_dr',array('normal_id' => $pull_data_order->id_order))->row();

			$data_status = array(

			          	"is_use" => 1,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			$this->db->where('prefix_invoice', $prefix_invoice);
			$this->db->where('invoice_no', $invoice_no);
			$this->db->update('normal_order',$data_status);

			$change_order = $this->db->get_where('book_order',array('id' => $pull_data_order->id_order))->row();



			$data_order = array(
						"remark_cancel" => "Billing | ".$remark_inv,
			          	"is_use" => 1,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('id', $change_order->id);
			$this->db->update('book_order',$data_order);

			$origin_order = $this->db->get_where('book_order',array('doc_ref_book' => $change_order->doc_ref_book, 'is_sub' => 2))->row();

			$data_order = array(

			          	"book_con" => $change_order->book_con + $origin_order->book_con,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('doc_ref_book', $change_order->doc_ref_book);
			$this->db->where('is_sub', 2);
			$this->db->update('book_order',$data_order);

			///////////////////////////Cancel///////////////////////////////////

			$result['msg'] = "success";
			echo json_encode($result);
			return false;

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function PreviewNormalInvoice($dr_no=null, $id_order=null ){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('normal_order', array('dr_no' => $dr_no))->row();

			$pull_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();

			//$id_order = $pull_order->id;
			$trm_an = $pull_order->terminal_doc_ref_an;

			$data['type'] = $this->db->get_where('setting_type', array('type' => $pull_order->type))->row();
			

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
			$data['order'] = $query_order->result_array();



			$data['trm'] = $id_order;

			$data['with_hold'] = $this->db->get('setting_holdtax')->row();

			$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ))->row();

			$data['rate'] = $this->db->get_where('setting_size_rate', array('is_delete' => 0 , 'size' => $pull_order->size_con ))->row();

			$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$query_wh = $this->db->get('setting_holdtax')->row();

			$data_order = array(
			          	"config_holdtax" => $query_wh->holdtax,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('dr_no', $dr_no);
			$this->db->update('normal_order',$data_order);

			$data_book_order = array(
			          	"config_holdtax" => $query_wh->holdtax,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('dr_no', $dr_no);
			$this->db->update('book_order',$data_book_order);

			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$query = $this->db->get('normal_order');
			$data['head']= $query->row();

			$this->db->where('dr_no',$dr_no);
			$query_barcode = $this->db->get('normal_order');
			$gen_barcode = $query_barcode->row();

			$this->view['main'] =  $this->load->view('billing/preview_invoice',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

			
	}

	public function RePreviewNormalInvoice($dr_no=null, $id_order=null ){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('normal_order', array('dr_no' => $dr_no))->row();

			$pull_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();

			//$id_order = $pull_order->id;
			$trm_an = $pull_order->terminal_doc_ref_an;
			
			$data['with_hold'] = $this->db->get('setting_holdtax')->row();

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
			$data['order'] = $query_order->result_array();

			$data['trm'] = $id_order;

			$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ))->row();

			$data['rate'] = $this->db->get_where('setting_size_rate', array('size' => $pull_order->size_con ,'is_delete' => 0 , 'status' => 0 ))->row();

			$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$datePrint = $this->db->get_where('status_invoice', array('is_use' => 0 , 'dr_no' => $dr_no))->row();

			$query_wh = $this->db->get('setting_holdtax')->row();

			if(strtotime($datePrint->created) <= strtotime("2020-03-31 23:59:59") and strtotime($datePrint->created) >= strtotime("2020-10-01 00:00:01")){

					 	$wh_c = '3.00';

			} else if (strtotime($datePrint->created) >= strtotime("2020-04-01 00:00:01") and strtotime($datePrint->created) <= strtotime("2020-09-30 23:59:59")){

						$wh_c = '1.50';

			} else {

						$wh_c =  $query_wh->holdtax;
			}

			$data_order = array(
			          	"config_holdtax" => $wh_c,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('dr_no', $dr_no);
			$this->db->update('normal_order',$data_order);

			$data_book_order = array(
			          	"config_holdtax" => $wh_c,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
			$this->db->where('dr_no', $dr_no);
			$this->db->update('book_order',$data_book_order);


			$this->db->limit(1);
			$this->db->where('id_order',$id_order);
			$query = $this->db->get('normal_order');
			$data['head']= $query->row();

			$this->db->where('id_order',$id_order);
			$query_barcode = $this->db->get('normal_order');
			$gen_barcode = $query_barcode->row();

			$this->view['main'] =  $this->load->view('billing/re_preview_invoice',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

			
	}

	public function PreviewSplitInvoice($dr_no = null){

		if($this->session->userdata('logged_in')) { 	
			$data['split_order'] = $this->db->get_where('split_order', array('dr_no' => $dr_no))->row();

			$split_comp = $this->db->get_where('split_order', array('dr_no' => $dr_no))->row();

			$data['head'] = $this->db->get_where('head_all_order', array('id_comp' => $split_comp->id_comp , 'terminal_doc_ref_an' => $split_comp->terminal_doc_ref_an))->row();

			$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0))->row();

			$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->view['main'] =  $this->load->view('billing/preview_normal_invoice',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function RePreviewSplitInvoice($dr_no = null){

		if($this->session->userdata('logged_in')) { 	
			$data['split_order'] = $this->db->get_where('split_order', array('dr_no' => $dr_no))->row();

			$split_comp = $this->db->get_where('split_order', array('dr_no' => $dr_no))->row();

			$data['head'] = $this->db->get_where('head_all_order', array('id_comp' => $split_comp->id_comp , 'terminal_doc_ref_an' => $split_comp->terminal_doc_ref_an))->row();

			$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0))->row();

			$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->view['main'] =  $this->load->view('billing/re_preview_normal_invoice',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

	public function ReSetInvoiceSplit($dr_no=null){

		if($this->session->userdata('logged_in')) { 	
			$this->db->where('dr_no',$dr_no);
			$query_split = $this->db->get('split_order');
			$split = $query_split->row();

			$data = array(
				"is_reprint" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('id', $split->id);
			$this->db->update('split_order', $data);
			
			redirect('Billing/RePrintSplitInvoice/'.$split->invoice_no, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	} 

	public function RePrintSplitInvoice($invoice_no = null){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

			$check_print = $this->db->get_where('log_re_print_invoice', array('invoice_no' => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no))->row();
			
			if($check_print){
					echo "Cannot Re Print Invoice Again.<br>";
					echo "<a href='".site_url()."Billing/'>Back To Main</a>";
					return false;
			} else {
					///////////////////LogRePrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');		
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
							
						$data = array(
							"username" => $username->username,
							"invoice_no" => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
								
						$this->db->insert('log_re_print_invoice', $data);

					///////////////////LogRePrint/////////////////////////


					$data['split_order'] = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

					$split_comp = $this->db->get_where('split_order', array('invoice_no' => $invoice_no))->row();

					$data['head'] = $this->db->get_where('head_all_order', array('id_comp' => $split_comp->id_comp))->row();

					$data['vat'] = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0))->row();

					$check_data = $this->session->userdata('logged_in');

					$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$this->view['main'] =  $this->load->view('billing/split_invoice',$data,true);
					$this->view();
			}
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function ReSetInvoiceNormal($dr_no=null , $id_order = null, $none_hold = null){

		if($this->session->userdata('logged_in')) { 	
			$this->db->where('dr_no',$dr_no);
			$query_split = $this->db->get('normal_order');
			$normalOrder = $query_split->row();

			$data = array(
				"hold_tax" => $none_hold,
				"is_reprint" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('id', $normalOrder->id);
			$this->db->update('normal_order', $data);

		
			redirect('Billing/RePrintNormalInvoice/'.$id_order.'/'.$dr_no.'/'.$none_hold, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	} 

	public function RePrintNormalInvoice($id_order=null, $dr_no=null, $none_hold = null){

		if($this->session->userdata('logged_in')) { 	
			$pull_data_split = $this->db->get_where('normal_order', array('id_order' => $id_order))->row();

			
			$check_print = $this->db->get_where('log_re_print_invoice', array('invoice_no' => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no))->row();

			$check_data = $this->session->userdata('logged_in');		
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			if ($username->role == 'SUPBILLING') {


					///////////////////LogRePrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');		
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
							
						$data = array(
							"username" => $username->username,
							"invoice_no" => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
								
						$this->db->insert('log_re_print_invoice', $data);

					///////////////////LogRePrint/////////////////////////

				$pull_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();

					//$id_order = $pull_order->id;
					$trm_an = $pull_order->terminal_doc_ref_an;
				

				$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
				$data['order'] = $query_order->result_array();

				$data['datePrint'] = $this->db->get_where('status_invoice', array('is_use' => 0 , 'dr_no' => $dr_no))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$query_dr_t = $this->db->get_where('setting_type', array('type' => $pull_order->type ,'is_delete' => 0 , 'status' => 0 ));
				$data['dr_t'] = $query_dr_t->row();

				$check_data = $this->session->userdata('logged_in');

				$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$data['hold_status'] = $none_hold;	

				$data['bookan'] = $pull_order->book_an;

				$this->db->limit(1);
				$this->db->where('id_order',$id_order);
				$query = $this->db->get('normal_order');
				$data['head']= $query->result_array();

				$this->db->where('id_order',$id_order);
				$query_barcode = $this->db->get('normal_order');
				$gen_barcode = $query_barcode->row();

				$this->view['main'] =  $this->load->view('billing/normal_invoice',$data,true);
				$this->view();

			} else if($check_print){

					echo "Cannot RePrint Invoice Again.<br>";
					echo "<a href='".site_url()."Billing/'>Back To Main</a>";
					return false;

			} else {
				///////////////////LogRePrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');		
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
							
						$data = array(
							"username" => $username->username,
							"invoice_no" => $pull_data_split->prefix_invoice.$pull_data_split->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
								
						$this->db->insert('log_re_print_invoice', $data);

					///////////////////LogRePrint/////////////////////////

				$pull_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();

					//$id_order = $pull_order->id;
					$trm_an = $pull_order->terminal_doc_ref_an;
				

				$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
				$data['order'] = $query_order->result_array();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();


				$query_dr_t = $this->db->get_where('setting_type', array('type' => $pull_order->type ,'is_delete' => 0 , 'status' => 0 ));
				$data['dr_t'] = $query_dr_t->row();

				$check_data = $this->session->userdata('logged_in');

				$data['datePrint'] = $this->db->get_where('status_invoice', array('is_use' => 0 , 'dr_no' => $dr_no))->row();

				$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$data['hold_status'] = $none_hold;	

				$data['bookan'] = $pull_order->book_an;

				$this->db->limit(1);
				$this->db->where('id_order',$id_order);
				$query = $this->db->get('normal_order');
				$data['head']= $query->result_array();

				$this->db->where('id_order',$id_order);
				$query_barcode = $this->db->get('normal_order');
				$gen_barcode = $query_barcode->row();

				$data['with_hold'] = $this->db->get('setting_holdtax')->row();

				$this->view['main'] =  $this->load->view('billing/normal_invoice',$data,true);
				$this->view();
			}
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

}