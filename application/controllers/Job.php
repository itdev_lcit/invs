<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Job extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Job');
        $this->load->model('M_Ssr');
    }

    private function set_barcode($code)
    {
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
       	$code = $code.".png";
	    $store_image = imagepng($file,FCPATH."public/img/barcode/".$code);

    }

   public function All() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

		if($sort != null OR $sort != ''){
			$this->session->unset_userdata('sess_sort');
			$sess_sort = array('sort' => $sort);
	  		$this->session->set_userdata('sess_sort', $sess_sort);
		}

	  	$check_sort = $this->session->userdata('sess_sort');

	  	if(empty($check_sort)){
			$check_sort = $sort;
		} else {
			$check_sort = $check_sort['sort'];
		}

      $check_data = $this->session->userdata('logged_in');

	        $config = array();
	        $config["base_url"] = site_url() . "Job/All";
	        $config["total_rows"] = $this->M_Job->CountDraft($search, $check_sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Job->CountDraft($search, $check_sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Job->FetchJob($config["per_page"], $page , $search , $check_sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $check_sort;
			$data['search'] = $search;
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$job_prefix = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_prefix'] = $job_prefix->result_array();

	       
	      $this->view['main'] =  $this->load->view('job/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

   public function JobApprove() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

		if($sort != null OR $sort != ''){
			$this->session->unset_userdata('sess_sort');
			$sess_sort = array('sort' => $sort);
	  		$this->session->set_userdata('sess_sort', $sess_sort);
		}

	  	$check_sort = $this->session->userdata('sess_sort');

	  	if(empty($check_sort)){
			$check_sort = $sort;
		} else {
			$check_sort = $check_sort['sort'];
		}

      $check_data = $this->session->userdata('logged_in');

	        $config = array();
	        $config["base_url"] = site_url() . "Job/JobApprove";
	        $config["total_rows"] = $this->M_Job->CountJobApprove($search, $check_sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Job->CountJobApprove($search, $check_sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Job->FetchJobApprove($config["per_page"], $page , $search , $check_sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $check_sort;
			$data['search'] = $search;
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$job_prefix = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_prefix'] = $job_prefix->result_array();

	       
	      $this->view['main'] =  $this->load->view('job/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
   }

   public function JobWait() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

		if($sort != null OR $sort != ''){
			$this->session->unset_userdata('sess_sort');
			$sess_sort = array('sort' => $sort);
	  		$this->session->set_userdata('sess_sort', $sess_sort);
		}

	  	$check_sort = $this->session->userdata('sess_sort');

	  	if(empty($check_sort)){
			$check_sort = $sort;
		} else {
			$check_sort = $check_sort['sort'];
		}

      $check_data = $this->session->userdata('logged_in');

	        $config = array();
	        $config["base_url"] = site_url() . "Job/JobWait";
	        $config["total_rows"] = $this->M_Job->CountJobWait($search, $check_sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Job->CountJobWait($search, $check_sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Job->FetchJobWait($config["per_page"], $page , $search , $check_sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $check_sort;
			$data['search'] = $search;
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$job_prefix = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_prefix'] = $job_prefix->result_array();

	       
	      $this->view['main'] =  $this->load->view('job/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }




   public function Orders() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

		if($sort != null OR $sort != ''){
			$this->session->unset_userdata('sess_sort');
			$sess_sort = array('sort' => $sort);
	  		$this->session->set_userdata('sess_sort', $sess_sort);
		}

	  	$check_sort = $this->session->userdata('sess_sort');

	  	if(empty($check_sort)){
			$check_sort = $sort;
		} else {
			$check_sort = $check_sort['sort'];
		}

      $check_data = $this->session->userdata('logged_in');

	        $config = array();
	        $config["base_url"] = site_url() . "Job/Orders";
	        $config["total_rows"] = $this->M_Job->CountFin($search, $check_sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Job->CountFin($search, $check_sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Job->FetchFin($config["per_page"], $page , $search , $check_sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $check_sort;
			$data['search'] = $search;
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$job_prefix = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_prefix'] = $job_prefix->result_array();

	       
	      $this->view['main'] =  $this->load->view('finjob/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function Create(){
    	if($this->session->userdata('logged_in')) { 	

    		$type = $this->input->post('sort');
    		$numOrder = $this->input->post('numOrder');
    		$visit_his = $this->input->post('visit_his');
    		$comp_his = $this->input->post('comp_his');
    		$id_comp_b = $this->input->post('id_comp_b');
    		$id_comp_f = $this->input->post('id_comp_f');


			$this->db->order_by("job_code", "asc");
			$data['job_type'] = $this->db->get('job_type')->result_array();

			if($numOrder == 0 or empty($numOrder)){
				$data['numOrder'] = 1;
			} else {
				$data['numOrder'] = $numOrder;
			}
			if(empty($comp_his)){
				$data['comp_his'] = '0';
			} else {
				$data['comp_his'] = $comp_his;
			}

			if(empty($visit_his)){
				$data['visit_his'] = '0';
			} else {
				$data['visit_his'] = $visit_his;
			}

			if(empty($id_comp_b)){
				$data['id_comp'] = '0';
			} else {
				$data['id_comp'] = $id_comp_b;
			}


			if(empty($id_comp_f)){
				$data['id_comp'] = '0';
			} else {
				$data['id_comp'] = $id_comp_f;
			}



			$data['type'] = $type;

			$query_customer = $this->db->get_where('customer');
			$data['customer'] = $query_customer->result_array();

			$job_cur_type = $this->db->get_where('job_cur_type');
			$data['job_cur_type'] = $job_cur_type->result_array();

			$this->view['main'] =  $this->load->view('job/create',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function GetTariff(){

    	$define_job = $this->input->post('define_job');

    	if($define_job){
    		$this->db->where('job_cat',$define_job);
    	}

		$tariff_type = $this->db->get('job_type')->result_array();

		echo json_encode($tariff_type);
		return false;
    }

     public function GetCur(){

    	$job_cur_type = $this->db->get_where('job_cur_type');
        $job_cur_type= $job_cur_type->result_array();

		echo json_encode($job_cur_type);
		return false;
    }

    public function getRate(){

		$id_type = $this->input->post('id_type');

		$query_type = $this->db->get_where('job_type', array('job_id' => $id_type));
		$rate = $query_type->row();

		if($rate){
			$result['rate'] = 0;
			$result['tariff_code'] = $rate->job_code;
			$result['tariff_des'] = $rate->job_desc;
			$result['job_cat'] = $rate->job_cat;	
		} else {
			$result['rate'] = 0;
			$result['tariff_des'] = '-';
			$result['tariff_code'] = '-';
			$result['job_cat'] = $rate->job_cat;	
		}

		
		echo json_encode($result);
		return false;
	}

	public function SaveJobOrder(){

	  		$type = $this->input->post('type');
	  		$qty = $this->input->post('qty');
	  		$customers_proj = $this->input->post('customers_proj');
	  		$bkg = $this->input->post('bkg');
	  		$dlv_from = $this->input->post('dlv_from');
	  		$dlv_to = $this->input->post('dlv_to');
	  		$vessel = $this->input->post('vessel');
	  		$voy_in = $this->input->post('voy_in');
	  		$payment = $this->input->post('payment');
	  		$rate_pro = $this->input->post('rate_pro');

	  		$th_rate = $this->input->post('th_rate');
	  		$usd_rate = $this->input->post('usd_rate');

	  		$proj_cur = $this->input->post('proj_cur');
	  		$roe = $this->input->post('roe');
	  		$c_rate_p = $this->input->post('c_rate_p');

	  		$vendor_nm = $this->input->post('vendor_nm');
	  		$vendor_qty = $this->input->post('vendor_qty');
            $vendor_rate = $this->input->post('vendor_rate');
            $vendor_cur = $this->input->post('vendor_cur');
            $vendor_remark1 = $this->input->post('vendor_remark1');
            $vendor_remark2 = $this->input->post('vendor_remark2');
            $vendor_remark3 = $this->input->post('vendor_remark3');

            $remark1 = $this->input->post('remark1');
            $remark2 = $this->input->post('remark2');
            $remark3 = $this->input->post('remark3');

            $job_date = strtotime($this->input->post('job_date'));


	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('job_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    	$i = 0;

	    	if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	} else {
	    		$remark = null;
	    	}

	    	if($vendor_remark1 != null and $vendor_remark2 != null and $vendor_remark3 != null){	

	    		$vendor_remark = $vendor_remark1."<br>".$vendor_remark2."<br>".$vendor_remark3;

	    	} else if($vendor_remark1 != null and $vendor_remark2 != null and $vendor_remark3 == null) {
	    		$vendor_remark = $vendor_remark1."<br>".$vendor_remark2;

	    	} else if($vendor_remark1 != null and $vendor_remark2 == null and $vendor_remark3 == null) {
	    		$vendor_remark = $vendor_remark1;
	    	} else {
	    		$vendor_remark = null;
	    	}	

	    	$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$real_name = $username->name;

	    			$data = array(	
					    "vendor_nm" => $vendor_nm,
	                    "vendor_qty" => $vendor_qty,
	                    "vendor_rate" => $vendor_rate,
	                    "vendor_cur" => $vendor_cur,
	                    "vendor_remark" => $vendor_remark,
	                    "created" => date('Y-m-d H:i:s'),
	                    "updated" => date('Y-m-d H:i:s')
					);
					$this->db->insert('job_vendor', $data);
					$insert_id = $this->db->insert_id();

	    	foreach ($type as $rsType) {

	    		if($rsType != null OR $rsType != '' OR !empty($rsType)){

					$this->db->where("job_id", $rsType);
					$Qtype = $this->db->get('job_type')->row();

		    		$data = array(	
					    "ref_id" => $refId,
	                    "customers_proj" => $customers_proj,
	                    "id_vendor" => $insert_id,
	                    "bkg" => $bkg,
	                    "dlv_from" => $dlv_from,
	                    "dlv_to" => $dlv_to,
	                    "job_cat" => $Qtype->job_cat,
	                    "job_id" => $Qtype->job_id,
	                    "vessel" => $vessel,
	                    "voy_in" => $voy_in,
	                    "qty" => $qty[$i],
	                    "cur_rate" => $rate_pro[$i],
	                    "currency" => $proj_cur[$i],
	                    "pricing" => $rate_pro[$i]*$qty[$i],
	                    "ex_rate" => $roe,
	                    "th_rate" => $th_rate[$i],
	                    "usd_rate" => $usd_rate[$i],
								"remark" => $remark,
								"job_date" => date('Y-m-d',$job_date),
								"users_created" => $real_name,
	                    "created" => date('Y-m-d H:i:s'),
	                    "updated" => date('Y-m-d H:i:s')
					);
						
					$this->db->insert('job_order', $data);

					$result['refId'] = $refId;
				}
				
	    	 $i++;}

	    	 		$this->db->where("ref_id", $result['refId']);
					$OrderType = $this->db->get('job_order')->row();

					$job_cat = $OrderType->job_cat;

					$this->db->order_by("id", "desc");
					$this->db->where("job_cat", $job_cat);
					$this->db->where("year_dr", date('Y'));
					$this->db->where("month_dr", date('m'));
					$runnumber = $this->db->get('status_job')->row();


					$this->db->where("jp_type", $job_cat);
					$prefix = $this->db->get('job_prefix')->row();

					$s_job = $prefix->jp_prefix;

					if(empty($runnumber)){

						$complete_dr = '001';
						$gen_dr = $s_job.date('Ym').'-001';

					} else {

						$rand_job = (int)$runnumber->job_no;
						$set_dr  = $rand_job + 1;

							if($set_dr <= 9){

								$complete_dr = "00".$set_dr;
								$gen_dr = $s_job.date('Ym')."-".$complete_dr;

							} else if($set_dr <= 99){

								$complete_dr = "0".$set_dr;
								$gen_dr = $s_job.date('Ym')."-".$complete_dr;

							} else {

								$complete_dr = $set_dr;
								$gen_dr = $s_job.date('Ym')."-".$complete_dr;
							}

					}


						$data_job_order = array(	
							"job_no" => $gen_dr,
							"updated" => date('Y-m-d H:i:s')
						);
							
						$this->db->where('ref_id', $result['refId']);
						$this->db->update('job_order', $data_job_order);

						$result['job_num'] = $gen_dr;

						$status_dr = array(
							"ref_id" => $result['refId'],
							"job_cat" => $job_cat,
							"prefix_dr" => $s_job,
							"year_dr" => date('Y'),
							"month_dr" => date('m'),
							"job_no" => $complete_dr,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
							);
															
						$this->db->insert('status_job', $status_dr);

	    	echo json_encode($result);
			return false;
   }

   public function UpdateJobOrder(){

   		$rm_order = $this->input->post('rm_order');

   		$job_count = $this->input->post('job_count');
   		$job_no = $this->input->post('job_no');
   		$order_id = $this->input->post('order_id');
	  		$type = $this->input->post('type');
	  		$qty = $this->input->post('qty');
	  		$customers_proj = $this->input->post('customers_proj');
	  		$bkg = $this->input->post('bkg');
	  		$dlv_from = $this->input->post('dlv_from');
	  		$dlv_to = $this->input->post('dlv_to');
	  		$vessel = $this->input->post('vessel');
	  		$voy_in = $this->input->post('voy_in');
	  		$payment = $this->input->post('payment');
	  		$rate_pro = $this->input->post('rate_pro');

	  		$th_rate = $this->input->post('th_rate');
	  		$usd_rate = $this->input->post('usd_rate');

	  		$proj_cur = $this->input->post('proj_cur');
	  		$roe = $this->input->post('roe');
	  		$c_rate_p = $this->input->post('c_rate_p');

	  		$vendor_nm = $this->input->post('vendor_nm');
	  		$vendor_qty = $this->input->post('vendor_qty');
         $vendor_rate = $this->input->post('vendor_rate');
         $vendor_cur = $this->input->post('vendor_cur');
         $vendor_remark1 = $this->input->post('vendor_remark1');
         $vendor_remark2 = $this->input->post('vendor_remark2');
         $vendor_remark3 = $this->input->post('vendor_remark3');

         $remark1 = $this->input->post('remark1');
         $remark2 = $this->input->post('remark2');
         $remark3 = $this->input->post('remark3');

         $job_date = strtotime($this->input->post('job_date'));

         if(!empty($rm_order) OR $rm_order != '' OR $rm_order != null){
         	$pieces = explode(", ", $rm_order);
         	$rm_id = preg_replace('/\s+/', '', $pieces);

         		$data_rm_order = array(	
					   "is_del" => 'Y',
	               "updated" => date('Y-m-d H:i:s')
					);
					$this->db->where_in('order_id', $rm_id);
					$this->db->update('job_order',$data_rm_order);

         }

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('job_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    	$i = 0;

	    	if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	} else {
	    		$remark = null;
	    	}

	    	if($vendor_remark1 != null and $vendor_remark2 != null and $vendor_remark3 != null){	

	    		$vendor_remark = $vendor_remark1."<br>".$vendor_remark2."<br>".$vendor_remark3;

	    	} else if($vendor_remark1 != null and $vendor_remark2 != null and $vendor_remark3 == null) {
	    		$vendor_remark = $vendor_remark1."<br>".$vendor_remark2;

	    	} else if($vendor_remark1 != null and $vendor_remark2 == null and $vendor_remark3 == null) {
	    		$vendor_remark = $vendor_remark1;
	    	} else {
	    		$vendor_remark = null;
	    	}	

	    	$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$fetch_job = $this->db->get_where('job_order', array('job_no' => $job_no))->row();

			$real_name = $username->name;

	    			$data_job_vendor = array(	
					   "vendor_nm" => $vendor_nm,
	               "vendor_qty" => $vendor_qty,
	               "vendor_rate" => $vendor_rate,
	               "vendor_cur" => $vendor_cur,
	               "vendor_remark" => $vendor_remark,
	               "updated" => date('Y-m-d H:i:s')
					);
					$this->db->where('vendor_id', $fetch_job->id_vendor);
					$this->db->update('job_vendor',$data_job_vendor);

	    	foreach ($type as $rsType) {

	    		if($order_id[$i] <> 'NA'){

	    			if($rsType != null OR $rsType != '' OR !empty($rsType)){

						$this->db->where("job_id", $rsType);
						$Qtype = $this->db->get('job_type')->row();

			    		$data_job_order = array(	
		               "customers_proj" => $customers_proj,
		               "id_vendor" => $fetch_job->id_vendor,
		               "bkg" => $bkg,
		               "dlv_from" => $dlv_from,
		               "dlv_to" => $dlv_to,
		               "job_cat" => $Qtype->job_cat,
		               "job_id" => $Qtype->job_id,
		               "vessel" => $vessel,
		               "voy_in" => $voy_in,
		               "qty" => $qty[$i],
		               "cur_rate" => $rate_pro[$i],
		               "currency" => $proj_cur[$i],
		               "pricing" => $rate_pro[$i]*$qty[$i],
		               "ex_rate" => $roe,
		               "th_rate" => $th_rate[$i],
		               "usd_rate" => $usd_rate[$i],
							"remark" => $remark,
							"job_date" => date('Y-m-d',$job_date),
							"users_created" => $real_name,
		               "updated" => date('Y-m-d H:i:s')
						);

						$this->db->where('order_id', $order_id[$i]);
						$this->db->update('job_order',$data_job_order);
					}

	    		} else {

	    			if($rsType != null OR $rsType != '' OR !empty($rsType)){
						$this->db->where("job_id", $rsType);
						$Qtype = $this->db->get('job_type')->row();

		    			$data_job_order = array(	
						   "ref_id" => $fetch_job->ref_id,
		               "customers_proj" => $customers_proj,
		               "id_vendor" => $fetch_job->id_vendor,
		               "job_no" => $fetch_job->job_no,
		               "bkg" => $bkg,
		               "dlv_from" => $dlv_from,
		               "dlv_to" => $dlv_to,
		               "job_cat" => $Qtype->job_cat,
		               "job_id" => $Qtype->job_id,
		               "vessel" => $vessel,
		               "voy_in" => $voy_in,
		               "qty" => $qty[$i],
		               "cur_rate" => $rate_pro[$i],
		               "currency" => $proj_cur[$i],
		               "pricing" => $rate_pro[$i]*$qty[$i],
		               "ex_rate" => $roe,
		               "th_rate" => $th_rate[$i],
		               "usd_rate" => $usd_rate[$i],
							"remark" => $remark,
							"job_date" => date('Y-m-d',$job_date),
							"users_created" => $real_name,
		               "created" => date('Y-m-d H:i:s'),
		               "updated" => date('Y-m-d H:i:s')
						);
							
						$this->db->insert('job_order', $data_job_order);

					}
	    		}
				
	    	 $i++;}

	    	 	$data_log = array(	
					   "ju_users" => $real_name,
	               "ju_no" => $job_no,
	               "ju_detail" => json_encode($data_job_order),
	               "created" => date('Y-m-d H:i:s')
				);

				$this->db->insert('job_update', $data_log);

	    	 $result['job_num'] = $job_no;

	    	echo json_encode($result);
			return false;
   }


   public function Generate($job_num = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('job_order', array('job_no' => $job_num, 'is_del' => 'N', 'type !=' => 'DRAFT'))->row();

    		$data['job_no'] = $orderCus->job_no;
    		$data['job_cat'] = $orderCus->job_cat;
    		$data['job_date'] = $orderCus->job_date;
    		$data['customers_proj'] = $orderCus->customers_proj;
    		$data['bkg'] = $orderCus->bkg;
    		$data['dlv_from'] = $orderCus->dlv_from;
    		$data['dlv_to'] = $orderCus->dlv_to;
    		$data['remark'] = $orderCus->remark;
    		$data['vessel'] = $orderCus->vessel;
    		$data['voy_in'] = $orderCus->voy_in;

    		$vendor_id = $orderCus->id_vendor;

			$this->db->order_by('job_order.order_id','ASC');
			$this->db->join(' job_type',' job_type.job_id = job_order.job_id');
			$this->db->where('job_order.job_no',$job_num);
			$this->db->where('job_order.type !=','DRAFT');
			$this->db->where('job_order.is_del','N');
			$this->db->select('job_order.*, job_type.job_code, job_type.job_desc, job_type.job_cat, job_type.job_unit');
			$data['order']  = $this->db->get('job_order')->result_array();

			$data['vendor'] = $this->db->get_where('job_vendor', array('vendor_id' => $vendor_id))->row();

			$this->view['main'] =  $this->load->view('job/rendor_page_job',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function Draft($job_num = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('job_order', array('job_no' => $job_num, 'is_del' => 'N'))->row();

    		if($orderCus->type <> 'CANCEL' AND $orderCus->type == 'DRAFT'){

    			$orderCus = $this->db->get_where('job_order', array('job_no' => $job_num, 'is_del' => 'N'))->row();

	    		$data['job_no'] = $orderCus->job_no;
	    		$data['job_cat'] = $orderCus->job_cat;
	    		$data['job_date'] = $orderCus->job_date;
	    		$data['customers_proj'] = $orderCus->customers_proj;
	    		$data['bkg'] = $orderCus->bkg;
	    		$data['dlv_from'] = $orderCus->dlv_from;
	    		$data['dlv_to'] = $orderCus->dlv_to;
	    		$data['remark'] = $orderCus->remark;
	    		$data['vessel'] = $orderCus->vessel;
	    		$data['voy_in'] = $orderCus->voy_in;

	    		$vendor_id = $orderCus->id_vendor;

				$this->db->order_by('job_order.order_id','ASC');
				$this->db->join('job_type',' job_type.job_id = job_order.job_id');
				$this->db->where('job_order.is_del','N');
				$this->db->where('job_order.type','DRAFT');
				$this->db->where('job_order.job_no',$job_num);
				$this->db->select('job_order.*, job_type.*');
				$data['order']  = $this->db->get('job_order')->result_array();

				$job_cur_type = $this->db->get_where('job_cur_type');
				$data['job_cur_type'] = $job_cur_type->result_array();

				$this->db->order_by("job_code", "asc");
				$this->db->where("job_cat",$orderCus->job_cat);
				$data['job_type'] = $this->db->get('job_type')->result_array();

				$data['vendor'] = $this->db->get_where('job_vendor', array('vendor_id' => $vendor_id))->row();

				$this->view['main'] =  $this->load->view('job/draft',$data,true);
				$this->view();

    		} else {
    			redirect('Job\All');
    		}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

   public function Detail($job_num = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('job_order', array('job_no' => $job_num, 'is_del' => 'N'))->row();

    		$data['type'] = $orderCus->type;
    		$data['job_no'] = $orderCus->job_no;
    		$data['job_cat'] = $orderCus->job_cat;
    		$data['job_date'] = $orderCus->job_date;
    		$data['customers_proj'] = $orderCus->customers_proj;
    		$data['bkg'] = $orderCus->bkg;
    		$data['dlv_from'] = $orderCus->dlv_from;
    		$data['dlv_to'] = $orderCus->dlv_to;
    		$data['remark'] = $orderCus->remark;
    		$data['vessel'] = $orderCus->vessel;
    		$data['voy_in'] = $orderCus->voy_in;
    		$data['sup_status'] = $orderCus->sup_status;
    		$data['mng_status'] = $orderCus->mng_status;
    		$data['sup_rmk_reject'] = $orderCus->sup_rmk_reject;
    		$data['mng_rmk_reject'] = $orderCus->mng_rmk_reject;

    		$data['remark_cancel'] = $orderCus->remark_cancel;
    		$data['users_cancel'] = $orderCus->users_cancel;

    		$check_data = $this->session->userdata('logged_in');
    		$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

    		$vendor_id = $orderCus->id_vendor;

			$this->db->order_by('job_order.order_id','ASC');
			$this->db->join('job_type',' job_type.job_id = job_order.job_id');
			$this->db->where('job_order.is_del','N');
			//$this->db->where('job_order.type !=','CANCEL');
			$this->db->where('job_order.job_no',$job_num);
			$this->db->select('job_order.*, job_type.*');
			$data['order']  = $this->db->get('job_order')->result_array();

			$job_cur_type = $this->db->get_where('job_cur_type');
			$data['job_cur_type'] = $job_cur_type->result_array();

			$this->db->order_by("job_code", "asc");
			$this->db->where("job_cat",$orderCus->job_cat);
			$data['job_type'] = $this->db->get('job_type')->result_array();

			$data['vendor'] = $this->db->get_where('job_vendor', array('vendor_id' => $vendor_id))->row();

			$this->view['main'] =  $this->load->view('job/detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function Cancel(){

    	$job_no_cancel = $this->input->post('job_no_cancel');
    	$remark_job = $this->input->post('remark_job');

    	$check_data = $this->session->userdata('logged_in');

		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$real_name = $username->name;

    			$data_status = array(
					"type" => 'CANCEL',
					"remark_cancel" => $remark_job,
					"users_cancel" => $real_name,
					"cancel_date" => date('Y-m-d H:i:s'),
		          	"updated" => date('Y-m-d H:i:s')
		        );
		         
			$this->db->where('job_no', $job_no_cancel);
			$this->db->update('job_order',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }


   public function ChangeJobCat(){

    	$jobid = $this->input->post('jobid');
    	$job_type = $this->input->post('job_type');

    	$check_data = $this->session->userdata('logged_in');

		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$real_name = $username->name;

			if($job_type == 'COMPLETE'){

    			$data_status = array(
					"type" => 'COMPLETE',
					"auth_completed" => $real_name,
					"auth_stamp" => date('Y-m-d H:i:s'),
		         "updated" => date('Y-m-d H:i:s')
		       );

			} else {

				$data_status = array(
					"type" => 'PROCESS',
					"sup_status" => 'WAIT',
					"mng_status" => 'WAIT',
					"users_process" => $real_name,
					"process_tm" => date('Y-m-d H:i:s'),
		         "updated" => date('Y-m-d H:i:s')
		       );

			}
		         
			$this->db->where('job_no', $jobid);
			$this->db->update('job_order',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
    		

    }

   public function ChangeSupJobCat(){

    	$jobid = $this->input->post('jobid');
    	$job_type = $this->input->post('job_type');
    	$comment_reject = $this->input->post('comment_reject');

    	$check_data = $this->session->userdata('logged_in');

		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$real_name = $username->name;

		if ($username->role == 'SUPOSL'){

			if($job_type == 'APPROVE'){

	    			$data_status = array(
						"sup_status" => 'APPROVE',
						"sup_auth" => $real_name,
						"sup_tm" => date('Y-m-d H:i:s'),
			         "updated" => date('Y-m-d H:i:s')
			       );

			} else if ($job_type == 'REJECT'){

					$data_status = array(
						"sup_status" => 'REJECT',
						"sup_auth" => $real_name,
						"sup_rmk_reject" => $comment_reject,
						"type" => 'DRAFT',
						"sup_tm" => date('Y-m-d H:i:s'),
			         "updated" => date('Y-m-d H:i:s')
			      );

			}
			         
			$this->db->where('job_no', $jobid);
			$this->db->update('job_order',$data_status);

			$result['msg'] = "success";
			echo json_encode($result);

		return false;

		} else {
			$result['msg'] = "Permission Denied";
			echo json_encode($result);
		}
    		

   }

   public function ChangeMngJobCat(){

    	$jobid = $this->input->post('jobid');
    	$job_type = $this->input->post('job_type');
    	$comment_reject = $this->input->post('comment_reject');

    	$check_data = $this->session->userdata('logged_in');

		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$real_name = $username->name;

		if ($username->role == 'MNGOSL'){

			if($job_type == 'APPROVE'){

    			$data_status = array(
					"mng_status" => 'APPROVE',
					"mng_auth" => $real_name,
					"type" => 'COMPLETE',
					"mng_tm" => date('Y-m-d H:i:s'),
		         "updated" => date('Y-m-d H:i:s')
		       );

			} else if ($job_type == 'REJECT'){

				$data_status = array(
					"mng_status" => 'REJECT',
					"mng_auth" => $real_name,
					"mng_rmk_reject" => $comment_reject,
					"type" => 'DRAFT',
					"sup_tm" => date('Y-m-d H:i:s'),
		         "updated" => date('Y-m-d H:i:s')
		      );

			}
		         
			$this->db->where('job_no', $jobid);
			$this->db->update('job_order',$data_status);

			$result['msg'] = "success";
			echo json_encode($result);
			return false;
			
		}  else {
			$result['msg'] = "Permission Denied";
			echo json_encode($result);
		}
    		

   }


	public function JobType(){
		
		if($this->session->userdata('logged_in')) { 	


			$query_size = $this->db->get_where('job_type', array('is_delete' => 'N'));
			$data['job_type'] = $query_size->result_array();

			$job_prefix = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_prefix'] = $job_prefix->result_array();

			$this->view['main'] =  $this->load->view('job/setting_job',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}


	public function JobPreFix(){
		
		if($this->session->userdata('logged_in')) { 	


			$query_size = $this->db->get_where('job_prefix', array('is_delete' => 'N'));
			$data['job_type'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('job/setting_prefix',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}


    public function SaveTypeJob(){

		$job_id = $this->input->post('job_id');
		$job_code = $this->input->post('job_code');
		$job_desc = $this->input->post('job_desc');
		$job_cat = $this->input->post('job_cat');
		$job_unit = $this->input->post('job_unit'); 

		if(empty($job_cat)){
			$result['msg'] = "400";
			echo json_encode($result);
			return false;
		}

		if(empty($job_unit)){
			$result['msg'] = "500";
			echo json_encode($result);
			return false;
		}

		if(empty($job_id)){

			if(!empty($job_code)) {

				if(empty($job_desc)){
					$result['msg'] = "300";
					echo json_encode($result);
					return false;
				}

				$check_type = $this->db->get_where('job_type', array('job_code' => $job_code , 'is_delete' => 'N'));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {

					$data_type = array(
	          			"job_code" => $job_code,
	          			"job_desc" => $job_desc,
	          			"job_cat" => $job_cat,
	          			"job_unit" => $job_unit,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('job_type', $data_type);
					$id_type = $this->db->insert_id();


					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}

		} else {

			if(empty($job_desc)){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}


			if(!empty($job_code)){

				$data = array(
	          		"job_code" => $job_code,
	          		"job_desc" => $job_desc,
	          		"job_cat" => $job_cat,
	          		"job_unit" => $job_unit,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('job_id', $job_id);
				$this->db->update('job_type',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}
		}
	}

	public function DeleteTypeJob(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 'Y',
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('job_id', $id);
		$this->db->update('job_type',$data);

		redirect('Job\JobType');
	}

	public function Reports($type = null){
        if($this->session->userdata('logged_in')) {     

        	$this->db->group_by('job_cat');
			$this->db->select('job_cat');
			$data['job_type']  = $this->db->get('job_order')->result_array();

            $startd = date('d') - 1;

            $data['first_date'] =  date('Y-m-').$startd;

            $data['second_date'] =  date('Y-m-d');

            
            $this->view['main'] =  $this->load->view('job/reports',$data,true);
            $this->view();

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }

    public function Export(){
        if($this->session->userdata('logged_in')) {     

        	$first_date = $this->input->post('first_date');
         $second_date = $this->input->post('second_date');
         $dlv_from = $this->input->post('dlv_from');
         $dlv_to = $this->input->post('dlv_to');
         $type = $this->input->post('type');
         $job_cat = $this->input->post('job_cat');
         $customers_proj = $this->input->post('customers_proj');

        	$this->db->order_by('job_order.job_no','asc');

        	if($customers_proj ){
        		if($customers_proj <> '%'){
        			$this->db->like('job_order.customers_proj', $customers_proj);
        		}
        	}

        	if($dlv_from <> '%' ){
        			$this->db->like('job_order.dlv_from', $dlv_from);
        	}

        	if($dlv_to <> '%' ){
        			$this->db->like('job_order.dlv_to', $dlv_to);
        	}

        	if($type  <> 'ALL'){
        		$this->db->like('job_order.type', $type);
        	}

        	if($job_cat  <> 'ALL'){
        		$this->db->like('job_order.job_cat', $job_cat);
        	}

        	$this->db->where("job_order.job_date BETWEEN '".$first_date."' AND '".$second_date."'");
        	$this->db->where('job_order.is_del', 'N');
        	$this->db->join('job_vendor','job_vendor.vendor_id = job_order.id_vendor');
        	$this->db->join('job_type','job_order.job_id = job_type.job_id');
        	$this->db->select('`job_order`.`job_no`, `job_order`.`ssr_no`, `job_order`.`res_no`, `job_order`.`type`, `job_order`.`customers_proj`, `job_order`.`job_cat`,`job_order`.bkg, `job_order`.`dlv_from`, `job_order`.`dlv_to`, `job_order`.`vessel`, `job_order`.`voy_in`, job_type.job_code, job_type.job_desc, `job_order`.`qty`, job_type.job_unit, `job_order`.`cur_rate`, `job_order`.`ex_rate`, `job_order`.`th_rate`, `job_order`.`usd_rate` , `job_order`.`currency`, `job_order`.`job_date`, `job_order`.`remark`, `job_order`.`users_created`, `job_order`.`created`, `job_order`.`users_process`, `job_order`.`process_tm`, `job_order`.`auth_completed`, `job_order`.`auth_stamp` , `job_order`.`sup_auth`, `job_order`.`sup_tm`, `job_order`.`mng_auth`, `job_order`.`mng_tm`, job_vendor.vendor_nm, job_vendor.vendor_qty, job_vendor.vendor_rate, job_vendor.vendor_cur, job_vendor.vendor_remark');
        	$query1 = $this->db->get('job_order')->result_array();

         $this->db->limit(1);
         $this->db->select("'Job No' as job_no, 'Invoice' as ssr_no, 'Receipt' as res_no  , 'Job Type' as type, 'Customers / Project' as customers_proj, 'Category' as job_cat, 'BKG' as bkg, 'DLV FROM' as dlv_from , 'DLV TO' as dlv_to, 'Vessel' as vessel , 'VOY IN' as voy_in, 'Job Code' as job_code, 'Job Desc' as job_desc, 'Qty' as qty, 'Unit' as job_unit, 'Rate' as cur_rate, 'ROE' as ex_rate, 'THB' as th_rate, 'USD' as usd_rate, 'Currency' as currency, 'Job Date' as job_date, 'Job Reamrk' as remark, 'Users Created' as users_created, 'Created TM' as created, 'Uers Process' as users_process, 'Process TM' as process_tm, 'Users Complete' as auth_completed, 'Complete TM' as auth_stamp , 'Supervisor' as sup_auth , 'Supervisor Approve' as sup_tm, 'Manager' as mng_auth, 'Manager Approve' as mng_tm  , 'Vendor Name' as vendor_nm, 'Vendor Qty' as vendor_qty, 'Vendor Rate' as vendor_rate, 'Vendor Currency' as vendor_cur, 'Vendor Remark' as vendor_remark");

         $query2 = $this->db->get('job_order')->result_array();
   		
   		$csv = array_merge($query2, $query1);

        	
        	header("Content-type: application/csv");
         header("Content-Disposition: attachment; filename=\"JobOrderReports".$first_date."_".$second_date.".csv\"");
         ob_end_clean();
         header("Pragma: no-cache");
         header("Expires: 0");

            $handle = fopen('php://output', 'w');

            foreach ($csv as $data) {
                fputcsv($handle, $data);
            }
                fclose($handle);
            exit;

        } else {
            $this->load->helper(array('form'));
            $this->load->view('login_view');
        }
    }


     public function SavePrefix(){

		$jp_id = $this->input->post('jp_id');
		$jp_type = $this->input->post('jp_type');
		$jp_prefix = $this->input->post('jp_prefix');


		if(empty($jp_id)){

			if(!empty($jp_type)) {

				if(empty($jp_prefix)){
					$result['msg'] = "300";
					echo json_encode($result);
					return false;
				}

				$check_type = $this->db->get_where('job_prefix', array('jp_type' => $jp_type , 'is_delete' => 'N'));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {

					$data_type = array(
	          			"jp_type" => $jp_type,
	          			"jp_prefix" => $jp_prefix,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('job_prefix', $data_type);
					$id_type = $this->db->insert_id();


					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}

		} else {

			if(empty($jp_prefix)){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}


			if(!empty($jp_type)){

				$data = array(
	          		"jp_type" => $jp_type,
	          		"jp_prefix" => $jp_prefix,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('jp_id', $jp_id);
				$this->db->update('job_prefix',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}
		}
	}


	public function DeletePrefix(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 'Y',
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('jp_id', $id);
		$this->db->update('job_prefix',$data);

		redirect('Job\JobPreFix');
	}

	public function UpdateFin(){

   		$ssr_no = $this->input->post('ssr_no');
   		$res_no = $this->input->post('res_no');
   		$order_id = $this->input->post('order_id');
   		$job_no = $this->input->post('job_no');

	    	$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$real_name = $username->name;

			$i = 0;

	    	foreach ($order_id as $rsorder_id) {

	    		if($ssr_no[$i] == null OR $ssr_no[$i] == ''){
		    		$c_ssr_no = null;
				} else {
					$c_ssr_no = $ssr_no[$i];
				}

				if($res_no[$i] == null OR $res_no[$i] == ''){
		    		$c_res_no = null;
				} else {
					$c_res_no = $res_no[$i];
				}

				$data_job_order = array(	
			      "ssr_no" => $c_ssr_no,
			      "res_no" => $c_res_no,
			      "updated" => date('Y-m-d H:i:s')
				);

				$this->db->where('order_id', $order_id[$i]);
				$this->db->update('job_order',$data_job_order);

				$data_log = array(	
					   "fj_users" => $real_name,
	               "fj_no" => $job_no,
	               "fj_detail" => json_encode($data_job_order),
	               "created" => date('Y-m-d H:i:s')
				);

				$this->db->insert('fin_job_update', $data_log);
				
	    	 $i++;}

	    	 	$this->db->order_by("job_order.job_no", "desc");
				$this->db->group_by('job_order.job_no');
	    	 	$this->db->where('job_order.job_no',$job_no);
	    	 	$this->db->where('job_order.is_del','N');
	    		$this->db->select('job_order.fin_status, job_order.ssr_no, job_order.res_no, job_order.job_cat, job_order.job_no, job_order.type, job_order.job_date, job_order.updated, job_order.created, job_order.customers_proj, job_order.remark_cancel, job_order.auth_completed, job_order.sup_auth, job_order.mng_auth, job_order.mng_tm, job_order.mng_status, job_order.sup_status, job_order.sup_rmk_reject, job_order.mng_rmk_reject,count(`job_order`.`job_no`) as c_job, count(`job_order`.`ssr_no`) as s_job');
	    		$chk_fin = $this->db->get('job_order')->row();


	    		if($chk_fin->c_job == $chk_fin->s_job){
	    			$up_fin = array(	
				      "fin_status" => 'INVOICE',
				      "updated" => date('Y-m-d H:i:s')
					);
					$this->db->where('job_no', $job_no);
					$this->db->update('job_order',$up_fin);
			
	    		} else {
	    			$up_fin = array(	
				      "fin_status" => 'NONE',
				      "updated" => date('Y-m-d H:i:s')
					);
					$this->db->where('job_no', $job_no);
					$this->db->update('job_order',$up_fin);
					
	    		}

	    	 $result['job_num'] = $job_no;

	    	echo json_encode($result);
			return false;
   }

}