<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class RemainOrder extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Remain');
    }

	public function index(){

		if($this->session->userdata('logged_in')) { 	


			$data['order_remain'] = $this->db->get_where('book_order', array('is_sub' => '2', 'book_con !=' => '0'))->result_array();


			$this->view['main'] =  $this->load->view('remain/index',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function RemainByBook($book_an = null) {

		if($this->session->userdata('logged_in')) { 	

			$text = str_replace('-', '/', $book_an);


			$data['order_remain'] = $this->db->get_where('book_order', array('book_an' => $text, 'is_sub' => '2', 'book_con !=' => '0'))->result_array();




			$this->view['main'] =  $this->load->view('remain/remain',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}


	public function AllRemain() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
	 	$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "RemainOrder/AllRemain";
	        $config["total_rows"] = $this->M_Remain->CountRemain($search);

	       	$config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Remain->fetch_remain($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	       
	        $this->view['main'] =  $this->load->view('remain/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }


}