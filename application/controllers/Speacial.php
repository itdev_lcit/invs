<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Speacial extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Ssr');
    }

    private function set_barcode($code)
    {
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
       	$code = $code.".png";
	    $store_image = imagepng($file,FCPATH."public/img/barcode/".$code);

    }

    public function SSR() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Speacial/SSR";
	        $config["total_rows"] = $this->M_Ssr->CountDraft($search, $sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Ssr->CountDraft($search, $sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Ssr->CountSsr($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $sort;
			$data['search'] = $search;

	       
	        $this->view['main'] =  $this->load->view('ssr/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function Preview($dr_no = null){

    	if($this->session->userdata('logged_in')) { 	

    		$orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no , 'is_use' => 'YES'))->row();

    		$data['dr_no'] = $dr_no;

    		$data['v_data'] = $orderCus;

			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.dr_no',$dr_no);
			$data['order']  = $this->db->get('ssr_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$query_wh = $this->db->get('setting_holdtax')->row();

			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.dr_no',$dr_no);
			$this->db->select('Sum(cal_vat) as all_vat, Sum(cal_wh) as all_wh');
			$data['Tax']  = $this->db->get('ssr_order')->row();


			if(strtotime($orderCus->created) <= strtotime("2020-03-31 23:59:59") OR strtotime($orderCus->created) >= strtotime("2020-09-30 23:59:59")){
				 	$wh_c = '3.00';

			} else {
					$wh_c =  $query_wh->holdtax;
			}


			$data['with_hold'] = $wh_c;

			$data_book = array(	
				"config_holdtax_inv" => $wh_c,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('dr_no', $dr_no);
			$this->db->update('ssr_order', $data_book);

			$this->view['main'] =  $this->load->view('ssr/preview',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

     public function RePreview($dr_no = null){

    	if($this->session->userdata('logged_in')) { 	

    		$check_data = $this->session->userdata('logged_in');
    		$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();



    		if($user->reprint == '1' OR $user->role == 'SUPBILLING'){
    			$orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no ))->row();

	    		$data['dr_no'] = $dr_no;

	    		$data['v_data'] = $orderCus;

				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('ssr_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('ssr_order')->result_array();

				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$date_receipt = $this->db->get_where('status_ssr_invoice', array('dr_no' => $orderCus->dr_no))->row();

				$query_wh = $this->db->get('setting_holdtax')->row();

				if($orderCus->type != 'CANCEL'){
					if(strtotime($date_receipt->created) <= strtotime("2020-03-31 23:59:59") and strtotime($date_receipt->created) >= strtotime("2020-10-01 00:00:01")){

						 	$wh_c = '3.00';

					} else if (strtotime($date_receipt->created) >= strtotime("2020-04-01 00:00:01") and strtotime($date_receipt->created) <= strtotime("2020-09-30 23:59:59")){

							$wh_c = '1.50';

					} else {

							$wh_c =  $query_wh->holdtax;
					}


						$data_book = array(	
							"config_holdtax_inv" => $wh_c,
							"updated" => date('Y-m-d H:i:s')
						);
										
						$this->db->where('dr_no', $dr_no);
						$this->db->update('ssr_order', $data_book);
				} else {
					$wh_c =  $query_wh->holdtax;
					$data['remark_cancel'] = $orderCus->remark_cancel;
				}

				$data['with_hold'] =  $wh_c;

				$Ckcan = $this->db->get_where('ssr_order', array('dr_no' => $dr_no ))->row();

				$data['type_can'] =  $Ckcan->type;


				$this->view['main'] =  $this->load->view('ssr/re-preview',$data,true);
				$this->view();

    		} else {
    			redirect('Inv/All/', 'refresh');
    		}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    /*public function index(){
    	if($this->session->userdata('logged_in')) { 	


			$this->view['main'] =  $this->load->view('ssr/index','',true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }*/

    public function Create(){
    	if($this->session->userdata('logged_in')) { 	

    		$type = $this->input->post('sort');
    		$numOrder = $this->input->post('numOrder');
    		$visit_his = $this->input->post('visit_his');
    		$comp_his = $this->input->post('comp_his');
    		$id_comp_b = $this->input->post('id_comp_b');
    		$id_comp_f = $this->input->post('id_comp_f');

			$query_size = $this->db->get_where('tariff_type', array('is_delete' => 'N'));
			$data['tariff_type'] = $query_size->result_array();

			$this->db->order_by("tariff_code", "asc");
			$this->db->where("is_delete", "N");
			$data['tariff_type'] = $this->db->get('tariff_type')->result_array();

			if($numOrder == 0 or empty($numOrder)){
				$data['numOrder'] = 1;
			} else {
				$data['numOrder'] = $numOrder;
			}

			if(empty($comp_his)){
				$data['comp_his'] = '0';
			} else {
				$data['comp_his'] = $comp_his;
			}

			if(empty($visit_his)){
				$data['visit_his'] = '0';
			} else {
				$data['visit_his'] = $visit_his;
			}

			if(empty($id_comp_b)){
				$data['id_comp'] = '0';
			} else {
				$data['id_comp'] = $id_comp_b;
			}


			if(empty($id_comp_f)){
				$data['id_comp'] = '0';
			} else {
				$data['id_comp'] = $id_comp_f;
			}



			$data['type'] = $type;

			$query_customer = $this->db->get_where('customer');
			$data['customer'] = $query_customer->result_array();

			$this->view['main'] =  $this->load->view('ssr/create',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function GetTariff(){

    	$this->db->order_by("tariff_code", "asc");
		$this->db->where("is_delete", "N");
		$tariff_type = $this->db->get('tariff_type')->result_array();

		echo json_encode($tariff_type);
		return false;
    }

    public function getRate(){

		$id_type = $this->input->post('id_type');

		$query_type = $this->db->get_where('tariff_type', array('tariff_id' => $id_type, 'is_delete' => 'N'));
		$rate = $query_type->row();

		if($rate){
			$result['rate'] = $rate->tariff_rate;
			$result['tariff_code'] = $rate->tariff_code;
			$result['tariff_des'] = $rate->tariff_des;	
			$result['vat'] = $rate->vat;
			$result['holdtax'] = $rate->holdtax;
			$result['code_wh'] = $rate->code_wh;
			$result['code_vat'] = $rate->code_vat;	
		} else {
			$result['rate'] = 0;
			$result['tariff_des'] = '-';
			$result['tariff_code'] = '-';
		}

		
		echo json_encode($result);
		return false;
	}

	public function SaveMultiOrder(){

	  		$type = $this->input->post('type');
	  		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$payment = $this->input->post('payment');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_pro = $this->input->post('rate_pro');
	  		$inv_date = $this->input->post('inv_date');
	  		$vat = $this->input->post('vat');

            $v_name = $this->input->post('v_name');
            $v_visit = $this->input->post('v_visit');
            $v_voyin = $this->input->post('v_voyin');
            $v_voyout = $this->input->post('v_voyout');
            $v_atb = strtotime($this->input->post('v_atb'));
            $v_atd = strtotime($this->input->post('v_atd'));

            $configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('ssr_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    	$i = 0;

	    	if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	} else {
	    		$remark = null;
	    	}

	    	foreach ($type as $rsType) {

	    		if($rsType != null OR $rsType != '' OR !empty($rsType)){
					$this->db->where("tariff_id", $rsType);
					$Qtype = $this->db->get('tariff_type')->row();

					if($Qtype->code_wh == 'N'){
						$wh = 'NO';
					} else {
						$wh = 'YES';
					}

					if($Qtype->code_vat == 'N'){
						$vat = 'NO';
					} else {
						$vat = 'YES';
					}


		    		$data = array(	
					    "ref_id" => $refId,
	                    "id_comp" => $id_comp,
	                    "tariff_id" => $Qtype->tariff_id,
	                    "vessel" => $v_name,
	                    "vsl_visit" => $v_visit,
	                    "voy_in" => $v_voyin,
	                    "voy_out" => $v_voyout,
	                    "type" => 'INVOICE',
	                    "atb" => date('Y-m-d H:i',$v_atb),
	                    "atd" => date('Y-m-d H:i',$v_atd),
	                    "qty" => $qty[$i],
	                    "cur_rate" => $rate_pro[$i],
	                    "payment" => $payment,
	                    "is_multi" => 'YES',
	                    "is_use" => 'YES',
						"remark" => $remark,
						"wh" => $wh,
						"vat" => $vat,
						"config_holdtax" => $Qtype->holdtax,
						"config_holdtax_inv" => $Qtype->holdtax,
						"cal_vat" => (float)(($qty[$i]*$rate_pro[$i])*($Qtype->vat/100)),
						"cal_wh" => (float)(($qty[$i]*$rate_pro[$i])*($Qtype->holdtax/100)),
						"invoice_date" => date('Y-m-d H:i:s'),
	                    "created" => $inv_date.date(' H:i'),
	                    "updated" => date('Y-m-d H:i:s')
					);
						
					$this->db->insert('ssr_order', $data);

					$result['refId'] = $refId;
				}
				
	    	 $i++;}

	    	echo json_encode($result);
			return false;
    }


    public function SaveOnceOrder(){

    		$type = $this->input->post('type');
    		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$payment = $this->input->post('payment');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_pro = $this->input->post('rate_pro');
	  		$inv_date = $this->input->post('inv_date');
	  		

	  		$v_name = $this->input->post('v_name');
	  		$v_visit = $this->input->post('v_visit');
	  		$v_voyin = $this->input->post('v_voyin');
	  		$v_voyout = $this->input->post('v_voyout');
	  		$v_atb = strtotime($this->input->post('v_atb'));
	  		$v_atd = strtotime($this->input->post('v_atd'));

	  		 $configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('ssr_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    		if($remark1 != null and $remark2 != null and $remark3 != null){	

		    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

		    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
		    		$remark = $remark1."<br>".$remark2;

		    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
		    		$remark = $remark1;
		    	} else {
		    		$remark = '';
		    	}
	    		
				$this->db->where("tariff_id", $type);
				$Qtype = $this->db->get('tariff_type')->row();

				if($Qtype->code_wh == 'N'){
					$wh = 'NO';
				} else {
					$wh = 'YES';
				}

				if($Qtype->code_vat == 'N'){
					$vat = 'NO';
				} else {
					$vat = 'YES';
				}

	    		$data = array(	
					"ref_id" => $refId,
					"id_comp" => $id_comp,
					"tariff_id" => $Qtype->tariff_id,
					"vessel" => $v_name,
					"vsl_visit" => $v_visit,
					"voy_in" => $v_voyin,
					"voy_out" => $v_voyout,
					"type" => 'INVOICE',
					"atb" => date('Y-m-d H:i',$v_atb),
					"atd" => date('Y-m-d H:i',$v_atd),
					"qty" => $qty,
					"cur_rate" => $rate_pro,
					"payment" => $payment,
					"is_multi" => 'NO',
					"is_use" => 'YES',
					"remark" => $remark,
					"wh" => $wh,
					"vat" => $vat,
					"config_holdtax" => $Qtype->holdtax,
					"config_holdtax_inv" => $Qtype->holdtax,
					"cal_vat" => (float)(($qty*$rate_pro)*($Qtype->vat/100)),
					"cal_wh" => (float)(($qty*$rate_pro)*($Qtype->holdtax/100)),
					"invoice_date" => date('Y-m-d H:i:s'),
				   	"created" => $inv_date.date(' H:i'),
				    "updated" => date('Y-m-d H:i:s')
				);
					
				$this->db->insert('ssr_order', $data);

				$result['refId'] = $refId;
				

	    	echo json_encode($result);
			return false;
    }

    public function Generate($RefId = null){

    	if($this->session->userdata('logged_in')) { 	

    		//$orderCus = $this->db->get_where('ssr_order', array('ref_id' => $RefId , 'is_use' => 0))->row();

			$order  = $this->db->get_where('ssr_order', array('ref_id' => $RefId , 'is_use' => 0))->result_array();

			//$DataCus = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$settingPre = $this->db->get_where('prefix_ssr', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;


			$this->db->select('ref_id');
			$chk_ssr_draft = $this->db->get('chk_ssr_draft')->result_array();

			$ignore = array_map('current', $chk_ssr_draft);

			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$this->db->where('year_dr', date('Y'));
			$this->db->where_not_in('ref_id', $ignore);

			$last_dr = $this->db->get('status_ssr_draft')->row();


					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."00001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}


					$this->db->select('dr_no');
					$chk_ssr_dr_no = $this->db->get('chk_ssr_draft')->result_array();


					for ($i=0; $i <= count($chk_ssr_dr_no); $i++) { 

						foreach($chk_ssr_dr_no as $rs_chk_draft){

							if($rs_chk_draft['dr_no'] == $complete_dr){

								$rand_dr = (int)$complete_dr;
								$complete_dr  = $rand_dr + 1;

							}
							
						}
					}

						$rand_dr = (int)$complete_dr;
						$set_dr  = $rand_dr;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

							$data_book = array(	
								"dr_no" => $gen_dr,
							    "updated" => date('Y-m-d H:i:s')
							);
							
							$this->db->where('ref_id', $RefId);
							$this->db->update('ssr_order', $data_book);

			//////////////////Log Print////////////////////////////////
			

								$status_dr = array(
									"ref_id" => $RefId,
									"prefix_dr" => $prefix,
									"year_dr" => date('Y'),
									"dr_no" => $complete_dr,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_ssr_draft', $status_dr);
			//////////////////Log Print////////////////////////////////

			$this->set_barcode($gen_dr);

			redirect('/Speacial/PrintDraft/'.$RefId, 'refresh');

    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }
    

    public function PrintDraft($RefId = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('ssr_order', array('ref_id' => $RefId))->row();

    		$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$data = array(
						"username" => $username->username,
						"print_no" => $orderCus->dr_no,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
					
			$this->db->insert('log_print_ssr_draft', $data);

    		$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$printlogs = $this->db->get('log_print_ssr_draft')->row();

			$data['with_hold'] = $orderCus->config_holdtax_inv;

			$data['datePrint'] = $orderCus->created;

    		$data['dr_no'] = $orderCus->dr_no;

    		$data['payment'] = $orderCus->payment;

    		$data['SsrOrder'] = $orderCus;

    		$data['typeOrder'] = $orderCus->type;

    		$data['c_vat'] = $orderCus->vat;

    		$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$this->db->order_by('ssr_id','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.ref_id',$RefId);
			$this->db->select('ssr_order.*, tariff_type.*');
			$data['order']  = $this->db->get('ssr_order')->result_array();


			$this->db->order_by('ssr_id','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.ref_id',$RefId);
			$this->db->select('ssr_order.*, tariff_type.*');
			$sum_inv  = $this->db->get('ssr_order')->result_array();

			$all_amount = 0;
			$sub_total = 0;
			$vat_total = 0;
			$wh_total = 0;

			foreach ($sum_inv as $rs_order) { 
				$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
				$all_amount += ($line_amount+$rs_order['cal_vat']-$rs_order['cal_wh']);
				$sub_total += $rs_order['cur_rate'] * $rs_order['qty'];

				if( $rs_order['code_vat']){
					$vat_total += $rs_order['cal_vat'];
				}
				
				if( $rs_order['code_wh']){
					$wh_total += $rs_order['cal_wh'];
				}

				
			}	

			$data['s_all_amount'] = $all_amount;
			$data['s_sub_total'] = $sub_total;
			$data['s_vat_total'] = $vat_total;
			$data['s_wh_total'] = $wh_total;

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$date2  = new DateTime("01-03-2022");
			$date1  = new DateTime($orderCus->created);

			if($date1 >= $date2){

				if(count($data['order']) <= 9){
					$this->view['main'] =  $this->load->view('ssr/print_draft',$data,true);
					$this->view();
				} else {
					$data['page_count'] = ceil(count($data['order'])/15);
					$this->view['main'] =  $this->load->view('ssr/print_draft_multi',$data,true);
					$this->view();
				}

			} else {

				if(count($data['order']) <= 9){
					$this->view['main'] =  $this->load->view('ssr/print_draft_old',$data,true);
					$this->view();
				} else {
					$data['page_count'] = ceil(count($data['order'])/15);
					$this->view['main'] =  $this->load->view('ssr/print_draft_multi_old',$data,true);
					$this->view();
				}

			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function SetReceipt($dr_no=null  , $none_hold = null , $receipt_date = null , $receipt_type = null){
    	if($this->session->userdata('logged_in')) { 	

			$this->db->where('dr_no',$dr_no);
			$query_split = $this->db->get('ssr_order');
			$data['order'] = $query_split->row();

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_ssr_receipt')->row();

			$query_holdtax = $this->db->get('setting_holdtax');
			$holdtax_re = $query_holdtax->row();


			if(strtotime($receipt_date) <= strtotime("2020-03-31 23:59:59") and strtotime($receipt_date) >= strtotime("2020-10-01 00:00:01")){

					 	$wh_c = '3.00';

				} else if (strtotime($receipt_date) >= strtotime("2020-04-01 00:00:01") and strtotime($receipt_date) <= strtotime("2020-09-30 23:59:59")){

						$wh_c = '1.50';

				} else {

						$wh_c =  $holdtax_re->holdtax;
			}


			if($none_hold == '1'){
				$s_hold = 'YES';
			} else {
				$s_hold = 'NO';
			}

			if($receipt_type == 'R'){
				$re_type = 'RECEIPT';
			} else {
				$re_type = 'RECEIPT / TAX INVOICE';
			}

					/////////Gen Invoice Number////////////////////////////////////////////////////////
						if(empty($data['order']->invoice_no)){

							if($receipt_type == 'RT'){

								$this->db->select('invoice_no');
								$chk_ssr_receipt = $this->db->get('chk_ssr_invoice')->result_array();


								$ignore = array_map('current', $chk_ssr_receipt);

								$this->db->limit(1);
								$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
								$this->db->where('receipt_type', 'RT');
								$this->db->where('SUBSTRING(invoice_no,1,4)', date('Y'));
								$this->db->where_not_in('invoice_no', $ignore);
								$this->db->order_by("invoice_no", "desc");
								$last_invoice = $this->db->get('status_ssr_invoice')->row();


								if(empty($last_invoice->invoice_no)){
									$rand_invoice = date('Y')."000001";
								} else {
									$check_invoice_no = $last_invoice->invoice_no+1;

									$rand_invoice = date('Y').substr($check_invoice_no,4);
								}

								$this->db->select('invoice_no');
								$this->db->where('prefix_invoice','RES');
								$chk_ssr_inv_no = $this->db->get('chk_ssr_invoice')->result_array();

								for ($i=0; $i <= count($chk_ssr_inv_no); $i++) { 

									foreach($chk_ssr_inv_no as $rs_chk_inv){

										if($rs_chk_inv['invoice_no'] == $rand_invoice){

											$rand_dr = (int)$rand_invoice;
											$rand_invoice  = $rand_dr + 1;

										}
										
									}
								}


								$status_invoice = array(	
									"dr_no" => $dr_no,
									"receipt_type" => 'RT',
									"prefix_invoice" => 'RES',
									"invoice_no" => $rand_invoice,
									"created" => $receipt_date.date(' H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
											
								$this->db->insert('status_ssr_invoice', $status_invoice);

								$data = array(
									"prefix_invoice" => 'RES',
									"invoice_no" => $rand_invoice,
									"receipt_type" => 'RECEIPT / TAX INVOICE',
									"type" => 'RECEIPT',
									"wh" => $s_hold,
									"config_holdtax_inv" => $wh_c,
									"updated" => date('Y-m-d H:i:s')
								);

								$this->db->where('dr_no', $dr_no);
								$this->db->update('ssr_order',$data);

							} else {

								$this->db->limit(1);
								$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
								$this->db->where('receipt_type', 'R');
								$this->db->where('SUBSTRING(invoice_no,1,4)', date('Y'));
								$this->db->order_by("invoice_no", "desc");
								$last_invoice = $this->db->get('status_ssr_invoice')->row();


								if(empty($last_invoice->invoice_no)){
									$rand_invoice = date('Y')."000001";
								} else {
									$check_invoice_no = $last_invoice->invoice_no+1;

									$rand_invoice = date('Y').substr($check_invoice_no,4);
								}

								$status_invoice = array(	
									"dr_no" => $dr_no,
									"receipt_type" => 'R',
									"prefix_invoice" => 'NRE',
									"invoice_no" => $rand_invoice,
									"created" => $receipt_date.date(' H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
											
								$this->db->insert('status_ssr_invoice', $status_invoice);

								$data = array(
									"prefix_invoice" => 'NRE',
									"invoice_no" => $rand_invoice,
									"receipt_type" => 'RECEIPT',
									"type" => 'RECEIPT',
									"wh" => $s_hold,
									"config_holdtax_inv" => $wh_c,
									"updated" => date('Y-m-d H:i:s')
								);

								$this->db->where('dr_no', $dr_no);
								$this->db->update('ssr_order',$data);


							}

							
						}
					/////////Gen Invoice Number////////////////////////////////////////////////////////

					redirect('Speacial/PrintReceipt/'.$dr_no, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function RePrintReceipt($dr_no=null){
    	if($this->session->userdata('logged_in')) { 	

			    $check_data = $this->session->userdata('logged_in');

			    $orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no))->row();

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

	    		if($orderCus->vat == 'YES'){
		    		if($orderCus->chk_wh == '1'){
						$s_hold = 'YES';
					} else {
						$s_hold = 'NO';
					}	
				} else {
					$s_hold = 'NO';
				}


						$SetOrder = array(
								"wh" => $s_hold,
								"updated" => date('Y-m-d H:i:s')
						);

						$this->db->where('dr_no', $dr_no);
						$this->db->update('ssr_order',$SetOrder);
						

						$data = array(
							"username" => $username->username,
							"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);

						$this->db->insert('log_re_print_ssr_invoice', $data);


				$this->db->where("dr_no",$orderCus->dr_no);
				$printlogs = $this->db->get('status_ssr_invoice')->row();

				$data['datePrint'] = $printlogs->created;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['v_data'] = $orderCus;

	    		$data['c_vat'] = $orderCus->vat;


	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

				$data['user_print'] = $username;

				$this->db->order_by('tariff_code','ASC');
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('ssr_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('ssr_order')->result_array();

				$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('ssr_order.dr_no',$dr_no);
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.vat, tariff_type.code_vat, tariff_type.rate_type, tariff_type.holdtax');
				$sum_inv = $this->db->get('ssr_order')->result_array();

				$all_amount = 0;
				$y_wh = 0;
				$y_vat = 0;

				foreach ($sum_inv as $rs_order) { 

					$line_amount = $rs_order['TotalInv'];
                    $all_amount += $line_amount;

                    if($rs_order['code_vat'] != 'N'){
                        $y_vat += $rs_order['cal_vat'];
                    }

                    if($rs_order['code_wh'] != 'N'){
                         $y_wh += $rs_order['cal_wh'];
                    }
				}	

				$data['s_all_amount'] = $all_amount;
				$data['s_y_vat'] = $y_vat;
				$data['s_y_wh'] = $y_wh;

				$data['with_hold'] =  $orderCus->config_holdtax;

				$data['hold_status'] = $s_hold;

				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->view['main'] =  $this->load->view('ssr/print_receipt',$data,true);
				$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function PrintReceipt($dr_no=null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no))->row();


    			$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();


	    		if($orderCus->vat == 'YES'){
		    		if($orderCus->wh == 'YES'){
						$s_hold = 'YES';
					} else {
						$s_hold = 'NO';
					}	
				} else {
					$s_hold = 'NO';
				}


						$SetOrder = array(
								"wh" => $s_hold,
								"updated" => date('Y-m-d H:i:s')
						);

						$this->db->where('dr_no', $dr_no);
						$this->db->update('ssr_order',$SetOrder);


						$data = array(
							"username" => $username->username,
							"invoice_no" => $orderCus->dr_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
				$this->db->insert('log_print_ssr_invoice', $data);


				$this->db->where("dr_no",$orderCus->dr_no);
				$printlogs = $this->db->get('status_ssr_invoice')->row();


				$data['datePrint'] = $printlogs->created;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['v_data'] = $orderCus;

	    		$data['hold_status'] = $s_hold;

	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

	    		$data['c_vat'] = $orderCus->vat;

				$data['user_print'] = $username;


				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('ssr_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('ssr_order')->result_array();

				$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('ssr_order.dr_no',$dr_no);
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.vat, tariff_type.code_vat, tariff_type.rate_type, tariff_type.holdtax');
				$sum_inv = $this->db->get('ssr_order')->result_array();

				$all_amount = 0;
				$y_wh = 0;
				$y_vat = 0;

				foreach ($sum_inv as $rs_order) { 

					$line_amount = $rs_order['TotalInv'];
                    $all_amount += $line_amount;

                    if($rs_order['code_vat'] != 'N'){
                        $y_vat += $rs_order['cal_vat'];
                    }

                    if($rs_order['code_wh'] != 'N'){
                         $y_wh += $rs_order['cal_wh'];
                    }
				}	

				$data['s_all_amount'] = $all_amount;
				$data['s_y_vat'] = $y_vat;
				$data['s_y_wh'] = $y_wh;


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$data['with_hold'] = $orderCus->config_holdtax;

				$this->view['main'] =  $this->load->view('ssr/print_receipt',$data,true);
				$this->view();



    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function Cancel(){

    	$dr_no = $this->input->post('dr_no');
    	$remark_inv = $this->input->post('remark_inv');

    	$orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no))->row();

    	$data_status = array(
					"remark_cancel" => $remark_inv,
		          	"type" => 'CANCEL',
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $dr_no);
		$this->db->update('ssr_order',$data_status);

		$check_data = $this->session->userdata('logged_in');

	    $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$data = array(
					"username" => $username->username,
					"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
					"dr_no" => $orderCus->dr_no,
					"remark_cancel" => $remark_inv,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
						
		$this->db->insert('log_cancel_ssr_invoice', $data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }


    public function RemarkDoc(){

    	$dr_no = $this->input->post('dr_no_doc');
    	$remark_inv = $this->input->post('comment_doc');

    	$orderCus = $this->db->get_where('ssr_order', array('dr_no' => $dr_no))->row();

    	$data_status = array(
					"remark_doc" => $remark_inv,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $dr_no);
		$this->db->update('ssr_order',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }

    public function CreateNote($RefId = null, $RefNoted = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('ssr_order', array('ref_id' => $RefId))->row();

			$data['with_hold'] = $orderCus->config_holdtax;

			$data['datePrint'] = $orderCus->created;

    		$data['dr_no'] = $orderCus->dr_no;

    		$data['payment'] = $orderCus->payment;  		

    		$data['typeOrder'] = $orderCus->type;

    		$data['ref_id'] = $orderCus->ref_id;

    		$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by('tariff_code','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('ssr_order.ref_id',$RefId);
			$originOrder  = $this->db->get('ssr_order')->result_array();

			$this->db->limit(1);
			$this->db->order_by("ref_noted", "desc");
			$last_ref_noted = $this->db->get('ssr_noted')->row();

			if($last_ref_noted){
				$updated_ref_noted = $last_ref_noted->ref_noted+1;
			} else {
				$updated_ref_noted = 1;
			}

			if($RefNoted == null){
				foreach ($originOrder  as $key) {



						$data = array(
							"ref_id" => $key['ref_id'],
							"ref_noted" => $updated_ref_noted,
							"id_comp" => $key['id_comp'],
							"dr_no" => $key['dr_no'],
							"prefix_invoice" => $key['prefix_invoice'],
							"invoice_no" => $key['invoice_no'],
							"type" => $key['type'],
							"tariff_id" => $key['tariff_id'],
							"vessel" => $key['vessel'],
							"vsl_visit" => $key['vsl_visit'],
							"voy_in" => $key['voy_in'],
							"voy_out" => $key['voy_out'],
							"atb" => $key['atb'],
							"atd" => $key['atd'],
							"qty" => $key['qty'],
							"cur_rate" => $key['cur_rate'],
							"payment" => $key['payment'],
							"remark" => $key['remark'],
							"is_multi" => $key['is_multi'],
							"is_use" => $key['is_use'],
							"remark_cancel" => $key['remark_cancel'],
							"remark_doc" => $key['remark_doc'],
							"billing_note" => $key['billing_note'],
							"bank_note" => $key['bank_note'],
							"date_note" => $key['date_note'],
							"config_holdtax" => $key['config_holdtax'],
							"config_holdtax_inv" => $key['config_holdtax_inv'],
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
						$this->db->insert('ssr_noted', $data);

				}
			}

			if($RefNoted){
				$data['RefNoted'] = $RefNoted;
			} else {
				$data['RefNoted'] = $updated_ref_noted;
			}
			

			$this->db->order_by('tariff_code','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_noted.tariff_id');
			$this->db->where('ssr_noted.ref_noted',$data['RefNoted']);
			$this->db->where('ssr_noted.status_noted','TEMP');
			$data['order']  = $this->db->get('ssr_noted')->result_array();

			$orderCus = $this->db->get_where('ssr_order', array('ref_id' => $RefId))->row();

			$data['SsrOrder'] = $orderCus;

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$this->view['main'] =  $this->load->view('ssr/create_note',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }


    public function DelItemNoted(){

    	$ssr_id = $this->input->post('ssr_id');

    	$data_status = array(
					"status_noted" => 'DEL',
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('ssr_id', $ssr_id);
		$this->db->update('ssr_noted',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }

    public function UpdatedNoted(){

    	$ssr_id = $this->input->post('ssr_id');
    	$RefNoted = $this->input->post('RefNoted');
    	$qty = $this->input->post('qty');
    	$rate_pro = $this->input->post('rate_pro');
    	$remark1 = $this->input->post('remark1');
	  	$remark2 = $this->input->post('remark2');
	  	$remark3 = $this->input->post('remark3');
    	$payment = $this->input->post('payment');
    	$noted_date = $this->input->post('noted_date');

    		$settingPre = $this->db->get_where('prefix_ssr_noted', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;

			if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	}

			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$last_dr = $this->db->get('status_ssr_noted')->row();

					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."0001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "000000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}

					$status_dr = array(
						"ref_id" => $RefNoted,
						"prefix_dr" => $prefix,
						"year_dr" => date('Y'),
						"dr_no" => $complete_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
															
					$this->db->insert('status_ssr_noted', $status_dr);

    	$i = 0;
    	foreach ($ssr_id as $destination_id) {

	    	$data_status = array(
	    				"noted_no" => $gen_dr,
						"qty" => $qty[$i],
						"noted_rate" => $rate_pro[$i],
						"remark_noted" => $remark,
						"type_noted" => $payment,
						"status_noted" => 'USE',
			          	"created_noted" => $noted_date
			         );
			         
			$this->db->where('ssr_id', $destination_id);
			$this->db->update('ssr_noted',$data_status);

		$i++;}

		$result['msg'] = $gen_dr;
		echo json_encode($result);
		return false;

    }


        public function UpdatedSingleNoted(){

    	$ssr_id = $this->input->post('ssr_id');
    	$RefNoted = $this->input->post('RefNoted');
    	$qty = $this->input->post('qty');
    	$rate_pro = $this->input->post('rate_pro');
    	$remark1 = $this->input->post('remark1');
	  	$remark2 = $this->input->post('remark2');
	  	$remark3 = $this->input->post('remark3');
    	$payment = $this->input->post('payment');
    	$noted_date = $this->input->post('noted_date');

    		$settingPre = $this->db->get_where('prefix_ssr_noted', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;

			if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	}


			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$last_dr = $this->db->get('status_ssr_noted')->row();

					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."0001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "000000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}

					$status_dr = array(
						"ref_id" => $RefNoted,
						"prefix_dr" => $prefix,
						"year_dr" => date('Y'),
						"dr_no" => $complete_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
															
					$this->db->insert('status_ssr_noted', $status_dr);

	    	$data_status = array(
	    				"noted_no" => $gen_dr,
						"qty" => $qty,
						"noted_rate" => $rate_pro,
						"remark_noted" => $remark,
						"type_noted" => $payment,
						"status_noted" => 'USE',
			          	"created_noted" => $noted_date
			         );
			         
			$this->db->where('ssr_id', $ssr_id);
			$this->db->update('ssr_noted',$data_status);

		$result['msg'] = $gen_dr;
		echo json_encode($result);
		return false;

    }


     public function PrintNoted($noted_no=null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('ssr_noted', array('noted_no' => $noted_no))->row();


    			$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

						$data = array(
							"username" => $username->username,
							"invoice_no" => $orderCus->dr_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
				$this->db->insert('log_print_ssr_noted', $data);

				$data['datePrint'] = $orderCus->created_noted;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['noted_no'] = $orderCus->noted_no;

	    		$data['v_data'] = $orderCus;

	    		$data['hold_status'] = $orderCus->config_holdtax;

	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

				$data['user_print'] = $username;

				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_noted.tariff_id');
				$this->db->where('ssr_noted.noted_no',$noted_no);
				$data['order']  = $this->db->get('ssr_noted')->result_array();


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$data['with_hold'] = $orderCus->config_holdtax;

				$this->view['main'] =  $this->load->view('ssr/print_noted',$data,true);
				$this->view();



    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }


    public function NotedList() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Speacial/NotedList";
	        $config["total_rows"] = $this->M_Ssr->CountNoted($search, $sort);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Ssr->CountNoted($search, $sort);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Ssr->FetchNoted($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;
			$data['sort'] = $sort;
			$data['search'] = $search;

	       
	        $this->view['main'] =  $this->load->view('ssr/noted_list',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function CancelNoted(){

    	$noted_no = $this->input->post('noted_no');
    	$remark_inv = $this->input->post('remark_inv');

    	$orderCus = $this->db->get_where('ssr_noted', array('noted_no' => $noted_no))->row();

    	$data_status = array(
					"remark_cancel" => $remark_inv,
		          	"status_noted" => 'CANCEL',
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('noted_no', $noted_no);
		$this->db->update('ssr_noted',$data_status);

		$check_data = $this->session->userdata('logged_in');

	    $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$data = array(
					"username" => $username->username,
					"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
					"dr_no" => $orderCus->noted_no,
					"remark_cancel" => $remark_inv,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
						
		$this->db->insert('log_cancel_noted', $data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }

    public function MultiPreview(){

    	if($this->session->userdata('logged_in')) { 	


    		$search = $this->input->post('multiinv');

    		$pieces = explode(", ", $search);


    		$this->db->where_in('ssr_order.dr_no',$pieces );
			$orderCus  = $this->db->get('ssr_order')->row();


    		$data['v_data'] = $orderCus;

			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where_in('ssr_order.dr_no',$pieces );
			$data['order']  = $this->db->get('ssr_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$query_wh = $this->db->get('setting_holdtax')->row();


			if(strtotime($orderCus->created) <= strtotime("2020-03-31 23:59:59") OR strtotime($orderCus->created) >= strtotime("2020-09-30 23:59:59")){
				 	$wh_c = '3.00';

			} else {
					$wh_c =  $query_wh->holdtax;
			}


			$data['with_hold'] = $wh_c;

			$data['multiinv'] = $search;

			/*$data_book = array(	
				"config_holdtax_inv" => $wh_c,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('dr_no', $dr_no);
			$this->db->update('ssr_order', $data_book);*/

			$this->view['main'] =  $this->load->view('ssr/multi_preview',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function SetMultiReceipt(){
    	if($this->session->userdata('logged_in')) { 	

			///////////////////////////////////////////////////////////

			$date_multiinv =  $this->input->post('date_multiinv');
			$chk_wht =  $this->input->post('chk_wht');
			$multiinv = $this->input->post('multiinv');
			$re_type = $this->input->post('re_type');


    		$pieces = explode(", ", $multiinv);

			$this->db->where_in('ssr_order.dr_no',$pieces);
			$orderCus  = $this->db->get('ssr_order')->row();


    		$data['v_data'] = $orderCus;


    		$this->db->order_by('ssr_order.dr_no', 'asc');
    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where_in('ssr_order.dr_no',$pieces );
			$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.code_vat, tariff_type.vat, tariff_type.rate_type, tariff_type.holdtax');
			$data['order']  = $this->db->get('ssr_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			///////////////////////////////////////////////////////////

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_ssr_receipt')->row();

			$query_holdtax = $this->db->get('setting_holdtax');
			$holdtax_re = $query_holdtax->row();

			if($orderCus->invoice_no == null or $orderCus->invoice_no == ''){

				if($chk_wht == '1'){
					$s_hold = 'YES';
				} else {
					$s_hold = 'NO';
				}

					if($re_type == 'RT'){

						$this->db->select('invoice_no');
						$chk_ssr_receipt = $this->db->get('chk_ssr_invoice')->result_array();

						$ignore = array_map('current', $chk_ssr_receipt);

						$this->db->limit(1);
						$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
						$this->db->where('receipt_type', 'RT');
						$this->db->where('SUBSTRING(invoice_no,1,4)', date('Y'));
						$this->db->where_not_in('invoice_no', $ignore);
						$this->db->order_by("invoice_no", "desc");
						$last_invoice = $this->db->get('status_ssr_invoice')->row();


						if(empty($last_invoice->invoice_no)){
							$rand_invoice = date('Y')."000001";
						} else {
							$check_invoice_no = $last_invoice->invoice_no+1;

							$rand_invoice = date('Y').substr($check_invoice_no,4);
						}

						$this->db->select('invoice_no');
						$this->db->where('prefix_invoice','RES');
						$chk_ssr_inv_no = $this->db->get('chk_ssr_invoice')->result_array();

								for ($i=0; $i <= count($chk_ssr_inv_no); $i++) { 

									foreach($chk_ssr_inv_no as $rs_chk_inv){

										if($rs_chk_inv['invoice_no'] == $rand_invoice){

											$rand_dr = (int)$rand_invoice;
											$rand_invoice  = $rand_dr + 1;

										}
										
									}
								}

						$status_invoice = array(	
							"dr_no" => $multiinv,
							"receipt_type" => 'RT',
							"prefix_invoice" => 'RES',
							"invoice_no" => $rand_invoice,
							"created" => $date_multiinv.date(' H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
											
						$this->db->insert('status_ssr_invoice', $status_invoice);

						$data = array(
							"prefix_invoice" => 'RES',
							"invoice_no" => $rand_invoice,
							"receipt_type" => 'RECEIPT / TAX INVOICE',
							"type" => 'RECEIPT',
							"chk_wh" => $chk_wht,
							"updated" => date('Y-m-d H:i:s')
						);

						$this->db->where_in('ssr_order.dr_no',$pieces);
						$this->db->update('ssr_order',$data);

					} else {

						$this->db->limit(1);
						$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
						$this->db->where('receipt_type', 'R');
						$this->db->where('SUBSTRING(invoice_no,1,4)', date('Y'));
						$this->db->order_by("invoice_no", "desc");
						$last_invoice = $this->db->get('status_ssr_invoice')->row();


						if(empty($last_invoice->invoice_no)){
							$rand_invoice = date('Y')."000001";
						} else {
							$check_invoice_no = $last_invoice->invoice_no+1;

							$rand_invoice = date('Y').substr($check_invoice_no,4);
						}

						$status_invoice = array(	
							"dr_no" => $multiinv,
							"prefix_invoice" => 'NRE',
							"invoice_no" => $rand_invoice,
							"receipt_type" => 'R',
							"created" => $date_multiinv.date(' H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
											
						$this->db->insert('status_ssr_invoice', $status_invoice);

						$data = array(
							"prefix_invoice" => 'NRE',
							"receipt_type" => 'RECEIPT',
							"invoice_no" => $rand_invoice,
							"type" => 'RECEIPT',
							"chk_wh" => $chk_wht,
							"updated" => date('Y-m-d H:i:s')
						);

						$this->db->where_in('ssr_order.dr_no',$pieces);
						$this->db->update('ssr_order',$data);

					}

					$check_data = $this->session->userdata('logged_in');

	    			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

						$data = array(
							"username" => $username->username,
							"invoice_no" => $rand_invoice,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
					$this->db->insert('log_print_ssr_invoice', $data);

			} 


				$this->db->where_in('ssr_order.dr_no',$pieces );
				$orderCus  = $this->db->get('ssr_order')->row();


	    		$data['v_data'] = $orderCus;


	    		$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where_in('ssr_order.dr_no',$pieces );
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.code_vat, tariff_type.vat,tariff_type.rate_type, tariff_type.holdtax');
				$data['order']  = $this->db->get('ssr_order')->result_array();


	    		$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where_in('ssr_order.dr_no',$pieces );
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.vat, tariff_type.code_vat, tariff_type.rate_type, tariff_type.holdtax');
				$sum_inv = $this->db->get('ssr_order')->result_array();

				$all_amount = 0;
				$y_wh = 0;
				$y_vat = 0;

				foreach ($sum_inv as $rs_order) { 

					$line_amount = $rs_order['TotalInv'];
                    $all_amount += $line_amount;

                    if($rs_order['code_vat'] != 'N'){
                        $y_vat += $rs_order['cal_vat'];
                    }

                    if($rs_order['code_wh'] != 'N'){
                         $y_wh += $rs_order['cal_wh'];
                    }
				}	

				$data['s_all_amount'] = $all_amount;
				$data['s_y_vat'] = $y_vat;
				$data['s_y_wh'] = $y_wh;

				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$this->db->limit(1);
				$this->db->where_in('ssr_order.dr_no',$pieces);
				$this->db->select('chk_wh');
				$OrderWht  = $this->db->get('ssr_order')->row();

				$data['chk_wht'] = $OrderWht->chk_wh;

				$this->db->limit(1);
				$this->db->where_in('ssr_order.dr_no',$pieces);
				$OrderPrint  = $this->db->get('ssr_order')->row();

				$this->db->limit(1);
				//$this->db->where('invoice_no', $OrderPrint->invoice_no);
				$this->db->where('CONCAT(prefix_invoice,invoice_no)',$OrderPrint->prefix_invoice.$OrderPrint->invoice_no);
				$maping_invoice = $this->db->get('status_ssr_invoice')->row();

				$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();


				$data['receipt_date'] = $maping_invoice->created;
				$data['res_no'] = $maping_invoice->prefix_invoice.$maping_invoice->invoice_no;
				$data['datePrint'] = date('Y-m-d H:i:s');
				$data['user_print'] = $username;


				$this->view['main'] =  $this->load->view('ssr/multi_print_receipt',$data,true);
				$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }


	public function ReMultiPreview($invoice_no = null){

    	if($this->session->userdata('logged_in')) { 	


    		$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$invoice_no );
			$orderCus  = $this->db->get('ssr_order')->row();

    		$data['v_data'] = $orderCus;

			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$invoice_no );
			$data['order']  = $this->db->get('ssr_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$query_wh = $this->db->get('setting_holdtax')->row();


			if(strtotime($orderCus->created) <= strtotime("2020-03-31 23:59:59") OR strtotime($orderCus->created) >= strtotime("2020-09-30 23:59:59")){
				 	$wh_c = '3.00';

			} else {
					$wh_c =  $query_wh->holdtax;
			}

				$this->db->limit(1);
				//$this->db->where('invoice_no', $orderCus->invoice_no);
				$this->db->where('CONCAT(prefix_invoice,invoice_no)',$orderCus->prefix_invoice.$orderCus->invoice_no);
				$maping_invoice = $this->db->get('status_ssr_invoice')->row();


			$data['with_hold'] = $wh_c;

			$data['res'] = $invoice_no;
			$data['date_res'] = $maping_invoice->created;

			/*$data_book = array(	
				"config_holdtax_inv" => $wh_c,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('dr_no', $dr_no);
			$this->db->update('ssr_order', $data_book);*/

			$this->db->group_by('dr_no');
			$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$invoice_no);
			$this->db->select('dr_no');
			$data['count_multi']  = $this->db->get('ssr_order')->result_array();

			$data['dr_no'] = $orderCus->dr_no;

			$this->view['main'] =  $this->load->view('ssr/re_multi_preview',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function ReMultiReceipt(){
    	if($this->session->userdata('logged_in')) { 	

			///////////////////////////////////////////////////////////

			$res =  $this->input->post('res');
			$date_res = $this->input->post('date_res');


			$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res );
			$orderCus  = $this->db->get('ssr_order')->row();


    		$data['v_data'] = $orderCus;

    		$this->db->order_by('ssr_order.dr_no', 'asc');
    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
			$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res );
			$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, tariff_type.code_vat, tariff_type.vat, ssr_order.created, tariff_type.code_wh, tariff_type.rate_type, tariff_type.holdtax');
			$data['order']  = $this->db->get('ssr_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			///////////////////////////////////////////////////////////


				$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$query_holdtax = $this->db->get('setting_holdtax');
			$holdtax_re = $query_holdtax->row();

						$data = array(
							"username" => $username->username,
							"invoice_no" => $res,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);

						$this->db->insert('log_re_print_ssr_invoice', $data);


				$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res );
				$orderCus  = $this->db->get('ssr_order')->row();


	    		$data['v_data'] = $orderCus;

	    		$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res );
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, tariff_type.code_wh, tariff_type.rate_type, tariff_type.code_vat, tariff_type.vat, tariff_type.holdtax');
				$data['order']  = $this->db->get('ssr_order')->result_array();

				$this->db->order_by('ssr_order.dr_no', 'asc');
	    		$this->db->group_by('ssr_order.dr_no, tariff_type.code_wh, tariff_type.holdtax'); 
				$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				//$this->db->where_in('ssr_order.dr_no',$pieces );
				$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res );
				$this->db->select('ssr_order.dr_no, Sum(ssr_order.cur_rate*ssr_order.qty) as TotalInv, Sum(ssr_order.cal_vat) as cal_vat,  Sum(ssr_order.cal_wh) as cal_wh, ssr_order.created, ssr_order.wh, tariff_type.code_wh, tariff_type.vat, tariff_type.code_vat, tariff_type.rate_type, tariff_type.holdtax');
				$sum_inv = $this->db->get('ssr_order')->result_array();

				$all_amount = 0;
				$y_wh = 0;
				$y_vat = 0;

				foreach ($sum_inv as $rs_order) { 

					$line_amount = $rs_order['TotalInv'];
                    $all_amount += $line_amount;

                    if($rs_order['code_vat'] != 'N'){
                         $y_vat += $rs_order['cal_vat'];
                    }

                    if($rs_order['code_wh'] != 'N'){
                        $y_wh += $rs_order['cal_wh'];
                    }
				}	

				$data['s_all_amount'] = $all_amount;
				$data['s_y_vat'] = $y_vat;
				$data['s_y_wh'] = $y_wh;


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


				$this->db->limit(1);
				$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res);
				$OrderPrint  = $this->db->get('ssr_order')->row();

				$this->db->limit(1);
				$this->db->where('CONCAT(ssr_order.prefix_invoice,ssr_order.invoice_no)',$res);
				$this->db->select('chk_wh');
				$OrderWht  = $this->db->get('ssr_order')->row();

				$data['chk_wht'] = $OrderWht->chk_wh;

				$this->db->limit(1);
				//$this->db->where('invoice_no', $OrderPrint->invoice_no);
				$this->db->where('CONCAT(prefix_invoice,invoice_no)',$res);
				$maping_invoice = $this->db->get('status_ssr_invoice')->row();

				$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();


				$data['receipt_date'] = $maping_invoice->created;
				$data['res_no'] = $maping_invoice->prefix_invoice.$maping_invoice->invoice_no;
				$data['datePrint'] = date('Y-m-d H:i:s');
				$data['user_print'] = $username;

				$this->view['main'] =  $this->load->view('ssr/multi_print_receipt',$data,true);
				$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }


    public function PreviewSaveMultiOrder(){

	  		$type = $this->input->post('type');
	  		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$payment = $this->input->post('payment');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_pro = $this->input->post('rate_pro');
	  		$inv_date = $this->input->post('inv_date');
	  		$vat = $this->input->post('vat');

            $v_name = $this->input->post('v_name');
            $v_visit = $this->input->post('v_visit');
            $v_voyin = $this->input->post('v_voyin');
            $v_voyout = $this->input->post('v_voyout');
            $v_atb = strtotime($this->input->post('v_atb'));
            $v_atd = strtotime($this->input->post('v_atd'));

            $configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('ssr_preview')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    	$i = 0;

	    	if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	} else {
	    		$remark = null;
	    	}

	    	foreach ($type as $rsType) {

					if($rsType != null OR $rsType != '' OR !empty($rsType)){
						$this->db->where("tariff_id", $rsType);
						$Qtype = $this->db->get('tariff_type')->row();

						if($Qtype->code_wh == 'N'){
							$wh = 'NO';
						} else {
							$wh = 'YES';
						}

						if($Qtype->code_vat == 'N'){
							$vat = 'NO';
						} else {
							$vat = 'YES';
						}


			    		$data = array(	
						    "ref_id" => $refId,
		                    "id_comp" => $id_comp,
		                    "tariff_id" => $Qtype->tariff_id,
		                    "vessel" => $v_name,
		                    "vsl_visit" => $v_visit,
		                    "voy_in" => $v_voyin,
		                    "voy_out" => $v_voyout,
		                    "type" => 'INVOICE',
		                    "atb" => date('Y-m-d H:i',$v_atb),
		                    "atd" => date('Y-m-d H:i',$v_atd),
		                    "qty" => $qty[$i],
		                    "cur_rate" => $rate_pro[$i],
		                    "payment" => $payment,
		                    "is_multi" => 'YES',
		                    "is_use" => 'YES',
							"remark" => $remark,
							"wh" => $wh,
							"vat" => $vat,
							"config_holdtax" => $Qtype->holdtax,
							"config_holdtax_inv" => $Qtype->holdtax,
							"cal_vat" => (float)(($qty[$i]*$rate_pro[$i])*($Qtype->vat/100)),
							"cal_wh" => (float)(($qty[$i]*$rate_pro[$i])*($Qtype->holdtax/100)),
							"invoice_date" => date('Y-m-d H:i:s'),
		                    "created" => $inv_date.date(' H:i'),
		                    "updated" => date('Y-m-d H:i:s')
						);
							
						$this->db->insert('ssr_preview', $data);

					$result['refId'] = $refId;
					}
				
	    	 $i++;}

	    	echo json_encode($result);
			return false;
    }


    public function PreviewSaveOnceOrder(){

    		$type = $this->input->post('type');
    		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$payment = $this->input->post('payment');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_pro = $this->input->post('rate_pro');
	  		$inv_date = $this->input->post('inv_date');
	  		

	  		$v_name = $this->input->post('v_name');
	  		$v_visit = $this->input->post('v_visit');
	  		$v_voyin = $this->input->post('v_voyin');
	  		$v_voyout = $this->input->post('v_voyout');
	  		$v_atb = strtotime($this->input->post('v_atb'));
	  		$v_atd = strtotime($this->input->post('v_atd'));

	  		 $configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('ssr_preview')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    		if($remark1 != null and $remark2 != null and $remark3 != null){	

		    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

		    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
		    		$remark = $remark1."<br>".$remark2;

		    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
		    		$remark = $remark1;
		    	} else {
		    		$remark = '';
		    	}
	    		
				$this->db->where("tariff_id", $type);
				$Qtype = $this->db->get('tariff_type')->row();

				if($Qtype->code_wh == 'N'){
					$wh = 'NO';
				} else {
					$wh = 'YES';
				}

				if($Qtype->code_vat == 'N'){
					$vat = 'NO';
				} else {
					$vat = 'YES';
				}

	    		$data = array(	
					"ref_id" => $refId,
					"id_comp" => $id_comp,
					"tariff_id" => $Qtype->tariff_id,
					"vessel" => $v_name,
					"vsl_visit" => $v_visit,
					"voy_in" => $v_voyin,
					"voy_out" => $v_voyout,
					"type" => 'INVOICE',
					"atb" => date('Y-m-d H:i',$v_atb),
					"atd" => date('Y-m-d H:i',$v_atd),
					"qty" => $qty,
					"cur_rate" => $rate_pro,
					"payment" => $payment,
					"is_multi" => 'NO',
					"is_use" => 'YES',
					"remark" => $remark,
					"wh" => $wh,
					"vat" => $vat,
					"config_holdtax" => $Qtype->holdtax,
					"config_holdtax_inv" => $Qtype->holdtax,
					"cal_vat" => (float)(($qty*$rate_pro)*($Qtype->vat/100)),
					"cal_wh" => (float)(($qty*$rate_pro)*($Qtype->holdtax/100)),
					"invoice_date" => date('Y-m-d H:i:s'),
				   	"created" => $inv_date.date(' H:i'),
				    "updated" => date('Y-m-d H:i:s')
				);
					
				$this->db->insert('ssr_preview', $data);

				$result['refId'] = $refId;
				

	    	echo json_encode($result);
			return false;
    }

    public function PreviewInvoice($RefId = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('ssr_preview', array('ref_id' => $RefId))->row();

    		$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['with_hold'] = $orderCus->config_holdtax_inv;

			$data['datePrint'] = $orderCus->created;

    		$data['dr_no'] = "PREVIEW";

    		$data['payment'] = $orderCus->payment;

    		$data['SsrOrder'] = $orderCus;

    		$data['typeOrder'] = $orderCus->type;

    		$data['c_vat'] = $orderCus->vat;

    		$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->order_by('ssr_id','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_preview.tariff_id');
			$this->db->where('ssr_preview.ref_id',$RefId);
			$data['order']  = $this->db->get('ssr_preview')->result_array();

			$this->db->order_by('ssr_preview.ssr_id','ASC');
			$this->db->join(' tariff_type',' tariff_type.tariff_id = ssr_preview.tariff_id');
			$this->db->where('ssr_preview.ref_id',$RefId);
			$this->db->select('ssr_preview.*, tariff_type.*');
			$sum_inv  = $this->db->get('ssr_preview')->result_array();

			$all_amount = 0;
			$sub_total = 0;
			$vat_total = 0;
			$wh_total = 0;

			foreach ($sum_inv as $rs_order) { 
				$line_amount = $rs_order['cur_rate'] * $rs_order['qty'];
				$all_amount += ($line_amount+$rs_order['cal_vat']-$rs_order['cal_wh']);
				$sub_total += $rs_order['cur_rate'] * $rs_order['qty'];
				$vat_total += $rs_order['cal_vat'];
				$wh_total += $rs_order['cal_wh'];
			}	

			$data['s_all_amount'] = $all_amount;
			$data['s_sub_total'] = $sub_total;
			$data['s_vat_total'] = $vat_total;
			$data['s_wh_total'] = $wh_total;


			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$this->view['main'] =  $this->load->view('ssr/preview_draft',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }


   	public function ClearPreview() {
        
        $this->db->truncate('ssr_preview');

    }

}