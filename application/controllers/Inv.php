<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Inv extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Inv');
    }

    private function set_barcode($code)
    {
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
       	$code = $code.".png";
	    $store_image = imagepng($file,FCPATH."public/img/barcode/".$code);

    }

    public function All() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Inv/All";
	        $config["total_rows"] = $this->M_Inv->CountDraft($search);

	        $config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Inv->CountInv($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;

	       
	        $this->view['main'] =  $this->load->view('invbilling/show',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function Preview($dr_no = null){

    	if($this->session->userdata('logged_in')) { 	

    		$orderCus = $this->db->get_where('billing_order', array('dr_no' => $dr_no , 'is_use' => 0))->row();

    		$data['dr_no'] = $dr_no;

    		$setting_holdtax= $this->db->get('setting_holdtax')->row();

    		$data['with_hold'] = $setting_holdtax->holdtax;

			$this->db->join('setting_billing','setting_billing.id = billing_order.type');
			$this->db->where('billing_order.dr_no',$dr_no);
			$this->db->where('billing_order.is_use',0);
			$data['order']  = $this->db->get('billing_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$this->view['main'] =  $this->load->view('invbilling/preview',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

     public function RePreview($dr_no = null){

    	if($this->session->userdata('logged_in')) { 	

    		$check_data = $this->session->userdata('logged_in');
    		$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

    		if($user->reprint == '1' OR $user->role == 'SUPBILLING'){
    			$orderCus = $this->db->get_where('billing_order', array('dr_no' => $dr_no ))->row();

	    		$data['dr_no'] = $dr_no;

				$this->db->join('setting_billing','setting_billing.id = billing_order.type');
				$this->db->where('billing_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('billing_order')->result_array();

				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$data['with_hold'] = $orderCus->config_holdtax;

				$this->view['main'] =  $this->load->view('invbilling/re-preview',$data,true);
				$this->view();

    		} else {
    			redirect('Inv/All/', 'refresh');
    		}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function index(){
    	if($this->session->userdata('logged_in')) { 	


			$this->view['main'] =  $this->load->view('invbilling/index','',true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function Create(){
    	if($this->session->userdata('logged_in')) { 	

    		$type = $this->input->post('sort');
    		$numOrder = $this->input->post('numOrder');
    		$comp_his = $this->input->post('comp_his');

    	
			if ($type == 'none'){

							  $this->db->order_by("type", "ASC");
							  $this->db->where("is_delete",0);
							  $this->db->where("is_vat","n");
				$query_size = $this->db->get('setting_billing');
				$data['setting_billing'] = $query_size->result_array();
			} else if ($type == 'vat'){
				$this->db->order_by("type", "ASC");
							  $this->db->where("is_delete",0);
							  $this->db->where("is_vat","y");
				$query_size = $this->db->get('setting_billing');
				$data['setting_billing'] = $query_size->result_array();
			}

			if($numOrder == 0 or empty($numOrder)){
				$data['numOrder'] = 1;
			} else {
				$data['numOrder'] = $numOrder;
			}

			if(empty($comp_his)){
				$data['comp_his'] = '0';
			} else {
				$data['comp_his'] = $comp_his;
			}

			$data['inv_date'] =  date('Y-m-d');

			$data['type'] = $type;

			$query_customer = $this->db->get_where('customer');
			$data['customer'] = $query_customer->result_array();

			$this->view['main'] =  $this->load->view('invbilling/create',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function getRate(){

		$id_type = $this->input->post('id_type');

		$query_type = $this->db->get_where('setting_billing', array('id' => $id_type, 'is_delete' => 0));
		$rate = $query_type->row();

		if($rate){
			$result['rate'] = $rate->rate;	
		} else {
			$result['rate'] = 0;
		}

		
		echo json_encode($result);
		return false;
	}

	public function SaveMultiOrder(){

	  		$type = $this->input->post('type');
	  		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_real = $this->input->post('rate_real');
	  		$payment = $this->input->post('payment');
	  		$inv_date = $this->input->post('inv_date');


	  		 $configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('billing_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    	$i = 0;

	    	if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	}

	    	foreach ($type as $rsType) {

					$this->db->where("id", $rsType);
					$Qtype = $this->db->get('setting_billing')->row();

					if($Qtype->rate == 0){
						$data = array(	
							"ref_id" => $refId,
							"id_comp" => $id_comp,
							"qty" => $qty[$i],
							"type" => $Qtype->id,
							"is_vat" => $Qtype->is_vat,
							"cur_rate" => $rate_real[$i],
							"is_multi" => 1,
							"remark" => $remark,
							"payment" => $payment,
							"inv_date" => $inv_date,
							"config_holdtax" => $configtax->holdtax,
						   	"created" => date('Y-m-d H:i:s'),
						    "updated" => date('Y-m-d H:i:s')
						);
							
						$this->db->insert('billing_order', $data);
					} else {
						$data = array(	
							"ref_id" => $refId,
							"id_comp" => $id_comp,
							"qty" => $qty[$i],
							"type" => $Qtype->id,
							"is_vat" => $Qtype->is_vat,
							"cur_rate" => $Qtype->rate,
							"is_multi" => 1,
							"remark" => $remark,
							"payment" => $payment,
							"inv_date" => $inv_date,
							"config_holdtax" => $configtax->holdtax,
						   	"created" => date('Y-m-d H:i:s'),
						    "updated" => date('Y-m-d H:i:s')
						);
							
						$this->db->insert('billing_order', $data);
					}

					$result['refId'] = $refId;
				
	    	 $i++;}

	    	echo json_encode($result);
			return false;
    }


    public function SaveOnceOrder(){

    		$type = $this->input->post('type');
    		$qty = $this->input->post('qty');
	  		$id_comp = $this->input->post('id_comp');
	  		$remark1 = $this->input->post('remark1');
	  		$remark2 = $this->input->post('remark2');
	  		$remark3 = $this->input->post('remark3');
	  		$rate_real = $this->input->post('rate_real');
	  		$payment = $this->input->post('payment');
	  		$inv_date = $this->input->post('inv_date');

	  		$configtax = $this->db->get_where('setting_holdtax')->row();

	  			$this->db->limit(1);
				$this->db->order_by("ref_id", "desc");
				$run = $this->db->get('billing_order')->row();

	    		if(empty($run->ref_id)){
	    			$refId = 1;
	    		} else {
	    			$refId = $run->ref_id+1;
	    		}

	    		if($remark1 != null and $remark2 != null and $remark3 != null){	

		    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

		    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
		    		$remark = $remark1."<br>".$remark2;

		    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
		    		$remark = $remark1;
		    	}

				$this->db->where("id", $type);
				$Qtype = $this->db->get('setting_billing')->row();

				if($Qtype->rate == 0){
					$data = array(	
						"ref_id" => $refId,
						"id_comp" => $id_comp,
						"qty" => $qty,
						"type" => $Qtype->id,
						"is_vat" => $Qtype->is_vat,
						"cur_rate" => $rate_real,
						"is_multi" => 1,
						"remark" => $remark,
						"payment" => $payment,
						"inv_date" => $inv_date,
						"config_holdtax" => $configtax->holdtax,
					   	"created" => date('Y-m-d H:i:s'),
					    "updated" => date('Y-m-d H:i:s')
					);
						
					$this->db->insert('billing_order', $data);
				} else {
					$data = array(	
						"ref_id" => $refId,
						"id_comp" => $id_comp,
						"qty" => $qty,
						"type" => $Qtype->id,
						"is_vat" => $Qtype->is_vat,
						"cur_rate" => $Qtype->rate,
						"is_multi" => 1,
						"remark" => $remark,
						"payment" => $payment,
						"inv_date" => $inv_date,
						"config_holdtax" => $configtax->holdtax,
					   	"created" => date('Y-m-d H:i:s'),
					    "updated" => date('Y-m-d H:i:s')
					);
						
					$this->db->insert('billing_order', $data);
				}
	    		
				$result['refId'] = $refId;
				

	    	echo json_encode($result);
			return false;
    }

    public function Generate($RefId = null){

    	if($this->session->userdata('logged_in')) { 	

    		$orderCus = $this->db->get_where('billing_order', array('ref_id' => $RefId , 'is_use' => 0))->row();

			$order  = $this->db->get_where('billing_order', array('ref_id' => $RefId , 'is_use' => 0))->result_array();

			$DataCus = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

			$settingPre = $this->db->get_where('prefix_billing', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;

			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$last_dr = $this->db->get('status_ssr_draft')->row();

					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."000001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}

							$data_book = array(	
								"dr_no" => $gen_dr,
							    "updated" => date('Y-m-d H:i:s')
							);
							
							$this->db->where('ref_id', $RefId);
							$this->db->update('billing_order', $data_book);

			//////////////////Log Print////////////////////////////////
			

								$status_dr = array(
									"ref_id" => $RefId,
									"prefix_dr" => $prefix,
									"year_dr" => date('Y'),
									"dr_no" => $complete_dr,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_ssr_draft', $status_dr);
			//////////////////Log Print////////////////////////////////


			$this->set_barcode($gen_dr);

			redirect('/Inv/PrintDraft/'.$RefId, 'refresh');

    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function PrintDraft($RefId = null){

    	if($this->session->userdata('logged_in')) { 

    		$orderCus = $this->db->get_where('billing_order', array('ref_id' => $RefId))->row();

    		$check_data = $this->session->userdata('logged_in');

			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$data = array(
						"username" => $username->username,
						"print_no" => $orderCus->dr_no,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
					
			$this->db->insert('log_print_billing_draft', $data);

    		$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$printlogs = $this->db->get('log_print_billing_draft')->row();

			$data['datePrint'] = $orderCus->inv_date;

    		$data['dr_no'] = $orderCus->dr_no;

    		$data['typeOrder'] = $orderCus->type;

    		$data['payment'] = $orderCus->payment;

    		$data['with_hold'] =  $orderCus->config_holdtax;

    		$check_data = $this->session->userdata('logged_in');

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$this->db->join('setting_billing','setting_billing.id = billing_order.type');
			$this->db->where('billing_order.ref_id',$RefId);
			$data['order']  = $this->db->get('billing_order')->result_array();

			$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();


			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$this->view['main'] =  $this->load->view('invbilling/print_draft',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function SetReceipt($dr_no=null  , $none_hold = null, $re_date = null){
    	if($this->session->userdata('logged_in')) { 	

    		$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$this->db->where('is_use',0);
			$query_split = $this->db->get('billing_order');
			$data['order'] = $query_split->row();

			$this->db->where('status',0);
			$this->db->where('is_delete',0);
			$query_prefix= $this->db->get('prefix_billing_receipt')->row();


			$setting_holdtax= $this->db->get('setting_holdtax')->row();

					/////////Gen Invoice Number////////////////////////////////////////////////////////
						if(empty($data['order']->invoice_no)){

							$this->db->limit(1);
							$this->db->where('invoice_no is NOT NULL', NULL, FALSE);
							$this->db->order_by("invoice_no", "desc");
							$last_invoice = $this->db->get('status_ssr_invoice')->row();


							if(empty($last_invoice->invoice_no)){
								$rand_invoice = date('Y')."000001";
							} else {
								$rand_invoice = $last_invoice->invoice_no+1;
							}

							$status_invoice = array(	
								"dr_no" => $dr_no,
								"prefix_invoice" => $query_prefix->prefix,
								"invoice_no" => $rand_invoice,
								"created" => date('Y-m-d H:i:s'),
								"updated" => date('Y-m-d H:i:s')
							);
										
							$this->db->insert('status_ssr_invoice', $status_invoice);

							$data = array(
								"prefix_invoice" => $query_prefix->prefix,
								"invoice_no" => $rand_invoice,
								"is_use" => 2,
								"hold_tax" => $none_hold,
								"re_date" => $re_date,
								"config_holdtax" => $setting_holdtax->holdtax,
								"updated" => date('Y-m-d H:i:s')
							);

							$this->db->where('dr_no', $dr_no);
							$this->db->update('billing_order',$data);
						}
					/////////Gen Invoice Number////////////////////////////////////////////////////////

					 redirect('Inv/PrintReceipt/'.$dr_no, 'refresh');
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function RePrintReceipt($dr_no=null  , $none_hold = null){
    	if($this->session->userdata('logged_in')) { 	

			    $check_data = $this->session->userdata('logged_in');

			    $orderCus = $this->db->get_where('billing_order', array('dr_no' => $dr_no))->row();

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

						$data = array(
							"username" => $username->username,
							"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);

						$SetOrder = array(
								"hold_tax" => $none_hold,
								"updated" => date('Y-m-d H:i:s')
						);

						$this->db->where('dr_no', $dr_no);
						$this->db->update('billing_order',$SetOrder);
						
				$this->db->insert('log_re_print_billing_invoice', $data);

				$this->db->limit(1);
				$this->db->order_by("id", "desc");
				$printlogs = $this->db->get('log_print_billing_invoice')->row();

				$data['datePrint'] = $orderCus->re_date;

				$data['with_hold'] = $orderCus->config_holdtax;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['hold_status'] = $orderCus->hold_tax;

	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

				$data['user_print'] = $username;

				$this->db->join('setting_billing','setting_billing.id = billing_order.type');
				$this->db->where('billing_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('billing_order')->result_array();


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->view['main'] =  $this->load->view('invbilling/print_receipt',$data,true);
				$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function PrintReceipt($dr_no=null){

    	if($this->session->userdata('logged_in')) { 

    						 $this->db->limit(1);
							 $this->db->order_by("id", "desc");
    						 $this->db->where('billing_order.dr_no',$dr_no);
				$orderCus  = $this->db->get('billing_order')->row();

    			$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

						$data = array(
							"username" => $username->username,
							"invoice_no" => $dr_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
				$this->db->insert('log_print_billing_invoice', $data);

				$this->db->limit(1);
				$this->db->order_by("id", "desc");
				$printlogs = $this->db->get('log_print_billing_invoice')->row();

				$data['datePrint'] = $orderCus->re_date;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['hold_status'] = $orderCus->hold_tax;

	    		$data['with_hold'] = $orderCus->config_holdtax;

	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

				$data['user_print'] = $username;

				$this->db->join('setting_billing','setting_billing.id = billing_order.type');
				$this->db->where('billing_order.dr_no',$dr_no);
				$data['order']  = $this->db->get('billing_order')->result_array();


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->view['main'] =  $this->load->view('invbilling/print_receipt',$data,true);
				$this->view();



    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

    public function Cancel(){

    	$dr_no = $this->input->post('dr_no');
    	$remark_inv = $this->input->post('remark_inv');

    	$orderCus = $this->db->get_where('billing_order', array('dr_no' => $dr_no))->row();

    	$data_status = array(
					"remark_cancel" => $remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $dr_no);
		$this->db->update('billing_order',$data_status);

		$check_data = $this->session->userdata('logged_in');

	    $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$data = array(
					"username" => $username->username,
					"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
					"dr_no" => $orderCus->dr_no,
					"remark_cancel" => $remark_inv,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
						
		$this->db->insert('log_cancel_billing_invoice', $data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }

        public function CreateNote($dr_no=null, $RefNoted = null){

    	if($this->session->userdata('logged_in')) { 

    						 $this->db->limit(1);
							 $this->db->order_by("id", "desc");
    						 $this->db->where('billing_order.dr_no',$dr_no);
				$orderCus  = $this->db->get('billing_order')->row();

				$data['datePrint'] = $orderCus->re_date;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['hold_status'] = $orderCus->hold_tax;

	    		$data['with_hold'] = $orderCus->config_holdtax;

	    		

	    		$data['typeOrder'] = $orderCus->type;


				$this->db->join('setting_billing','setting_billing.id = billing_order.type');
				$this->db->where('billing_order.dr_no',$dr_no);
				$originOrder  = $this->db->get('billing_order')->result_array();

				$this->db->limit(1);
				$this->db->order_by("ref_noted", "desc");
				$last_ref_noted = $this->db->get('billing_noted')->row();

				if($last_ref_noted){
					$updated_ref_noted = $last_ref_noted->ref_noted+1;
				} else {
					$updated_ref_noted = 1;
				}

				if($RefNoted == null){
					foreach ($originOrder  as $key) {

							$data = array(
								"ref_id" => $key['ref_id'],
								"ref_noted" => $updated_ref_noted,
								"id_comp" => $key['id_comp'],
								"dr_no" => $key['dr_no'],
								"prefix_invoice" => $key['prefix_invoice'],
								"invoice_no" => $key['invoice_no'],
								"qty" => $key['qty'],
								"type" => $key['type'],
								"is_vat" => $key['is_vat'],
								"cur_rate" => $key['cur_rate'],
								"is_multi" => 1,
								"hold_tax" => $key['hold_tax'],
								"remark" => $key['remark'],
								"payment" => $key['payment'],
								"inv_date" => $key['inv_date'],
								"config_holdtax" => $key['config_holdtax'],
							   	"created" => date('Y-m-d H:i:s'),
							    "updated" => date('Y-m-d H:i:s')
							);
							
							$this->db->insert('billing_noted', $data);

					}
				}

				if($RefNoted){
					$data['RefNoted'] = $RefNoted;
				} else {
					$data['RefNoted'] = $updated_ref_noted;
				}

				$this->db->join('setting_billing','setting_billing.type = billing_noted.type');
				$this->db->where('billing_noted.dr_no',$dr_no);
				$this->db->where('billing_noted.status_noted','TEMP');
				$this->db->where('billing_noted.ref_noted',$data['RefNoted']);
				$this->db->select('billing_noted.*, setting_billing.type, setting_billing.description, setting_billing.rate');
				$data['order']  = $this->db->get('billing_noted')->result_array();


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

							 $this->db->limit(1);
							 $this->db->order_by("id", "desc");
    						 $this->db->where('billing_order.dr_no',$dr_no);
				$orderCus  = $this->db->get('billing_order')->row();

				$data['inv_no'] = $orderCus;

				$this->view['main'] =  $this->load->view('invbilling/create_note',$data,true);
				$this->view();



    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

     public function DelItemNoted(){

    	$idbilling = $this->input->post('idbilling');

    	$data_status = array(
					"status_noted" => 'DEL',
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $idbilling);
		$this->db->update('billing_noted',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }


        public function UpdatedNoted(){

    	$billing_id = $this->input->post('billing_id');
    	$RefNoted = $this->input->post('RefNoted');
    	$qty = $this->input->post('qty');
    	$rate_pro = $this->input->post('rate_pro');
    	$remark1 = $this->input->post('remark1');
	  	$remark2 = $this->input->post('remark2');
	  	$remark3 = $this->input->post('remark3');
    	$payment = $this->input->post('payment');
    	$noted_date = $this->input->post('noted_date');

    		$settingPre = $this->db->get_where('prefix_ssr_noted', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;

			if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	}

			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$last_dr = $this->db->get('status_ssr_noted')->row();

					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."0001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "000000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}

					$status_dr = array(
						"ref_id" => $RefNoted,
						"prefix_dr" => $prefix,
						"year_dr" => date('Y'),
						"dr_no" => $complete_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
															
					$this->db->insert('status_ssr_noted', $status_dr);

    	$i = 0;
    	foreach ($billing_id as $destination_id) {

	    	$data_status = array(
	    				"noted_no" => $gen_dr,
						"qty" => $qty[$i],
						"noted_rate" => $rate_pro[$i],
						"remark_noted" => $remark,
						"type_noted" => $payment,
						"status_noted" => 'USE',
			          	"created_noted" => $noted_date
			         );
			         
			$this->db->where('id', $destination_id);
			$this->db->update('billing_noted',$data_status);

		$i++;}

		$result['msg'] = $gen_dr;
		echo json_encode($result);
		return false;

    }

        public function UpdatedSingleNoted(){

    	$billing_id = $this->input->post('billing_id');
    	$RefNoted = $this->input->post('RefNoted');
    	$qty = $this->input->post('qty');
    	$rate_pro = $this->input->post('rate_pro');
    	$remark1 = $this->input->post('remark1');
	  	$remark2 = $this->input->post('remark2');
	  	$remark3 = $this->input->post('remark3');
    	$payment = $this->input->post('payment');
    	$noted_date = $this->input->post('noted_date');

    		$settingPre = $this->db->get_where('prefix_ssr_noted', array('status' => 0 , 'is_delete' => 0 ))->row();

			$prefix = $settingPre->prefix;

			if($remark1 != null and $remark2 != null and $remark3 != null){	

	    		$remark = $remark1."<br>".$remark2."<br>".$remark3;

	    	} else if($remark1 != null and $remark2 != null and $remark3 == null) {
	    		$remark = $remark1."<br>".$remark2;

	    	} else if($remark1 != null and $remark2 == null and $remark3 == null) {
	    		$remark = $remark1;
	    	}

			$this->db->limit(1);
			$this->db->order_by("id", "desc");
			$last_dr = $this->db->get('status_ssr_noted')->row();

					if(empty($last_dr->dr_no)){

						$gen_dr = $prefix."-".date('Y')."0001";
						$rand_dr = 1;
						$set_dr  = $rand_dr;

						if($set_dr <= 9){

							$complete_dr = "000000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "00000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "0000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "000".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "00".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else if($set_dr <= 99999){

							$complete_dr = "0".$set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;

						} else {

							$complete_dr = $set_dr;
							$gen_dr = $prefix."-".date('Y').$complete_dr;
						}

					}

					$status_dr = array(
						"ref_id" => $RefNoted,
						"prefix_dr" => $prefix,
						"year_dr" => date('Y'),
						"dr_no" => $complete_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
															
					$this->db->insert('status_ssr_noted', $status_dr);

	    	$data_status = array(
	    				"noted_no" => $gen_dr,
						"qty" => $qty,
						"noted_rate" => $rate_pro,
						"remark_noted" => $remark,
						"type_noted" => $payment,
						"status_noted" => 'USE',
			          	"created_noted" => $noted_date
			         );
			         
			$this->db->where('id', $billing_id);
			$this->db->update('billing_noted',$data_status);

		$result['msg'] = $gen_dr;
		echo json_encode($result);
		return false;

    }


    public function PrintNoted($noted_no=null){

    	if($this->session->userdata('logged_in')) { 

    						 $this->db->limit(1);
							 $this->db->order_by("id", "desc");
    						 $this->db->where('billing_noted.noted_no',$noted_no);
				$orderCus  = $this->db->get('billing_noted')->row();



    			$check_data = $this->session->userdata('logged_in');

	    		$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();
						
						$data = array(
							"username" => $username->username,
							"invoice_no" => $orderCus->dr_no,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);
						
				$this->db->insert('log_print_ssr_noted', $data);

				$data['datePrint'] = $orderCus->created_noted;

	    		$data['dr_no'] = $orderCus->dr_no;

	    		$data['hold_status'] = $orderCus->hold_tax;

	    		$data['with_hold'] = $orderCus->config_holdtax;

	    		$data['inv_no'] = $orderCus->prefix_invoice.$orderCus->invoice_no;

	    		$data['typeOrder'] = $orderCus->type;

				$data['user_print'] = $username;

				$this->db->join('setting_billing','setting_billing.type = billing_noted.type');
				$this->db->where('billing_noted.noted_no',$noted_no);
				$data['order']  = $this->db->get('billing_noted')->result_array();


				$data['cus'] = $this->db->get_where('customer', array('custom_id' => $orderCus->id_comp))->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->db->limit(1);
				 $this->db->order_by("id", "desc");
    			$this->db->where('billing_noted.noted_no',$noted_no);
				$orderV  = $this->db->get('billing_noted')->row();

				$data['v_data'] = $orderV;

				$this->view['main'] =  $this->load->view('invbilling/print_noted',$data,true);
				$this->view();



    	} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

    }

     public function NotedList() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Inv/NotedList";
	        $config["total_rows"] = $this->M_Inv->CountNoted($search);

	        $config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Inv->FetchNoted($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

	        $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;

	       
	        $this->view['main'] =  $this->load->view('invbilling/noted_list',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }

    public function CancelNoted(){

    	$noted_no = $this->input->post('noted_no');
    	$remark_inv = $this->input->post('remark_inv');

    	$orderCus = $this->db->get_where('billing_noted', array('noted_no' => $noted_no))->row();

    	$data_status = array(
					"remark_cancel" => $remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('noted_no', $noted_no);
		$this->db->update('billing_noted',$data_status);

		$check_data = $this->session->userdata('logged_in');

	    $username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

		$data = array(
					"username" => $username->username,
					"invoice_no" => $orderCus->prefix_invoice.$orderCus->invoice_no,
					"dr_no" => $orderCus->noted_no,
					"remark_cancel" => $remark_inv,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
						
		$this->db->insert('log_cancel_noted', $data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

    }
	
}