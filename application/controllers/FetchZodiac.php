<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require('Welcome.php');

class FetchZodiac extends Welcome {

	public function ZodiacOrder()
    {
        require_once('nusoap/nusoap.php');

        $client = new nusoap_client('http://www.lcit.com/Lcit.asmx?wsdl', 'WSDL');

        $trm =  $this->input->post('trm');

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$param = $trm;
		$answer = $client->call('drJson', array('trm' => $param), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }
	
	public function ZodiacCustomerdb()
	{

		$payer = $this->input->post('payer');

		$this->db->order_by('customer_code','ASC');
		$this->db->like('customer_code',$payer, 'both');
		$this->db->or_like('customer_name',$payer, 'both');
		$cus  = $this->db->get('customer')->result_array();


		foreach ($cus as $rs) {
			    $jsonData = $rs;
		}

		echo json_encode($cus);
		return false;


	}

		public function ZodiacCustomer()
	{

		$payer = $this->input->post('payer');

		if(!empty($payer)){
			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('CustomerJson',array('comp_nm' => $payer), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }



			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			echo $jsonData;
			return false;
		} else {
			echo '0';
			return false;
		}

	}


	public function ZodiacCustomerReNew()
	{

		$payer = $this->input->post('payer');

		if(!empty($payer)){
			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('CustomerReNewJson',array('comp_nm' => $payer), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }



			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			echo $jsonData;
			return false;
		} else {
			echo '0';
			return false;
		}

	}
	
	public function ZodiacCusBranch()
	{	

			$comp_id = $this->input->post('comp_id');

			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('CusBranch',array('comp_id' => $comp_id ), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$data = json_decode($jsonData, TRUE);

			foreach ($data as $comp) {
				$updateCus = array(
					"customer_branch" => $comp['TAX_OFFICE_AN'],
				    "updated" => date('Y-m-d H:i:s')
				 );
				         
				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$this->db->update('customer',$updateCus);
			}
			
			$this->db->where('custom_id', $comp['COMPANY_ID']);
			$originCus = $this->db->get('customer')->row();

			$updateBookCus = array(
				"company_branch" => $comp['TAX_OFFICE_AN'],
				"updated" => date('Y-m-d H:i:s')
			);
				         
			$this->db->where('code_comp', $originCus->customer_code);
			$this->db->update('normal_order',$updateBookCus);

			echo $jsonData;
			return false;

	}	

	public function UpdateCusBranch()
	{	


			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('UpdateCusBranch','', '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$data = json_decode($jsonData, TRUE);

			foreach ($data as $comp) {
				$updateCus = array(
					"customer_branch" => $comp['TAX_OFFICE_AN'],
				    "updated" => date('Y-m-d H:i:s')
				 );
				         
				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$this->db->update('customer',$updateCus);

				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$originCus = $this->db->get('customer')->row();

				$updateBookCus = array(
					"company_branch" => $comp['TAX_OFFICE_AN'],
					"updated" => date('Y-m-d H:i:s')
				);
					         
				$this->db->where('id_comp', $originCus->id);
				$this->db->update('normal_order',$updateBookCus);

			}
			

			echo $jsonData;
			return false;

	}	


	public function ZodiacCusPost()
	{	

			$comp_id = $this->input->post('comp_id');

			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('CusPost',array('comp_id' => $comp_id ), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$data = json_decode($jsonData, TRUE);

			foreach ($data as $comp) {
				$updateCus = array(
					"customer_post" => $comp['POSTAL_CODE_AN'],
				    "updated" => date('Y-m-d H:i:s')
				 );
				         
				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$this->db->update('customer',$updateCus);
			}
			
			$this->db->where('custom_id', $comp['COMPANY_ID']);
			$originCus = $this->db->get('customer')->row();

			$updateBookCus = array(
				"company_post" => $comp['POSTAL_CODE_AN'],
				"updated" => date('Y-m-d H:i:s')
			);
				         
			$this->db->where('code_comp', $originCus->customer_code);
			$this->db->update('normal_order',$updateBookCus);

			echo $jsonData;
			return false;
			return false;

	}	

	public function UpdateCusPost()
	{	


			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('UpdateCusPost','', '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$data = json_decode($jsonData, TRUE);

			foreach ($data as $comp) {
				$updateCus = array(
					"customer_post" => $comp['POSTAL_CODE_AN'],
				    "updated" => date('Y-m-d H:i:s')
				 );
				         
				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$this->db->update('customer',$updateCus);

				$this->db->where('custom_id', $comp['COMPANY_ID']);
				$originCus = $this->db->get('customer')->row();

				$updateBookCus = array(
					"company_post" => $comp['POSTAL_CODE_AN'],
					"updated" => date('Y-m-d H:i:s')
				);
					         
				$this->db->where('id_comp', $originCus->id);
				$this->db->update('normal_order',$updateBookCus);

			}
			

			echo $jsonData;
			return false;
	}	

	public function UpdatedCus(){

		$comp_code = $this->input->post('comp_code');

		if(!empty($comp_code)){
			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Customer.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('CustomerJson',array('comp_nm' => $comp_code), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$data = json_decode($jsonData, TRUE);



			foreach ($data as $comp) {
				$data_update = array(
					"custom_id" => $comp['COMPANY_ID'],
					"ar_company" => $comp['AR_COMPANY_ID'],
					"customer_code" => $comp['OPS_COMPANY_ID'],
					"customer_name" => $comp['COMPANY_NM'],
					"customer_address" => $comp['STREET_ADDRESS1_DS'],
					"customer_address2" => $comp['STREET_ADDRESS2_DS'],
					"customer_address3" => $comp['STREET_ADDRESS3_DS'],
					"telephone_no" => $comp['TELEPHONE_AN'],
					"tax_reg_no" => $comp['TAX_REG_NO'],
					"is_del" => "NO",
				    "updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('custom_id', $comp['COMPANY_ID']);
				$this->db->update('customer',$data_update);

				$data_update1 = array(
					"tax_reg_no" => $comp['TAX_REG_NO'],
					"code_comp" => $comp['OPS_COMPANY_ID'],
					"company_bill" => $comp['COMPANY_NM'],
					"address_bill" => $comp['STREET_ADDRESS1_DS'],
					"address_bill2" => $comp['STREET_ADDRESS2_DS'],
					"address_bill3" => $comp['STREET_ADDRESS3_DS'],
				    "updated" => date('Y-m-d H:i:s')
		          );
				$this->db->where('code_comp', $comp['OPS_COMPANY_ID']);
				$this->db->update('normal_order',$data_update1);
			}
			

			echo $jsonData;
			return false;
		} else {
			echo '0';
			return false;
		}


	}

	public function ZodiacVsl()
	{

		$vsl_visit = $this->input->post('vsl_order');

		if(!empty($vsl_visit)){
			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Iport.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('ServiceVesselSsr',array('vessel' => $vsl_visit), '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			echo $jsonData;
			return false;
		} else {
			echo '0';
			return false;
		}

	}


	public function PowerBi()
	{


			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Lcit.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('RtgReal','', '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }


			 echo '<pre>',print_r($answer,1),'</pre>';
			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			echo $jsonData;
			return false;


	}

	public function RTG_GMPH()
	{

			require_once('nusoap/nusoap.php');
			$client = new nusoap_client('http://www.lcit.com/Lcit.asmx?wsdl', 'WSDL');

			$error = $client->getError();
			if ($error) {
			    die("client construction error: {$error}\n");
			}

			$answer = $client->call('RtgGMPH','', '', '', false, true);

			$error = $client->getError();
			if ($error) {
			    print_r($client->response);
			    print_r($client->getDebug());
			    die();
			 }

			
		
			foreach ($answer as $rs) {
			    $jsonData = $rs;
			}

			$ca_gmhp = json_decode($jsonData);


			$avg = 0;

			foreach ($ca_gmhp as $rs) {
				$avg += $rs->GMPH;
			}


			$result['GMPH'] = $avg/count($ca_gmhp);

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.powerbi.com/beta/2bd16c9b-7e21-4274-9c06-7919f7647bbb/datasets/dbaf83ce-0c26-4642-b43d-5aca8ce1f15b/rows?key=221MibfUHx%2F3Sz8A%2FaObSUfD2rnQodCFpMHaNvCG7CPT6KFAUr4fU%2FtCsJUyb76eKnXbMkG%2FAJC6X368EKU8SA%3D%3D",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $jsonData,
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: text/plain"
			  ),
			));

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.powerbi.com/beta/2bd16c9b-7e21-4274-9c06-7919f7647bbb/datasets/494899dc-5be0-4453-bf84-5ef1f6398e13/rows?key=%2FiTmVBxREeTgN1iAKSx8BA5xsUaAHWpHYrCc1lJElA%2FGg9jzF6bJ8vQj1F8usboRPHWgkctn2Bvw7oUILGng7Q%3D%3D",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => json_encode($result),
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: text/plain"
			  ),
			));


			$response = curl_exec($curl);

			curl_close($curl);
			echo $response;

	}
}