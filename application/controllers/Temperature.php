<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Temperature extends Welcome {

	 public function __construct() {
	 	parent:: __construct();
        $this->load->helper("url");
    }

	public function Index(){

		$data['list_emp'] = '';
		$data['emp_temp'] = '';
	   $this->load->view('tempurature/index',$data);


	}

	public function SearchEmp(){

		$emp_code = $this->input->post('emp_code');
		$query_order = $this->db->get_where('list_emp', array('emp_code' => $emp_code));
		$data['list_emp'] = $query_order->result_array();

		$data['l_emp']  = $this->db->get_where('emp_temp', array('emp_code' => $emp_code, 'created' => date('Y-m-d')))->row();

		$this->db->limit(14);
		$this->db->order_by('emp_temp.created','desc');
		$this->db->join('list_emp ','list_emp.emp_code  = emp_temp.emp_code','left');
		$this->db->select('list_emp.emp_code, list_emp.emp_name, list_emp.emp_pos, list_emp.emp_dept, emp_temp.*');
		$this->db->where('emp_temp.created !=',date('Y-m-d'));
        $this->db->where('emp_temp.emp_code',$emp_code);
        $query = $this->db->get('emp_temp');
       	$data['emp_temp'] = $query->result_array();

		$this->load->view('tempurature/index',$data);
	}

	public function SaveTemp(){

		$emp_code = $this->input->post('emp_code');
	  	$emp_temp = $this->input->post('emp_temp');
	  	$type_temp = $this->input->post('type_temp');



	  	$query_order = $this->db->get_where('emp_temp', array('emp_code' => $emp_code, 'created' => date('Y-m-d')));
		$emp_temp = $query_order->row();

			if($emp_temp){

				$emp_code = $this->input->post('emp_code');
			  	$emp_temp = $this->input->post('emp_temp');
			  	$type_temp = $this->input->post('type_temp');

				if($type_temp == 'temp_1'){
					if($emp_temp->temp_1){

						redirect('Temperature/CheckEmp/'.$emp_code, 'refresh');

					} else {

						$data = array(	
							"emp_code" => $emp_code,
					        "temp_1" => $emp_temp,
					        "stamp_t1" => date('Y-m-d H:i:s')
						);

						$this->db->where('emp_code', $emp_code);
						$this->db->where('created', date('Y-m-d'));
						$this->db->update('emp_temp',$data);

					}
					
				} else if ($type_temp == 'temp_2'){
					if($emp_temp->temp_2){
						
						redirect('Temperature/CheckEmp/'.$emp_code, 'refresh');

					} else {

						$data = array(	
							"emp_code" => $emp_code,
					        "temp_2" => $emp_temp,
					        "stamp_t2" => date('Y-m-d H:i:s')
						);

						$this->db->where('emp_code', $emp_code);
						$this->db->where('created', date('Y-m-d'));
						$this->db->update('emp_temp',$data);

					}
				} else {
					if($emp_temp->temp_3){
						
						redirect('Temperature/CheckEmp/'.$emp_code, 'refresh');

					} else {

						$data = array(	
							"emp_code" => $emp_code,
					        "temp_3" => $emp_temp,
					        "stamp_t3" => date('Y-m-d H:i:s')
						);

						$this->db->where('emp_code', $emp_code);
						$this->db->where('created', date('Y-m-d'));
						$this->db->update('emp_temp',$data);

					}
				}


			} else {

					$emp_code = $this->input->post('emp_code');
				  	$emp_temp = $this->input->post('emp_temp');
				  	$type_temp = $this->input->post('type_temp');

					$data = array(	
						"emp_code" => $emp_code,
				        "temp_1" => $emp_temp,
				        "stamp_t1" => date('Y-m-d H:i:s'),
				        "created" => date('Y-m-d')
					);
		        				
				$this->db->insert('emp_temp', $data);

			}

		redirect('Temperature/CheckEmp/'.$emp_code, 'refresh');
	}


	public function CheckEmp($emp_code = null){


		$query_order = $this->db->get_where('list_emp', array('emp_code' => $emp_code));
		$data['list_emp'] = $query_order->result_array();

		$data['l_emp']  = $this->db->get_where('emp_temp', array('emp_code' => $emp_code, 'created' => date('Y-m-d')))->row();

		$this->db->limit(14);
		$this->db->order_by('emp_temp.created','desc');
		$this->db->join('list_emp ','list_emp.emp_code  = emp_temp.emp_code','left');
		$this->db->select('list_emp.emp_code, list_emp.emp_name, list_emp.emp_pos, list_emp.emp_dept, emp_temp.*');
		$this->db->where('emp_temp.created !=',date('Y-m-d'));
        $this->db->where('emp_temp.emp_code',$emp_code);
        $query = $this->db->get('emp_temp');
       	$data['emp_temp'] = $query->result_array();

		$this->load->view('tempurature/index',$data);
	}

	public function Report(){

		$s_date = $this->input->post('s_date');
		$e_date = $this->input->post('e_date');
		$emp_code = $this->input->post('emp_code');
		$emp_dept = $this->input->post('emp_dept');

       	$this->db->group_by('emp_dept ');
        $this->db->select('emp_dept');
        $query = $this->db->get('list_emp');
       	$data['list_dept']  = $query->result_array();



		$this->db->join('list_emp ','list_emp.emp_code  = emp_temp.emp_code','left');

		if($emp_code){
			 $this->db->where('emp_temp.emp_code',$emp_code);
		}

		if($emp_dept != 'all'){
			 $this->db->where('list_emp.emp_dept',$emp_dept);
		}

        $this->db->where("emp_temp.created BETWEEN '".$s_date." 00:00"."' AND '".$e_date." 23:59"."'");
        $this->db->select('list_emp.emp_code, list_emp.emp_name, list_emp.emp_pos, list_emp.emp_dept, emp_temp.*');
        $query = $this->db->get('emp_temp');
       	$data['list_emp']  = $query->result_array();

       	$data['s_date'] = $s_date;
       	$data['e_date'] = $e_date;
       	$data['emp_code'] = $emp_code;
       	$data['emp_dept'] = $emp_dept;


	   $this->load->view('tempurature/report',$data);


	}

	public function GenReport(){

		$rq_date = $this->input->post('rq_date');

		$this->db->join('list_emp ','list_emp.emp_code  = emp_temp.emp_code','left');
		$this->db->select('list_emp.emp_code, list_emp.emp_name, list_emp.emp_pos, list_emp.emp_dept, emp_temp.emp_temp, emp_temp.created');
        //$this->db->where("emp_temp.created BETWEEN '".$rq_date." 00:00"."' AND '".$rq_date." 23:59"."'");
        $query = $this->db->get('emp_temp');
       	$data = $query->result_array();
       	
        	header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=\"EmpReport".$rq_date.".csv\"");
            header("Pragma: no-cache");
            header("Expires: 0");

            $handle = fopen('php://output', 'w');

            foreach ($data as $data) {
                fputcsv($handle, $data);
            }
                fclose($handle);
            exit;
	}
}