<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Setting extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Cus');
    }

	public function Vat(){
		
		if($this->session->userdata('logged_in')) { 	

				//show vat list
				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0));
				$data['setting_vat'] = $query_vat->result_array();

				$this->view['main'] =  $this->load->view('setting/setting_vat',$data,true);
				$this->view();


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveVat(){

		$vat = $this->input->post('vat');
		$status = $this->input->post('status');
		$id = $this->input->post('id');
		$code_vat = $this->input->post('code_vat');

		$check_vat = $this->db->get_where('setting_vat', array('code_vat' => $code_vat , 'is_delete' => 0))->row();


		if(!empty($vat)){

			if(is_numeric($vat)){

					if(empty($code_vat)){

						$result['msg'] = "200";
						echo json_encode($result);
						return false;

					} else {

						if($id){
							$tonew  = array(
			          			"vat" => $vat,
			          			"code_vat" => $code_vat,
			          			"status" => $status,
			          			"updated" => date('Y-m-d H:i:s')
			          		);
							$this->db->where('id', $id);
							$this->db->update('setting_vat',$tonew);

						} else {

							if($check_vat){

								$result['msg'] = "100";
								echo json_encode($result);
								return false;

							} else {

								$data = array(
				          			"vat" => $vat,
				          			"code_vat" => $code_vat,
				          			"status" => $status,
				          			"created" => date('Y-m-d H:i:s'),
				          			"updated" => date('Y-m-d H:i:s')
				          		);
								$this->db->insert('setting_vat', $data);

							}
							
						}
				
						$result['msg'] = "success";
						echo json_encode($result);
						return false;

					}

			} else {

				$result['msg'] = "400";
				echo json_encode($result);
				return false;

			}
		} else {
				$result['msg'] = "500";
				echo json_encode($result);
				return false;
		}
		
	}

	public function RmvVat(){

		$id = $this->input->post('id');

		$tonew  = 	array(
			          	"is_delete" => 1,
			          	"updated" => date('Y-m-d H:i:s')
		          	);
					$this->db->where('id', $id);
					$this->db->update('setting_vat',$tonew);
		
	}

	public function SizeRate($id = null){
		
		if($this->session->userdata('logged_in')) { 	

				//show vat list
				$query_size = $this->db->get_where('setting_size_rate', array('is_delete' => 0  , 'type' => $id));
				$data['setting_size_rate'] = $query_size->result_array();

				$data['type'] = $this->db->get_where('setting_type', array('is_delete' => 0  , 'id' => $id))->row();

				$this->view['main'] =  $this->load->view('setting/setting_size',$data,true);
				$this->view();


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveSizeRate(){

		$id = $this->input->post('id');
		$size = $this->input->post('size');
		$rate = $this->input->post('rate');

		if(!empty($rate)){

			if(is_numeric($id) AND is_numeric($rate)){
				$data = array(

		          	"rate" => $rate,
		          	"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('id', $id);
				$this->db->update('setting_size_rate',$data);

				
				$result['msg'] = "success";
				echo json_encode($result);
				return false;

			} else {
				$result['msg'] = "600";
				echo json_encode($result);
				return false;
			}

		} else {
				$result['msg'] = "500";
				echo json_encode($result);
				return false;
		}
	}

	public function TypeDoc(){
		
		if($this->session->userdata('logged_in')) { 	

			$query_size = $this->db->get_where('setting_type', array('is_delete' => 0));
			$data['setting_type'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_typedoc',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveTypeDoc(){

		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$description = $this->input->post('description');

		if(empty($id)){

			if(!empty($type)) {

				$check_type = $this->db->get_where('setting_type', array('type' => $type , 'is_delete' => 0));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {
					$data_type = array(
						"status" => $status,
	          			"type" => $type,
	          			"description" => $description,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_type', $data_type);
					$id_type = $this->db->insert_id();


					$data_size1 = array(
						"size" => '20',
	          			"rate" => '',
	          			"type" => $id_type,
	          			"status" => 0,
	          			"is_delete" => 0,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_size_rate', $data_size1);

					$data_size2 = array(
						"size" => '40',
	          			"rate" => '',
	          			"type" => $id_type,
	          			"status" => 0,
	          			"is_delete" => 0,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_size_rate', $data_size2);

					$data_size3 = array(
						"size" => '45',
	          			"rate" => '',
	          			"type" => $id_type,
	          			"status" => 0,
	          			"is_delete" => 0,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_size_rate', $data_size3);

					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
					echo json_encode($result);
					return false;
			}

		} else {

			if(!empty($type)){
				$data = array(
		          	"status" => $status,
	          		"type" => $type,
	          		"description" => $description,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('id', $id);
				$this->db->update('setting_type',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
					echo json_encode($result);
					return false;
			}
		}
	}

	public function DeleteTypeDoc(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 1,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('setting_type',$data);

		redirect('Setting\TypeDoc');
	}

	public function Prefix(){
		
		if($this->session->userdata('logged_in')) { 	
			$query_size = $this->db->get_where('prefix_invoice', array('is_delete' => 0));
			$data['prefix_invoice'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_prefix',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function PrefixDraft(){
		
		if($this->session->userdata('logged_in')) { 	
			$query_size = $this->db->get_where('prefix_billing', array('is_delete' => 0));
			$data['prefix_invoice'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_billing_draft',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function PrefixReceipt(){
		
		if($this->session->userdata('logged_in')) { 	
			$query_size = $this->db->get_where('prefix_billing_receipt', array('is_delete' => 0));
			$data['prefix_invoice'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_billing_receipt',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SavePrefixReceipt(){

		$prefix = $this->input->post('prefix');

		if(!empty($prefix)){

				$tonew  = array(
          			"is_delete" => '1',
          			"status" => '1'
          		);

				$this->db->update('prefix_billing_receipt',$tonew);

				$data = array(
          			"prefix" => $prefix,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);
				$this->db->insert('prefix_billing_receipt', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;

		} else {

			$result['msg'] = "100";
			echo json_encode($result);
			return false;

		}
		
	}

	public function SavePrefixDraft(){

		$prefix = $this->input->post('prefix');

		if(!empty($prefix)){

				$tonew  = array(
          			"is_delete" => '1',
          			"status" => '1'
          		);

				$this->db->update('prefix_billing',$tonew);

				$data = array(
          			"prefix" => $prefix,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);
				$this->db->insert('prefix_billing', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;

		} else {

			$result['msg'] = "100";
			echo json_encode($result);
			return false;

		}
		
	}
	
	public function SavePrefix(){

		$prefix = $this->input->post('prefix');

		if(!empty($prefix)){

				$tonew  = array(
          			"is_delete" => '1',
          			"status" => '1'
          		);

				$this->db->update('prefix_invoice',$tonew);

				$data = array(
          			"prefix" => $prefix,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);
				$this->db->insert('prefix_invoice', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;

		} else {

			$result['msg'] = "100";
			echo json_encode($result);
			return false;

		}
		
	}

	public function Customer(){
		
		if($this->session->userdata('logged_in')) { 	

				//show vat list
				$query_customer = $this->db->get_where('customer');
				$data['customer'] = $query_customer->result_array();

				$this->view['main'] =  $this->load->view('setting/setting_customer',$data,true);
				$this->view();


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function GetCustomerZodiac(){

		$customer_code = $this->input->post('customer_code');
		$customer_name = $this->input->post('customer_name');
		$customer_address = $this->input->post('customer_address');
		$telephone_no = $this->input->post('telephone_no');
		$contact_person = $this->input->post('contact_person');
		$customer_mail = $this->input->post('customer_mail');

		$customer = $this->db->get_where('customer', array('customer_code' => $customer_code))->row();

			if(empty($customer)){

				$data_customer = array(	
						"customer_code" => $customer_code,
						"customer_name" => $customer_name,
						"customer_address" => $customer_address,
						"telephone_no" => $telephone_no,
						"contact_person" => $contact_person,
						"customer_mail" => $customer_mail,
				        "created" => date('Y-m-d H:i:s'),
				        "updated" => date('Y-m-d H:i:s')
				    );
					
				$this->db->insert('customer', $data_customer);

				$result['msg'] = "New recode Up to Date.";
				echo json_encode($result);
				return false;

			} else {
				$result['msg'] = "Up to Date.";
				echo json_encode($result);
				return false;
			}

	}

	public function SavePayer(){

		$id_customer = $this->input->post('id_customer');
		$customer_address = $this->input->post('customer_address');
		$customer_address2 = $this->input->post('customer_address2');
		$customer_address3 = $this->input->post('customer_address3');
		$customer_name = $this->input->post('customer_name');
		$customer_code = $this->input->post('customer_code');
		$telephone_an = $this->input->post('telephone_an');
		$tax_reg_no = $this->input->post('tax_reg_no');
		$ar_company = $this->input->post('ar_company_id');

		$customer = $this->db->get_where('customer', array('custom_id' => $id_customer))->row();

			if(empty($customer)){

				$data_customer = array(	
						"custom_id" => $id_customer,
						"customer_code" => $customer_code,
						"ar_company" => $ar_company,
						"customer_name" => $customer_name,
						"customer_address" => $customer_address,
						"customer_address2" => $customer_address2,
						"customer_address3" => $customer_address3,
						"telephone_no" => $telephone_an,
						"tax_reg_no" => $tax_reg_no,
				        "created" => date('Y-m-d H:i:s'),
				        "updated" => date('Y-m-d H:i:s')
				    );
					
				$this->db->insert('customer', $data_customer);

				$result['msg'] = "New recode Customer id : ".$id_customer;
				echo json_encode($result);
				return false;

			} else {
				$data_update = array(
					"custom_id" => $id_customer,
					"customer_code" => $customer_code,
					"ar_company" => $ar_company,
					"customer_name" => $customer_name,
					"customer_address" => $customer_address,
					"customer_address2" => $customer_address2,
					"customer_address3" => $customer_address3,
					"telephone_no" => $telephone_an,
					"tax_reg_no" => $tax_reg_no,
				    "updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('custom_id', $id_customer);
				$this->db->update('customer',$data_update);

				$result['msg'] = "Update Customer id :".$id_customer;
				echo json_encode($result);
				return false;
			}

	}

	public function SavePayerTax(){

		$id_customer = $this->input->post('id_customer');
		$customer_address = $this->input->post('customer_address');
		$customer_address2 = $this->input->post('customer_address2');
		$customer_address3 = $this->input->post('customer_address3');
		$customer_name = $this->input->post('customer_name');
		$customer_code = $this->input->post('customer_code');
		$telephone_an = $this->input->post('telephone_an');
		$tax_reg_no = $this->input->post('tax_reg_no');
		$country = $this->input->post('country');

		$customer = $this->db->get_where('customer', array('custom_id' => $id_customer))->row();


			if(empty($customer)){

				$data_customer = array(	
						"custom_id" => $id_customer,
						"customer_code" => $customer_code,
						"customer_name" => $customer_name,
						"customer_address" => $customer_address,
						"customer_address2" => $customer_address2,
						"customer_address3" => $customer_address3,
						"telephone_no" => $telephone_an,
						"tax_reg_no" => $tax_reg_no,
						"country" => $country,
				        "created" => date('Y-m-d H:i:s'),
				        "updated" => date('Y-m-d H:i:s')
				    );
					
				$this->db->insert('customer', $data_customer);

				$result['msg'] = "New recode Customer id : ".$id_customer;
				echo json_encode($result);
				return false;

			} else {
				$data_update = array(
					"custom_id" => $id_customer,
					"customer_code" => $customer_code,
					"customer_name" => $customer_name,
					"customer_address" => $customer_address,
					"customer_address2" => $customer_address2,
					"customer_address3" => $customer_address3,
					"telephone_no" => $telephone_an,
					"tax_reg_no" => $tax_reg_no,
					"country" => $country,
				    "updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('custom_id', $id_customer);
				$this->db->update('customer',$data_update);

				$result['msg'] = "Update Customer id : ".$id_customer;
				echo json_encode($result);
				return false;
			}

	}

	public function getRate(){

		$id_type = $this->input->post('id_type');
		$size_con = $this->input->post('size_con');

		$query_type = $this->db->get_where('setting_size_rate', array('size' => $size_con, 'type' => $id_type, 'is_delete' => 0));
		$rate_con = $query_type->row();

		$result['rate_con'] = $rate_con->rate;
		echo json_encode($result);
		return false;
	}

	public function getAllRate(){

		$id_type = $this->input->post('id_type');

		$query_type = $this->db->get_where('setting_size_rate', array('type' => $id_type, 'is_delete' => 0));
		$rate_con = $query_type->result_array();

		echo json_encode($rate_con);
		return false;
	}

	public function CheckCountry(){

		$in_country = $this->input->post('in_country');
		$check = $this->db->get_where('country', array('country_name like' => $in_country))->row();

		if($check){
			$result['msg'] = 'True';
			echo json_encode($result);
			return false;
		} else {
			$result['msg'] = 'False';
			echo json_encode($result);
			return false;
		}

	}
	
	public function UpdateCustomer(){
		
		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Setting/UpdateCustomer";
	        $config["total_rows"] = $this->M_Cus->CountCus($search);

	        $config["per_page"] = 20;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Cus->fetch_Cus($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();
	        $data['search'] = $search;
	       
	        $this->view['main'] =  $this->load->view('setting/updateCus',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function TypeBill(){
		
		if($this->session->userdata('logged_in')) { 	

			$type = $this->input->post('sort');

			if(empty($type) OR $type == 'all'){
				$query_size = $this->db->get_where('setting_billing', array('is_delete' => 0));
				$data['setting_billing'] = $query_size->result_array();
			} else if ($type == 'none'){
				$query_size = $this->db->get_where('setting_billing', array('is_delete' => 0 , 'is_vat' => 'n'));
				$data['setting_billing'] = $query_size->result_array();
			} else if ($type == 'vat'){
				$query_size = $this->db->get_where('setting_billing', array('is_delete' => 0 , 'is_vat' => 'y'));
				$data['setting_billing'] = $query_size->result_array();
			}

			$data['type_vat'] = $type;

			$this->view['main'] =  $this->load->view('setting/setting_billing',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

		public function SaveTypeBill(){

		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$description = $this->input->post('description');
		$vat = $this->input->post('vat'); 
		$rate = $this->input->post('rate');


		if(empty($id)){

			if(!empty($type)) {

				if(empty($description)){
					$result['msg'] = "300";
					echo json_encode($result);
					return false;
				}

				$check_type = $this->db->get_where('setting_billing', array('type' => $type , 'is_delete' => 0));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {
					$data_type = array(
						"status" => $status,
	          			"type" => $type,
	          			"description" => $description,
	          			"is_vat" => $vat,
	          			"rate" => $rate,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_billing', $data_type);
					$id_type = $this->db->insert_id();


					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}

		} else {

			if(empty($description)){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}


			if(!empty($type)){
				$data = array(
		          	"status" => $status,
	          		"type" => $type,
	          		"is_vat" => $vat,
	          		"rate" => $rate,
	          		"description" => $description,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('id', $id);
				$this->db->update('setting_billing',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}
		}
	}

	public function DeleteTypeBill(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 1,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('setting_billing',$data);

		redirect('Setting\TypeBill');
	}
	/*public function AutoCustomer(){

		$auto_customer = $this->input->post('auto_customer'); 

		//$this->db->like('customer_name', $auto_customer, 'After');
		$this->db->like('customer_name', $auto_customer, 'After');
		$this->db->or_like('customer_code', $auto_customer, 'After');
		$data_customer = $this->db->get('customer')->result_array();

		$query = $this->db->select('*')
            ->from('customer')
            ->where("(customer_name LIKE '".$auto_customer."%' OR customer_code LIKE '".$auto_customer."%'  )")
            ->get();


		if (count($data_customer) > 0) {
			foreach ($data_customer as $rs) {

				$result['customer_name'] = $rs['customer_name'];
				$result['customer_address'] = $rs['customer_address'];
				$result['customer_id'] = $rs['id'];
				
			}
			
		} else {
			$result['customer_name'] = 'Not Found';
			$result['customer_address'] = 'Not Found';
			$result['customer_id'] = '';
			

		}

	
	}*/

	public function TypeTariff(){
		
		if($this->session->userdata('logged_in')) { 	


			$query_size = $this->db->get_where('tariff_type', array('is_delete' => 'N'));
			$data['tariff_type'] = $query_size->result_array();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0));
			$data['setting_vat'] = $query_vat->result_array();

			$query_vat = $this->db->get_where('setting_holdtax', array('is_delete' => 0));
			$data['setting_holdtax'] = $query_vat->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_tariff',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveTypeTariff(){

		$tariff_id = $this->input->post('tariff_id');
		$tariff_code = $this->input->post('tariff_code');
		$tariff_des = $this->input->post('tariff_des');
		$tariff_rate = $this->input->post('tariff_rate');
		$is_delete = $this->input->post('is_delete'); 
		$code_wh = $this->input->post('code_wh');
		$code_vat = $this->input->post('code_vat');


		if(empty($tariff_id)){

			if(!empty($tariff_code)) {

				if(empty($tariff_des)){
					$result['msg'] = "300";
					echo json_encode($result);
					return false;
				}

				$check_type = $this->db->get_where('tariff_type', array('tariff_code' => $tariff_code , 'is_delete' => 'N'));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {

					$withhold = $this->db->get_where('setting_holdtax', array('code_wh' => $code_wh, 'is_delete' => '0'))->row();
					$vat = $this->db->get_where('setting_vat', array('code_vat' => $code_vat, 'is_delete' => '0'))->row();

					if($code_vat == 'N'){
						$c_vat = 0.00;
					} else {
						$c_vat = $vat->vat;
					}

					if($code_wh == 'N'){
						$c_wh = 0.00;
					} else {
						$c_wh = $withhold->holdtax;
					}

					$data_type = array(
	          			"tariff_code" => $tariff_code,
	          			"tariff_des" => $tariff_des,
	          			"tariff_rate" => $tariff_rate,
	          			"code_vat" => $code_vat,
	          			"code_wh" => $code_wh,
	          			"holdtax" => $c_wh,
	          			"vat" => $c_vat,
	          			"is_delete" => $is_delete,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('tariff_type', $data_type);
					$id_type = $this->db->insert_id();


					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}

		} else {

			if(empty($tariff_des)){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}


			if(!empty($tariff_code)){

				$withhold = $this->db->get_where('setting_holdtax', array('code_wh' => $code_wh, 'is_delete' => '0'))->row();
				$vat = $this->db->get_where('setting_vat', array('code_vat' => $code_vat, 'is_delete' => '0'))->row();

				$data = array(
	          		"tariff_code" => $tariff_code,
	          		"tariff_rate" => $tariff_rate,
	          		"tariff_des" => $tariff_des,
	          		"code_vat" => $code_vat,
	          		"code_wh" => $code_wh,
	          		"holdtax" => $withhold->holdtax,
	          		"vat" => $vat->vat,
	          		"is_delete" => $is_delete,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('tariff_id', $tariff_id);
				$this->db->update('tariff_type',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
				echo json_encode($result);
				return false;
			}
		}
	}

	public function DeleteTypeTariff(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 'Y',
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('tariff_id', $id);
		$this->db->update('tariff_type',$data);

		redirect('Setting\TypeTariff');
	}
	
	public function PrefixSsr(){
		
		if($this->session->userdata('logged_in')) { 	
			$query_size = $this->db->get_where('prefix_ssr', array('is_delete' => 0));
			$data['prefix_ssr'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_prefix_ssr',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SavePrefixSsr(){

		$prefix = $this->input->post('prefix');

		if(!empty($prefix)){

				$tonew  = array(
          			"is_delete" => '1',
          			"status" => '1'
          		);

				$this->db->update('prefix_ssr',$tonew);

				$data = array(
          			"prefix" => $prefix,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);
				$this->db->insert('prefix_ssr', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;

		} else {

			$result['msg'] = "100";
			echo json_encode($result);
			return false;

		}
		
	}

	public function PrefixSsrReceipt(){
		
		if($this->session->userdata('logged_in')) { 	
			$query_size = $this->db->get_where('prefix_ssr_receipt', array('is_delete' => 0));
			$data['prefix_ssr_receipt'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_prefix_ssr_receipt',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SavePrefixSsrReceipt(){

		$prefix = $this->input->post('prefix');

		if(!empty($prefix)){

				$tonew  = array(
          			"is_delete" => '1',
          			"status" => '1'
          		);

				$this->db->update('prefix_ssr_receipt',$tonew);

				$data = array(
          			"prefix" => $prefix,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);
				$this->db->insert('prefix_ssr_receipt', $data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;

		} else {

			$result['msg'] = "100";
			echo json_encode($result);
			return false;

		}
		
	}

	public function TypeBank(){
		
		if($this->session->userdata('logged_in')) { 	


			$query_size = $this->db->get_where('setting_bank', array('is_delete' => 0));
			$data['setting_bank'] = $query_size->result_array();

			$this->view['main'] =  $this->load->view('setting/setting_typebank',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveTypeBank(){

		$sb_id = $this->input->post('id');
		$sb_type = $this->input->post('sb_type');
		$status = $this->input->post('status');

		if(empty($sb_id)){

			if(!empty($sb_type)) {

				$check_type = $this->db->get_where('setting_bank', array('sb_type' => $sb_type , 'is_delete' => 0));

				if($check_type->num_rows() > 0 ){
					
					$result['msg'] = "200";
					echo json_encode($result);
					return false;

				} else {

					$data_size3 = array(
	          			"sb_type" => $sb_type,
	          			"status" => 0,
	          			"is_delete" => 0,
	          			"created" => date('Y-m-d H:i:s'),
	          			"updated" => date('Y-m-d H:i:s')
	          		);
					$this->db->insert('setting_bank', $data_size3);

					$result['msg'] = "success";
					echo json_encode($result);
					return false;

				}
			} else {
				$result['msg'] = "100";
					echo json_encode($result);
					return false;
			}

		} else {

			if(!empty($sb_type)){
				$data = array(
		          	"status" => $status,
	          		"sb_type" => $sb_type,
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('sb_id', $sb_id);
				$this->db->update('setting_bank',$data);

				$result['msg'] = "success";
				echo json_encode($result);
				return false;
			} else {
				$result['msg'] = "100";
					echo json_encode($result);
					return false;
			}
		}
	}

	public function DeleteTypeBank(){

		$id = $this->input->post('remove_id');

		$data = array(

	          	"is_delete" => 1,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('sb_id', $id);
		$this->db->update('setting_bank',$data);

		redirect('Setting\TypeBank');
	}


	public function CreatedCustomers(){

		$ar_company = $this->input->post('ar_company');
		$credit_term = $this->input->post('credit_term');
		$customer_code = $this->input->post('customer_code');
		$customer_name = $this->input->post('customer_name');
		$tax_reg_no = $this->input->post('tax_reg_no');
		$customer_branch = $this->input->post('customer_branch');
		$customer_address = $this->input->post('customer_address');
		$customer_address2 = $this->input->post('customer_address2');
		$customer_address3 = $this->input->post('customer_address3');
		$customer_post = $this->input->post('customer_post');
		$country = $this->input->post('country');
		$contact_person = $this->input->post('contact_person');
		$customer_mail = $this->input->post('customer_mail');
		$telephone_no = $this->input->post('telephone_no');
		$customers_id = $this->input->post('customers_id');


			if($customers_id){

				$data_customer = array(	
								"ar_company" => $ar_company,
								"customer_code" => $customer_code,
								"credit_term" => $credit_term,
								"customer_name" => $customer_name,
								"tax_reg_no" => $tax_reg_no,
								"customer_branch" => $customer_branch,
								"customer_address" => $customer_address,
								"customer_address2" => $customer_address2,
								"customer_address3" => $customer_address3,
								"country" => $country,
								"customer_post" => $customer_post,
								"contact_person" => $contact_person,
								"customer_mail" => $customer_mail,
								"telephone_no" => $telephone_no,
						        "updated" => date('Y-m-d H:i:s')
						    );
							
						$this->db->where('id', $customers_id);
						$this->db->update('customer',$data_customer);

						$result['msg'] = "Updated record Customer : ".$customer_code;
						echo json_encode($result);
						return false;

			} else {
				$cus_code = $this->db->get_where('customer', array('customer_code' => $customer_code))->row();

				if($cus_code){

					$result['msg'] = "100";
					echo json_encode($result);
					return false;

				} else {



					$data_customer = array(	
								"ar_company" => $ar_company,
								"customer_code" => $customer_code,
								"credit_term" => $credit_term,
								"customer_name" => $customer_name,
								"tax_reg_no" => $tax_reg_no,
								"customer_branch" => $customer_branch,
								"customer_address" => $customer_address,
								"customer_address2" => $customer_address2,
								"customer_address3" => $customer_address3,
								"country" => $country,
								"customer_post" => $customer_post,
								"contact_person" => $contact_person,
								"customer_mail" => $customer_mail,
								"telephone_no" => $telephone_no,
						        "created" => date('Y-m-d H:i:s'),
						        "updated" => date('Y-m-d H:i:s')
						    );
							
						$this->db->insert('customer', $data_customer);
						$insert_id = $this->db->insert_id();


						$data = array(
			          		"custom_id" => 'OSL'.$insert_id,
			          		"updated" => date('Y-m-d H:i:s')
				          );
				         
				        $this->db->where('id', $insert_id);
						$this->db->update('customer',$data);

						$result['msg'] = "New record Customer : ".$customer_code;
						echo json_encode($result);
						return false;
				}
			}

	}

	public function DeletedCustomers(){

		$cusid = $this->input->post('cusid');


				$data = array(
	          		"is_del" => 'YES',
	          		"updated" => date('Y-m-d H:i:s')
		          );
		         
		        $this->db->where('id', $cusid);
				$this->db->update('customer',$data);

				$result['msg'] = "New recode Customer : ".$customer_code;
				echo json_encode($result);
				return false;

	}


	public function Wh(){
		
		if($this->session->userdata('logged_in')) { 	

				//show vat list
				$query_vat = $this->db->get_where('setting_holdtax', array('is_delete' => 0));
				$data['setting_holdtax'] = $query_vat->result_array();

				$this->view['main'] =  $this->load->view('setting/setting_holdtax',$data,true);
				$this->view();


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function SaveWh(){

		$holdtax = $this->input->post('holdtax');
		$status = $this->input->post('status');
		$id = $this->input->post('id');
		$code_wh = $this->input->post('code_wh');

		$check_vat = $this->db->get_where('setting_holdtax', array('code_wh' => $code_wh , 'is_delete' => 0))->row();


		if(!empty($holdtax)){

			if(is_numeric($holdtax)){

					if(empty($code_wh)){

						$result['msg'] = "200";
						echo json_encode($result);
						return false;

					} else {

						if($id){
							$tonew  = array(
			          			"holdtax" => $holdtax,
			          			"code_wh" => $code_wh,
			          			"status" => $status,
			          			"updated" => date('Y-m-d H:i:s')
			          		);
							$this->db->where('id', $id);
							$this->db->update('setting_holdtax',$tonew);

						} else {

							if($check_vat){

								$result['msg'] = "100";
								echo json_encode($result);
								return false;

							} else {

								$data = array(
				          			"holdtax" => $holdtax,
				          			"code_wh" => $code_wh,
				          			"status" => $status,
				          			"created" => date('Y-m-d H:i:s'),
				          			"updated" => date('Y-m-d H:i:s')
				          		);
								$this->db->insert('setting_holdtax', $data);

							}
							
						}
				
						$result['msg'] = "success";
						echo json_encode($result);
						return false;

					}

			} else {

				$result['msg'] = "400";
				echo json_encode($result);
				return false;

			}
		} else {
				$result['msg'] = "500";
				echo json_encode($result);
				return false;
		}
		
	}

	public function RmvWh(){

		$id = $this->input->post('id');

		$tonew  = 	array(
			          	"is_delete" => 1,
			          	"updated" => date('Y-m-d H:i:s')
		          	);
					$this->db->where('id', $id);
					$this->db->update('setting_holdtax',$tonew);
		
	}

}