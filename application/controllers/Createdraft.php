<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Createdraft extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$data['order_remain'] = $this->db->get_where('book_order', array('is_sub' => '2', 'book_con !=' => '0'))->result_array();


			$this->view['main'] =  $this->load->view('pro/create_draft',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	private function set_barcode($code)
    {
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
       	$code = $code.".png";
	    $store_image = imagepng($file,FCPATH."public/img/barcode/".$code);

    }

	public function CancelDrNormal(){
		$id = $this->input->post('id_dr');
		$remark_inv = $this->input->post('remark_inv');

		$data = array(

		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $id);
		$this->db->update('status_dr',$data);

		$change_status = $this->db->get_where('status_dr',array('id' => $id))->row();

		$data_status = array(
					"remark_cancel" => "Document | ".$remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $change_status->normal_id);
		$this->db->update('normal_order',$data_status);

		$change_order = $this->db->get_where('book_order',array('id' => $change_status->trm_doc))->row();

		$data_order = array(
					"remark_cancel" => "Document | ".$remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $change_order->id);
		$this->db->update('book_order',$data_order);

		$origin_order = $this->db->get_where('book_order',array('doc_ref_book' => $change_order->doc_ref_book, 'is_sub' => 2))->row();

		$data_order = array(

		          	"book_con" => $change_order->book_con + $origin_order->book_con,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('doc_ref_book', $change_order->doc_ref_book);
		$this->db->where('is_sub', 2);
		$this->db->update('book_order',$data_order);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
	}

	public function reCancelDrNormal(){
		$id = $this->input->post('id');

		$data = array(

		          	"is_use" => 0,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $id);
		$this->db->update('status_dr',$data);

		$change_status = $this->db->get_where('status_dr',array('id' => $id))->row();

		$data_status = array(

		          	"is_use" => 0,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $change_status->normal_id);
		$this->db->update('normal_order',$data_status);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
	}

	public function CancelDrSplit(){
		$id = $this->input->post('id');

		$data = array(

		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $id);
		$this->db->update('split_order',$data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
	}

	public function reCancelDrSplit(){
		$id = $this->input->post('id');

		$data = array(

		          	"is_use" => 0,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $id);
		$this->db->update('split_order',$data);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
	}

	public function SaveDR(){

		$SIZETYPE_SIZE_AN = $this->input->post('SIZETYPE_SIZE_AN');
		$SIZETYPE_TYPE_AN = $this->input->post('SIZETYPE_TYPE_AN');
		$CREATE_TM = $this->input->post('CREATE_TM');
		$BOOKED_COUNT = $this->input->post('BOOKED_COUNT');
		$OPER_GRP_C = $this->input->post('OPER_GRP_C');
		$CUSTOMER_NAME = $this->input->post('CUSTOMER_NAME');
		$CUSTOMER_ADDRESS = $this->input->post('CUSTOMER_ADDRESS');
		$AREA_C = $this->input->post('AREA_C');
		$TERMINAL_DOC_REF_ID = $this->input->post('TERMINAL_DOC_REF_ID');
		$BOOKING_AN = $this->input->post('BOOKING_AN');
		$LAST_GATEPASS_PRINT_USER_AN = $this->input->post('LAST_GATEPASS_PRINT_USER_AN');
		$LAST_GATEPASS_PRINT_TM = $this->input->post('LAST_GATEPASS_PRINT_TM');
		$LANDSIDE_BILL_METHOD_CODE = $this->input->post('LANDSIDE_BILL_METHOD_CODE');
		$REMARKS_DS = $this->input->post('REMARKS_DS');
		$LAST_UPDATE_TM = $this->input->post('LAST_UPDATE_TM');
		$TERMINAL_DOC_REF_AN = $this->input->post('TERMINAL_DOC_REF_AN');
		$TRML_DOC_REF_BOOKING_ID = $this->input->post('TRML_DOC_REF_BOOKING_ID');

		$check_drs = $this->db->get_where('split_order', array('terminal_doc_ref_an' => $TERMINAL_DOC_REF_AN));

		$check_drn = $this->db->get_where('normal_order',array('terminal_doc_ref_an' => $TERMINAL_DOC_REF_AN))->row();

		$checkBook = $this->db->get_where('book_order',  array('doc_ref_book' =>  $TRML_DOC_REF_BOOKING_ID))->row();


		$MergeBook = $this->db->get_where('book_order',  array('book_an' =>  $BOOKING_AN))->row();

			$this->db->limit(1);
			$this->db->where('book_an', $BOOKING_AN);
			$this->db->order_by("id_merge", "desc");
			$checkLogs = $this->db->get('logs_merge_book')->row();

			if(empty($checkLogs)){

				$logsMerge = array(
					"cur_book" => $BOOKED_COUNT,
				    "origin_book" => 0,
				    "book_an" => $BOOKING_AN,
				    "add_book" =>  $BOOKED_COUNT,
				    "doc_ref_book" =>  $TRML_DOC_REF_BOOKING_ID,
				    "date_merge" => date('Y-m-d H:i:s')
				);

				   
				$this->db->insert('logs_merge_book', $logsMerge);
			}


		if(!empty($MergeBook)){
			if($MergeBook->size_con == $SIZETYPE_SIZE_AN And $MergeBook->type_con == $SIZETYPE_TYPE_AN  And $MergeBook->book_an == $BOOKING_AN){


				$this->db->limit(1);
				$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
				$DiffMerge = $this->db->get('logs_merge_book')->row();

				$this->db->limit(1);
				$this->db->order_by('id_merge' , 'asc');
				$this->db->where('book_an', $BOOKING_AN);
				$FirstOrders = $this->db->get('logs_merge_book')->row();

				$this->db->limit(1);
				$this->db->order_by('id_merge' , 'desc');
				$this->db->where('book_an', $BOOKING_AN);
				$LastOrders = $this->db->get('logs_merge_book')->row();

				if(!empty($DiffMerge)){

					if($DiffMerge->add_book == $BOOKED_COUNT){
						$Merge = 2;
					} else if($DiffMerge->add_book != $BOOKED_COUNT){
						if($DiffMerge->add_book > $BOOKED_COUNT){


							$dataFirst = $this->db->get_where('logs_merge_book', array('id_merge' => $FirstOrders->id_merge))->row();

							$CountBook = $this->db->get_where('logs_merge_book', array('book_an' => $DiffMerge->book_an))->result_array();


							$addBook = $DiffMerge->add_book - $BOOKED_COUNT;

							if(!empty($DiffMerge)){

								$updateLogs = array(
									"cur_book" => $DiffMerge->cur_book - $addBook,
									"add_book" => $DiffMerge->add_book - $addBook,
								);
								         
								$this->db->where('doc_ref_book', $DiffMerge->doc_ref_book);
								$this->db->update('logs_merge_book',$updateLogs);

								for ($i=1; $i <= count($CountBook) ; $i++) { 

									$idBuild =  (int)$DiffMerge->id_merge + $i;

									$datB = $this->db->get_where('logs_merge_book', array('id_merge' => $idBuild))->row();

									if($DiffMerge->book_an == $BOOKING_AN){
											$updateLogs = array(
												"cur_book" => $datB->cur_book - $addBook,
												"origin_book" => $datB->origin_book - $addBook,
									         );
									         
										   	$this->db->where('id_merge', $idBuild);
											$this->db->update('logs_merge_book',$updateLogs);
									} 

								}

							
							} else {

								$logsMerge = array(
									"cur_book" => $DiffMerge->cur_book - $addBook,
								    "origin_book" => $DiffMerge->cur_book,
								    "book_an" => $BOOKING_AN,
								    "add_book" =>  "-".$addBook,
								    "doc_ref_book" =>  $TRML_DOC_REF_BOOKING_ID,
								    "date_merge" => date('Y-m-d H:i:s')
								);
									   
								$this->db->insert('logs_merge_book', $logsMerge);

							}

								$this->db->limit(1);
								$this->db->where('is_sub', 2);
								$this->db->where('doc_ref_book', $FirstOrders->doc_ref_book);
								$BookOrder = $this->db->get('book_order')->row();

								$update_book = array(
									"book_con" => $BookOrder->book_con- $addBook,
							        "origin_book_con" => $BookOrder->origin_book_con  - $addBook,
								    "updated" => date('Y-m-d H:i:s')
						         );
						         
							   	$this->db->where('book_an', $BOOKING_AN);
							   	$this->db->where('is_sub', 2);
							   	$this->db->where('doc_ref_book', $FirstOrders->doc_ref_book);
								$this->db->update('book_order',$update_book);

							$Merge = 1;

						} else if($DiffMerge->add_book < $BOOKED_COUNT) {

							$addBook = $BOOKED_COUNT - $DiffMerge->add_book;

							$dataFirst = $this->db->get_where('logs_merge_book', array('id_merge' => $FirstOrders->id_merge))->row();

							$CountBook = $this->db->get_where('logs_merge_book', array('book_an' => $DiffMerge->book_an))->result_array();

							

							if(!empty($DiffMerge)){

								$updateLogs = array(
									"cur_book" => $DiffMerge->cur_book + $addBook,
									"add_book" => $DiffMerge->add_book + $addBook,
								);
								         
								$this->db->where('doc_ref_book', $DiffMerge->doc_ref_book);
								$this->db->update('logs_merge_book',$updateLogs);

								for ($i=1; $i <= count($CountBook) ; $i++) { 

									$idBuild =  (int)$DiffMerge->id_merge + $i;

									$datB = $this->db->get_where('logs_merge_book', array('id_merge' => $idBuild))->row();

									if($DiffMerge->book_an == $BOOKING_AN){
											$updateLogs = array(
												"cur_book" => $datB->cur_book + $addBook,
												"origin_book" => $datB->origin_book + $addBook,
									         );
									         
										   	$this->db->where('id_merge', $idBuild);
											$this->db->update('logs_merge_book',$updateLogs);
									} 

								}


							} else {

								$logsMerge = array(
									"cur_book" => $DiffMerge->cur_book + $addBook,
								    "origin_book" => $DiffMerge->cur_book,
								    "book_an" => $BOOKING_AN,
								    "add_book" =>  $addBook,
								    "doc_ref_book" =>  $TRML_DOC_REF_BOOKING_ID,
								    "date_merge" => date('Y-m-d H:i:s')
								);
									   
								$this->db->insert('logs_merge_book', $logsMerge);

							}



							$this->db->limit(1);
							$this->db->where('is_sub', 2);
							$this->db->where('doc_ref_book', $FirstOrders->doc_ref_book);
							$BookOrder = $this->db->get('book_order')->row();


							$update_book = array(
								"book_con" => $BookOrder->book_con + $addBook,
						        "origin_book_con" => $BookOrder->origin_book_con + $addBook,
							    "updated" => date('Y-m-d H:i:s')
					         );
					         
						   	$this->db->where('book_an', $BOOKING_AN);
						   	$this->db->where('is_sub', 2);
						   	$this->db->where('doc_ref_book', $FirstOrders->doc_ref_book);
							$this->db->update('book_order',$update_book);

							$Merge = 1;
						}

					}

				} else {


						$this->db->limit(1);
						$this->db->order_by("id_merge", "desc");
						$checkMerge = $this->db->get('logs_merge_book')->row();

						if(!empty($checkMerge)){
							$originBook = $checkMerge->cur_book;
						} else {
							$originBook = $MergeBook->book_con;
						}

						$logsMerge = array(
							"cur_book" => $originBook+ $BOOKED_COUNT,
						    "origin_book" => $originBook,
						    "book_an" => $BOOKING_AN,
						    "add_book" =>  $BOOKED_COUNT,
						    "doc_ref_book" =>  $TRML_DOC_REF_BOOKING_ID,
						    "date_merge" => date('Y-m-d H:i:s')
						);

						$this->db->insert('logs_merge_book', $logsMerge);



						$updateBook = array(
							"book_con" => $MergeBook->book_con + $BOOKED_COUNT,
						    "origin_book_con" => $MergeBook->origin_book_con + $BOOKED_COUNT,
							"updated" => date('Y-m-d H:i:s')
					    );
						$this->db->where('is_sub', 2);						
						$this->db->where('book_an', $MergeBook->book_an);
						$this->db->update('book_order', $updateBook);

						$Merge = 1;

				}



			} else {
				$Merge = 0;
			}

		}  else {
				$Merge = 0;
		}

		//////////////////
		$this->db->limit(1);
		$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
		$this->db->order_by("id_merge", "desc");
		$checkInsert = $this->db->get('logs_merge_book')->row();


		if(!empty($checkBook) AND $Merge == 0){

			if($checkBook->origin_book_con > $BOOKED_COUNT){
				$diffBook =  $checkBook->origin_book_con - $BOOKED_COUNT;
			} else if($checkBook->origin_book_con < $BOOKED_COUNT) {
				$diffBook =  $BOOKED_COUNT- $checkBook->origin_book_con;
			} else {
				$diffBook = 0;
			}

		}   else {
				$diffBook = 0;
			}

		if(!empty($check_drn->dr_no)){


			if($diffBook != 0  AND $Merge == 0){

				$update_book = array(
					"book_con" => $checkBook->book_con + $diffBook,
			        "origin_book_con" => $BOOKED_COUNT,
				    "updated" => date('Y-m-d H:i:s')
		         );
		         
			   	$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
			   	$this->db->where('is_sub', 2);
				$this->db->update('book_order',$update_book);

				$result['msg'] = "Already Draft";
				$result['process'] = "Updated doc_ref_book : ".$TRML_DOC_REF_BOOKING_ID;
				$result['is_use'] = "1001";
				echo json_encode($result);
				return false;

			}

		}

		if($check_drs->num_rows() > 0){

			if($diffBook != 0 AND $Merge == 0){

				$update_book = array(
					"book_con" => $checkBook->book_con + $diffBook,
		          	"origin_book_con" => $BOOKED_COUNT,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
			   	$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
			   	$this->db->where('is_sub', 2);
				$this->db->update('book_order',$update_book);

				$result['msg'] = "Already Draft";
				$result['process'] = "Updated doc_ref_book : ".$TRML_DOC_REF_BOOKING_ID;
				$result['is_use'] = "1001";
				echo json_encode($result);
				return false;

			}
		}


		if(!empty($SIZETYPE_SIZE_AN)){

			$check_comp = $this->db->get_where('company_deal', array('code_comp' => $OPER_GRP_C , 'is_use' => 0));
			$check = $this->db->get_where('book_order', array('doc_ref_book' => $TRML_DOC_REF_BOOKING_ID , 'is_use' => 0));
			$check_print = $this->db->get_where('book_order', array('doc_ref_book' => $TRML_DOC_REF_BOOKING_ID , 'is_use' => 1));


			if($check->num_rows() > 0){


				if($diffBook != 0  AND $Merge == 0){

					$update_book = array(
						"book_con" => $checkBook->book_con + $diffBook,
			          	"origin_book_con" => $BOOKED_COUNT,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
				   	$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
				   	$this->db->where('is_sub', 0);
					$this->db->update('book_order',$update_book);

					$result['msg'] = "Already Draft";
					$result['process'] = "Updated doc_ref_book : ".$TRML_DOC_REF_BOOKING_ID;
					$result['is_use'] = "1001";
					echo json_encode($result);
					return false;

				} else {

					$result['msg'] = "Already Data";
					$result['is_use'] = "1001";
					echo json_encode($result);
					return false;

				}


			} else if ($check_print->num_rows() > 0){


				if($diffBook != 0  AND $Merge == 0){

					$update_book = array(
						"book_con" => $checkBook->book_con + $diffBook,
			          	"origin_book_con" => $BOOKED_COUNT,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			         
				   	$this->db->where('doc_ref_book', $TRML_DOC_REF_BOOKING_ID);
				   	$this->db->where('is_sub', 0);
					$this->db->update('book_order',$update_book);

					$result['msg'] = "Already Draft";
					$result['process'] = "Updated doc_ref_book : ".$TRML_DOC_REF_BOOKING_ID;
					$result['is_use'] = "1001";
					echo json_encode($result);
					return false;

				} else {


					$result['msg'] = "Already Data";
					echo json_encode($result);
					return false;

				}

			} else {


				if($check_comp->num_rows() > 0){

					$query_comp = $this->db->get_where('company_deal', array('code_comp' => $OPER_GRP_C , 'is_use' => 0));
					$data['comp'] = $query_comp->result_array();

					foreach ($data['comp'] as $rs) {
						$id_comp = $rs['id'];
					}

					
				} else {
					$data = array(
									"company_bill" => $CUSTOMER_NAME,
				          			"address_bill" => $CUSTOMER_ADDRESS,
				          			"code_comp" => $OPER_GRP_C,
				          			"created" => date('Y-m-d H:i:s'),
				          			"updated" => date('Y-m-d H:i:s')
				    );

				   
					$this->db->insert('company_deal', $data);
					$id_comp = $this->db->insert_id();
				}

				$query_rate= $this->db->get_where('setting_size_rate', array('is_delete' => 0));
				$data['rate'] = $query_rate->result_array();

				$query_vat= $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0));
				$data['vat'] = $query_vat->result_array();

				$configtax = $this->db->get_where('setting_holdtax')->row();


				foreach ($data['rate'] as $rs_rate) {
					if($rs_rate['size'] == $SIZETYPE_SIZE_AN){
						$cur_rate = $rs_rate['rate'];
					}
				}

					if($Merge == 0){
						$data = array(
					        "id_comp" => $id_comp,
					        "type" => '',
					        "payment" => $LANDSIDE_BILL_METHOD_CODE,
					        "atb" => $CREATE_TM,
					        "atd" => $LAST_UPDATE_TM,
					        "size_con" => $SIZETYPE_SIZE_AN,
					        "type_con" => $SIZETYPE_TYPE_AN,
					        "book_con" => $BOOKED_COUNT,
					        "is_sub" => 2,
					        "origin_book_con" => $BOOKED_COUNT,
					        "terminal_doc_ref_id" => $TERMINAL_DOC_REF_ID,
					        "terminal_doc_ref_an" => $TERMINAL_DOC_REF_AN,
					        "doc_ref_book" => $TRML_DOC_REF_BOOKING_ID,
					        "book_an" => $BOOKING_AN,
					        "area" => $AREA_C,
					        "remark" => $REMARKS_DS,
					        "cur_rate" => $cur_rate,
					        "config_holdtax" => $configtax->holdtax,
					        "created" => date('Y-m-d H:i:s'),
					        "updated" => date('Y-m-d H:i:s')
					    );

					   
						$this->db->insert('book_order', $data);
						$insert_id = $this->db->insert_id();
					} else {
						
					}

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0));
				$data['setting_vat'] = $query_vat->result_array();

				if(empty($insert_id)){
					$insert_id = 0;
				} else {
					$insert_id = $insert_id;
				}

				$result['msg'] = "success";
				$result['id'] = $insert_id;
				echo json_encode($result);
				return false;
			}

		}

	}

	public function PrintDraft($trm = null){


		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');


		$username = $this->db->get_where('user', array('id' => $check_data['id']))->result_array();
		$data['username'] = $username;

		foreach ($data['username'] as $rs) {
			$username = $rs['username'];
		}

		$query_order= $this->db->get_where('book_order', array('terminal_doc_ref_id' => $trm , 'is_use' => 0));
		$data['order'] = $query_order->result_array();

		foreach ($data['order'] as $rs) {
			$id_order = $rs['id'];
		}
		
		$data = array(
			"username" => $username,
			"print_no" => $id_order,
			"created" => date('Y-m-d H:i:s'),
			"updated" => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('log_print', $data);
		$id_comp = $this->db->insert_id();

		///////////////////Auto Draft NO://///////////////////////

		$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'terminal_doc_ref_id' => $trm));
		$data['order'] = $query_order->result_array();

		foreach ($data['order'] as $rs) {
			//$id_comp = $rs['id_comp'];
			$id_order = $rs['id'];
			$trm_an = $rs['terminal_doc_ref_an'];
		}


		$query_check_dr_no = $this->db->get_where('head_all_order', array('terminal_doc_ref_an' => $trm_an));
		$data['check_dr_no'] = $query_check_dr_no->result_array();

		$this->db->limit(1);
		$this->db->order_by("id", "desc");
		$last_dr = $this->db->get('status_dr')->row();

		foreach ($data['check_dr_no'] as $rs_dr_no) {
			$dr_no = $rs_dr_no['dr_no'];
			$dr_id = $rs_dr_no['id'];
			$id_comp = $rs_dr_no['id_comp'];
		}

			if(empty($dr_no)){ 

				if(empty($last_dr->dr_no)){

					$gen_dr = 'DRN-'.date('Y')."00001";

				} else {
					
					$rand_dr = (int)$last_dr->dr_no;	
					$set_dr  = $rand_dr + 1;

					if($set_dr <= 9){

						$complete_dr = "0000".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

					}  else if($set_dr <= 99){

						$complete_dr = "000".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

					}  else if($set_dr <= 999){

						$complete_dr = "00".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

					} else if($set_dr <= 9999){

						$complete_dr = "0".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

					}  else {

						$complete_dr = $set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

					}

				}

				$data = array(

		          	"dr_no" => $gen_dr,
		          	"print_date" => date('Y-m-d H:i:s'),
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		        $this->db->where('terminal_doc_ref_an', $trm_an);
				$this->db->update('head_all_order',$data);
			}

		///////////////////Auto Draft NO://///////////////////////


		$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
		$data['order'] = $query_order->result_array();

			foreach ($data['order'] as $rs) {
				$id_comp = $rs['id_comp'];
				$size = $rs['size_con'];
			}

		$query_size= $this->db->get_where('setting_size_rate', array('is_delete' => 0 , 'size' => $size));
		$data['size'] = $query_size->result_array();

		$query_vat= $this->db->get_where('setting_vat', array('is_delete' => 0 ));
		$data['vat'] = $query_vat->result_array();

		$query_comp = $this->db->get_where('company_deal', array('is_use' => 0 , 'id' => $id_comp));
		$data['comp'] = $query_comp->result_array();

		$this->view['main'] =  $this->load->view('pro/draft_invoice',$data,true);
		$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}


	}

	public function PrintDR($id_order = null , $id_comp = null , $type_dr = null){

		if($this->session->userdata('logged_in')) { 	

				$this->db->limit(1);
		$this->db->where('id',$type_dr);
		$typeDraft = $this->db->get('setting_type');
		$typeDraft  = $typeDraft->row();

		$this->db->where('id',$id_order);
		$book_order = $this->db->get('book_order')->row();

		$this->db->where('size',$book_order->size_con);
		$this->db->where('type',$type_dr);
		$query_rate_con = $this->db->get('setting_size_rate');
		$rate_con  = $query_rate_con->row();

		$this->db->limit(1);
		$this->db->where('id',$id_order);
		$book_order = $this->db->get('book_order');
		$book_order  = $book_order->row();

		$this->db->limit(1);
		$this->db->where('trm_doc',$book_order->id);
		$status_dr = $this->db->get('status_dr');
		$check_dr  = $status_dr->row();

		$this->db->limit(1);
		$this->db->where('trm_doc',$book_order->id);
		$check_trmdr = $this->db->get('status_dr');
		$result_check_trmdr  = $check_trmdr->row();


		if($check_trmdr->num_rows() <= 0 ){

			if($status_dr->num_rows() <= 0){
					/////////////////create Customer from user/////////////////////////////////
					$this->db->where('id',$id_comp);
					$customer = $this->db->get('customer')->row();

					$this->db->where('terminal_doc_ref_id',$trm);
					$book_order = $this->db->get('book_order')->row();

					$configtax = $this->db->get_where('setting_holdtax')->row();

					$query_check_dr_no = $this->db->get_where('normal_order', array('id_order' => $id_order))->row();

					if(empty($query_check_dr_no->dr_no)){
							$data_order = array(	
								"id_comp" => $customer->id,
								"code_comp" => $customer->customer_code,
							    "company_bill" => $customer->customer_name,
							    "address_bill" => $customer->customer_address,
							    "address_bill2" => $customer->customer_address2,
							    "address_bill3" => $customer->customer_address3,
							    "company_branch" => $customer->customer_branch,
							    "company_post" => $customer->customer_post,
							    "type" => $typeDraft->type,
							    "payment" => $book_order->payment,
							    "atb" => $book_order->atb,
							    "atd" => $book_order->atd,
							    "remark" => $book_order->remark,
							    "terminal_doc_ref_an" => $book_order->terminal_doc_ref_an,
							    "doc_ref_book" => $book_order->doc_ref_book,
							    "config_holdtax" => $configtax->holdtax,
							    "created" => date('Y-m-d H:i:s'),
							    "updated" => date('Y-m-d H:i:s')
							);
										
							$this->db->insert('normal_order', $data_order);
							$data_order_id = $this->db->insert_id();

							$data_book = array(	
								"id_comp" => $customer->id,
								"type" => $typeDraft->type,
								"cur_rate" => $rate_con->rate,
							    "updated" => date('Y-m-d H:i:s')
							);
							
							$this->db->where('doc_ref_book', $book_order->doc_ref_book);
							$this->db->update('book_order', $data_book);

					}

			}
					/////////////////create Customer from user/////////////////////////////////


					///////////////////Auto Draft NO://///////////////////////

					$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order))->row();
					
					//$id_order = $query_order->id;
					$trm_an = $query_order->terminal_doc_ref_an;

					$query_check_dr_no = $this->db->get_where('normal_order', array('id_order' => $id_order))->row();

					$dr_no = $query_check_dr_no->dr_no;
					$dr_id = $query_check_dr_no->id;

					if(empty($dr_no)){ 

							$this->db->limit(1);
							$this->db->order_by("id", "desc");
							$last_dr = $this->db->get('status_dr')->row();

							if(empty($last_dr->dr_no)){
								$gen_dr = 'DRN-'.date('Y')."00001";
							} else {

								$rand_dr = (int)$last_dr->dr_no;
								$set_dr  = $rand_dr + 1;
				
								if($set_dr <= 9){

									$complete_dr = "0000".$set_dr;
									$gen_dr = 'DRN-'.date('Y').$complete_dr;

								} else if($set_dr <= 99){

									$complete_dr = "000".$set_dr;
									$gen_dr = 'DRN-'.date('Y').$complete_dr;

								}  else if($set_dr <= 999){

									$complete_dr = "00".$set_dr;
									$gen_dr = 'DRN-'.date('Y').$complete_dr;

								} else if($set_dr <= 9999){

									$complete_dr = "0".$set_dr;
									$gen_dr = 'DRN-'.date('Y').$complete_dr;

								}  else {

									$complete_dr = $set_dr;
									$gen_dr = 'DRN-'.date('Y').$complete_dr;
								}

							}

							$data = array(

					          	"dr_no" => $gen_dr,
					          	"print_date" => date('Y-m-d H:i:s'),
					          	"updated" => date('Y-m-d H:i:s')
					         );
					         
					        $this->db->where('id_order', $id_order);
							$this->db->update('normal_order',$data);

							//////////////Change Status Split////////////////

								if(empty($last_dr->dr_no)){

									$dr_no = "00001";

								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
				
									if($set_dr <= 9){

										$dr_no = "0000".$set_dr;

									} else if($set_dr <= 99){

										$dr_no = "000".$set_dr;

									}  else if($set_dr <= 999){

										$dr_no = "00".$set_dr;

									} else if($set_dr <= 9999){

										$dr_no = "0".$set_dr;

									}  else {

										$dr_no = $set_dr;

									}

								}
								$status_dr = array(
									"trm_doc" => $book_order->id,
									"normal_id" =>  $data_order_id, 
									"prefix_dr" => 'DRN-',
									"year_dr" => date('Y'),
									"dr_no" => $dr_no,
									"is_split" => 0,
									"is_sub" => 1,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_dr', $status_dr);

							//////////////Change Status Split////////////////


						}

					///////////////////Auto Draft NO://///////////////////////

					///////////////////LogPrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');

				
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$username = $username->username;

					if(empty($dr_no)){

						if(empty($last_dr->dr_no)){
							$print_dr = 'DRN-'.date('Y')."00001";
						} else {
									

							$rand_dr = (int)$last_dr->dr_no;
							$set_dr  = $rand_dr + 1;

							if($set_dr <= 9){

								$complete_dr = "0000".$set_dr;
								$print_dr = 'DRN-'.date('Y').$complete_dr;

							} else if($set_dr <= 99){

								$complete_dr = "000".$set_dr;
								$print_dr = 'DRN-'.date('Y').$complete_dr;

							}  else if($set_dr <= 999){

								$complete_dr = "00".$set_dr;
								$print_dr = 'DRN-'.date('Y').$complete_dr;

							} else if($set_dr <= 9999){

								$complete_dr = "0".$set_dr;
								$print_dr = 'DRN-'.date('Y').$complete_dr;

							}  else {

								$complete_dr = $set_dr;
								$print_dr = 'DRN-'.date('Y').$complete_dr;
								
							}

						}

					} else {
						 $print_dr = $dr_no;
					}


					$check_data = $this->session->userdata('logged_in');

					$this->db->where('id',$id_comp);
					$customer = $this->db->get('customer')->row();

					$query_check_dr_no = $this->db->get_where('normal_order', array('id_order' => $id_order))->row();

					$dr_id = $query_check_dr_no->id;
							
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$username = $username->username;

					if(empty($dr_no)){

					if(empty($last_dr->dr_no)){
						$print_dr = 'DRN-'.date('Y')."00001";
					} else {

						$rand_dr = (int)$last_dr->dr_no;
						$set_dr  = $rand_dr + 1;
				
						if($set_dr <= 9){

							$complete_dr = "0000".$set_dr;
							$print_dr = 'DRN-'.date('Y').$complete_dr;

						} else if($set_dr <= 99){

							$complete_dr = "000".$set_dr;
							$print_dr = 'DRN-'.date('Y').$complete_dr;

						}  else if($set_dr <= 999){

							$complete_dr = "00".$set_dr;
							$print_dr = 'DRN-'.date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "0".$set_dr;
							$print_dr = 'DRN-'.date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$print_dr = 'DRN-'.date('Y').$complete_dr;
							
						}

					}

				} else {
					$print_dr = $dr_no;
				}


		}		



				$this->db->limit(1);
				$this->db->where('id',$id_order);
				$query_dr_book = $this->db->get('book_order');
				$result_query_dr_book  = $query_dr_book->row();

				$check_data = $this->session->userdata('logged_in');

				$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$username = $username->username;

				$data = array(
						"username" => $username,
						"print_no" => $result_query_dr_book->dr_no,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
					
					$this->db->insert('log_print_dr', $data);
					///////////////////LogPrint/////////////////////////

				$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
					$data['order'] = $query_order->result_array();

					foreach ($data['order'] as $rs) {
						//$id_comp = $rs['id_comp'];
						//$id_order = $rs['id'];
						$data['bookan'] = $rs['book_an'];
						$trm_an = $rs['terminal_doc_ref_an'];
					}


				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
					$data['vat'] = $query_vat->row();

					$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$this->db->limit(1);
					$this->db->order_by("created", "desc");
					$this->db->where('id_order',$id_order);
					$query = $this->db->get('normal_order');
					$data['head']= $query->result_array();


					$this->db->limit(1);
					$this->db->order_by("created", "desc");
					$this->db->where('id_order',$id_order);
					$query_t = $this->db->get('normal_order');
					$typedr= $query_t->row();	
					$data['dr_t'] = $this->db->get_where('setting_type', array('type' => $typedr->type))->row();
					

					$this->db->where('id_order',$id_order);
					$query_barcode = $this->db->get('normal_order');
					$gen_barcode = $query_barcode->row();

					$this->set_barcode($gen_barcode->dr_no);

					$data['dr_no'] = $gen_barcode->dr_no;
					$data['with_hold'] = $this->db->get('setting_holdtax')->row();

					$this->view['main'] =  $this->load->view('pro/draft_invoice_normal',$data,true);
					$this->view();

			
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	

	}

	public function PreviewDraft($trm = null){

		if($this->session->userdata('logged_in')) { 	

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'terminal_doc_ref_id' => $trm));
			$data['order'] = $query_order->result_array();

			foreach ($data['order'] as $rs) {
				//$id_comp = $rs['id_comp'];
				$terminal_doc_ref_an = $rs['terminal_doc_ref_an'];
			}

				$this->db->limit(1);
				$this->db->where('trm_doc',$terminal_doc_ref_an);
				$query = $this->db->get('status_dr');
				$data['status_dr']= $query->row();

			$query_type = $this->db->get_where('setting_type', array('is_delete' => 0));
			$data['type'] = $query_type->result_array();



			//$query_comp = $this->db->get_where('company_deal', array('is_use' => 0 , 'id' => $id_comp));
			//$data['comp'] = $query_comp->result_array();

			$this->view['main'] =  $this->load->view('pro/preview_dr',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

		
	}

	public function PreviewSubDraft($drf_book = null){

		if($this->session->userdata('logged_in')) { 	

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'doc_ref_book' => $drf_book));
			$data['order'] = $query_order->result_array();

			$query_type = $this->db->get_where('setting_type', array('is_delete' => 0));
			$data['type'] = $query_type->result_array();

			$query_customer = $this->db->get_where('customer'); 
			$data['customer'] = $query_customer->result_array();

			$query_customer = $this->db->get_where('country'); 
			$data['country'] = $query_customer->result_array();

			//$query_comp = $this->db->get_where('company_deal', array('is_use' => 0 , 'id' => $id_comp));
			//$data['comp'] = $query_comp->result_array();

			$this->view['main'] =  $this->load->view('pro/preview_sub_dr',$data,true);
		$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}


		
	}


    public function reNormalDraft($trm = null, $dr_no = null){

    	if($this->session->userdata('logged_in')) { 	

    		$re_dr = $this->db->get_where('book_order', array('is_use' => 0 , 'terminal_doc_ref_id' => $trm))->row();

			$this->db->where('trm_doc', $re_dr->terminal_doc_ref_an);
	      	$this->db->delete('status_dr');

			$data = array(
				"dr_no" => '',
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('dr_no', $dr_no);
			$this->db->update('normal_order',$data);

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'terminal_doc_ref_id' => $trm));
			$data['order'] = $query_order->result_array();

			foreach ($data['order'] as $rs) {
				$id_comp = $rs['id_comp'];
				$terminal_doc_ref_an = $rs['terminal_doc_ref_an'];
			}

			$this->db->limit(1);
			$this->db->where('trm_doc',$terminal_doc_ref_an);
			$query = $this->db->get('status_dr');
			$data['status_dr']= $query->row();

			$query_comp = $this->db->get_where('company_deal', array('is_use' => 0 , 'id' => $id_comp));
			$data['comp'] = $query_comp->result_array();

			$this->view['main'] =  $this->load->view('pro/preview_dr',$data,true);
			$this->view();
			
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
		
	}

	public function reSplitDraft($dr_no=null , $qty = null){
		$re_dr = $this->db->get_where('split_order', array('dr_no' => $dr_no))->row();

		$rate = $this->db->get_where('setting_size_rate', array('size' => $re_dr->size_con, 'status' => 0 , 'is_delete' => 0))->row();

		$check_redr = $this->db->get_where('book_order', array('terminal_doc_ref_id' => "RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_id , 'is_use' => 0))->row();

		$configtax = $this->db->get_where('setting_holdtax')->row();

		if(empty($check_redr)){
			$data = array(
				"id_comp" => $re_dr->id_comp,
				"type" => '',
				"payment" => $re_dr->payment,
				"atb" => $re_dr->atb,
				"atd" => $re_dr->atd,
				"size_con" => $re_dr->size_con,
				"type_con" => $re_dr->type_con,
				"book_con" => $qty,
				"terminal_doc_ref_id" => "RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_id,
				"terminal_doc_ref_an" => "RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_an,
				"doc_ref_book" => $re_dr->doc_ref_book,
				"book_an" => $re_dr->book_an,
				"area" => $re_dr->area,
				"remark" => "Re-draft :".$dr_no.$re_dr->remark,
				"cur_rate" => $rate->rate,
				"config_holdtax" => $configtax->holdtax,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $data);

			$re_drComp = $this->db->get_where('head_all_order', array('id_comp' => $re_dr->id_comp))->row();

					$data_normal = array(	
						"id_comp" => $re_dr->id_comp,
						"code_comp" => $re_drComp->code_comp,
				        "company_bill" => $re_drComp->company_bill,
				        "address_bill" => $re_drComp->address_bill,
				        "company_branch" => $customer->customer_branch,
				        "company_post" => $customer->customer_post,
				        "type" => '',
				        "payment" => $re_drComp->payment,
				        "atb" => $re_drComp->atb,
				        "atd" => $re_drComp->atd,
				        "remark" => $re_drComp->remark,
				        "terminal_doc_ref_an" => "RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_an,
				        "doc_ref_book" => $re_dr->doc_ref_book,
				        "config_holdtax" => $configtax->holdtax,
				        "created" => date('Y-m-d H:i:s'),
				        "updated" => date('Y-m-d H:i:s')
				    );

				    $data_head = array(	
						"id_comp" => $re_dr->id_comp,
						"code_comp" => $re_drComp->code_comp,
				        "company_bill" => $re_drComp->company_bill,
				        "address_bill" => $re_drComp->address_bill,
				        "type" => '',
				        "payment" => $re_drComp->payment,
				        "atb" => $re_drComp->atb,
				        "atd" => $re_drComp->atd,
				        "remark" => $re_drComp->remark,
				        "terminal_doc_ref_an" => "RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_an,
				        "doc_ref_book" => $re_dr->doc_ref_book,
				        "config_holdtax" => $configtax->holdtax,
				        "created" => date('Y-m-d H:i:s'),
				        "updated" => date('Y-m-d H:i:s')
				    );
					
					$this->db->insert('normal_order', $data_normal);
					$this->db->insert('head_order', $data_head);	
		}

		$this->db->where('dr_no', $dr_no);
      	$this->db->delete('split_order');

		redirect('/Createdraft/PreviewDraft/'."RED-".$re_dr->id."-".$re_dr->terminal_doc_ref_id, 'refresh');

	}

	public function PrintSubDR ($doc = null , $id_comp = null, $qty_con = null , $type_dr = null){

			$this->db->limit(1);
			$this->db->where('doc_ref_book',$doc);
			$SubDR = $this->db->get('book_order');
			$SubDR  = $SubDR->row();

			$payer = $this->db->get_where('customer', array('custom_id' => $id_comp))->row();

			$rate = $this->db->get_where('setting_size_rate', array('size' => $SubDR->size_con, 'status' => 0 , 'is_delete' => 0))->row();

			 $configtax = $this->db->get_where('setting_holdtax')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => '',
				"payment" => $SubDR->payment,
				"atb" => $SubDR->atb,
				"atd" => $SubDR->atd,
				"size_con" => $SubDR->size_con,
				"type_con" => $SubDR->type_con,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $SubDR->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
				"doc_ref_book" => $SubDR->doc_ref_book,
				"book_an" => $SubDR->book_an,
				"area" => $SubDR->area,
				"remark" => $SubDR->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"config_holdtax" => $configtax->holdtax,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id = $this->db->insert_id();

			$this->db->limit(1);
			$this->db->where('doc_ref_book',$doc);
			$SubDR = $this->db->get('book_order');
			$SubDR  = $SubDR->row();

			$data = array(
					"is_sub" => '2',
		          	"book_con" => $SubDR->book_con - $qty_con,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		    $this->db->where('doc_ref_book', $doc);
			$this->db->where('is_sub', '0');
			$this->db->update('book_order',$data);

			$this->db->where('terminal_doc_ref_an',$SubDR->terminal_doc_ref_an);
			$SubDR_check = $this->db->get('book_order');
			$checkSubDR  = $SubDR_check->result_array();

			foreach ($checkSubDR as $rs_SubDR_check) {
				$data_sub_order = array(
					"is_sub" => '2',
			        "updated" => date('Y-m-d H:i:s')
			    );

			    $this->db->where('terminal_doc_ref_an', $rs_SubDR_check['terminal_doc_ref_an']);
				$this->db->where('is_sub', '0');
				$this->db->update('book_order',$data_sub_order);
			}

		redirect('/Createdraft/GenerateDR/'.$dataSub_id."/".$payer->id."/".$type_dr, 'refresh');

	}

	public function GenerateDR($id_sub = null , $id_comp = null , $type_dr = null){

		if($this->session->userdata('logged_in')) { 	

				$book_order = $this->db->get_where('book_order', array('id' => $id_sub))->row();

		$this->db->limit(1);
		$this->db->where('id',$type_dr);
		$typeDraft = $this->db->get('setting_type');
		$typeDraft  = $typeDraft->row();

		$this->db->where('size',$book_order->size_con);
		$this->db->where('type',$type_dr);
		$query_rate_con = $this->db->get('setting_size_rate');
		$rate_con  = $query_rate_con->row();


		$this->db->limit(1);
		$this->db->where('trm_doc',$book_order->id);
		$status_dr = $this->db->get('status_dr');
		$check_dr  = $status_dr->row();

		$this->db->where('id',$id_comp);
		$customer = $this->db->get('customer')->row();

				if($status_dr->num_rows() <= 0){
				/////////////////create Customer from user/////////////////////////////////
						

						$customer = $this->db->get_where('customer', array('id' => $id_comp))->row();

						$book_order = $this->db->get_where('book_order', array('id' => $id_sub))->row();

						$query_check_dr_no = $this->db->get_where('normal_order', array('id_order' => $book_order->id))->row();

						$configtax = $this->db->get_where('setting_holdtax')->row();


						if(empty($query_check_dr_no->dr_no)){
							$data_order = array(	
								"id_comp" => $customer->id,
								"tax_reg_no" => $customer->tax_reg_no,
								"code_comp" => $customer->customer_code,
							    "company_bill" => $customer->customer_name,
							    "address_bill" => $customer->customer_address,
							    "address_bill2" => $customer->customer_address2,
							    "address_bill3" => $customer->customer_address3,
							    "company_branch" => $customer->customer_branch,
							    "company_post" => $customer->customer_post,
							    "id_order" => $book_order->id,
							    "type" => $typeDraft->type,
							    "payment" => $book_order->payment,
							    "atb" => $book_order->atb,
							    "atd" => $book_order->atd,
							    "remark" => $book_order->remark,
							    "terminal_doc_ref_an" => $book_order->terminal_doc_ref_an,
							    "doc_ref_book" => $book_order->doc_ref_book,
							    "config_holdtax" => $configtax->holdtax,
							    "created" => date('Y-m-d H:i:s'),
							    "updated" => date('Y-m-d H:i:s')
							);
										
							$this->db->insert('normal_order', $data_order);
							$data_order_id = $this->db->insert_id();


							$data_book = array(	
								"id_comp" => $customer->id,
								"type" => $typeDraft->type,
								"cur_rate" => $rate_con->rate,
							    "updated" => date('Y-m-d H:i:s')
							);
							
							$this->db->where('id', $id_sub);
							$this->db->update('book_order', $data_book);
						}
					/////////////////create Customer from user/////////////////////////////////


						}

					///////////////////Auto Draft NO://///////////////////////

						$query_order = $this->db->get_where('book_order', array('id' => $id_sub))->row();
						
						$id_order = $query_order->id;
						$trm_an = $query_order->terminal_doc_ref_an;
						$book = $query_order->book_an;


						$query_check_dr_no = $this->db->get_where('normal_order', array('id' => $data_order_id))->row();

						$dr_no = $query_check_dr_no->dr_no;
						$dr_id = $query_check_dr_no->id;

						$this->db->limit(1);
						$this->db->order_by("id", "desc");
						$last_dr = $this->db->get('status_dr')->row();

							if(empty($dr_no)){ 

								

								if(empty($last_dr->dr_no)){
									$gen_dr = 'DRN-'.date('Y')."00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;

									if($set_dr <= 9){

										$complete_dr = "0000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 99){

										$complete_dr = "000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else if($set_dr <= 999){

										$complete_dr = "00".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 9999){

										$complete_dr = "0".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else {

										$complete_dr = $set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;
										
									}

								}
						}
								$data = array(

						          	"dr_no" => $gen_dr,
						          	"print_date" => date('Y-m-d H:i:s'),
						          	"updated" => date('Y-m-d H:i:s')
						         );
						         
						        $this->db->where('id', $data_order_id);
								$this->db->update('normal_order',$data);

								$data_book_dr = array(	
									"dr_no" => $gen_dr,
								    "updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->where('id', $id_sub);
								$this->db->update('book_order', $data_book_dr);



							//////////////Change Status Split////////////////

								if(empty($last_dr->dr_no)){
									$dr_no = "00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;

									if($set_dr <= 9){

										$dr_no = "0000".$set_dr;


									} else if($set_dr <= 99){

										$dr_no = "000".$set_dr;


									}  else if($set_dr <= 999){

										$dr_no = "00".$set_dr;


									} else if($set_dr <= 9999){

										$dr_no = "0".$set_dr;


									}  else {

										$dr_no = $set_dr;

										
									}

								}

								$status_dr = array(
									"trm_doc" => $book_order->id,
									"normal_id" =>  $data_order_id, 
									"prefix_dr" => 'DRN-',
									"year_dr" => date('Y'),
									"dr_no" => $dr_no,
									"is_split" => 0,
									"is_sub" => 1,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_dr', $status_dr);

							//////////////Change Status Split////////////////

						///////////////////Auto Draft NO://///////////////////////



					///////////////////LogPrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');
				
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$username = $username->username;

					if(empty($dr_no)){

						if(empty($last_dr->dr_no)){
							$print_dr = 'DRN-'.date('Y')."00001";
						}   else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;

									if($set_dr <= 9){

										$complete_dr = "0000".$set_dr;
										$print_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 99){

										$complete_dr = "000".$set_dr;
										$print_dr = 'DRN-'.date('Y').$complete_dr;

									}  else if($set_dr <= 999){

										$complete_dr = "00".$set_dr;
										$print_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 9999){

										$complete_dr = "0".$set_dr;
										$print_dr = 'DRN-'.date('Y').$complete_dr;

									}  else {

										$complete_dr = $set_dr;
										$print_dr = 'DRN-'.date('Y').$complete_dr;
										
									}

								}


					} else {
						 $print_dr = $dr_no;
					}

					$data = array(
						"username" => $username,
						"print_no" => $print_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
					
					$this->db->insert('log_print_dr', $data);
					///////////////////LogPrint/////////////////////////

					$data['bookan'] = $book;

					$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_sub));
					$data['order'] = $query_order->result_array();


					$data['with_hold'] = $this->db->get('setting_holdtax')->row();
					
					$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
					$data['vat'] = $query_vat->row();

					$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					/*$this->db->limit(1);
					$this->db->order_by("created", "desc");
					$this->db->where('id',$data_order_id);*/

					$data['dr_t'] = $this->db->get_where('setting_type',array('id', $type_dr))->row();

					$this->db->limit(1);
					$this->db->order_by("created", "desc");
					$this->db->where('id',$data_order_id);
					$query_barcode = $this->db->get('normal_order');
					$gen_barcode = $query_barcode->row();

					$this->set_barcode($gen_barcode->dr_no);

					$this->db->limit(1);
					$this->db->where('id_order',$gen_barcode->id_order);
					$query = $this->db->get('normal_order');
					$data['head']= $query->result_array();

					$data['dr_no'] = $gen_barcode->dr_no;

					$this->view['main'] =  $this->load->view('pro/draft_invoice_normal',$data,true);
					$this->view();
			
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}


	public function PreviewRemainDraft($id_order = null){

		if($this->session->userdata('logged_in')) { 	

			$query_order = $this->db->get_where('book_order', array('is_use' => 0 , 'id' => $id_order));
			$data['order'] = $query_order->result_array();


			$query_type = $this->db->get_where('setting_type', array('is_delete' => 0));
			$data['type'] = $query_type->result_array();

			$query_customer = $this->db->get_where('customer');
			$data['customer'] = $query_customer->result_array();

			//$query_comp = $this->db->get_where('company_deal', array('is_use' => 0 , 'id' => $id_comp));
			//$data['comp'] = $query_comp->result_array();

			$this->view['main'] =  $this->load->view('pro/preview_remain_dr',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

		
	}

	public function PrintRemainDR ($doc = null , $id_comp = null, $qty_con = null , $type_dr = null){

			$this->db->limit(1);
			$this->db->where('doc_ref_book',$doc);
			$SubDR = $this->db->get('book_order');
			$SubDR  = $SubDR->row();

			if($SubDR->book_con > 0){
				$payer = $this->db->get_where('customer', array('custom_id' => $id_comp))->row();


				$rate = $this->db->get_where('setting_size_rate', array('size' => $SubDR->size_con, 'status' => 0 , 'is_delete' => 0))->row();

				$configtax = $this->db->get_where('setting_holdtax')->row();

				$dataSub = array(
					"id_comp" => $payer->id,
					"type" => '',
					"payment" => $SubDR->payment,
					"atb" => $SubDR->atb,
					"atd" => $SubDR->atd,
					"size_con" => $SubDR->size_con,
					"type_con" => $SubDR->type_con,
					"book_con" => $qty_con,
					"terminal_doc_ref_id" => $SubDR->terminal_doc_ref_id,
					"terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
					"doc_ref_book" => $SubDR->doc_ref_book,
					"book_an" => $SubDR->book_an,
					"area" => $SubDR->area,
					"remark" => $SubDR->remark,
					"cur_rate" => $rate->rate,
					"is_sub" => '1',
					"config_holdtax" => $configtax->holdtax,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);

				$this->db->insert('book_order', $dataSub);
				$dataSub_id = $this->db->insert_id();

				$this->db->limit(1);
				$this->db->where('doc_ref_book',$doc);
				$SubDR = $this->db->get('book_order');
				$SubDR  = $SubDR->row();

				$data = array(
			          	"book_con" => $SubDR->book_con -  $qty_con,
			          	"updated" => date('Y-m-d H:i:s')
			         );
			    $this->db->where('doc_ref_book', $doc);
				$this->db->where('is_sub', '2');
				$this->db->update('book_order',$data);


				redirect('/Createdraft/GenerateDR/'.$dataSub_id."/".$payer->id."/".$type_dr, 'refresh');
			} else {
				echo "Cannot Generate DR QTY is not enough.<br>";
				echo "<a href='".site_url()."Createdraft'>Back To Main</a>";
			}

	}

	public function PrintSplitRemainDR ($doc = null , $id_comp = null, $qty_con = null , $type_dr = null , $num_split = null){

			$this->db->limit(1);
			$this->db->where('doc_ref_book',$doc);
			$SubDR = $this->db->get('book_order');
			$SubDR  = $SubDR->row();

			if($SubDR->book_con > 0){
				$this->db->limit(1);
			$this->db->where('id',$type_dr);
			$typeDraft = $this->db->get('setting_type');
			$typeDraft  = $typeDraft->row();

			$payer = $this->db->get_where('customer', array('custom_id' => $id_comp))->row();

			$check_data = $this->session->userdata('logged_in');
				
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$username = $username->username;	

			$data = array(
				"is_sub" => '2',
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('terminal_doc_ref_id', $SubDR->terminal_doc_ref_id);
			$this->db->where('is_sub', '0');
			$this->db->update('book_order',$data);


			$rate = $this->db->get_where('setting_size_rate', array('size' => $SubDR->size_con, 'status' => 0 , 'is_delete' => 0))->row();

			$configtax = $this->db->get_where('setting_holdtax')->row();
			

			$result_split = $qty_con  / $num_split;

				if ((int) $result_split == $result_split) {

					for ($i=1; $i <=$result_split; $i++) { 

						$dataSub = array(
							"id_comp" => $payer->id,
							"type" => $typeDraft->type,
							"payment" => $SubDR->payment,
							"atb" => $SubDR->atb,
							"atd" => $SubDR->atd,
							"size_con" => $SubDR->size_con,
							"type_con" => $SubDR->type_con,
							"book_con" => $num_split,
							"terminal_doc_ref_id" => $SubDR->terminal_doc_ref_id,
							"terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
							"doc_ref_book" => $SubDR->doc_ref_book,
							"book_an" => $SubDR->book_an,
							"area" => $SubDR->area,
							"remark" => $SubDR->remark,
							"cur_rate" => $rate->rate,
							"is_sub" => '1',
							"is_split" => '1',
							"config_holdtax" => $configtax->holdtax,
							"created" => date('Y-m-d H:i:s'),
							"updated" => date('Y-m-d H:i:s')
						);

						$this->db->insert('book_order', $dataSub);
						$split_order_id = $this->db->insert_id();

						$id_split[] = $split_order_id;

						$this->db->limit(1);
						$this->db->where('doc_ref_book',$doc);
						$SubDR = $this->db->get('book_order');
						$SubDR  = $SubDR->row();

						$data = array(
					          	"book_con" => $SubDR->book_con -  $num_split,
					          	"updated" => date('Y-m-d H:i:s')
					         );
					    $this->db->where('doc_ref_book', $doc);
						$this->db->where('is_sub', '2');
						$this->db->update('book_order',$data);

						$this->db->where('id',$payer->id);
							$customer = $this->db->get('customer')->row();

								$data_order = array(	
									"id_comp" => $customer->id,
									"tax_reg_no" => $customer->tax_reg_no,
									"code_comp" => $customer->customer_code,
								    "company_bill" => $customer->customer_name,
								    "address_bill" => $customer->customer_address,
								    "address_bill2" => $customer->customer_address2,
								    "address_bill3" => $customer->customer_address3,
								    "company_branch" => $customer->customer_branch,
								    "company_post" => $customer->customer_post,
								    "id_order" => $split_order_id,
								    "type" => $typeDraft->type,
								    "payment" => $SubDR->payment,
								    "atb" => $SubDR->atb,
									"atd" => $SubDR->atd,
								    "remark" => $SubDR->remark,
								    "terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
								    "doc_ref_book" => $SubDR->doc_ref_book,
								    "config_holdtax" => $configtax->holdtax,
								    "created" => date('Y-m-d H:i:s'),
								    "updated" => date('Y-m-d H:i:s')
								);
											
								$this->db->insert('normal_order', $data_order);
								$data_order_id = $this->db->insert_id();

					

								$this->db->limit(1);
								$this->db->order_by("id", "desc");
								$last_dr = $this->db->get('status_dr')->row();

								if(empty($last_dr->dr_no)){
									$gen_dr = 'DRN-'.date('Y')."00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
				
									if($set_dr <= 9){

										$complete_dr = "0000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 99){

										$complete_dr = "000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else if($set_dr <= 999){

										$complete_dr = "00".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 9999){

										$complete_dr = "0".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else {

										$complete_dr = $set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;
										
									}

								}

								$data = array(

						          	"dr_no" => $gen_dr,
						          	"print_date" => date('Y-m-d H:i:s'),
						          	"updated" => date('Y-m-d H:i:s')
						         );
						         
						        $this->db->where('id', $data_order_id);
								$this->db->update('normal_order',$data);

								$data_book_dr = array(	
									"dr_no" => $gen_dr,
								    "updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->where('id', $split_order_id);
								$this->db->update('book_order', $data_book_dr);

								$this->set_barcode($gen_dr);

								//////////////Change Status Split////////////////

								if(empty($last_dr->dr_no)){
									$dr_no = "00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
							
									if($set_dr <= 9){

										$dr_no = "0000".$set_dr;

									} else if($set_dr <= 99){

										$dr_no = "000".$set_dr;

									}  else if($set_dr <= 999){

										$dr_no = "00".$set_dr;

									} else if($set_dr <= 9999){

										$dr_no = "0".$set_dr;

									}  else {

										$dr_no = $set_dr;
										
									}

								}

								$status_dr = array(
									"trm_doc" => $split_order_id,
									"normal_id" =>  $data_order_id, 
									"prefix_dr" => 'DRN-',
									"year_dr" => date('Y'),
									"dr_no" => $dr_no,
									"is_split" => 1,
									"is_sub" => 1,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_dr', $status_dr);

								//////////////Change Status Split////////////////


								///////////////////LogPrint/////////////////////////
								$data = array(
									"username" => $username,
									"print_no" => $gen_dr,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->insert('log_print_dr', $data);
								///////////////////LogPrint/////////////////////////


					}
				} else {
						$result_else = $qty_con - $num_split;
						$result_else1 = (int) ($qty_con / $num_split);

						$configtax = $this->db->get_where('setting_holdtax')->row();

						for ($i=1; $i <=$result_else1; $i++) { 

							$dataSub = array(
								"id_comp" => $payer->id,
								"payment" => $SubDR->payment,
								"atb" => $SubDR->atb,
								"atd" => $SubDR->atd,
								"type" => $typeDraft->type,
								"size_con" => $SubDR->size_con,
								"type_con" => $SubDR->type_con,
								"book_con" => $num_split,
								"terminal_doc_ref_id" => $SubDR->terminal_doc_ref_id,
								"terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
								"doc_ref_book" => $SubDR->doc_ref_book,
								"book_an" => $SubDR->book_an,
								"area" => $SubDR->area,
								"remark" => $SubDR->remark,
								"cur_rate" => $rate->rate,
								"is_sub" => '1',
								"is_split" => '1',
								"config_holdtax" => $configtax->holdtax,
								"created" => date('Y-m-d H:i:s'),
								"updated" => date('Y-m-d H:i:s')
							);

							$this->db->insert('book_order', $dataSub);
							$split_order_id = $this->db->insert_id();

							$id_split[] = $split_order_id;

							$this->db->limit(1);
							$this->db->where('doc_ref_book',$doc);
							$SubDR = $this->db->get('book_order');
							$SubDR  = $SubDR->row();

							$data = array(
						          	"book_con" => $SubDR->book_con -  $num_split,
						          	"updated" => date('Y-m-d H:i:s')
						         );
						    $this->db->where('doc_ref_book', $doc);
							$this->db->where('is_sub', '2');
							$this->db->update('book_order',$data);

							$this->db->where('id',$payer->id);
							$customer = $this->db->get('customer')->row();

								$data_order = array(	
									"id_comp" => $customer->id,
									"tax_reg_no" => $customer->tax_reg_no,
									"code_comp" => $customer->customer_code,
								    "company_bill" => $customer->customer_name,
								    "address_bill" => $customer->customer_address,
								    "address_bill2" => $customer->customer_address2,
								    "address_bill3" => $customer->customer_address3,
								    "company_branch" => $customer->customer_branch,
								    "company_post" => $customer->customer_post,
								    "id_order" => $split_order_id,
								    "type" => $typeDraft->type,
								    "payment" => $SubDR->payment,
								    "atb" => $SubDR->atb,
									"atd" => $SubDR->atd,
								    "remark" => $SubDR->remark,
								    "terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
								    "doc_ref_book" => $SubDR->doc_ref_book,
								    "config_holdtax" => $configtax->holdtax,
								    "created" => date('Y-m-d H:i:s'),
								    "updated" => date('Y-m-d H:i:s')
								);
											
								$this->db->insert('normal_order', $data_order);
								$data_order_id = $this->db->insert_id();


							

								$this->db->limit(1);
								$this->db->order_by("id", "desc");
								$last_dr = $this->db->get('status_dr')->row();

								if(empty($last_dr->dr_no)){
									$gen_dr = 'DRN-'.date('Y')."00001";
								} else {
									
									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;

									if($set_dr <= 9){

										$complete_dr = "0000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 99){

										$complete_dr = "000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else if($set_dr <= 999){

										$complete_dr = "00".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 9999){

										$complete_dr = "0".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else {

										$complete_dr = $set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;
										
									}

								}

								$data = array(

						          	"dr_no" => $gen_dr,
						          	"print_date" => date('Y-m-d H:i:s'),
						          	"updated" => date('Y-m-d H:i:s')
						         );
						         
						        $this->db->where('id', $data_order_id);
								$this->db->update('normal_order',$data);

								$data_book_dr = array(	
									"dr_no" => $gen_dr,
								    "updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->where('id', $split_order_id);
								$this->db->update('book_order', $data_book_dr);

								$this->set_barcode($gen_dr);

								//////////////Change Status Split////////////////

								if(empty($last_dr->dr_no)){
									$dr_no = "00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
				
									if($set_dr <= 9){

										$dr_no = "0000".$set_dr;

									} else if($set_dr <= 99){

										$dr_no = "000".$set_dr;

									}  else if($set_dr <= 999){

										$dr_no = "00".$set_dr;

									} else if($set_dr <=9999){

										$dr_no = "0".$set_dr;

									}  else {

										$dr_no = $set_dr;
										
									}

								}
								$status_dr = array(
									"trm_doc" => $split_order_id,
									"normal_id" =>  $data_order_id, 
									"prefix_dr" => 'DRN-',
									"year_dr" =>  date('Y'),
									"dr_no" => $dr_no,
									"is_split" => 1,
									"is_sub" => 1,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_dr', $status_dr);

							//////////////Change Status Split////////////////

								///////////////////LogPrint/////////////////////////
								$data = array(
									"username" => $username,
									"print_no" => $gen_dr,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->insert('log_print_dr', $data);
								///////////////////LogPrint/////////////////////////
						}

						$last_total = $num_split * $result_else1;
						$last_num = $qty_con - $last_total;

						$configtax = $this->db->get_where('setting_holdtax')->row();

						$dataSub = array(
								"id_comp" => $payer->id,
								"type" => $typeDraft->type,
								"payment" => $SubDR->payment,
								"atb" => $SubDR->atb,
								"atd" => $SubDR->atd,
								"size_con" => $SubDR->size_con,
								"type_con" => $SubDR->type_con,
								"book_con" => $last_num,
								"terminal_doc_ref_id" => $SubDR->terminal_doc_ref_id,
								"terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
								"doc_ref_book" => $SubDR->doc_ref_book,
								"book_an" => $SubDR->book_an,
								"area" => $SubDR->area,
								"remark" => $SubDR->remark,
								"cur_rate" => $rate->rate,
								"is_sub" => '1',
								"is_split" => '1',
								"config_holdtax" => $configtax->holdtax,
								"created" => date('Y-m-d H:i:s'),
								"updated" => date('Y-m-d H:i:s')
							);

							$this->db->insert('book_order', $dataSub);
							$split_order_id = $this->db->insert_id();

							$id_split[] = $split_order_id;

							$this->db->limit(1);
							$this->db->where('doc_ref_book',$doc);
							$SubDR = $this->db->get('book_order');
							$SubDR  = $SubDR->row();

							$data = array(
						          	"book_con" => $SubDR->book_con -  $last_num,
						          	"updated" => date('Y-m-d H:i:s')
						         );
						    $this->db->where('doc_ref_book', $doc);
							$this->db->where('is_sub', '2');
							$this->db->update('book_order',$data);

							$this->db->where('id',$payer->id);
							$customer = $this->db->get('customer')->row();

								$data_order = array(	
									"id_comp" => $customer->id,
									"tax_reg_no" => $customer->tax_reg_no,
									"code_comp" => $customer->customer_code,
								    "company_bill" => $customer->customer_name,
								    "address_bill" => $customer->customer_address,
								    "address_bill2" => $customer->customer_address2,
								    "address_bill3" => $customer->customer_address3,
								    "company_branch" => $customer->customer_branch,
								    "company_post" => $customer->customer_post,
								    "id_order" => $split_order_id,
								    "type" => $typeDraft->type,
								    "payment" => $SubDR->payment,
								    "atb" => $SubDR->atb,
									"atd" => $SubDR->atd,
								    "remark" => $SubDR->remark,
								    "terminal_doc_ref_an" => $SubDR->terminal_doc_ref_an,
								    "doc_ref_book" => $SubDR->doc_ref_book,
								    "config_holdtax" => $configtax->holdtax,
								    "created" => date('Y-m-d H:i:s'),
								    "updated" => date('Y-m-d H:i:s')
								);
											
								$this->db->insert('normal_order', $data_order);
								$data_order_id = $this->db->insert_id();

								
								$this->db->limit(1);
								$this->db->order_by("id", "desc");
								$last_dr = $this->db->get('status_dr')->row();



								if(empty($last_dr->dr_no)){
									$gen_dr = 'DRN-'.date('Y')."00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
				
									if($set_dr <= 9){

										$complete_dr = "0000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 99){

										$complete_dr = "000".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else if($set_dr <= 999){

										$complete_dr = "00".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									} else if($set_dr <= 9999){

										$complete_dr = "0".$set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;

									}  else {

										$complete_dr = $set_dr;
										$gen_dr = 'DRN-'.date('Y').$complete_dr;
										
									}

								}

								$data = array(

						          	"dr_no" => $gen_dr,
						          	"print_date" => date('Y-m-d H:i:s'),
						          	"updated" => date('Y-m-d H:i:s')
						         );
						         
						        $this->db->where('id', $data_order_id);
								$this->db->update('normal_order',$data);

								$data_book_dr = array(	
									"dr_no" => $gen_dr,
								    "updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->where('id', $split_order_id);
								$this->db->update('book_order', $data_book_dr);

								//////////////Change Status Split////////////////

								if(empty($last_dr->dr_no)){
									$dr_no = "00001";
								} else {

									$rand_dr = (int)$last_dr->dr_no;
									$set_dr  = $rand_dr + 1;
				
									if($set_dr <= 9){

										$dr_no = "0000".$set_dr;

									} else if($set_dr <= 99){

										$dr_no = "000".$set_dr;

									}  else if($set_dr <= 999){

										$dr_no = "00".$set_dr;

									} else if($set_dr <= 9999){

										$dr_no = "0".$set_dr;

									}  else {

										$dr_no = $set_dr;
										
									}

								}

								$status_dr = array(
									"trm_doc" => $split_order_id,
									"normal_id" =>  $data_order_id, 
									"prefix_dr" => 'DRN-',
									"year_dr" => date('Y'),
									"dr_no" => $dr_no,
									"is_split" => 1,
									"is_sub" => 1,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
															
								$this->db->insert('status_dr', $status_dr);

							//////////////Change Status Split////////////////


								$this->set_barcode($gen_dr);

								///////////////////LogPrint/////////////////////////
								$data = array(
									"username" => $username,
									"print_no" => $gen_dr,
									"created" => date('Y-m-d H:i:s'),
									"updated" => date('Y-m-d H:i:s')
								);
								
								$this->db->insert('log_print_dr', $data);
								///////////////////LogPrint/////////////////////////
				}

				 $this->GenerateSplitDR($id_split,  $type_dr);
			}  else {
				echo "Cannot Generate DR QTY is not enough.<br>";
				echo "<a href='".site_url()."Createdraft'>Back To Main</a>";
			}

	}

	public function GenerateSplitDR($id_split = null , $type_dr = null){

			if($this->session->userdata('logged_in')) { 	

				
			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();


			$this->db->join('book_order','book_order.id = normal_order.id_order');
			$this->db->where_in('book_order.id',$id_split);
			$this->db->order_by("normal_order.dr_no", "asc");
			$query_split = $this->db->get('normal_order');
			$data['split_order']= $query_split->result_array();


			$query_rate_con = $this->db->get('setting_size_rate',array('type' => $type_dr));
			$data['rate_con']  = $query_rate_con->result_array();

			$this->db->where('id',$type_dr);
			$data['dr_t'] = $this->db->get('setting_type')->row();


			$check_data = $this->session->userdata('logged_in');
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['name_print'] = $username->name;

			$this->view['main'] =  $this->load->view('pro/split',$data,true);
			$this->view();

			
			
			} else {
				$this->load->helper(array('form'));
				$this->load->view('login_view');
			}

	}

}