<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class MultiDraft extends Welcome {

	public function index(){

		
	}

	private function set_barcode($code)
    {
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
       	$code = $code.".png";
	    $store_image = imagepng($file,FCPATH."public/img/barcode/".$code);

    }

	public function PreviewDraft($order1 = null , $order2 = null, $order3 = null , $order4 = null, $order5 = null , $order6 = null){

		if($this->session->userdata('logged_in')) { 	

			$data['order1'] = $order1;
			$data['order2'] = $order2;
			$data['order3'] = $order3;
			$data['order4'] = $order4;
			$data['order5'] = $order5;
			$data['order6'] = $order6;



			$this->db->where('book_order.is_use',0);
			$this->db->where_in('id', array($order1, $order2, $order3, $order4, $order5, $order6));
			$query_order = $this->db->get('book_order');
			$data['order'] = $query_order->result_array();

			$data['count_order'] = count($data['order']);

			$query_type = $this->db->get_where('setting_type', array('is_delete' => 0));
			$data['type'] = $query_type->result_array();



			$this->view['main'] =  $this->load->view('multi/preview_multi',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

		
	} 

	public function PreviewZodiacDraft($order1 = null , $order2 = null, $order3 = null , $order4 = null, $order5 = null , $order6 = null){

		if($this->session->userdata('logged_in')) { 	

				if(!empty($order1)){
			$this->db->where('doc_ref_book',$order1);
			$book_order1 = $this->db->get('book_order')->row();
			$id_order1 = $book_order1->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order1);
			$this->db->update('book_order', $data_book_dr);

		} else {
			$id_order1 = 0;
		}

		if(!empty($order2)){
			$this->db->where('doc_ref_book',$order2);
			$book_order2 = $this->db->get('book_order')->row();
			$id_order2 = $book_order2->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order2);
			$this->db->update('book_order', $data_book_dr);

		} else {
			$id_order2 = 0;
		}

		if(!empty($order3)){
			$this->db->where('doc_ref_book',$order3);
			$book_order3 = $this->db->get('book_order')->row();
			$id_order3 = $book_order3->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order3);
			$this->db->update('book_order', $data_book_dr);

		} else {
			$id_order3 = 0;
		}

		if(!empty($order4)){
			$this->db->where('doc_ref_book',$order4);
			$book_order4 = $this->db->get('book_order')->row();
			$id_order4 = $book_order4->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order4);
			$this->db->update('book_order', $data_book_dr);

		} else {
			$id_order4 = 0;
		}

		if(!empty($order5)){
			$this->db->where('doc_ref_book',$order5);
			$book_order5 = $this->db->get('book_order')->row();
			$id_order5 = $book_order5->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order5);
			$this->db->update('book_order', $data_book_dr);

		} else {
			$id_order5 = 0;
		}


		if(!empty($order6)){
			$this->db->where('doc_ref_book',$order6);
			$book_order6 = $this->db->get('book_order')->row();
			$id_order6 = $book_order6->id;

			$data_book_dr = array(	
				"is_sub" => 2,
				"updated" => date('Y-m-d H:i:s')
			);
								
			$this->db->where('id', $id_order6);
			$this->db->update('book_order', $data_book_dr);
			
		} else {
			$id_order6 = 0;
		}


		$data['order1'] = $id_order1;
		$data['order2'] = $id_order2;
		$data['order3'] = $id_order3;
		$data['order4'] = $id_order4;
		$data['order5'] = $id_order5;
		$data['order6'] = $id_order6;



		$this->db->where('book_order.is_use',0);
		$this->db->where_in('doc_ref_book', array($order1, $order2, $order3, $order4, $order5, $order6));
		$query_order = $this->db->get('book_order');
		$data['order'] = $query_order->result_array();

		$data['count_order'] = count($data['order']);

		$query_type = $this->db->get_where('setting_type', array('is_delete' => 0));
		$data['type'] = $query_type->result_array();



		$this->view['main'] =  $this->load->view('multi/preview_multi',$data,true);
		$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	} 

	public function GenrateDraft ($id_comp = null, $type_dr = null , $order_v20 = null , $order_v40 = null , $order_v45 = null,  $order_id1 = null , $order_id2 = null, $order_id3 = null, $order_id4 = null , $order_id5 = null, $order_id6 = null){


		if(is_numeric($order_v20)){
			$orderv20 = $order_v20;
		} else {
			$orderv20 = 0;
		}

		if(is_numeric($order_v40)){
			$orderv40 = $order_v40;
		} else {
			$orderv40 = 0;
		}

		if(is_numeric($order_v45)){
			$orderv45 = $order_v45;
		} else {
			$orderv45 = 0;
		}

		$this->db->where('custom_id',$id_comp);
		$payer = $this->db->get('customer')->row();

		$this->db->where('id',$type_dr);
		$type_d = $this->db->get('setting_type')->row();

		$this->db->limit(1);
		$this->db->order_by("normal_id", "desc");
		$drn = $this->db->get('status_dr')->row();

		///////////////////Auto Draft NO://///////////////////////

		$this->db->limit(1);
		$this->db->order_by("id", "desc");
		$last_dr = $this->db->get('status_dr')->row();

			if(empty($last_dr->dr_no)){
				$gen_dr = 'DRN-'.date('Y')."00001";
				$complete_dr = "00001";
			} else {

				$rand_dr = (int)$last_dr->dr_no;
				$set_dr  = $rand_dr + 1;
				
					if($set_dr <= 9){

						$complete_dr = "0000".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

						} else if($set_dr <= 99){

						$complete_dr = "000".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

						}  else if($set_dr <= 999){

						$complete_dr = "00".$set_dr;
						$gen_dr = 'DRN-'.date('Y').$complete_dr;

						} else if($set_dr <= 9999){

							$complete_dr = "0".$set_dr;
							$gen_dr = 'DRN-'.date('Y').$complete_dr;

						}  else {

							$complete_dr = $set_dr;
							$gen_dr = 'DRN-'.date('Y').$complete_dr;
						}

			}

		///////////////////Auto Draft NO://///////////////////////

		if($order_id1 != 0){
			$this->db->where('id',$order_id1);
			$book_order1 = $this->db->get('book_order')->row();

			if($book_order1->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order1->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order1->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();


			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order1->payment,
				"atb" => $book_order1->atb,
				"atd" => $book_order1->atd,
				"is_multi" => 1 ,
				"size_con" => $book_order1->size_con,
				"type_con" => $book_order1->type_con,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order1->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order1->terminal_doc_ref_an,
				"doc_ref_book" => $book_order1->doc_ref_book,
				"book_an" => $book_order1->book_an,
				"area" => $book_order1->area,
				"remark" => $book_order1->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id1 = $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order1->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order1->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"is_multi" => 1 ,
				"id_order" => $dataSub_id1,
				"type" => $book_order1->type,
				"payment" => $book_order1->payment,
				"atb" => $book_order1->atb,
				"atd" => $book_order1->atd,
				"remark" => $book_order1->remark,
				"terminal_doc_ref_an" => $book_order1->terminal_doc_ref_an,
				"doc_ref_book" => $book_order1->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id1 = $this->db->insert_id();



		/////////////////////Update DRN-REF//////////////////////
		$data_book = array(	
				"dr_no" => $gen_dr,
				"updated" => date('Y-m-d H:i:s')
			);
							
		$this->db->where('id', $dataSub_id1);
		$this->db->update('book_order', $data_book);
		////////////////////////////////////////////////////////
		$data_book = array(	
				"dr_no" => $gen_dr,
				"updated" => date('Y-m-d H:i:s')
			);
							
		$this->db->where('id', $data_order_id1);
		$this->db->update('normal_order', $data_book);
		/////////////////////Update DRN-REF//////////////////////


		///////////////////LogPrint/////////////////////////

					$check_data = $this->session->userdata('logged_in');
				
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$username = $username->username;


					$data = array(
						"username" => $username,
						"print_no" => $gen_dr,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
					
					$this->db->insert('log_print_dr', $data);
		///////////////////LogPrint/////////////////////////

			//////////////Change Status Split////////////////

				$status_dr = array(
					"trm_doc" => $dataSub_id1,
					"normal_id" =>  $data_order_id1, 
					"prefix_dr" => 'DRN-',
					"year_dr" => date('Y'),
					"dr_no" => $complete_dr,
					"is_split" => 0,
					"is_sub" => 1,
					"created" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
															
				$this->db->insert('status_dr', $status_dr);

			//////////////Change Status Split////////////////
		}

		if($order_id2 != 0){
			$this->db->where('id',$order_id2);
			$book_order2 = $this->db->get('book_order')->row();

			if($book_order2->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order2->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order2->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order2->payment,
				"atb" => $book_order2->atb,
				"atd" => $book_order2->atd,
				"size_con" => $book_order2->size_con,
				"type_con" => $book_order2->type_con,
				"is_multi" => 1 ,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order2->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order2->terminal_doc_ref_an,
				"doc_ref_book" => $book_order2->doc_ref_book,
				"book_an" => $book_order2->book_an,
				"area" => $book_order2->area,
				"remark" => $book_order2->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id2 = $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order2->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order2->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"id_order" => $dataSub_id2,
				"type" => $book_order2->type,
				"payment" => $book_order2->payment,
				"is_multi" => 1 ,
				"atb" => $book_order2->atb,
				"atd" => $book_order2->atd,
				"remark" => $book_order2->remark,
				"terminal_doc_ref_an" => $book_order2->terminal_doc_ref_an,
				"doc_ref_book" => $book_order2->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id2 = $this->db->insert_id();



		/////////////////////Update DRN-REF//////////////////////
		$data_book = array(	
				"dr_no" => $gen_dr,
				"updated" => date('Y-m-d H:i:s')
			);
							
		$this->db->where('id', $dataSub_id2);
		$this->db->update('book_order', $data_book);
		////////////////////////////////////////////////////////
		$data_book = array(	
				"dr_no" => $gen_dr,
				"updated" => date('Y-m-d H:i:s')
			);
							
		$this->db->where('id', $data_order_id2);
		$this->db->update('normal_order', $data_book);
		/////////////////////Update DRN-REF//////////////////////

		}

		if($order_id3 != 0){
			$this->db->where('id',$order_id3);
			$book_order3 = $this->db->get('book_order')->row();

			if($book_order3->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order3->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order3->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order3->payment,
				"atb" => $book_order3->atb,
				"atd" => $book_order3->atd,
				"size_con" => $book_order3->size_con,
				"type_con" => $book_order3->type_con,
				"is_multi" => 1 ,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order3->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order3->terminal_doc_ref_an,
				"doc_ref_book" => $book_order3->doc_ref_book,
				"book_an" => $book_order3->book_an,
				"area" => $book_order3->area,
				"remark" => $book_order3->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id3= $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order3->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order3->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"is_multi" => 1 ,
				"id_order" => $dataSub_id3,
				"type" => $book_order3->type,
				"payment" => $book_order3->payment,
				"atb" => $book_order3->atb,
				"atd" => $book_order3->atd,
				"remark" => $book_order3->remark,
				"terminal_doc_ref_an" => $book_order3->terminal_doc_ref_an,
				"doc_ref_book" => $book_order3->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id3 = $this->db->insert_id();



			/////////////////////Update DRN-REF//////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $dataSub_id3);
			$this->db->update('book_order', $data_book);
			////////////////////////////////////////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $data_order_id3);
			$this->db->update('normal_order', $data_book);
		/////////////////////Update DRN-REF//////////////////////

		}

		if($order_id4 != 0){
			$this->db->where('id',$order_id4);
			$book_order4 = $this->db->get('book_order')->row();

			if($book_order4->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order4->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order4->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order4->payment,
				"atb" => $book_order4->atb,
				"atd" => $book_order4->atd,
				"size_con" => $book_order4->size_con,
				"type_con" => $book_order4->type_con,
				"is_multi" => 1 ,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order4->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order4->terminal_doc_ref_an,
				"doc_ref_book" => $book_order4->doc_ref_book,
				"book_an" => $book_order4->book_an,
				"area" => $book_order4->area,
				"remark" => $book_order4->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id4 = $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order4->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order4->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"is_multi" => 1 ,
				"id_order" => $dataSub_id4,
				"type" => $book_order4->type,
				"payment" => $book_order4->payment,
				"atb" => $book_order4->atb,
				"atd" => $book_order4->atd,
				"remark" => $book_order4->remark,
				"terminal_doc_ref_an" => $book_order4->terminal_doc_ref_an,
				"doc_ref_book" => $book_order4->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id4 = $this->db->insert_id();



			/////////////////////Update DRN-REF//////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $dataSub_id4);
			$this->db->update('book_order', $data_book);
			////////////////////////////////////////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $data_order_id4);
			$this->db->update('normal_order', $data_book);
			/////////////////////Update DRN-REF//////////////////////


		}

		if($order_id5 != 0){
			$this->db->where('id',$order_id5);
			$book_order5 = $this->db->get('book_order')->row();

			if($book_order5->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order5->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order5->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order5->payment,
				"atb" => $book_order5->atb,
				"atd" => $book_order5->atd,
				"size_con" => $book_order5->size_con,
				"type_con" => $book_order5->type_con,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order5->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order5->terminal_doc_ref_an,
				"is_multi" => 1 ,
				"doc_ref_book" => $book_order5->doc_ref_book,
				"book_an" => $book_order5->book_an,
				"area" => $book_order5->area,
				"remark" => $book_order5->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id5 = $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order5->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order5->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"is_multi" => 1 ,
				"id_order" => $dataSub_id5,
				"type" => $book_order5->type,
				"payment" => $book_order5->payment,
				"atb" => $book_order5->atb,
				"atd" => $book_order5->atd,
				"remark" => $book_order5->remark,
				"terminal_doc_ref_an" => $book_order5->terminal_doc_ref_an,
				"doc_ref_book" => $book_order5->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id5 = $this->db->insert_id();




			/////////////////////Update DRN-REF//////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $dataSub_id5);
			$this->db->update('book_order', $data_book);
			////////////////////////////////////////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $data_order_id5);
			$this->db->update('normal_order', $data_book);
			/////////////////////Update DRN-REF//////////////////////

		}

		if($order_id6 != 0){
			$this->db->where('id',$order_id6);
			$book_order6 = $this->db->get('book_order')->row();

			if($book_order6->size_con == '20'){
				$qty_con = $orderv20;
			} else if($book_order6->size_con == '40'){
				$qty_con = $orderv40;
			} else {
				$qty_con = $orderv45;
			}

			$this->db->where('type',$type_dr);
			$this->db->where('size',$book_order6->size_con);
			$rate = $this->db->get(' setting_size_rate')->row();

			$dataSub = array(
				"id_comp" => $payer->id,
				"type" => $type_d->type,
				"payment" => $book_order6->payment,
				"is_multi" => 1 ,
				"atb" => $book_order6->atb,
				"atd" => $book_order6->atd,
				"size_con" => $book_order6->size_con,
				"type_con" => $book_order6->type_con,
				"book_con" => $qty_con,
				"terminal_doc_ref_id" => $book_order6->terminal_doc_ref_id,
				"terminal_doc_ref_an" => $book_order6->terminal_doc_ref_an,
				"doc_ref_book" => $book_order6->doc_ref_book,
				"book_an" => $book_order6->book_an,
				"area" => $book_order6->area,
				"remark" => $book_order6->remark,
				"cur_rate" => $rate->rate,
				"is_sub" => '1',
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->insert('book_order', $dataSub);
			$dataSub_id6 = $this->db->insert_id();

			$data_book = array(	
				"book_con" => $book_order6->book_con - $qty_con,
				"updated" => date('Y-m-d H:i:s')
			);
							
			$this->db->where('id', $book_order6->id);
			$this->db->update('book_order', $data_book);

			$data_order = array(	
				"id_comp" => $payer->id,
				"tax_reg_no" => $payer->tax_reg_no,
				"code_comp" => $payer->customer_code,
				"company_bill" => $payer->customer_name,
				"address_bill" => $payer->customer_address,
				"address_bill2" => $payer->customer_address2,
				"address_bill3" => $payer->customer_address3,
				"id_order" => $dataSub_id6,
				"is_multi" => 1 ,
				"type" => $book_order6->type,
				"payment" => $book_order6->payment,
				"atb" => $book_order6->atb,
				"atd" => $book_order6->atd,
				"remark" => $book_order6->remark,
				"terminal_doc_ref_an" => $book_order6->terminal_doc_ref_an,
				"doc_ref_book" => $book_order6->doc_ref_book,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);
										
			$this->db->insert('normal_order', $data_order);
			$data_order_id6 = $this->db->insert_id();



			/////////////////////Update DRN-REF//////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $dataSub_id6);
			$this->db->update('book_order', $data_book);
			////////////////////////////////////////////////////////
			$data_book = array(	
					"dr_no" => $gen_dr,
					"updated" => date('Y-m-d H:i:s')
				);
								
			$this->db->where('id', $data_order_id6);
			$this->db->update('normal_order', $data_book);
			/////////////////////Update DRN-REF//////////////////////


		}

		 redirect('/MultiDraft/PrintDR/'.$gen_dr.'/1', 'refresh');

	}

	public function PrintDR($dr_no = null , $request_dr = null){

			
		if($this->session->userdata('logged_in')) { 	

			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$bookref = $this->db->get('book_order')->row();

			$this->db->limit(1);
			$this->db->where('id',$bookref->id_comp);
			$data['payer_order'] = $this->db->get('customer')->row();

			$data['bookan'] = $bookref->book_an;

			$this->db->order_by("created", "asc");
			$this->db->where('dr_no',$dr_no);
			$query = $this->db->get('book_order');
			$data['order'] = $query->result_array();

			$data['dr_no'] = $dr_no;
			
			$data['date_create'] = $bookref->created;

			$data['type_dr'] = $bookref->type;

			$data['terminal_doc_ref_an'] = $bookref->terminal_doc_ref_an;

			$this->db->limit(1);
			$this->db->where('type',$bookref->type);
			$data['dr_t'] = $this->db->get('setting_type')->row();

			$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
			$data['vat'] = $query_vat->row();

			$check_data = $this->session->userdata('logged_in');


			if($request_dr != '1'){
				///////////////////LogPrint/////////////////////////
						
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$username = $username->username;


					$data_log = array(
						"username" => $username,
						"print_no" => $dr_no,
						"created" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
							
					$this->db->insert('log_print_dr', $data_log);
			///////////////////LogPrint/////////////////////////
			}

			$data['user_print'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->set_barcode($dr_no);

			$this->view['main'] =  $this->load->view('multi/draft_invoice',$data,true);
			$this->view();
			
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	
	}

	public function CancelDrNormal(){

		$id_status_dr = $this->input->post('id_dr');
		$dr_no = $this->input->post('dr_no');
		$remark_inv = $this->input->post('remark_inv');

		$data = array(

		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('id', $id_status_dr);
		$this->db->update('status_dr',$data);

		$data_status = array(
					"remark_cancel" => "Document | ".$remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $dr_no);
		$this->db->update('normal_order',$data_status);

		$data_order = array(
					"remark_cancel" => "Document | ".$remark_inv,
		          	"is_use" => 1,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $dr_no);
		$this->db->update('book_order',$data_order);

		$current_order = $this->db->get_where('book_order', array('is_sub' => '1', 'dr_no' => $dr_no))->result_array();



		foreach ($current_order as $rs_fetch) {

			$origin_order = $this->db->get_where('book_order',array('doc_ref_book' => $rs_fetch['doc_ref_book'], 'is_sub' => 2))->row();

			$data_order = array(

		          	"book_con" => $origin_order->book_con + $rs_fetch['book_con'],
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
			$this->db->where('doc_ref_book', $rs_fetch['doc_ref_book']);
			$this->db->where('is_sub', 2);
			$this->db->update('book_order',$data_order);
		}

		$result['msg'] = "success";
		echo json_encode($result);
		return false;

	}

}