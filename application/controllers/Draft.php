<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Draft extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Draft');
    }

	public function index(){

	}	

	public function AllDraft() {

		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');
		$typeD = $this->input->post('typeD');

	        $config = array();
	        $config["base_url"] = site_url() . "Draft/AllDraft";
	        $config["total_rows"] = $this->M_Draft->CountDraft($search);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Draft->CountDraft($search);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Draft->fetch_draft($config["per_page"], $page , $search , $sort , $typeD);
	        $data["links"] = $this->pagination->create_links();

	       
	        $this->view['main'] =  $this->load->view('draft/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
    }


	public function Preview($dr_no = null , $is_multi = null){

			$this->db->limit(1);
			$this->db->where('dr_no',$dr_no);
			$check_cancel = $this->db->get('normal_order')->row();


			if($check_cancel->is_use != '1'){
				$this->db->limit(1);
				$this->db->where('dr_no',$dr_no);
				$bookref = $this->db->get('book_order')->row();

				$this->db->limit(1);
				$this->db->where('id',$bookref->id_comp);
				$data['payer_order'] = $this->db->get('customer')->row();

				$data['bookan'] = $bookref->book_an;

				$data['bookan'] = $bookref->book_an;

				$data['is_multi'] = $is_multi;

				$this->db->order_by("created", "asc");
				$this->db->where('dr_no',$dr_no);
				$query = $this->db->get('book_order');
				$data['order'] = $query->result_array();

				$data['dr_no'] = $dr_no;
				
				$data['date_create'] = $bookref->created;

				$data['type_dr'] = $bookref->type;

				$data['terminal_doc_ref_an'] = $bookref->terminal_doc_ref_an;

				$this->db->limit(1);
				$this->db->where('type',$bookref->type);
				$data['dr_t'] = $this->db->get('setting_type')->row();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->view['main'] =  $this->load->view('draft/preview_draft',$data,true);
				$this->view();
			} else {
				redirect('/Draft/AllDraft/', 'refresh');
			}
	}


	public function Remark(){

		$id = $this->input->post('id_dr');
		$remark_inv = $this->input->post('remark_inv');


		$change_status = $this->db->get_where('status_dr',array('id' => $id))->row();

		$data_status = array(
					"remark_cancel" => "Comment | ".$remark_inv,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $change_status->prefix_dr.$change_status->year_dr.$change_status->dr_no);
		$this->db->update('normal_order',$data_status);

		$data_order = array(
					"remark_cancel" => "Comment | ".$remark_inv,
		          	"updated" => date('Y-m-d H:i:s')
		         );
		         
		$this->db->where('dr_no', $change_status->prefix_dr.$change_status->year_dr.$change_status->dr_no);
		$this->db->update('book_order',$data_order);

		$result['msg'] = "success";
		echo json_encode($result);
		return false;
	}

}
?>