<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("Welcome.php");

class Logout extends Welcome {

	function __construct() {
        parent::__construct();
	}

	public function index() {


		$check_data = $this->session->userdata('logged_in');

        
        if($check_data){

            $user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

            $user_id= $check_data['id'];

            $data = array(
               "date_process" => NULL
             );
                     
            $this->db->where('id', $user_id);
            $this->db->update('user',$data);

            $this->session->unset_userdata('logged_in');
            session_destroy();

        }
	    redirect('login', 'refresh');
 	}

}	