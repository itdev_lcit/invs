<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Reports extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Billing');
        $this->load->model('M_Reports');
    }

    public function LogprintDr(){
		if($this->session->userdata('logged_in')) { 	

			$search = $this->input->post('search');
			$sort = $this->input->post('sort');
			$typeD = $this->input->post('typeD');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/LogprintDr";
	        $config["total_rows"] = $this->M_Reports->CountPrintDR($search);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Reports->CountPrintDR($search);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Reports->fetchPrintDR($config["per_page"], $page , $search , $sort , $typeD);
	        $data["links"] = $this->pagination->create_links();

			$this->view['main'] =  $this->load->view('reports/log_print_dr',$data,true);
			$this->view();
			
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}


	public function LogprintInv(){

		if($this->session->userdata('logged_in')) { 	
			$search = $this->input->post('search');
			$sort = $this->input->post('sort');
			$typeD = $this->input->post('typeD');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/LogprintInv";
	        $config["total_rows"] = $this->M_Reports->CountPrintINV($search);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Reports->CountPrintINV($search);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Reports->fetchPrintINV($config["per_page"], $page , $search , $sort , $typeD);
	        $data["links"] = $this->pagination->create_links();


			$this->view['main'] =  $this->load->view('reports/log_print_inv',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function PrintLogDr(){
		$this->db->select('username , print_no, updated, created , count(print_no) as num_print');
		$this->db->group_by('print_no, username');
		$query_print_dr = $this->db->get('log_print_dr');
		$data['log']= $query_print_dr->result_array();

		$this->view['main'] =  $this->load->view('reports/r_logprint',$data,true);
		$this->view();
	}

	public function PrintLogINV(){
		$query_print_inv = $this->db->get('log_print_invoice');
		$data['log']= $query_print_inv->result_array();

		$this->view['main'] =  $this->load->view('reports/r_printinv',$data,true);
		$this->view();
	}

	public function LogreprintInv(){

		$search = $this->input->post('search');
			$sort = $this->input->post('sort');
			$typeD = $this->input->post('typeD');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/LogreprintInv";
	        $config["total_rows"] = $this->M_Reports->CountPrintReINV($search);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Reports->CountPrintReINV($search);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Reports->fetchPrintReINV($config["per_page"], $page , $search , $sort , $typeD);
	        $data["links"] = $this->pagination->create_links();

		

		$this->view['main'] =  $this->load->view('reports/log_re_print_inv',$data,true);
		$this->view();
	}

	public function LogCancelInv(){
			
			$search = $this->input->post('search');
			$sort = $this->input->post('sort');
			$typeD = $this->input->post('typeD');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/LogprintInv";
	        $config["total_rows"] = $this->M_Reports->CountPrintCanINV($search);

	        if(!empty($search)){
	        	$config["per_page"] = $this->M_Reports->CountPrintCanINV($search);
	        } else {
	        	$config["per_page"] = 20;
	        }

	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Reports->fetchPrintCanINV($config["per_page"], $page , $search , $sort , $typeD);
	        $data["links"] = $this->pagination->create_links();


		$this->view['main'] =  $this->load->view('reports/log_cencel_inv',$data,true);
		$this->view();
	}

	public function AllSSR(){
		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
	 	$type = $this->input->post('type');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/AllSSR";
	        $config["total_rows"] = $this->M_Billing->CountSSR($search, $type);

	        $config["per_page"] = 200;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Billing->fetch_SSR($config["per_page"], $page , $search , $sort, $type);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

			$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;

			if(empty($type)){
				$c_type = 'ALL';
			} else {
				$c_type = $type;
			}

			$data['type'] = $c_type;			

			if(!empty($search)){
				$date = date_create($search);
				$search_d = date_format($date,"Y-m-d");
				$data['date_s'] =  $search_d;
			} else {
				$data['date_s'] =  date('Y-m-d');
			}

	       
	        $this->view['main'] =  $this->load->view('reports/all_ssr',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function AllInv(){
		if($this->session->userdata('logged_in')) { 	

	 	$search = $this->input->post('search');
		$sort = $this->input->post('sort');

	        $config = array();
	        $config["base_url"] = site_url() . "Reports/AllInv";
	        $config["total_rows"] = $this->M_Billing->CountInv($search);

	        $config["per_page"] = 200;
	        $config["uri_segment"] = 3;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';


	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["results"] = $this->M_Billing->fetch_inv($config["per_page"], $page , $search , $sort);
	        $data["links"] = $this->pagination->create_links();

	        $check_data = $this->session->userdata('logged_in');

			$user = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$data['special'] = $user->reprint;

			$data['role'] = $user->role;

			

			if(!empty($search)){
				$date = date_create($search);
				$search_d = date_format($date,"Y-m-d");
				$data['date_s'] =  $search_d;
			} else {
				$data['date_s'] =  date('Y-m-d');
			}

	       
	        $this->view['main'] =  $this->load->view('reports/all_inv',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function PrintInv($date = null){
		if($this->session->userdata('logged_in')) { 	
			
			$date_c = date_create($date);
			$date_inv = date_format($date_c,"Y-m-d");

			$this->db->select('dr_no');
			$this->db->like('created',$date_inv);
			$dr_data = $this->db->get('status_invoice');

				foreach ($dr_data->result_array() as $dr) {
			        $dr_no[] = $dr['dr_no'];
			    }

			$check = count($dr_no);

			if($check >= 1){

				$this->db->where_in('book_order.dr_no',$dr_no);
				$this->db->join('setting_type','book_order.type = setting_type.type');
				$query_order = $this->db->get('book_order');

				$this->db->select('normal_order.* , book_order.book_an');
				$this->db->where_in('normal_order.dr_no',$dr_no);
				$this->db->join('book_order','book_order.id = normal_order.id_order');
				$this->db->group_by('normal_order.dr_no');
				$this->db->order_by("normal_order.invoice_no", "asc");
				$normal_order = $this->db->get('normal_order');

				$logs = $this->db->get('log_print_invoice');
				$data['log_print'] = $logs->result_array();

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$data['head'] = $normal_order->result_array();
				$data['order'] = $query_order->result_array();

			   	$this->view['main'] =  $this->load->view('reports/inv_report',$data,true);
				$this->view();

			} else {
				redirect('/Reports/AllInv', 'refresh');
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function PrintSSR($date = null, $type = null){
		if($this->session->userdata('logged_in')) { 	
			
			$date_c = date_create($date);
			$date_inv = date_format($date_c,"Y-m-d");

			$this->db->select('dr_no');
			$this->db->like('created',$date_inv);
			$dr_data = $this->db->get('ssr_order');

				foreach ($dr_data->result_array() as $dr) {
			        $dr_no[] = $dr['dr_no'];
			    }

			$check = count($dr_no);

			if($check >= 1){

				$query_vat = $this->db->get_where('setting_vat', array('is_delete' => 0 , 'status' => 0 ));
				$data['vat'] = $query_vat->row();

				$this->db->group_by('ssr_order.dr_no');
				$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
				$this->db->join('tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->join('log_print_ssr_draft',' log_print_ssr_draft.print_no = ssr_order.dr_no');
				$this->db->join('user',' user.username = log_print_ssr_draft.username');
				$this->db->where_in('ssr_order.dr_no',$dr_no);

				if($type != 'ALL'){
					$this->db->like('ssr_order.payment',$type);
				} 
				$this->db->select('ssr_order.dr_no, ssr_order.type, ssr_order.tariff_id, ssr_order.vessel, ssr_order.vsl_visit, ssr_order.voy_in, ssr_order.voy_out, ssr_order.atb, ssr_order.atd, ssr_order.qty, ssr_order.cur_rate, ssr_order.payment, ssr_order.is_multi, ssr_order.created, ssr_order.updated, customer.ar_company, customer.credit_term, customer.customer_code, customer.customer_branch, customer.customer_name, customer.customer_address, customer.customer_address2, customer.customer_address3, customer.customer_post, customer.tax_reg_no, tariff_type.tariff_code, tariff_type.tariff_des, tariff_type.tariff_rate, user.name');

				$data['g_order'] = $this->db->get('ssr_order')->result_array();

				$this->db->order_by('ssr_order.ssr_id', 'asc');
				$this->db->join('customer','customer.custom_id = ssr_order.id_comp');
				$this->db->join('tariff_type',' tariff_type.tariff_id = ssr_order.tariff_id');
				$this->db->where_in('ssr_order.dr_no',$dr_no);

				if($type != 'ALL'){
					$this->db->like('ssr_order.payment',$type);
				} 
				$this->db->select('ssr_order.dr_no, ssr_order.type, ssr_order.ref_id, ssr_order.tariff_id, ssr_order.vessel, ssr_order.vsl_visit, ssr_order.voy_in, ssr_order.voy_out, ssr_order.atb, ssr_order.atd, ssr_order.qty, ssr_order.cur_rate, ssr_order.payment, ssr_order.is_multi, ssr_order.created, ssr_order.updated, customer.ar_company, customer.credit_term, customer.customer_code, customer.customer_branch, customer.customer_name, customer.customer_address, customer.customer_address2, customer.customer_address3, customer.customer_post, customer.tax_reg_no, tariff_type.tariff_code, tariff_type.tariff_des, tariff_type.tariff_rate');

				$data['order'] = $this->db->get('ssr_order')->result_array();



			   	$this->view['main'] =  $this->load->view('reports/ssr_report',$data,true);
				$this->view();

			} else {
				redirect('/Reports/AllSSR', 'refresh');
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

}